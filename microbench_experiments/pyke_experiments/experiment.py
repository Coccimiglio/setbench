from _basic_functions import *
import copy
from plot_style2 import *



# distribution = "-dist-uniform"
# distribution = "-dist-zipf 0.99"

use_recordmanager = True
preload_allocator = False
allocType = "libvmalloc"

# flushType = "clflush"
#flushType = "clflushopt"
flushType = "clwb"
# flushType = "disable_flushing"

make_cmd = 'make -j all'
partial_alg_names = 'partial_algorithm_names="tudor_linked_list zuriel_linked_list_ coccimiglio_"'

# common_xargs = '-latomic -D DURABLE_TOOLS_FORCE_SFENCE'
common_xargs = '-latomic'

if use_recordmanager: 
  common_xargs = common_xargs + ' -D RECORDMANAGER_PMEM_SUPPORT'

if not preload_allocator and allocType == "libvmalloc":
  common_xargs = common_xargs + ' -D USE_LIBVMMALLOC'

if not preload_allocator and allocType == "memkind":
  common_xargs = common_xargs + ' -D USE_MEMKIND'

# TODO::GUY:: these should just update common_xargs rather than the compile command
if flushType == "clflush":
  compileCmdStr = make_cmd + ' ' + partial_alg_names + ' xargs="' + common_xargs + '"'
elif flushType == "clflushopt":
  compileCmdStr = make_cmd + ' ' + partial_alg_names + ' xargs="' + common_xargs + ' -D USE_CLFLUSHOPT -D ONE_FLUSH_INSERT"'
elif flushType == "clwb":
  compileCmdStr = make_cmd + ' ' + partial_alg_names + ' xargs="' + common_xargs + ' -D USE_CLWB -D ONE_FLUSH_INSERT"'
else:
  compileCmdStr = make_cmd + ' ' + partial_alg_names + ' xargs="' + common_xargs + ' -D DISABLE_FLUSHING"'

pinning_policy='-pin ' + shell_to_str('cd ../../../setbench/tools ; ./get_pinning_cluster.sh', exit_on_error=True)



# DISC colors
# color_list = [
#       '#FFD300' # CYBER
#     , '#7C0A02' # 
#     , '#DE3163' # 
#     , '#FDA50F' # 
#     , '#0B6623' # 
#     , '#598BAF' # 
#     , '#003151' # 
#     , '#2E8B57' # 
#     , '#000000' # 
#     , '#0F52BA' # 
#     , '#4682B4' # appricot
#     , '#131E3A' # lolipop
#     , '#FFFFFF'
# ]

color_list = [
      '#df7126' # 0
    , '#37946e' # 1
    , '#639bff' # 2
    , '#ac3232' # 3
    , '#76428a' # 4
    , '#fbf236' # 5
    , '#000000' # 6
    , '#ffffff' # 7

    , '#000000' # 8
    , '#fbf236' # 9
    , '#000000' # 10
    , '#ffffff' # 11
    , '#b300b0'
]


# spaa_colors = [
#   '#d665ba',   
#   '#d9ac0d',  
#   '#ff832b',  
#   '#c41d12',
#   '#bae6ff',  
#   '#47e675',  
#   '#3e6b4b',  
#   '#7aabff',  
#   '#000000',
# ]

spaa_colors = [
  '#76428a', 
  '#f1c21b',
  '#ff832b',
  '#7C0A02',
  '#bae6ff',
  '#007822',
  '#3bff00',
  '#4589ff',
  '#6b6767',
]

unfiltered_algs = dict({
          ''                                            : dict(name=''                    , use=False    , hatch=''      , color=''              , line_style=''             , line_width=''     , marker_style=''       , marker_size=''        )
        # # , 'coccimiglio_persist_all_harris'              : dict(name='PA-Logical-Del'          , use=True     , hatch='/'        , color=color_list[9]   , line_style=(2,2)          , line_width='1'    , marker_style='.'      , marker_size=''        )
        # # , 'coccimiglio_persist_all_ellen'               : dict(name='PA-Physical-Del'         , use=True     , hatch='//'       , color=color_list[9]   , line_style=(2,2)          , line_width='1'    , marker_style='o'      , marker_size=''        )
        # # , 'coccimiglio_persist_async_harris'            : dict(name='APA-Logical-Del'         , use=True     , hatch='///'      , color=color_list[9]   , line_style=(2,2)          , line_width='1'    , marker_style='s'      , marker_size=''        )
        # # , 'coccimiglio_persist_async_ellen'             : dict(name='APA-Physical-Del'        , use=True     , hatch='////'     , color=color_list[9]   , line_style=(2,2)          , line_width='1'    , marker_style='P'      , marker_size=''        )
        
        # , 'coccimiglio_no_help_harris'                  : dict(name='PFLD'          , use=True     , hatch=None       , color=spaa_colors[0]     , line_style=(4,4)          , line_width='1'    , marker_style='^'      , marker_size=''        )
        # , 'coccimiglio_no_help_harris_swcas'            : dict(name='PFLD-S'        , use=True     , hatch='//'       , color=spaa_colors[0]     , line_style=(2,2)          , line_width='1'    , marker_style='P'      , marker_size=''        )
        , 'coccimiglio_no_help_ellen'                   : dict(name='PFPD'         , use=True     , hatch=None       , color=spaa_colors[1]     , line_style=(4,4)          , line_width='1'    , marker_style='*'      , marker_size=''        )
        # , 'coccimiglio_no_help_ellen_swcas'             : dict(name='PFPD-S'       , use=True     , hatch='//'       , color=spaa_colors[1]     , line_style=(4,4)          , line_width='1'    , marker_style='^'      , marker_size=''        )
        # , 'coccimiglio_persist_last_harris'             : dict(name='PLLD'          , use=True     , hatch=None       , color=spaa_colors[2]     , line_style=(None,None)    , line_width='1'    , marker_style='d'      , marker_size=''        )
        # , 'coccimiglio_persist_last_harris_swcas'       : dict(name='PLLD-S'        , use=True     , hatch='//'       , color=spaa_colors[2]     , line_style=(4,4)          , line_width='1'    , marker_style='*'      , marker_size=''        )
        , 'coccimiglio_persist_last_ellen'              : dict(name='PLPD'         , use=True     , hatch=None       , color=spaa_colors[3]     , line_style=(None,None)    , line_width='1'    , marker_style='P'      , marker_size=''        )        
        # , 'coccimiglio_persist_last_ellen_swcas'        : dict(name='PLPD-S'       , use=True     , hatch='//'       , color=spaa_colors[3]     , line_style=(None,None)    , line_width='1'    , marker_style='d'      , marker_size=''        )
        
        # , 'coccimiglio_persist_all_harris_swcas'        : dict(name='PA-Logical-Del-S'        , use=True     , hatch='**'       , color=color_list[0]   , line_style=(None,None)    , line_width='1'    , marker_style='X'      , marker_size=''        )
        # # , 'coccimiglio_persist_all_ellen_swcas'         : dict(name='PA-Physical-Del-S'       , use=True     , hatch='*'        , color=color_list[9]   , line_style=(2,2)          , line_width='1'    , marker_style='.'      , marker_size=''        )
        # # , 'coccimiglio_persist_async_harris_swcas'      : dict(name='APA-Logical-Del-S'       , use=True     , hatch='o'        , color=color_list[2]   , line_style=(2,2)          , line_width='1'    , marker_style='o'      , marker_size=''        )
        # # , 'coccimiglio_persist_async_ellen_swcas'       : dict(name='APA-Physical-Del-S'      , use=True     , hatch='-'        , color=color_list[3]   , line_style=(2,2)          , line_width='1'    , marker_style='s'      , marker_size=''        )                    
        
        , 'tudor_linked_list'                           : dict(name='L&P'               , use=True     , hatch=None       , color=spaa_colors[4]     , line_style=(None,None)    , line_width='1'    , marker_style='P'      , marker_size=''        )        
        # , 'zuriel_linked_list_link_free'                : dict(name='LF'               , use=True     , hatch=None       , color=spaa_colors[5]    , line_style=(2,2)          , line_width='1'    , marker_style='.'      , marker_size=''        )
        , 'zuriel_linked_list_link_free_v2'             : dict(name='LF-V2'            , use=True     , hatch=None       , color=spaa_colors[6]     , line_style=(2,2)          , line_width='1'    , marker_style='.'      , marker_size=''        )
        # , 'zuriel_linked_list_link_free_v3'             : dict(name='LF-V3'            , use=True     , hatch=None       , color=color_list[10]    , line_style=(2,2)          , line_width='1'    , marker_style='.'      , marker_size=''        )
        , 'zuriel_linked_list_soft'                     : dict(name='SOFT'             , use=True     , hatch=None       , color=spaa_colors[7]    , line_style=(None,None)    , line_width='1'    , marker_style='X'      , marker_size=''        )        
        , 'zuriel_linked_list_soft_strict'              : dict(name='SOFT-SLE'            , use=True     , hatch=None       , color=spaa_colors[8]    , line_style=(None,None)    , line_width='1'    , marker_style='X'      , marker_size=''        )        
})

# unfiltered_algs = dict({
#         #  ''                                            : dict(name=''                    , use=False    , hatch=''      , color=''              , line_style=''             , line_width=''     , marker_style=''       , marker_size=''        )
#          'tudor_linked_list'                           : dict(name='David-L&P'               , use=True     , hatch=None       , color=color_list[9]   , line_style=(None,None)    , line_width='1'    , marker_style='P'      , marker_size=''        )
#         , 'zuriel_linked_list_soft'                     : dict(name='Zuriel-SOFT'             , use=True     , hatch=None       , color=color_list[10]   , line_style=(None,None)    , line_width='1'    , marker_style='X'      , marker_size=''        )
#         , 'zuriel_linked_list_link_free'                : dict(name='Zuriel-LF'               , use=True     , hatch=None       , color=color_list[11]   , line_style=(2,2)          , line_width='1'    , marker_style='.'      , marker_size=''        )        
# })



short_to_long = dict()
for k,v in unfiltered_algs.items():
    if k and v and v['name']:
        short_to_long[v['name']] = k

filtered_algs = dict()
for k,v in unfiltered_algs.items():
    if k and v and v['name'] and v['use']:
        filtered_algs[k] = v['name']

def extract_filtered_algs(exp_dict, file_name, field_name):
  result = grep_line(exp_dict, file_name, 'DS_TYPENAME')
  if result in filtered_algs.keys():
      return filtered_algs[result]
  return ''

## reindex styles by short alg and style property name
short_to_style_props = dict()
for prop in list(list(unfiltered_algs.values())[0].keys()):    
    short_to_style_props[prop] = dict({short_name: unfiltered_algs[short_to_long[short_name]][prop] for short_name in short_to_long.keys()})


## beware: should copy.deepcopy() this to use it
base_config = dict(
      dummy = None
    , dark_mode = False
    , legend_columns = 3
    , height_inches = 4
    , hatch_map = short_to_style_props['hatch']
    , color_map = short_to_style_props['color']
    , marker_style_map = short_to_style_props['marker_style']
    , line_style_map = short_to_style_props['line_style']
    , legend_markerscale = 3
    , font_size = 24
    , xtitle = None
)

def define_experiment(exp_dict, args):
    set_dir_tools    (exp_dict, os.getcwd() + '/../../tools')
    set_dir_compile  (exp_dict, os.getcwd() + '/../../microbench')
    set_dir_run      (exp_dict, os.getcwd() + '/../../microbench/bin')
        
    set_dir_data     (exp_dict, os.getcwd() + '/test_data')     
    
    
    set_cmd_compile  (exp_dict, compileCmdStr)

    os.environ["VMMALLOC_POOL_DIR"] = "/mnt/pmem1_mount/"
    os.environ["VMMALLOC_POOL_SIZE"] = "8589934592"

    add_run_param    (exp_dict, 'thread_pinning'    , [pinning_policy])
    
    add_run_param    (exp_dict, 'DS_TYPENAME'       , list(filtered_algs))    

    
    add_run_param    (exp_dict, '__trials'          , [1])

    add_run_param    (exp_dict, 'TOTAL_THREADS'     , [12, 24, 48, 72, 96])    
    add_run_param    (exp_dict, 'MAXKEY'            , [50, 500])
    add_run_param    (exp_dict, 'INS_DEL_FRAC'      , ["0.5 0.5", "5.0 5.0", "25.0 25.0", "49.5 49.5"])        
    add_run_param    (exp_dict, 'millis'            , [10000])    

    if preload_allocator:    
      if allocType == "libvmalloc":
        print ("PRELOADING LIBVMMALLOC")
        set_cmd_run      (exp_dict, 'LD_PRELOAD=/usr/local/lib/libvmmalloc.so.1 numactl --interleave=all time ./{DS_TYPENAME}.new.debra.pnone -nwork {TOTAL_THREADS} -prefill-insert -nprefill {TOTAL_THREADS} -insdel {INS_DEL_FRAC} -k {MAXKEY} -t {millis} {thread_pinning}')  
    else:
      set_cmd_run      (exp_dict, 'numactl --interleave=all time ./{DS_TYPENAME}.new.debra.pnone -nwork {TOTAL_THREADS} -prefill-insert -nprefill {TOTAL_THREADS} -insdel {INS_DEL_FRAC} -k {MAXKEY} -t {millis} {thread_pinning}')  
    
    
    add_data_field   (exp_dict, 'alg' , coltype='TEXT', extractor=extract_filtered_algs)
    add_data_field   (exp_dict, 'total_throughput', coltype='INTEGER', validator=is_positive)
    add_data_field   (exp_dict, 'total_deletes', coltype='INTEGER')
    add_data_field   (exp_dict, 'total_updates', coltype='INTEGER')
    add_data_field   (exp_dict, 'total_queries', coltype='INTEGER')
    add_data_field   (exp_dict, 'total_ops', coltype='INTEGER')

    add_data_field   (exp_dict, 'avg_flush_per_op', coltype='REAL')
    add_data_field   (exp_dict, 'avg_flush_per_update', coltype='REAL')
    add_data_field   (exp_dict, 'avg_flush_per_contains', coltype='REAL')

    add_data_field   (exp_dict, 'search_flushes', coltype='INTEGER')
    add_data_field   (exp_dict, 'contains_flushes', coltype='INTEGER')
    add_data_field   (exp_dict, 'update_flushes', coltype='INTEGER')
    add_data_field   (exp_dict, 'total_flushes', coltype='INTEGER') 
    add_data_field   (exp_dict, 'num_pmem_alloc', coltype='INTEGER') 

    add_data_field   (exp_dict, 'PAPI_L1_DCM', coltype='REAL')
    add_data_field   (exp_dict, 'PAPI_L2_TCM', coltype='REAL')
    add_data_field   (exp_dict, 'PAPI_L3_TCM', coltype='REAL')
    add_data_field   (exp_dict, 'PAPI_TOT_CYC', coltype='REAL')
    add_data_field   (exp_dict, 'PAPI_TOT_INS', coltype='REAL')



    alg_groups = dict()    
     

    alg_groups['vs_others_2']     = ['L&P', 'PLPD', 'PFPD', 'LF-V2', 'SOFT', 'SOFT-SLE']    
    alg_groups['strict_only']     = ['L&P', 'PLPD', 'LF-V2', 'SOFT-SLE']    
    alg_groups['others']     = ['LF-V2', 'SOFT', 'SOFT-SLE']    

    alg_groups['legend_order']    = ['PFPD','PLPD', 'L&P', 'LF-V2', 'SOFT', 'SOFT-SLE']
    

    plotDirs = [
        '_plots/'
    ]


    plotDir = '_plots/'
    set_sub_dir_plots (exp_dict, list(alg_groups.keys()))

    enabled_plots = dict(
      legend=             True,
      throughput=         True,
      flush_per_update=   True,
      flush_per_contains= True,
      total_flushes=      False,
      num_pmem_alloc=     False,
      papi_total_ins=     False,
      papi_L1_DCM=        False,
      papi_L2_TCM=        False,
      papi_L3_TCM=        False,
    )    

    for i, group in enumerate(alg_groups):
        # config1 = copy.deepcopy(base_config)
        # config1['series_order'] = list(filtered_algs.values())
      
        # filter_sql = filterQueries[i]
        # filter_sql = 'TOTAL_THREADS >= 12 AND alg in ({})'.format(','.join(["'"+x+"'" for x in alg_groups[group]]))
        filter_sql = 'alg in ({})'.format(','.join(["'"+x+"'" for x in alg_groups[group]]))

        config2 = copy.deepcopy(base_config)
        config2['series_order'] = alg_groups[group]
        # config2['xtitle'] = 'Thread Count'
        # config2['ytitle'] = 'Ops/Sec'
        config2['width_inches'] = 18
        config2['height_inches'] = 12   
        config2['font_size'] = 48  
        # config2['legend_columns'] = len(alg_groups[group])
        config2['legend_columns'] = 6
        # config2['legend_columns'] = 3
        
        
        if enabled_plots['legend']:
          add_plot_set(
                exp_dict
              # , name=plotDirs[i] + 'legend.png'
              , name=plotDir + group + '/legend.png'
              , series='alg'
              , x_axis='TOTAL_THREADS'
              , y_axis='total_throughput'
              , plot_type=plot_bars_legend
              #, plot_cmd_args='--legend-only --legend-columns 3'
              , plot_cmd_args=config2
              , filter=filter_sql
          )
        
        if enabled_plots['throughput']:
          add_plot_set(
                exp_dict
              # , name=plotDirs[i] + 'Throughput-{INS_DEL_FRAC}-key={MAXKEY}.png'
              , name=plotDir + group + '/Throughput-{INS_DEL_FRAC}-key={MAXKEY}.png'
              , title='Throughput - Ins/Del:{INS_DEL_FRAC} - MaxK:{MAXKEY}'
              , varying_cols_list=['MAXKEY', 'INS_DEL_FRAC']
              , series='alg'
              , x_axis='TOTAL_THREADS'
              , y_axis='total_throughput'
              , plot_type=plot_bars
              #, plot_cmd_args='--x-title "Thread Count" --y-title "Ops/Sec"'
              , plot_cmd_args=config2
              , filter=filter_sql
          )       

        if enabled_plots['flush_per_update']:
          add_plot_set(
                exp_dict
              # , name=plotDirs[i] + 'Flush-per-Update-{INS_DEL_FRAC}-key={MAXKEY}.png'
              , name=plotDir + group + '/Flush-per-Update-{INS_DEL_FRAC}-key={MAXKEY}.png'
              , title='Avg Psync/Update - Ins/Del:{INS_DEL_FRAC} - MaxK:{MAXKEY}'
              , varying_cols_list=['MAXKEY', 'INS_DEL_FRAC']
              , series='alg'
              , x_axis='TOTAL_THREADS'
              , y_axis='avg_flush_per_update'
              , plot_type=plot_bars
              #, plot_cmd_args='--x-title "Thread Count" --y-title "Avg Psync/Update"'
              , plot_cmd_args=config2
              , filter=filter_sql
          )  

        if enabled_plots['flush_per_contains']:
          add_plot_set(
                exp_dict
              # , name=plotDirs[i] + 'Flush-per-Contains-{INS_DEL_FRAC}-key={MAXKEY}.png'
              , name=plotDir + group + '/Flush-per-Contains-{INS_DEL_FRAC}-key={MAXKEY}.png'
              , title='Avg Psync/Search - Ins/Del:{INS_DEL_FRAC} - MaxK:{MAXKEY}'
              , varying_cols_list=['MAXKEY', 'INS_DEL_FRAC']
              , series='alg'
              , x_axis='TOTAL_THREADS'
              , y_axis='avg_flush_per_contains'
              , plot_type=plot_bars
              #, plot_cmd_args='--x-title "Thread Count" --y-title "Avg Psync/Search"'
              , plot_cmd_args=config2
              , filter=filter_sql
          )

        if enabled_plots['papi_total_ins']:
          add_plot_set(
                exp_dict
              # , name=plotDirs[i] + 'PAPI_TOT_INS-{INS_DEL_FRAC}-key={MAXKEY}.png'
              , name=plotDir + group + '/PAPI_TOT_INS-{INS_DEL_FRAC}-key={MAXKEY}.png'
              , title='PAPI_TOT_INS - Ins/Del:{INS_DEL_FRAC} - MaxK:{MAXKEY}'
              , varying_cols_list=['MAXKEY', 'INS_DEL_FRAC']
              , series='alg'
              , x_axis='TOTAL_THREADS'
              , y_axis='PAPI_TOT_INS'
              , plot_type=plot_bars
              #, plot_cmd_args='--x-title "Thread Count" --y-title "Avg Psync/Search"'
              , plot_cmd_args=config2
              , filter=filter_sql
          )

        if enabled_plots['papi_L1_DCM']:
          add_plot_set(
                exp_dict
              # , name=plotDirs[i] + 'PAPI_L1_DCM-{INS_DEL_FRAC}-key={MAXKEY}.png'
              , name=plotDir + group + '/PAPI_L1_DCM-{INS_DEL_FRAC}-key={MAXKEY}.png'
              , title='PAPI_L1_DCM - Ins/Del:{INS_DEL_FRAC} - MaxK:{MAXKEY}'
              , varying_cols_list=['MAXKEY', 'INS_DEL_FRAC']
              , series='alg'
              , x_axis='TOTAL_THREADS'
              , y_axis='PAPI_L1_DCM'
              , plot_type=plot_bars
              #, plot_cmd_args='--x-title "Thread Count" --y-title "Avg Psync/Search"'
              , plot_cmd_args=config2
              , filter=filter_sql
          )

        if enabled_plots['papi_L2_TCM']:
          add_plot_set(
                exp_dict
              # , name=plotDirs[i] + 'PAPI_L2_TCM-{INS_DEL_FRAC}-key={MAXKEY}.png'
              , name=plotDir + group + '/PAPI_L2_TCM-{INS_DEL_FRAC}-key={MAXKEY}.png'
              , title='PAPI_L2_TCM - Ins/Del:{INS_DEL_FRAC} - MaxK:{MAXKEY}'
              , varying_cols_list=['MAXKEY', 'INS_DEL_FRAC']
              , series='alg'
              , x_axis='TOTAL_THREADS'
              , y_axis='PAPI_L2_TCM'
              , plot_type=plot_bars
              #, plot_cmd_args='--x-title "Thread Count" --y-title "Avg Psync/Search"'
              , plot_cmd_args=config2
              , filter=filter_sql
          )

        if enabled_plots['papi_L3_TCM']:
          add_plot_set(
                exp_dict
              # , name=plotDirs[i] + 'PAPI_L3_TCM-{INS_DEL_FRAC}-key={MAXKEY}.png'
              , name=plotDir + group + '/PAPI_L3_TCM-{INS_DEL_FRAC}-key={MAXKEY}.png'
              , title='PAPI_L3_TCM - Ins/Del:{INS_DEL_FRAC} - MaxK:{MAXKEY}'
              , varying_cols_list=['MAXKEY', 'INS_DEL_FRAC']
              , series='alg'
              , x_axis='TOTAL_THREADS'
              , y_axis='PAPI_L3_TCM'
              , plot_type=plot_bars
              #, plot_cmd_args='--x-title "Thread Count" --y-title "Avg Psync/Search"'
              , plot_cmd_args=config2
              , filter=filter_sql
          )

        if enabled_plots['total_flushes']:
          add_plot_set(
                exp_dict
              # , name=plotDirs[i] + 'total_flushes-{INS_DEL_FRAC}-key={MAXKEY}.png'
              , name=plotDir + group + '/total_flushes-{INS_DEL_FRAC}-key={MAXKEY}.png'
              , title='total_flushes - Ins/Del:{INS_DEL_FRAC} - MaxK:{MAXKEY}'
              , varying_cols_list=['MAXKEY', 'INS_DEL_FRAC']
              , series='alg'
              , x_axis='TOTAL_THREADS'
              , y_axis='total_flushes'
              , plot_type=plot_bars
              #, plot_cmd_args='--x-title "Thread Count" --y-title "Avg Psync/Search"'
              , plot_cmd_args=config2
              , filter=filter_sql
          )        

        if enabled_plots['num_pmem_alloc']:
          add_plot_set(
                exp_dict
              # , name=plotDirs[i] + 'num_pmem_alloc-{INS_DEL_FRAC}-key={MAXKEY}.png'
              , name=plotDir + group + '/num_pmem_alloc-{INS_DEL_FRAC}-key={MAXKEY}.png'
              , title='num_pmem_alloc - Ins/Del:{INS_DEL_FRAC} - MaxK:{MAXKEY}'
              , varying_cols_list=['MAXKEY', 'INS_DEL_FRAC']
              , series='alg'
              , x_axis='TOTAL_THREADS'
              , y_axis='num_pmem_alloc'
              , plot_type=plot_bars
              #, plot_cmd_args='--x-title "Thread Count" --y-title "Avg Psync/Search"'
              , plot_cmd_args=config2
              , filter=filter_sql
          )       