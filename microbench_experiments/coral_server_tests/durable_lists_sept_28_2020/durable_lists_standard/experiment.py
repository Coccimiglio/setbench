from _basic_functions import *

compileCmdStr = 'make -j all partial_algorithm_names="coccimiglio_durable_" xargs="-latomic"'

def define_experiment(exp_dict, args):
    set_dir_tools    (exp_dict, os.getcwd() + '/../../tools')
    set_dir_compile  (exp_dict, os.getcwd() + '/../../microbench')
    set_dir_run      (exp_dict, os.getcwd() + '/../../microbench/bin')

    set_cmd_compile  (exp_dict, compileCmdStr)

    add_run_param    (exp_dict, '__trials', [1])
    add_run_param    (exp_dict, 'TOTAL_THREADS', [32, 100])
    add_run_param    (exp_dict, 'DS_TYPENAME', ['coccimiglio_durable_list_no_flush_no_dur_bit', 'coccimiglio_durable_list_spin', 'coccimiglio_durable_list_persist_all', 'coccimiglio_durable_list_persist_all_asynch', 'coccimiglio_durable_list_persist_last', 'coccimiglio_durable_list_no_help'])
    add_run_param    (exp_dict, 'MAXKEY', [100, 500])   
    add_run_param    (exp_dict, 'INS_DEL_FRAC', ["1.0 1.0", "50.0 50.0"])
    
    

    set_cmd_run      (exp_dict, 'LD_PRELOAD=../../lib/libjemalloc.so numactl --interleave=all time ./{DS_TYPENAME}.new.debra.pnone -nwork {TOTAL_THREADS} -nprefill {TOTAL_THREADS} -insdel {INS_DEL_FRAC} -k {MAXKEY} -t 3000')

    add_data_field   (exp_dict, 'total_throughput', coltype='INTEGER', validator=is_positive)
    add_data_field   (exp_dict, 'search_flushes', coltype='INTEGER')
    add_data_field   (exp_dict, 'contains_flushes', coltype='INTEGER')
    add_data_field   (exp_dict, 'update_flushes', coltype='INTEGER')
    add_data_field   (exp_dict, 'total_flushes', coltype='INTEGER') 


    add_data_field   (exp_dict, 'PAPI_L2_TCM', coltype='REAL')
    add_data_field   (exp_dict, 'PAPI_L3_TCM', coltype='REAL')
    add_data_field   (exp_dict, 'PAPI_TOT_CYC', coltype='REAL')
    add_data_field   (exp_dict, 'PAPI_TOT_INS', coltype='REAL')

    ## render one legend for all plots (since the legend is the same for all).
    ## if legend varies from plot to plot, you might enable legends for all plots,
    ## or write a custom plotting command that determines what to do, given your data
    add_plot_set(exp_dict, name='legend.png', series='DS_TYPENAME', x_axis='TOTAL_THREADS', y_axis='total_throughput', plot_type='bars', plot_cmd_args='--legend-only --legend-columns 3')

    ## render a plot_set for EVERY numeric data field extracted above
    for field in get_numeric_data_fields(exp_dict):
        add_plot_set(
              exp_dict
            , name=field+'-{INS_DEL_FRAC}-{MAXKEY}k.png'
            , title='Ins/Del:{INS_DEL_FRAC} - MaxKey:{MAXKEY} - '+field
            , varying_cols_list=['MAXKEY', 'INS_DEL_FRAC']
            , series='DS_TYPENAME'
            , x_axis='TOTAL_THREADS'
            , y_axis=field
            , plot_type='bars'
        )

        ## and also add a page_set for each data field.
        ## we place the above legend at the bottom of *each* table by providing "legend_file"
        add_page_set(
              exp_dict
            , image_files=field+'-{INS_DEL_FRAC}-{MAXKEY}k.png'
            , name=field
            , column_field='INS_DEL_FRAC'
            , row_field='MAXKEY'
            , legend_file='legend.png'
        )
