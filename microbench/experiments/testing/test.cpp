// SPDX-License-Identifier: BSD-3-Clause
/* Copyright 2016-2020, Intel Corporation */

/*
 * make_persistent.cpp -- C++ documentation snippets.
 */

#include <iostream>
//! [make_example]
#include <libpmemobj++/make_persistent.hpp>


//! [make_atomic_example]
#include <fcntl.h>
#include <libpmemobj++/make_persistent_atomic.hpp>
#include <libpmemobj++/p.hpp>
#include <libpmemobj++/persistent_ptr.hpp>
#include <libpmemobj++/pool.hpp>
#include <libpmemobj++/transaction.hpp>

using namespace pmem::obj;

void
make_persistent_atomic_example()
{
	struct Node {
		long long key;
		long long val;
		persistent_ptr<Node> next;		
	};

	/* pool root structure */
	struct root {
		persistent_ptr<Node> head;
	};

	/* create a pmemobj pool */
	auto pop = pool<Node>::create("/mnt/pmem1_mount/guy_test_pool1", "layout", PMEMOBJ_MIN_POOL);
	persistent_ptr<Node> head = pop.root();
	head->next = NULL;
	/* typical usage schemes */

	/* atomic allocation and construction with arguments passing */
	make_persistent_atomic<Node>(pop, head->next);

	printf("%ld\n", sizeof(head));
	// printf("%d\n", sizeof(proot->head));
	printf("%ld\n", sizeof(Node));
	
	persistent_ptr<Node> tmp;
	make_persistent_atomic<Node>(pop, tmp);

	head->key = 5;
	head->next->key = 10;

	Node* h = head.get();
	Node* n = head->next.get();

	printf("%p, %lld\n", h, h->key);	
	printf("%p, %lld\n", n, n->key);

	/* atomic object deallocation, ~compound_type() is not called */
	// delete_persistent_atomic<Node>(proot->head);
}
//! [make_atomic_example]


int
main()
{
	remove("/mnt/pmem1_mount/guy_test_pool1");

	try {
	
		make_persistent_atomic_example();
		
	} catch (const std::exception &e) {
		std::cerr << "Exception " << e.what() << std::endl;
		return -1;
	}

	return 0;
}
