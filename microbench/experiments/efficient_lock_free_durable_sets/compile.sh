#!/bin/sh

partial_algorithm_names="zuriel_linked_list_ coccimiglio_durable_list_"

#apparently you need this flag if you want double wide CAS?
args_for_both="-latomic"

compile_args_for_disabled="${args_for_both}"

echo "#########################################################################"
echo "#### Compiling binaries with the desired functionality disabled"
echo "#########################################################################"

here=`pwd`
mkdir ${here}/bin 2>/dev/null
cd .. ; cd .. ; make -j all partial_algorithm_names="$partial_algorithm_names" bin_dir=${here}/bin_disabled xargs="$compile_args_for_disabled" > compiling.txt 2>&1
if [ "$?" -ne "0" ]; then
    echo ""
    echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    echo "ERROR compiling; see compiling.txt"
    echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
    exit
else
    echo ""
    echo "*************************************************************************"
    echo "SUCCESS - with desired functionality disabled"
    echo "*************************************************************************"
    echo ""
    echo ""
fi
cd $here

if [ ! -z ${compile_args_for_enabled} ]; then
    echo "#########################################################################"
    echo "#### Compiling binaries with the desired functionality enabled"
    echo "#########################################################################"

    here=`pwd`
    mkdir ${here}/bin 2>/dev/null
    cd .. ; cd .. ; make -j all bin_dir=${here}/bin_enabled xargs="$compile_args_for_enabled" >> compiling.txt 2>&1
    if [ "$?" -ne "0" ]; then
        echo ""
        echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
        echo "ERROR compiling; see compiling.txt"
        echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
        exit
    else
        echo ""
        echo "*************************************************************************"
        echo "SUCCESS - with desired functionality enabled"
        echo "*************************************************************************"
        echo ""
        echo ""
    fi
    cd $here
fi