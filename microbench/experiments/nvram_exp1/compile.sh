#!/bin/sh

partial_algorithm_names="zuriel_"

compile_args_for_disabled="-DNVRAM_USE_CLFUSH"
compile_args_for_enabled="-DNVRAM_USE_CLWB"
#compile_args_for_enabled=""

echo "#########################################################################"
echo "#### Compiling binaries with the desired functionality disabled"
echo "#########################################################################"

here=`pwd`
mkdir ${here}/bin 2>/dev/null
cd .. ; cd .. ; make -j `make -Bnd all | grep prereq | grep ubench | cut -d"'" -f2 | grep $partial_algorithm_names` bin_dir=${here}/bin_disabled xargs="$compile_args_for_disabled" > compiling.txt 2>&1
if [ "$?" -ne "0" ]; then
    echo "ERROR compiling; see compiling.txt"
    exit
fi
cd $here


if [ ! -z "$compile_args_for_enabled" ]; then
    echo "#########################################################################"
    echo "#### Compiling binaries with the desired functionality enabled"
    echo "#########################################################################"

    here=`pwd`
    mkdir ${here}/bin 2>/dev/null
    cd .. ; cd .. ; make -j `make -Bnd all | grep prereq | grep ubench | cut -d"'" -f2 | grep $partial_algorithm_names` bin_dir=${here}/bin_enabled xargs="$compile_args_for_enabled" >> compiling.txt 2>&1
    if [ "$?" -ne "0" ]; then
        echo "ERROR compiling; see compiling.txt"
        exit
    fi
    cd $here
else 
    echo "#### You have chosen to only compile with desired functionality disabled"
fi 