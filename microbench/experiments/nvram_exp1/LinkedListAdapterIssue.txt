cmd=LD_PRELOAD=../../../lib/libjemalloc.so timeout 600 numactl --interleave=all time bin_disabled/ubench_zuriel_soft_linked_list.alloc_new.reclaim_debra.pool_none.out -nwork 8 -nprefill 8 -i 20 -d 20 -rq 0 -rqsize 1 -k 100 -nrq 0 -t 10000 -pin 0-23,96-119,24-47,120-143,48-71,144-167,72-95,168-191
step=10010
fname=nvram_exp1/step10010.txt
binary=bin_disabled/ubench_zuriel_soft_linked_list.alloc_new.reclaim_debra.pool_none.out
parsed custom binding: 0-23,96-119,24-47,120-143,48-71,144-167,72-95,168-191
DS_TYPENAME=zuriel_soft_linked_list
FIND_FUNC=FIND_FUNC
INSERT_FUNC=insertIfAbsent
ERASE_FUNC=ERASE_FUNC
RQ_FUNC=RQ_FUNC
RECLAIM=reclaimer_debra
ALLOC=allocator_new
POOL=pool_none
MAX_THREADS_POW2=256
CPU_FREQ_GHZ=2.1
MILLIS_TO_RUN=10000
INS=20
DEL=20
RQ=0
RQSIZE=1
MAXKEY=100
PREFILL_THREADS=8
DESIRED_PREFILL_SIZE=-1
TOTAL_THREADS=8
WORK_THREADS=8
RQ_THREADS=0
distribution=0
Size of node=40
ACTUAL_THREAD_BINDINGS=0,1,2,3,4,5,6,7


$$$ Head $$$ 0x7fb07533a210
$$$ Tail $$$ 0x7fb07533a240
$$$ Size of uintptr_r 8 $$$ 
Info: prefilling using INSERTION ONLY.
timing_start inserting 50 keys with 8 threads...
timing_elapsed 0.00114278s
first_node_allocated_addresses_by_index=
first_descriptor_allocated_addresses_by_index=
first_extra_type1_allocated_addresses_by_index=
first_extra_type2_allocated_addresses_by_index=
first_extra_type3_allocated_addresses_by_index=
first_extra_type4_allocated_addresses_by_index=
sum_num_inserts_total=66
sum_num_deletes_total=
sum_num_searches_total=
sum_num_rq_total=
sum_num_operations_by_thread=
average_num_operations_total=
stdev_num_operations_total=
sum_num_operations_total=
min_num_operations_total=
max_num_operations_total=

log_histogram_of_none_limbo_reclamation_event_size_full_data=1:0
    [2^00, 2^01]: 0
sum_limbo_reclamation_event_size_total=
sum_pool_cpu_get_total=
sum_pool_cpu_add_total=
sum_move_block_reclaimer_to_cpu_total=
sum_move_block_cpu_to_node_total=
sum_move_block_node_to_global_total=
sum_move_block_global_to_alloc_total=
sum_move_block_alloc_to_cpu_total=
sum_move_block_global_to_cpu_total=
sum_move_block_node_to_cpu_total=
sum_num_bail_from_addkv_at_depth_by_index=
sum_num_bail_from_build_at_depth_by_index=
sum_num_help_subtree_total=
sum_num_try_rebuild_at_depth_by_index=
sum_num_complete_rebuild_at_depth_by_index=
sum_num_help_rebuild_total=

log_histogram_of_none_num_prop_epoch_latency_full_data=1:0
    [2^00, 2^01]: 0
average_num_prop_epoch_latency_total=
stdev_num_prop_epoch_latency_total=
min_num_prop_epoch_latency_total=
max_num_prop_epoch_latency_total=

log_histogram_of_none_bag_rotation_reclaim_size_full_data=1:0
    [2^00, 2^01]: 0

log_histogram_of_none_token_received_time_split_ms_full_data=1:0
    [2^00, 2^01]: 0
first_token_received_time_last_ms_by_thread=

log_histogram_of_none_bag_rotation_duration_split_ms_full_data=1:0
    [2^00, 2^01]: 0
first_bag_curr_size_by_thread=
first_bag_last_size_by_thread=
first_token_counts_by_thread=
first_thread_announced_epoch_by_index=
sum_elapsed_rebuild_depth_by_index=
sum_elapsed_rebuild_depth_total=
sum_duration_all_ops_total=
sum_duration_markAndCount_total=
sum_duration_wastedWorkBuilding_total=
sum_duration_buildAndReplace_total=
sum_duration_rotateAndFree_total=
sum_duration_traverseAndRetire_total=
sum_rebuild_is_subsumed_total=
sum_rebuild_is_subsumed_at_depth_by_index=
first_time_thread_terminate_by_thread=
min_time_thread_terminate_total=
max_time_thread_terminate_total=
first_time_thread_start_by_thread=
min_time_thread_start_total=
max_time_thread_start_total=
finished prefilling to size 50 keysum=2513, performing 66 successful updates in 0.001s
pref_size=50
prefill_elapsed_ms=59
main thread: starting timer...

###############################################################################
################################ BEGIN RUNNING ################################
###############################################################################


###############################################################################
################################## TIME IS UP #################################
###############################################################################

joining threads...
###############################################################################
################################# END RUNNING #################################
###############################################################################

10s
gstats_timer_elapsed timer_bag_rotation_start=6.52574e+06
PRODUCING OUTPUT
% Node: 0x7fb07533a210 %
% Size of Node: 8 %
% Node as unintptr_t: 140395857289744 %
State: 0
% Node->next: 0x7fb00d921440 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394118648896 %

% Node: 0x7fb00d921440 %
% Size of Node: 8 %
% Node as unintptr_t: 140394118648896 %
State: 0
% Node->next: 0x7fb00f122460 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394143818848 %

% Node: 0x7fb00f122460 %
% Size of Node: 8 %
% Node as unintptr_t: 140394143818848 %
State: 0
% Node->next: 0x7fb00f1205d0 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394143811024 %

% Node: 0x7fb00f1205d0 %
% Size of Node: 8 %
% Node as unintptr_t: 140394143811024 %
State: 0
% Node->next: 0x7fb00f121bf0 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394143816688 %

% Node: 0x7fb00f121bf0 %
% Size of Node: 8 %
% Node as unintptr_t: 140394143816688 %
State: 0
% Node->next: 0x7fb00e933b30 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394135501616 %

% Node: 0x7fb00e933b30 %
% Size of Node: 8 %
% Node as unintptr_t: 140394135501616 %
State: 0
% Node->next: 0x7fb00f120600 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394143811072 %

% Node: 0x7fb00f120600 %
% Size of Node: 8 %
% Node as unintptr_t: 140394143811072 %
State: 0
% Node->next: 0x7fb00e934160 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394135503200 %

% Node: 0x7fb00e934160 %
% Size of Node: 8 %
% Node as unintptr_t: 140394135503200 %
State: 0
% Node->next: 0x7fb00e933b00 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394135501568 %

% Node: 0x7fb00e933b00 %
% Size of Node: 8 %
% Node as unintptr_t: 140394135501568 %
State: 0
% Node->next: 0x7fb00e932e70 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394135498352 %

% Node: 0x7fb00e932e70 %
% Size of Node: 8 %
% Node as unintptr_t: 140394135498352 %
State: 0
% Node->next: 0x7fb00f120330 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394143810352 %

% Node: 0x7fb00f120330 %
% Size of Node: 8 %
% Node as unintptr_t: 140394143810352 %
State: 0
% Node->next: 0x7fb00f1219b0 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394143816112 %

% Node: 0x7fb00f1219b0 %
% Size of Node: 8 %
% Node as unintptr_t: 140394143816112 %
State: 0
% Node->next: 0x7fb00e320960 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394129131872 %

% Node: 0x7fb00e320960 %
% Size of Node: 8 %
% Node as unintptr_t: 140394129131872 %
State: 0
% Node->next: 0x7fb00f122280 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394143818368 %

% Node: 0x7fb00f122280 %
% Size of Node: 8 %
% Node as unintptr_t: 140394143818368 %
State: 0
% Node->next: 0x7fb00dd20750 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394122839888 %

% Node: 0x7fb00dd20750 %
% Size of Node: 8 %
% Node as unintptr_t: 140394122839888 %
State: 0
% Node->next: 0x7fb00e320c30 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394129132592 %

% Node: 0x7fb00e320c30 %
% Size of Node: 8 %
% Node as unintptr_t: 140394129132592 %
State: 0
% Node->next: 0x7fb00d9216b0 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394118649520 %

% Node: 0x7fb00d9216b0 %
% Size of Node: 8 %
% Node as unintptr_t: 140394118649520 %
State: 0
% Node->next: 0x7fb00e932450 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394135495760 %

% Node: 0x7fb00e932450 %
% Size of Node: 8 %
% Node as unintptr_t: 140394135495760 %
State: 0
% Node->next: 0x7fb00e934040 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394135502912 %

% Node: 0x7fb00e934040 %
% Size of Node: 8 %
% Node as unintptr_t: 140394135502912 %
State: 0
% Node->next: 0x7fb00f121da0 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394143817120 %

% Node: 0x7fb00f121da0 %
% Size of Node: 8 %
% Node as unintptr_t: 140394143817120 %
State: 0
% Node->next: 0x7fb00d921260 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394118648416 %

% Node: 0x7fb00d921260 %
% Size of Node: 8 %
% Node as unintptr_t: 140394118648416 %
State: 0
% Node->next: 0x7fb00e933620 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394135500320 %

% Node: 0x7fb00e933620 %
% Size of Node: 8 %
% Node as unintptr_t: 140394135500320 %
State: 0
% Node->next: 0x7fb00e120de0 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394127035872 %

% Node: 0x7fb00e120de0 %
% Size of Node: 8 %
% Node as unintptr_t: 140394127035872 %
State: 0
% Node->next: 0x7fb00e932a20 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394135497248 %

% Node: 0x7fb00e932a20 %
% Size of Node: 8 %
% Node as unintptr_t: 140394135497248 %
State: 0
% Node->next: 0x7fb00e9336e0 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394135500512 %

% Node: 0x7fb00e9336e0 %
% Size of Node: 8 %
% Node as unintptr_t: 140394135500512 %
State: 0
% Node->next: 0x7fb00e932e40 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394135498304 %

% Node: 0x7fb00e932e40 %
% Size of Node: 8 %
% Node as unintptr_t: 140394135498304 %
State: 0
% Node->next: 0x7fb00e321050 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394129133648 %

% Node: 0x7fb00e321050 %
% Size of Node: 8 %
% Node as unintptr_t: 140394129133648 %
State: 0
% Node->next: 0x7fb00f122a00 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394143820288 %

% Node: 0x7fb00f122a00 %
% Size of Node: 8 %
% Node as unintptr_t: 140394143820288 %
State: 0
% Node->next: 0x7fb00e933e00 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394135502336 %

% Node: 0x7fb00e933e00 %
% Size of Node: 8 %
% Node as unintptr_t: 140394135502336 %
State: 0
% Node->next: 0x7fb00f120d80 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394143812992 %

% Node: 0x7fb00f120d80 %
% Size of Node: 8 %
% Node as unintptr_t: 140394143812992 %
State: 0
% Node->next: 0x7fb00d921980 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394118650240 %

% Node: 0x7fb00d921980 %
% Size of Node: 8 %
% Node as unintptr_t: 140394118650240 %
State: 0
% Node->next: 0x7fb00e932d50 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394135498064 %

% Node: 0x7fb00e932d50 %
% Size of Node: 8 %
% Node as unintptr_t: 140394135498064 %
State: 0
% Node->next: 0x7fb00f122910 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394143820048 %

% Node: 0x7fb00f122910 %
% Size of Node: 8 %
% Node as unintptr_t: 140394143820048 %
State: 0
% Node->next: 0x7fb00e320570 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394129130864 %

% Node: 0x7fb00e320570 %
% Size of Node: 8 %
% Node as unintptr_t: 140394129130864 %
State: 0
% Node->next: 0x7fb00e934820 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394135504928 %

% Node: 0x7fb00e934820 %
% Size of Node: 8 %
% Node as unintptr_t: 140394135504928 %
State: 0
% Node->next: 0x7fb00e9329f0 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394135497200 %

% Node: 0x7fb00e9329f0 %
% Size of Node: 8 %
% Node as unintptr_t: 140394135497200 %
State: 0
% Node->next: 0x7fb00f121980 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394143816064 %

% Node: 0x7fb00f121980 %
% Size of Node: 8 %
% Node as unintptr_t: 140394143816064 %
State: 0
% Node->next: 0x7fb00f121b00 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394143816448 %

% Node: 0x7fb00f121b00 %
% Size of Node: 8 %
% Node as unintptr_t: 140394143816448 %
State: 0
% Node->next: 0x7fb00f122850 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394143819856 %

% Node: 0x7fb00f122850 %
% Size of Node: 8 %
% Node as unintptr_t: 140394143819856 %
State: 0
% Node->next: 0x7fb00f122310 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394143818512 %

% Node: 0x7fb00f122310 %
% Size of Node: 8 %
% Node as unintptr_t: 140394143818512 %
State: 0
% Node->next: 0x7fb00f122070 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394143817840 %

% Node: 0x7fb00f122070 %
% Size of Node: 8 %
% Node as unintptr_t: 140394143817840 %
State: 0
% Node->next: 0x7fb00d920570 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394118645104 %

% Node: 0x7fb00d920570 %
% Size of Node: 8 %
% Node as unintptr_t: 140394118645104 %
State: 0
% Node->next: 0x7fb00e934a90 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394135505552 %

% Node: 0x7fb00e934a90 %
% Size of Node: 8 %
% Node as unintptr_t: 140394135505552 %
State: 0
% Node->next: 0x7fb00f1227f0 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394143819760 %

% Node: 0x7fb00f1227f0 %
% Size of Node: 8 %
% Node as unintptr_t: 140394143819760 %
State: 0
% Node->next: 0x7fb00e934b80 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140394135505792 %

% Node: 0x7fb00e934b80 %
% Size of Node: 8 %
% Node as unintptr_t: 140394135505792 %
State: 0
% Node->next: 0x7fb07533a243 %
% Size of Node->next: 8 %
% Node->next: unintptr_t: 140395857289795 %


tree_stats_computeWalltime=0s

tree_stats_numInternalsAtDepth=1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
tree_stats_numLeavesAtDepth=0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
tree_stats_numNodesAtDepth=1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
tree_stats_numKeysAtDepth=0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
tree_stats_avgDegreeAtDepth=1 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 1

tree_stats_height=46
tree_stats_numInternals=46
tree_stats_numLeaves=0
tree_stats_numNodes=46
tree_stats_numKeys=45

tree_stats_avgDegreeInternal=1
tree_stats_avgDegreeLeaves=0
tree_stats_avgDegree=1.97826
tree_stats_avgKeyDepth=23

first_node_allocated_addresses_by_index=
first_descriptor_allocated_addresses_by_index=
first_extra_type1_allocated_addresses_by_index=
first_extra_type2_allocated_addresses_by_index=
first_extra_type3_allocated_addresses_by_index=
first_extra_type4_allocated_addresses_by_index=
sum_num_inserts_total=26943402
sum_num_deletes_total=26939544
sum_num_searches_total=80845042
sum_num_rq_total=
sum_num_operations_by_thread=16584878 16719764 16966909 17032021 16840800 17124435 16627850 16831331
average_num_operations_total=16840998
stdev_num_operations_total=72632
sum_num_operations_total=134727988
min_num_operations_total=16584878
max_num_operations_total=17124435

log_histogram_of_none_limbo_reclamation_event_size_full_data=1:292 2:4646 4:44328 8:30596 16:52
    [2^00, 2^01]: 292
    (2^01, 2^02]: 4646
    (2^02, 2^03]: 44328
    (2^03, 2^04]: 30596
    (2^04, 2^05]: 52
sum_limbo_reclamation_event_size_total=551758
sum_pool_cpu_get_total=
sum_pool_cpu_add_total=
sum_move_block_reclaimer_to_cpu_total=
sum_move_block_cpu_to_node_total=
sum_move_block_node_to_global_total=
sum_move_block_global_to_alloc_total=
sum_move_block_alloc_to_cpu_total=
sum_move_block_global_to_cpu_total=
sum_move_block_node_to_cpu_total=
sum_num_bail_from_addkv_at_depth_by_index=
sum_num_bail_from_build_at_depth_by_index=
sum_num_help_subtree_total=
sum_num_try_rebuild_at_depth_by_index=
sum_num_complete_rebuild_at_depth_by_index=
sum_num_help_rebuild_total=

log_histogram_of_none_num_prop_epoch_latency_full_data=1:0 2:0 4:0 8:0 16:0 32:0 64:0 128:0 256:0 512:0 1024:0 2048:0 4096:0 8192:0 16384:0 32768:148 65536:1172 131072:1442 262144:1404 524288:671 1048576:142 2097152:9 4194304:2 8388608:1 16777216:0 33554432:0 67108864:0 134217728:0 268435456:0 536870912:0 1073741824:0 2147483648:0 4294967296:0 8589934592:0 17179869184:0 34359738368:0 68719476736:0 137438953472:0 274877906944:0 549755813888:0 1099511627776:0 2199023255552:0 4398046511104:0 8796093022208:0 17592186044416:0 35184372088832:0 70368744177664:0 140737488355328:0 281474976710656:0 562949953421312:0 1125899906842624:0 2251799813685248:0 4503599627370496:8
    (2^15, 2^16]: 148
    (2^16, 2^17]: 1172
    (2^17, 2^18]: 1442
    (2^18, 2^19]: 1404
    (2^19, 2^20]: 671
    (2^20, 2^21]: 142
    (2^21, 2^22]: 9
    (2^22, 2^23]: 2
    (2^23, 2^24]: 1
    (2^24, 2^25]: 0
    (2^25, 2^26]: 0
    (2^26, 2^27]: 0
    (2^27, 2^28]: 0
    (2^28, 2^29]: 0
    (2^29, 2^30]: 0
    (2^30, 2^31]: 0
    (2^31, 2^32]: 0
    (2^32, 2^33]: 0
    (2^33, 2^34]: 0
    (2^34, 2^35]: 0
    (2^35, 2^36]: 0
    (2^36, 2^37]: 0
    (2^37, 2^38]: 0
    (2^38, 2^39]: 0
    (2^39, 2^40]: 0
    (2^40, 2^41]: 0
    (2^41, 2^42]: 0
    (2^42, 2^43]: 0
    (2^43, 2^44]: 0
    (2^44, 2^45]: 0
    (2^45, 2^46]: 0
    (2^46, 2^47]: 0
    (2^47, 2^48]: 0
    (2^48, 2^49]: 0
    (2^49, 2^50]: 0
    (2^50, 2^51]: 0
    (2^51, 2^52]: 0
    (2^52, 2^53]: 8
average_num_prop_epoch_latency_total=10443254112216
stdev_num_prop_epoch_latency_total=
min_num_prop_epoch_latency_total=32791
max_num_prop_epoch_latency_total=6525728223409984

log_histogram_of_none_bag_rotation_reclaim_size_full_data=1:0
    [2^00, 2^01]: 0

log_histogram_of_none_token_received_time_split_ms_full_data=1:0
    [2^00, 2^01]: 0
first_token_received_time_last_ms_by_thread=

log_histogram_of_none_bag_rotation_duration_split_ms_full_data=1:0
    [2^00, 2^01]: 0
first_bag_curr_size_by_thread=
first_bag_last_size_by_thread=
first_token_counts_by_thread=
first_thread_announced_epoch_by_index=496512
sum_elapsed_rebuild_depth_by_index=
sum_elapsed_rebuild_depth_total=
sum_duration_all_ops_total=
sum_duration_markAndCount_total=
sum_duration_wastedWorkBuilding_total=
sum_duration_buildAndReplace_total=
sum_duration_rotateAndFree_total=
sum_duration_traverseAndRetire_total=
sum_rebuild_is_subsumed_total=
sum_rebuild_is_subsumed_at_depth_by_index=
first_time_thread_terminate_by_thread=10000000 10000000 10000001 10000000 10000000 10000000 10000000 10000000
min_time_thread_terminate_total=10000000
max_time_thread_terminate_total=10000001
first_time_thread_start_by_thread=3 1 0 0 0 1
min_time_thread_start_total=1
max_time_thread_start_total=3


threads_final_keysum=1835
threads_final_size=43
final_keysum=549755815818
final_size=45
validate_result=fail
Validation FAILURE: threadsKeySum=1835 dsKeySum=549755815818 threadsSize=43 dsSize=45
Validation comment: data structure is LARGER than it should be according to the operation return values
total_execution_walltime=10.253s
Command exited with non-zero status 255
80.42user 3.12system 0:10.44elapsed 799%CPU (0avgtext+0avgdata 1067132maxresident)k
0inputs+32outputs (0major+278979minor)pagefaults 0swaps
maxresident_mb=1067
