#include <iostream>

#define TYPE_MASK                   (0x6LL)
#define MSB_48_MASK                 (0x800000000000LL) //the 48th bit
#define MSB_64_MASK					(0x8000000000000000LL)

#define DURABLE_MASK                (0x8000000000000000LL)
#define IS_DURABLE(x)               ((x)&DURABLE_MASK) //true if unmarked

class Node {
	int K;
	int V;
};

int main() {
	void * ptr = malloc(sizeof(Node)); 
	
	intptr_t test = 0xF64c762e1e70;
	std::cout << std::hex << (((intptr_t)test) & MSB_64_MASK) << std::endl;
	std::cout << std::hex << (((intptr_t)test) | MSB_64_MASK) << std::endl;

	std::cout << sizeof(size_t) << std::endl;

	size_t dirty = 0x000000000000000F;
	std::cout << std::hex << dirty << std::endl;
	std::cout << std::hex << (dirty << 3) << std::endl;

	size_t x = 1;
	std::cout << IS_DURABLE(x) << std::endl;
	if(IS_DURABLE(x | DURABLE_MASK)){
		std::cout << "woo" << std::endl;
	}
	if(!IS_DURABLE(x)){
		std::cout << "woo 2" << std::endl;
	}
	std::cout << std::dec << IS_DURABLE(x | DURABLE_MASK) << std::endl;



	//0000 1111 -> 0F
	//0111 1000 -> 0F << 3 = 78

	//564c762e1e70
	//1000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000
	//0101 0110 0100 1100 0111 0110 0010 1110 0001 1110 0111 0000
}