cmd=LD_PRELOAD=../../../lib/libjemalloc.so timeout 600 numactl --interleave=all time bin_disabled/ubench_zuriel_soft_linked_list.alloc_new.reclaim_debra.pool_none.out -nwork 8 -nprefill 8 -i 20 -d 20 -rq 0 -rqsize 1 -k 100 -nrq 0 -t 10000 -pin 0-23,96-119,24-47,120-143,48-71,144-167,72-95,168-191
step=10058
fname=nvram_exp1/step10058.txt
binary=bin_disabled/ubench_zuriel_soft_linked_list.alloc_new.reclaim_debra.pool_none.out
parsed custom binding: 0-23,96-119,24-47,120-143,48-71,144-167,72-95,168-191
DS_TYPENAME=zuriel_soft_linked_list
FIND_FUNC=FIND_FUNC
INSERT_FUNC=insertIfAbsent
ERASE_FUNC=ERASE_FUNC
RQ_FUNC=RQ_FUNC
RECLAIM=reclaimer_debra
ALLOC=allocator_new
POOL=pool_none
MAX_THREADS_POW2=256
CPU_FREQ_GHZ=2.1
MILLIS_TO_RUN=10000
INS=20
DEL=20
RQ=0
RQSIZE=1
MAXKEY=100
PREFILL_THREADS=8
DESIRED_PREFILL_SIZE=-1
TOTAL_THREADS=8
WORK_THREADS=8
RQ_THREADS=0
distribution=0
Size of node=40
ACTUAL_THREAD_BINDINGS=0,1,2,3,4,5,6,7


Info: prefilling using INSERTION ONLY.
timing_start inserting 50 keys with 8 threads...
timing_elapsed 0.00115525s
first_node_allocated_addresses_by_index=
first_descriptor_allocated_addresses_by_index=
first_extra_type1_allocated_addresses_by_index=
first_extra_type2_allocated_addresses_by_index=
first_extra_type3_allocated_addresses_by_index=
first_extra_type4_allocated_addresses_by_index=
sum_num_inserts_total=74
sum_num_deletes_total=
sum_num_searches_total=
sum_num_rq_total=
sum_num_operations_by_thread=
average_num_operations_total=
stdev_num_operations_total=
sum_num_operations_total=
min_num_operations_total=
max_num_operations_total=

log_histogram_of_none_limbo_reclamation_event_size_full_data=1:0
    [2^00, 2^01]: 0
sum_limbo_reclamation_event_size_total=
sum_pool_cpu_get_total=
sum_pool_cpu_add_total=
sum_move_block_reclaimer_to_cpu_total=
sum_move_block_cpu_to_node_total=
sum_move_block_node_to_global_total=
sum_move_block_global_to_alloc_total=
sum_move_block_alloc_to_cpu_total=
sum_move_block_global_to_cpu_total=
sum_move_block_node_to_cpu_total=
sum_num_bail_from_addkv_at_depth_by_index=
sum_num_bail_from_build_at_depth_by_index=
sum_num_help_subtree_total=
sum_num_try_rebuild_at_depth_by_index=
sum_num_complete_rebuild_at_depth_by_index=
sum_num_help_rebuild_total=

log_histogram_of_none_num_prop_epoch_latency_full_data=1:0
    [2^00, 2^01]: 0
average_num_prop_epoch_latency_total=
stdev_num_prop_epoch_latency_total=
min_num_prop_epoch_latency_total=
max_num_prop_epoch_latency_total=

log_histogram_of_none_bag_rotation_reclaim_size_full_data=1:0
    [2^00, 2^01]: 0

log_histogram_of_none_token_received_time_split_ms_full_data=1:0
    [2^00, 2^01]: 0
first_token_received_time_last_ms_by_thread=

log_histogram_of_none_bag_rotation_duration_split_ms_full_data=1:0
    [2^00, 2^01]: 0
first_bag_curr_size_by_thread=
first_bag_last_size_by_thread=
first_token_counts_by_thread=
first_thread_announced_epoch_by_index=
sum_elapsed_rebuild_depth_by_index=
sum_elapsed_rebuild_depth_total=
sum_duration_all_ops_total=
sum_duration_markAndCount_total=
sum_duration_wastedWorkBuilding_total=
sum_duration_buildAndReplace_total=
sum_duration_rotateAndFree_total=
sum_duration_traverseAndRetire_total=
sum_rebuild_is_subsumed_total=
sum_rebuild_is_subsumed_at_depth_by_index=
first_time_thread_terminate_by_thread=
min_time_thread_terminate_total=
max_time_thread_terminate_total=
first_time_thread_start_by_thread=
min_time_thread_start_total=
max_time_thread_start_total=
finished prefilling to size 50 keysum=2575, performing 74 successful updates in 0.001s
pref_size=50
prefill_elapsed_ms=59
main thread: starting timer...

###############################################################################
################################ BEGIN RUNNING ################################
###############################################################################


###############################################################################
################################## TIME IS UP #################################
###############################################################################

joining threads...
###############################################################################
################################# END RUNNING #################################
###############################################################################

10s
gstats_timer_elapsed timer_bag_rotation_start=6.26611e+06
PRODUCING OUTPUT
Command terminated by signal 11
79.96user 3.80system 0:10.56elapsed 792%CPU (0avgtext+0avgdata 1716664maxresident)k
0inputs+8outputs (0major+435353minor)pagefaults 0swaps
maxresident_mb=1716




GUY::NOTES::
- I am pretty sure this is seg fault is happening in tree stats but I have no idea where need to check that
- we at least know it isnt happening in the data structure itself since the output progressed past joining the threads
    without any issues
- apparently the seg fault is in the adapter::
return node == NULL ? 0 : (node->key == minKey ? 0 : (node->key == maxKey ? 0 : 1));

specifically seg faulting on the key which is incredibly strange
