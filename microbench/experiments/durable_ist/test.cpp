// #include <thread>
#include <iostream>
#include <stdlib.h>

typedef unsigned __int128 uint128_t;

#define CAS(ptr, oldVal, newVal)	__sync_bool_compare_and_swap(ptr, oldVal, newVal)
#define VCAS(ptr, oldVal, newVal)	__sync_val_compare_and_swap(ptr, oldVal, newVal)

#define MAX_FLUSHED_NODES 100

#define NODE_NEXT(nodePtr)			(PTR_TO_NODE((Node<long, long>*)nodePtr->nextPair))
#define NODE_NEXT_PTR(nodePtr)		((Node<long, long>*)nodePtr->nextPair)

#define DURABLE_MASK                (0x1ll) //001
#define MARK_NODE_DURABLE(x)     	((Node<long, long> *)(((uintptr_t)(x)) | DURABLE_MASK))
#define IS_DURABLE(x)               (((uintptr_t)(x)) & DURABLE_MASK)


//Ignores durability bit
#define PTR_MASK					(0x6ll) //110

//clean = 00X
#define CLEAN_MASK					(0x0ll) //00X
#define IS_CLEAN(x)					((((uintptr_t)(x)) & PTR_MASK) == CLEAN_MASK)

//Dflag = 01X
#define DFLAG_MASK 					(0x2ll) //01X
#define IS_DFLAGGED(x)				((((uintptr_t)(x)) & PTR_MASK) == DFLAG_MASK)
#define MARK_NODE_DFLAG(x)			((Node<long, long> *)(((uintptr_t)(x)) | DFLAG_MASK))

//IFlag = 10X
#define IFLAG_MASK 					(0x4ll) //10X
#define IS_IFLAGGED(x)				((((uintptr_t)(x)) & PTR_MASK) == IFLAG_MASK)
#define MARK_NODE_IFLAG(x)			((Node<long, long> *)(((uintptr_t)(x)) | IFLAG_MASK))

#define UNFLAG_NODE(x)				((Node<long, long> *)(((uintptr_t)(x)) & ~PTR_MASK))

//DMark = 11X						
#define DEL_MASK 					(0x6ll) //11X - same as PTR_MASK
#define IS_MARKED_FOR_DEL(x)		((((uintptr_t)(x)) & PTR_MASK) == DEL_MASK)
#define MARK_NODE_DEL(x)			((Node<long, long> *)(((uintptr_t)(x)) | DEL_MASK))

#define TOTAL_MASK					(0x7ll) //111
#define PTR_TO_NODE(x)				((Node<long, long> *)((uintptr_t)(x) & ~TOTAL_MASK))



#define MARK_OLD_IFLAG(x)			((((uintptr_t)(x)) | IFLAG_MASK))




template<typename K, typename V>
struct Node{
	uint128_t volatile nextPair;	

	K key;
	V value;	
};

int main() {
	Node<long, long>* nodeToDel = new Node<long, long>();
	nodeToDel->key = 2;
	nodeToDel->value = 4;
	
	Node<long, long>* nodeToDelSucc = new Node<long, long>();
	nodeToDelSucc->key = 8;
	nodeToDelSucc->value = 16;
	nodeToDelSucc->nextPair = 0;

	nodeToDel->nextPair = (uint128_t)MARK_NODE_DURABLE(nodeToDelSucc);

	uint128_t expNext = (uint128_t)(MARK_NODE_DURABLE(nodeToDelSucc));		
	uint128_t newNext = (uint128_t)(MARK_NODE_DURABLE(MARK_NODE_DEL(nodeToDelSucc)));		
			
	uint128_t result = VCAS(&nodeToDel->nextPair, expNext, newNext);

	if (result == expNext) {
		printf("theyre equal\n");
	}

	if (IS_MARKED_FOR_DEL(newNext)) {
		printf("this works too\n");
	}

	Node<long, long>* n = PTR_TO_NODE(newNext);
	printf("n key: %d\n", n->key);

	// if (result == expNext || IS_MARKED_FOR_DEL(result)) {			
	// 	this->helpMarked(tid, parent, nodeToDel);
	// 	return;
	// }
	// else {
	// 	if (IS_DFLAGGED(result)){
	// 		helpRemove(tid, nodeToDel, PTR_TO_NODE((Node<long, long>*)result));
	// 	}				
	// 	//else we failed bc nodeToDel->next was modified by an insert or a helper completed remove					
	// }
}