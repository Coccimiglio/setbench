#pragma once

#include "indirect_dcss.h"
#include <cassert>
#include <stdint.h>
#include <sstream>

#define BOOL_CAS __sync_bool_compare_and_swap
#define VAL_CAS __sync_val_compare_and_swap

#define DCSS_TAGBIT 0x1

inline static
bool isDcss(casword_t val) {
    return (val & DCSS_TAGBIT);
}

template <typename Unused>
dcssresult_t indirectDcssProvider<Unused>::dcssHelp(const int tid, dcsstagptr_t tagptr, dcssptr_t snapshot, bool helpingOther) {
    // figure out what the state should be
    casword_t state = DCSS_STATE_FAILED;

    SOFTWARE_BARRIER;
    casword_t val1 = *(snapshot->addr1);
    SOFTWARE_BARRIER;
    
    if (val1 == snapshot->old1) { // linearize here(?)
        state = DCSS_STATE_SUCCEEDED;
    }
    
    // try to cas the state to the appropriate value
    dcssptr_t ptr = TAGPTR_UNPACK_PTR(dcssDescriptors, tagptr);
    casword_t retval;
    bool failedBit;
    MUTABLES_VAL_CAS_FIELD(failedBit, retval, ptr->mutables, snapshot->mutables, DCSS_STATE_UNDECIDED, state, DCSS_MUTABLES_MASK_STATE, DCSS_MUTABLES_OFFSET_STATE); 
    if (failedBit) return {DCSS_IGNORED_RETVAL,0};                             // failed to access the descriptor: we must be helping another process complete its operation, so we will NOT use this return value!
    
    
    // finish the operation based on the descriptor's state
    if ((retval == DCSS_STATE_UNDECIDED && state == DCSS_STATE_SUCCEEDED)     // if we changed the state to succeeded OR
      || retval == DCSS_STATE_SUCCEEDED) {                                     // if someone else changed the state to succeeded
        assert(helpingOther || ((snapshot->mutables & DCSS_MUTABLES_MASK_STATE) >> DCSS_MUTABLES_OFFSET_STATE) == DCSS_STATE_SUCCEEDED);
        BOOL_CAS(snapshot->addr2, (casword_t) tagptr, snapshot->new2); 
        return {DCSS_SUCCESS,0};
    } else {                                                                    // either we or someone else changed the state to failed
        assert((retval == DCSS_STATE_UNDECIDED && state == DCSS_STATE_FAILED)
                || retval == DCSS_STATE_FAILED);
        assert(helpingOther || ((snapshot->mutables & DCSS_MUTABLES_MASK_STATE) >> DCSS_MUTABLES_OFFSET_STATE) == DCSS_STATE_FAILED);
        BOOL_CAS(snapshot->addr2, (casword_t) tagptr, snapshot->old2);
        return {DCSS_FAILED_ADDR1,val1};
    }
}

template <typename Unused>
void indirectDcssProvider<Unused>::dcssHelpOther(const int tid, dcsstagptr_t tagptr) {
    const int otherTid = TAGPTR_UNPACK_TID(tagptr);
#ifndef NDEBUG
    if (!(otherTid >= 0 && otherTid < NUM_PROCESSES)) {
        std::cout<<"otherTid="<<otherTid<<" NUM_PROCESSES="<<NUM_PROCESSES<<std::endl;
    }
#endif
    assert(otherTid >= 0 && otherTid < NUM_PROCESSES);
    dcssdesc_t newSnapshot;
    const int sz = dcssdesc_t::size;
    assert((((tagptr & MASK_SEQ) >> OFFSET_SEQ) & 1) == 1);
    if (DESC_SNAPSHOT(dcssdesc_t, dcssDescriptors, &newSnapshot, tagptr, sz)) {
        dcssHelp(tid, tagptr, &newSnapshot, true);
    } else {
        //TRACE COUTATOMICTID("helpOther unable to get snapshot of "<<tagptrToString(tagptr)<<std::endl);
    }
}

template <typename Unused>
inline
tagptr_t indirectDcssProvider<Unused>::getDescriptorTagptr(const int otherTid) {
    dcssptr_t ptr = &dcssDescriptors[otherTid];
    tagptr_t tagptr = TAGPTR_NEW(otherTid, ptr->mutables, DCSS_TAGBIT);
    if ((UNPACK_SEQ(tagptr) & 1) == 0) {
        // descriptor is being initialized! essentially,
        // we can think of there being NO ongoing operation,
        // so we can imagine we return NULL = no descriptor.
        return (tagptr_t) NULL;
    }
    return tagptr;
}

template <typename Unused>
inline
dcssptr_t indirectDcssProvider<Unused>::getDescriptorPtr(tagptr_t tagptr) {
    return TAGPTR_UNPACK_PTR(dcssDescriptors, tagptr);
}

template <typename Unused>
inline
bool indirectDcssProvider<Unused>::getDescriptorSnapshot(tagptr_t tagptr, dcssptr_t const dest) {
    if (tagptr == (tagptr_t) NULL) return false;
    return DESC_SNAPSHOT(dcssdesc_t, dcssDescriptors, dest, tagptr, dcssdesc_t::size);
}

template <typename Unused>
inline
void indirectDcssProvider<Unused>::helpProcess(const int tid, const int otherTid) {
    tagptr_t tagptr = getDescriptorTagptr(otherTid);
    if (tagptr != (tagptr_t) NULL) dcssHelpOther(tid, tagptr);
}

template <typename Unused>
dcssresult_t indirectDcssProvider<Unused>::dcssVal(const int tid, casword_t * descriptorContainer, casword_t descriptorContainerOld, casword_t * addr1, casword_t old1, casword_t * addr2, casword_t old2, casword_t new2) {
    return dcssPtr(tid, descriptorContainer, descriptorContainerVal, addr1, old1, addr2, old2 << DCSS_LEFTSHIFT , new2 << DCSS_LEFTSHIFT);
}

template <typename Unused>
dcssresult_t indirectDcssProvider<Unused>::dcssPtr(const int tid, casword_t * descriptorContainer, casword_t descriptorContainerOld, casword_t * addr1, casword_t old1, casword_t * addr2, casword_t old2, casword_t new2) {
    
    
    // create dcss descriptor
    dcssptr_t ptr = DESC_NEW(dcssDescriptors, DCSS_MUTABLES_NEW, tid);
    assert((((dcssDescriptors[tid].mutables & MASK_SEQ) >> OFFSET_SEQ) & 1) == 0);
    ptr->addr1 = addr1;
    ptr->old1 = old1;
    ptr->addr2 = addr2;
    ptr->old2 = old2;
    ptr->new2 = new2;
    
    DESC_INITIALIZED(dcssDescriptors, tid);
    
    // create tagptr
    assert((((dcssDescriptors[tid].mutables & MASK_SEQ) >> OFFSET_SEQ) & 1) == 1);
    tagptr_t tagptr = TAGPTR_NEW(tid, ptr->mutables, DCSS_TAGBIT);
    

    
    // perform the dcss operation described by our descriptor
    casword_t r;
    //for indirect DCSS our descriptor field might can safely go from DCSS to non-DCSS and still allow us to retry
    //so we just try until we succeed this CAS -- with a really bad schedule we could get stuck here but we only fail 
    //to CAS in our descriptor if someone else CAS'd in theirs so this isn't a major concern 
    do {
        assert(!isDcss(ptr->old2));
        assert(isDcss(tagptr));
        //r = VAL_CAS(ptr->addr2, ptr->old2, (casword_t) tagptr);
        r = VAL_CAS(descriptorContainer, descriptorContainerOld, (casword_t) tagptr);
        if (isDcss(r)) {
#ifdef USE_DEBUGCOUNTERS
            this->dcssHelpCounter->inc(tid);
#endif
            dcssHelpOther(tid, (dcsstagptr_t) r);
        }
    } while (r != descriptorContainerOld);

    // The return value of LLX does not relate to the field changing so we cant use LLX to determine which
    // field (if any) caused the SCX to fail. We would need to change the implementation of SCX
    // LLX addr1

    // LLX addr2

    // SCX from old2 to new2

    // TODO: we cant just check the return value of the CAS for the descriptor anymore
    if (r == ptr->old2){
        return dcssHelp(tid, tagptr, ptr, false); // finish our own operation      
    } 

    return {DCSS_FAILED_ADDR2,r};//DCSS_FAILED_ADDR2;
}

template <typename Unused>
inline
casword_t indirectDcssProvider<Unused>::dcssRead(const int tid, casword_t volatile * addr) {
    casword_t r;
    while (1) {
        r = *addr;
        if (unlikely(isDcss(r))) {
//            std::cout<<"found supposed dcss descriptor @ "<<(size_t) r<<std::endl;
#ifdef USE_DEBUGCOUNTERS
            this->dcssHelpCounter->inc(tid);
#endif
            dcssHelpOther(tid, (dcsstagptr_t) r);
        } else {
            return r;
        }
    }
}

template <typename Unused>
indirectDcssProvider<Unused>::indirectDcssProvider(const int numProcesses) : NUM_PROCESSES(numProcesses) {
    sxcProv = new SCXProvider<casword_t, 2>(numProcesses);

#ifdef USE_DEBUGCOUNTERS
    dcssHelpCounter = new debugCounter(NUM_PROCESSES);
#endif
    DESC_INIT_ALL(dcssDescriptors, DCSS_MUTABLES_NEW, NUM_PROCESSES);
    for (int tid=0;tid<numProcesses;++tid) {
        dcssDescriptors[tid].addr1 = 0;
        dcssDescriptors[tid].addr2 = 0;
        dcssDescriptors[tid].new2 = 0;
        dcssDescriptors[tid].old1 = 0;
        dcssDescriptors[tid].old2 = 0;
    }
}

template <typename Unused>
indirectDcssProvider<Unused>::~indirectDcssProvider() {
#ifdef USE_DEBUGCOUNTERS
    delete dcssHelpCounter;
#endif
}

template <typename Unused>
inline
casword_t indirectDcssProvider<Unused>::readPtr(const int tid, casword_t volatile * addr) {
    casword_t r;
    r = dcssRead(tid, addr);
    return r;
}

template <typename Unused>
casword_t indirectDcssProvider<Unused>::readVal(const int tid, casword_t volatile * addr) {
    return ((casword_t) readPtr(tid, addr))>>DCSS_LEFTSHIFT;
}

template <typename Unused>
void indirectDcssProvider<Unused>::writePtr(casword_t volatile * addr, casword_t ptr) {
    //assert((*addr & DCSS_TAGBIT) == 0);
    assert((ptr & DCSS_TAGBIT) == 0);
    *addr = ptr;
}

template <typename Unused>
void indirectDcssProvider<Unused>::writeVal(casword_t volatile * addr, casword_t val) {
    writePtr(addr, val<<DCSS_LEFTSHIFT);
}

template <typename Unused>
void indirectDcssProvider<Unused>::initThread(const int tid) {}

template <typename Unused>
void indirectDcssProvider<Unused>::deinitThread(const int tid) {}

template <typename Unused>
void indirectDcssProvider<Unused>::debugPrint() {
#ifdef USE_DEBUGCOUNTERS
    std::cout<<"dcss helping : "<<this->dcssHelpCounter->getTotal()<<std::endl;
#endif
}
