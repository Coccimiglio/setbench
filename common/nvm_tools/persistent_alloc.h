#ifndef PERSISTENT_ALLOC_H
#define PERSISTENT_ALLOC_H

#include <cstddef>
#include <dlfcn.h>

class AllocatorLibvmmalloc_Simple  {
public:
    void* (*pmallocFunc)(size_t);
	void (*pfreeFunc)(void*);  
 
	void init() {        
        void *libvmmalloc = dlopen("/usr/local/lib/libvmmalloc.so", RTLD_NOW | RTLD_LOCAL);        
        
        if (!libvmmalloc) {
            printf("Error opening libvammloc.so with dlopen.\nExiting\n");
            exit(-1);
        }
        
        *(void **)(&pmallocFunc) = dlsym(libvmmalloc, "malloc");                
        *(void **)(&pfreeFunc) = dlsym(libvmmalloc, "free");		 
        printf("Initialized AllocatorLibvmmalloc_Simple\n");      
    }

    void* allocate(size_t size){
        return pmallocFunc(size);
    } 

    void deallocate(void* const ptr) {
        pfreeFunc(ptr);
    }


    template <typename T, typename... Args> 	
    T* allocateNew(Args&&... args){
        void* addr = pmallocFunc(sizeof(T));
        // assert(addr != nullptr);
        T* ptr = new (addr) T(std::forward<Args>(args)...); // placement new
        return ptr;
    } 

    template <typename T>
    void deallocateDelete(T* const ptr) {
        if (ptr == nullptr) return;
        ptr->~T();
        pfreeFunc(ptr);
    }
};

#endif