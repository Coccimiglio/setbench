#pragma once

#include <emmintrin.h>

// #define DISABLE_FLUSHING 1

namespace durableTools {
	static inline void FLUSH(void volatile * p){
#ifndef DISABLE_FLUSHING
	#if defined(USE_CLFLUSHOPT)
		asm volatile("clflushopt (%0)" :: "r"(p));	
	#elif defined(USE_CLWB) 
		asm volatile("clwb (%0)" :: "r"(p));		
	#else
		asm volatile("clflush (%0)" :: "r"(p));						
	#endif		
#endif
	}

	static inline void CLWB(void volatile * p){
#ifndef DISABLE_FLUSHING
		asm volatile("clwb (%0)" :: "r"(p));
		
		// This was used in CXPUC and I also saw it in Mirror I guess they were using CPUs that didn't
		// implement CLWB? not sure why they would use xsaveopt? Mirror also used the version of clwb that I 
		// above
		// asm volatile(".byte 0x66; xsaveopt %0" : "+m" (*(volatile char *)(p)));
#endif
	}

	static inline void CLFLUSHOPT(void volatile * p){
#ifndef DISABLE_FLUSHING
		asm volatile("clflushopt (%0)" :: "r"(p));
#endif
	}

	static inline void SFENCE(){
#if !defined(DISABLE_FLUSHING) && !defined(DURABLE_TOOLS_FORCE_SFENCE)
		asm volatile("sfence" ::: "memory");
#endif
	}

	static inline void CLFLUSH(void volatile * p){
#ifndef DISABLE_FLUSHING
		asm volatile("clflush (%0)" :: "r"(p));
#endif
	}

	static inline uint64_t nv_getticks(void) { //took this from log-free concurrent DS
#ifdef DISABLE_FLUSHING
		unsigned hi, lo;
		__asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
		return ( (unsigned long long)lo)|( ((unsigned long long)hi)<<32 );
#else
		return 0;
#endif
	}

	//TODO:: probably should remove this flushing individual bytes is basically guaranteed to waste
	//calls to CLFLUSH or CLFLUSHOPT should flush the primitives instead
	// static inline void flushData(void * start, int bytes){
	// 	char * ptr = (char *)start;

	// 	for (int i = 0; i < bytes; i++) {
	// 		FLUSH(ptr);
	// 		ptr++;
	// 	}
	// }
};