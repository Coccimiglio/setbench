#pragma once

#include <vector>
#include <climits>
#include "LinkFreeUtilities.h"
#include <atomic>
#include <cassert>
#include <stdint.h>
#include <stdlib.h>
#include <unordered_set>

#if defined(USE_LIBVMMALLOC) && !defined(RECORDMANAGER_PMEM_SUPPORT)
	#include "persistent_alloc.h"
#endif

#if defined(USE_MEMKIND) && !defined(RECORDMANAGER_PMEM_SUPPORT)
	#include <memkind.h>
#endif 

#if defined(USE_MEMKIND) && !defined(RECORDMANAGER_PMEM_SUPPORT)
	#define PMEM_POOL_PATH "/mnt/pmem1_mount/"
	#define PMEM_POOL_SIZE 0 // 0 means the heap is not a fixed size and can grow as long as the temp file can grow
#endif

//#define LOAD_MO std::memory_order_relaxed
#define LOAD_MO

template <typename K, typename V>
class PNode
{
  public:
	std::atomic<uchar> metaData;
	std::atomic<bool> insertFlag;
	std::atomic<bool> deleteFlag;
	K key;
	V value;

	PNode() : metaData(0), insertFlag(false), deleteFlag(false) {}

} __attribute__((aligned((32))));

template <typename K, typename V>
class Node
{
public:
	// std::atomic<uchar> metaData;
	// std::atomic<bool> insertFlag;
	// std::atomic<bool> deleteFlag;
	K key;
	// V value;
	std::atomic<PNode<K,V>*> pnode;
	std::atomic<Node *> next;

	Node() : pnode(nullptr), next(nullptr) {}

	// Node() : metaData(0), next(nullptr), insertFlag(false), deleteFlag(false) {}
	// Node(K key, V value, Node *next) : key(key), value(value), next(next), insertFlag(false), deleteFlag(false) {}

	bool isMarked()
	{
		return linkFreeUtils::isMarked(next.load());
	}
} __attribute__((aligned((32))));

template <class RecordManager, typename K, typename V>
class LinkFreeList
{
private:
	PAD;
	Node<K, V>  * head;
	PAD;

#ifdef RECORDMANAGER_PMEM_SUPPORT
	RecordManager * recmgr;
#elif defined(USE_MEMKIND)
	struct memkind *pmem_kind = NULL;
#elif defined(USE_LIBVMMALLOC)
	AllocatorLibvmmalloc_Simple* alloc;
#endif

	V noVal;
	PAD;

	Node<K, V> * allocNode(int tid, K key, V value, Node<K, V>  *next)
	{
		Node<K,V>* newNode = new Node<K,V>();

#ifdef RECORDMANAGER_PMEM_SUPPORT
		PNode<K,V> * pnode = recmgr->template allocate<PNode<K, V>>(tid);
#elif defined(USE_MEMKIND)
		PNode<K,V> * pnode = (PNode<K,V>*)memkind_malloc(pmem_kind, sizeof(Node<K,V>));
#elif defined(USE_LIBVMMALLOC)
		PNode<K,V> * pnode = (PNode<K,V>*)alloc->allocate(sizeof(PNode<K, V>));
#endif		
		GSTATS_ADD(tid, num_pmem_alloc, 1);  

		newNode->pnode.store(pnode, std::memory_order_relaxed);

		linkFreeUtils::flipV1(&pnode->metaData);
		std::atomic_thread_fence(std::memory_order_release);
		pnode->insertFlag.store(false, std::memory_order_relaxed);
		pnode->deleteFlag.store(false, std::memory_order_relaxed);
		pnode->key = key;
		pnode->value = value;
		newNode->key = key;		
		newNode->next.store(next, std::memory_order_relaxed);
		return newNode;
	}

	void FLUSH_DELETE(int tid, PNode<K, V> *pnode)
	{
		if (LIKELY(pnode->deleteFlag.load()))
			return;
		durableTools::FLUSH(pnode);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif			
		pnode->deleteFlag.store(true, std::memory_order_release);
		GSTATS_ADD(tid, num_update_flushes, 1);	
	}

	void CONTAINS_FLUSH_DELETE(int tid, PNode<K, V>  *pnode)
	{
		if (LIKELY(pnode->deleteFlag.load()))
			return;
		durableTools::FLUSH(pnode);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif			
		pnode->deleteFlag.store(true, std::memory_order_release);
		GSTATS_ADD(tid, num_contains_flushes, 1);	
	}

	void SEARCH_FLUSH_DELETE(int tid, PNode<K, V>  *pnode)
	{
		if (LIKELY(pnode->deleteFlag.load()))
			return;
		durableTools::FLUSH(pnode);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif			
		pnode->deleteFlag.store(true, std::memory_order_release);
		GSTATS_ADD(tid, num_search_flushes, 1);
	}




	void CONTAINS_FLUSH_INSERT(int tid, PNode<K, V>  *pnode)
	{
		if (LIKELY(pnode->insertFlag.load()))
			return;
		durableTools::FLUSH(pnode);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif			
		pnode->insertFlag.store(true, std::memory_order_release);
		GSTATS_ADD(tid, num_contains_flushes, 1);	
	}

	void SEARCH_FLUSH_INSERT(int tid, PNode<K, V>  *pnode)
	{
		if (LIKELY(pnode->insertFlag.load()))
			return;
		durableTools::FLUSH(pnode);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif			
		pnode->insertFlag.store(true, std::memory_order_release);
		GSTATS_ADD(tid, num_search_flushes, 1);
	}

	void FLUSH_INSERT(int tid, PNode<K, V>  *pnode)
	{
		if (LIKELY(pnode->insertFlag.load()))
			return;
		durableTools::FLUSH(pnode);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif			
		pnode->insertFlag.store(true, std::memory_order_release);
		GSTATS_ADD(tid, num_update_flushes, 1);	
	}

	//trim curr
	bool trim(int tid, Node<K, V>  *pred, Node<K, V>  *curr, PNode<K,V>* pnode)
	{
		FLUSH_DELETE(tid, pnode);			
		Node<K, V> *succ = linkFreeUtils::getRef<Node<K, V> >(curr->next.load());
		bool result = pred->next.compare_exchange_strong(curr, succ);
		if (LIKELY(result))
			//ssmem_free(alloc, curr);
#ifdef RECORDMANAGER_PMEM_SUPPORT
			recmgr->retire(tid, pnode);
#else 		
#endif	
		return result;
	}

	bool trim_search(int tid, Node<K, V>  *pred, Node<K, V>  *curr)
	{
		PNode<K,V>* pnode = curr->pnode.load();
		SEARCH_FLUSH_DELETE(tid, pnode);			
		Node<K, V> *succ = linkFreeUtils::getRef<Node<K, V> >(curr->next.load());
		bool result = pred->next.compare_exchange_strong(curr, succ);
		if (LIKELY(result))
			//ssmem_free(alloc, curr);
#ifdef RECORDMANAGER_PMEM_SUPPORT
			recmgr->retire(tid, pnode);
#else 		
#endif	
		return result;
	}

	Node<K, V> * find(int tid, K key, Node<K, V>  **predPtr)
	{
		Node<K, V>  *prev = head, *curr = head->next.load();

		while (true)
		{
			// curr is not marked
			if (LIKELY(!linkFreeUtils::isMarked(curr->next)))
			{
				if (UNLIKELY(curr->key >= key))
					break;
				prev = curr;
			}
			else
			{
				trim_search(tid, prev, curr);
				//putting the search-flush counter add here because trim is also called from Remove				
			}
			curr = linkFreeUtils::getRef<Node<K, V> >(curr->next);
		}
		*predPtr = prev;
		return curr;
	}

public:
	LinkFreeList(int numThreads, K minKey, K maxKey, V noValue)
	{		
#ifdef	RECORDMANAGER_PMEM_SUPPORT	
		recmgr = new RecordManager(numThreads);

		// TODO:: fix RecordManager then remove this later
		PNode<K,V> * temp = recmgr->template allocate<PNode<K,V>>(tid);
		recmgr->deallocate(0, temp);	
#elif defined(USE_MEMKIND)
		int err = memkind_create_pmem(PMEM_POOL_PATH, PMEM_POOL_SIZE, &pmem_kind);
		if (err) {			
			printf("Error creating PMEM pool. Exiting\n");
			// print_err_message(err);
			exit(-1);
		}
#elif defined(USE_LIBVMMALLOC)
		alloc = new AllocatorLibvmmalloc_Simple();
		alloc->init();
#endif
		
		Node<K, V>  *max = allocNode(0, maxKey, 0, nullptr);
		Node<K, V>  *min = allocNode(0, minKey, 0, max);
		
		head = min;
		noVal = noValue;
	}

	Node<K, V> * getHead() {
		return head;
	}

	void initThread(const int tid) {
#ifdef RECORDMANAGER_PMEM_SUPPORT		
		recmgr->initThread(tid);
#endif
	}
    
    void deinitThread(const int tid) {
#ifdef RECORDMANAGER_PMEM_SUPPORT				
		recmgr->deinitThread(tid);
#endif
	}

	V insertIfAbsent(int tid, K key, V value)
	{
#ifdef RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif

		while (true)
		{
			Node<K, V>  *pred = nullptr;
			Node<K, V>  *curr = find(tid, key, &pred);
			PNode<K,V> *pnode;

			if (curr->key == key)
			{
				pnode = curr->pnode.load();
				linkFreeUtils::makeValid(&pnode->metaData);
				SEARCH_FLUSH_INSERT(tid, pnode);
				return pnode->value;
			}

			Node<K, V> *newNode = allocNode(tid, key, value, curr);
			pnode = newNode->pnode.load();

			if (pred->next.compare_exchange_strong(curr, newNode))
			{
				linkFreeUtils::makeValid(&pnode->metaData);
				FLUSH_INSERT(tid, pnode);						
				return noVal;
			}
			else {
				// freeing newNode in a deleted and valid state
				newNode->next.store(linkFreeUtils::mark<Node<K, V> >(nullptr));
				linkFreeUtils::makeValid(&pnode->metaData);
				//ssmem_free(alloc, newNode);
#ifdef RECORDMANAGER_PMEM_SUPPORT
				recmgr->deallocate(tid, pnode);
#elif defined(USE_MEMKIND)
			 	memkind_free(pmem_kind, pnode);
#elif defined(USE_LIBVMMALLOC)
				alloc->deallocate(pnode);				
#endif				
				delete newNode;
			}
		}
	}

	V remove(int tid, K key)
	{
#ifdef RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif
		
		Node<K, V>  *pred, *curr, *succ, *markedSucc;
		PNode<K,V>* pnode;
		V val = noVal;
		while (true)
		{
			curr = find(tid, key, &pred);			
			if (curr->key != key)
				return noVal;

 			pnode = curr->pnode.load();
			succ = linkFreeUtils::getRef<Node<K, V> >(curr->next);
			markedSucc = linkFreeUtils::mark<Node<K, V> >(succ);
			linkFreeUtils::makeValid(&pnode->metaData);
			if (curr->next.compare_exchange_strong(succ, markedSucc)) {				
				val = pnode->value;
				break;
			}
		};
		trim(tid, pred, curr, pnode);
		return val;
	}

	bool contains(int tid, K key)
	{
#ifdef RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif

		Node<K, V>  *curr = head->next.load();
		bool marked = false;
		//wait free find
		while (curr->key < key)
		{
			curr = linkFreeUtils::getRef<Node<K, V> >(curr->next.load());
		}
		
		if (curr->key != key) {
			return false;
		}

		PNode<K,V>* pnode = curr->pnode.load();

		marked = linkFreeUtils::isMarked(curr->next.load());
		if (marked)
		{
			//if the node is marked, it must be valid
			CONTAINS_FLUSH_DELETE(tid, pnode);
			return false;
		}

		linkFreeUtils::makeValid(&pnode->metaData);
		CONTAINS_FLUSH_INSERT(tid, pnode);		
		return true;
	}

	bool validate(){
		// std::unordered_set<K> keySet = {};
		// Node<K, V> * curr = head->next;

		// int64_t total = 0; 
		// int64_t count = 0;
    	// while(curr != NULL){
		// 	K key = curr->key;
			
		// 	assert(keySet.count(key) == 0);
		// 	keySet.insert(key);

		// 	++count;

		// 	total += curr->key;
		// 	curr = curr->next;
    	// }

		// if (count != keySet.size()){
		// 	return false;
		// }
		
		return true;
	}

	/*
	//this is only used during recovery
	void quickInsert(Node *newNode)
	{
		K key = newNode->key;
		Node *pred = nullptr, *curr = nullptr, *succ = nullptr;
	retry:
		pred = const_cast<Node *>(head);
		curr = linkFreeUtils::getRef<Node>(pred->next.load());
		//inline the find function
		while (true)
		{
			succ = curr->next.load();
			//trimming
			while (linkFreeUtils::isMarked(succ))
			{
				assert(false);
			}
			//continue searching
			if (curr->key < key)
			{
				pred = curr;
				curr = succ;
			}
			//found the same
			else if (curr->key == key)
			{
				assert(false);
			}
			else
			{
				newNode->next.store(curr, std::memory_order_relaxed);
				if (!pred->next.compare_exchange_strong(curr, newNode))
					goto retry;
				return;
			}
		}
	}
	*/

	/*	
	//this recovery function relies on knowing things about the allocator and I need to check if
	//setbench's exposes the required information
	void recover()
	{
		
		auto curr = alloc->mem_chunks;
		for (; curr != nullptr; curr = curr->next)
		{
			Node *currChunk = static_cast<Node *>(curr->obj);
			uint64_t numOfNodes = SSMEM_DEFAULT_MEM_SIZE / sizeof(Node);
			for (uint64_t i = 0; i < numOfNodes; i++)
			{
				Node *currNode = currChunk[i];
				// the node was never initialized, no need to free it or add it
				if (currNode->next.load() == nullptr && linkFreeUtils::isValid(currNode->metaData.load()))
					continue;
				if (!linkFreeUtils::isValid(currNode->metaData.load()) || currNode->isMarked())
				{
					currNode->next.store(linkFreeUtils::mark<Node>(nullptr));
					linkFreeUtils::makeValid(&currNode->metaData);
					ssmem_free(alloc, currNode);
				}
				else
					quickInsert(currNode);
			}
		}
		
	}
	*/
};