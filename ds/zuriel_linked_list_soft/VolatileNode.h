#pragma once

#include "PNode.h"
#include <atomic>

template <typename K, typename V>
class Node
{
  public:
	K key;
	V value;
	PNode<K,V> *pptr;
	bool pValidity;
	std::atomic<Node<K,V> *> next;

	//YOU MUST HAVE DEFAULT CONSTRUCTOR
	Node() : pptr(nullptr), pValidity(false), next(nullptr) {}

	Node(K key, V value, PNode<K, V> *pptr, bool pValidity) : key(key), value(value), pptr(pptr), pValidity(pValidity), next(nullptr) {}

}; 