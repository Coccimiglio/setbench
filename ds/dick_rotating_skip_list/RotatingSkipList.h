/*
Guys notes: not sure if I want to go through with porting this because of the use of a background thread
*/

#pragma once

#include <atomic_ops.h>

#include "common.h"
#include "ptst.h"
#include "garbagecoll.h"

#define MAX_LEVELS 20

#define NUM_SIZES 1
#define NODE_SIZE 0

#define IDX(_i, _z) ((_z) + (_i)) % MAX_LEVELS
unsigned long sl_zero;

/* bottom-level nodes */
typedef VOLATILE struct sl_node node_t;
struct sl_node
{
        unsigned long level;
        struct sl_node *prev;
        struct sl_node *next;
        unsigned long key;
        void *val;
        struct sl_node *succs[MAX_LEVELS];
        unsigned long marker;
        unsigned long raise_or_remove;
};

/* the skip list set */
typedef struct sl_set set_t;
struct sl_set
{
        struct sl_node *head;
};


//this just frees a node with the garbage collector
void node_delete(node_t *node, ptst_t *ptst);
//this just frees a marker node with the garbage collector
void marker_delete(node_t *node, ptst_t *ptst);

set_t *set_new(int start);
void set_delete(set_t *set);
void set_print(set_t *set, int flag);
int set_size(set_t *set, int flag);

void set_subsystem_init(void);
void set_print_nodenums(set_t *set, int flag);





template <typename K, typename V>
struct Node
{
	unsigned long level;
	Node<K,V>* prev;
	Node<K,V>* next;
	K key;
	V val;
	Node<K,V>* succs[MAX_LEVELS];
	unsigned long marker;
	unsigned long raise_or_remove;
};

/* 
GUY COCCIMIGLIO::
The original authors have their VOLATILE definition commented out so I am not going to use it but if 
we wanted to use it exactly as they have then the following 2 lines of code are how we would. 
(I have left them commented out)
(also VOLATILE appears to just have been defined as volatile --- WHY??????????????)
::GUY COCCIMIGLIO 
*/
//template <typename K, typename V>
//using Node_t = VOLATILE Node<K, V>;

template <class RecordManager, typename K, typename V>
class RotatingSkiplist
{
private:
	RecordManager* recmgr;
	Node<K,V>* head;
	V noVal;
	
	//from background.h
	int raised; /* keep track of if we raised index level */
	int threshold;  /* for testing if we should lower index level */
	unsigned long i;        
	unsigned long zero;

	//Guy::TODO:: not sure if we want to use this variable in the same way -- currently doing that
	// seems like this was used for flagging
	bg_should_delete = 1;

	Node<K,V>* node_new(K key, V val, Node<K,V>* prev, Node<K,V>* next, unsigned int level, ptst_t *ptst) {
		Node<K,V>* node;
		unsigned long i;

		node  = gc_alloc(ptst, gc_id[curr_id]);

		node->key       = key;
		node->val       = val;
		node->prev      = prev;
		node->next      = next;
		node->level     = level;
		node->marker    = 0;
		node->raise_or_remove = 0;

		for (i = 0; i < MAX_LEVELS; i++)
			node->succs[i] = NULL;

		//assert (node->next != node);

		return node;
	}

	Node<K,V>* marker_new(Node<K,V> *prev, Node<K,V> *next, ptst_t *ptst)
	{
		Node<K,V> *node;
		unsigned long i;

		node  = gc_alloc(ptst, gc_id[curr_id]);

		node->key       = 0;
		node->val       = node;
		node->prev      = prev;
		node->next      = next;
		node->level     = 0;
		node->marker    = 1;

		for (i = 0; i < MAX_LEVELS; i++)
				node->succs[i] = NULL;		

		return node;
	}

public:
	RotatingSkiplist(int numThreads, K minKey, K maxKey, V noValue) {
		noVal = noValue;
        recmgr = new RecordManager(numThreads); 
		
		//background thread stuff
		raised = 0;		
        bg_should_delete = 1;
	}

	void initThread(const int tid) {
		recmgr->initThread(tid);
	}

	void deinitThread(const int tid) {
		recmgr->deinitThread(tid);
	}

	Node<K, V> *getHead() {
		return head;
	}

	void initBGThread(const int tid) {
		recmgr->initThread(tid);

	}

	void deinitBGThread(const int tid) {
		recmgr->deinitThread(tid);

	}

	void bgLoopStep(int tid, int sleepTime){
		//THE CODE BELOW IS THE ONE BG LOOP ITERATION from background.c
                

		//not sure if I really want to keep a sleep here
		//this definitely seems like it clashes a bit with the work done by setbench
		usleep(sleepTime);

		//we no longer want to end the loop here
		/*
		if (bg_finished)
			break;
		*/

		zero = sl_zero;

		bg_non_deleted = 0;
		bg_deleted = 0;
		bg_tall_deleted = 0;

		// traverse the node level and try deletes/raises
		raised = bg_trav_nodes(ptst);

		if (raised && (1 == head->level)) {
			// add a new index level

			// nullify BEFORE we increase the level
			head->succs[IDX(head->level, zero)] = NULL;
			BARRIER();
			++head->level;
		}

		// raise the index level nodes
		for (i = 0; (i+1) < set->head->level; i++) {
			assert(i < MAX_LEVELS);
			raised = bg_raise_ilevel(i + 1, ptst);

			if ((((i+1) == (head->level-1)) && raised) && head->level < MAX_LEVELS) {
				// add a new index level

				// nullify BEFORE we increase the level
				head->succs[IDX(head->level,zero)] = NULL;
				BARRIER();
				++head->level;
			}
		}

		// if needed, remove the lowest index level
		threshold = bg_non_deleted * 10;
		if (bg_tall_deleted > threshold) {
			if (head->level > 1) {
				bg_lower_ilevel(ptst);
			}
		}

		if (bg_deleted > bg_non_deleted * 3) {
			bg_should_delete = 1;
			bg_stats.should_delete += 1;
		}
		else {
			bg_should_delete = 0;
		}
		BARRIER();
        

		//after breaking the loop returned null
        //return NULL;
	}

	int sl_do_operation(sl_optype_t optype, unsigned int key, void *val)
	{
		node_t *item = NULL, *next_item = NULL;
		node_t *node = NULL, *next = NULL;
		
		void *node_val = NULL, *next_val = NULL;
		int result = 0;
		ptst_t *ptst;
		unsigned long zero, i;

				auto gaurd = recmgr->getGuard(tid);
		ptst = ptst_critical_enter();

		zero = sl_zero;
		i = head->level - 1;

		/* find an entry-point to the node-level */
		item = head;
		while (1) {
				next_item = item->succs[IDX(i,zero)];

				if (NULL == next_item || next_item->key > key) {

						next_item = item;
						if (zero == i) {
								node = item;
								break;
						} else {
								--i;
						}
				}
				item = next_item;
		}

		/* find the correct node and next */
		while (1) {
				while (node == (node_val = node->val)) {
						node = node->prev;
				}
				next = node->next;
				if (NULL != next) {
						next_val = next->val;
						if (next_val == next) {
								bg_help_remove(node, next, ptst);
								continue;
						}
				}
				if (NULL == next || next->key > key) {
						if (CONTAINS == optype)
								result = sl_finish_contains(key, node,
														node_val,
														ptst);
						else if (DELETE == optype)
								result = sl_finish_delete(key, node,
														node_val,
														ptst);
						else if (INSERT == optype)
								result = sl_finish_insert(key, val, node,
														node_val, next,
														ptst);
						if (-1 != result)
								break;
						continue;
				}
				node = next;
		}

		ptst_critical_exit(ptst);

		return result;
	}        
};