#pragma once

#include "durable_tools.h"

#if defined(USE_LIBVMMALLOC) && !defined(RECORDMANAGER_PMEM_SUPPORT)
	#include "persistent_alloc.h"
#endif

#if !defined(RECORDMANAGER_PMEM_SUPPORT) && defined(USE_MEMKIND)
    #include <memkind.h>

	#define PMEM_POOL_PATH "/mnt/pmem1_mount/"
	#define PMEM_POOL_SIZE 0 // 0 means the heap is not a fixed size and can grow as long as the temp file can grow
#endif

#define UINT_PTR uintptr_t
#define node_t Node<K,V>
#define PVOID void*
#define UNUSED __attribute__ ((unused))


#define NODE_NEXT(nodePtr)			(UNMARKED_PTR(nodePtr->next))


//----------------------------------------------
#define NODE_PADDING 1
#define CACHE_LINES_PER_NV_NODE 1 
#define CACHE_LINE_SIZE 64

static inline UINT_PTR unmarked_ptr(UINT_PTR p) {
	return(p & ~(UINT_PTR)0x01);
}

#define UNMARKED_PTR(p) (node_t*)unmarked_ptr((UINT_PTR) p)

static inline UINT_PTR marked_ptr(UINT_PTR p) {
	return (p | (UINT_PTR)0x01);
}

#define MARKED_PTR(p) (node_t*)marked_ptr((UINT_PTR) p)

static inline int ptr_is_marked(UINT_PTR p) {
	return (int)(p & (UINT_PTR)0x01);
}

#define PTR_IS_MARKED(p) ptr_is_marked((UINT_PTR) p)

static inline UINT_PTR unmarked_ptr_all(UINT_PTR p) {
    return(p & ~(UINT_PTR)0x07);
}

#define UNMARKED_PTR_ALL(p) (node_t*)unmarked_ptr_all((UINT_PTR) p)

//-------------------------------------------------
//-------------------------------------------------
//-------------------------------------------------
//-------------------------------------------------
//-------------------------------------------------


static inline UINT_PTR mark_ptr_cache(UINT_PTR p) {
	return (p | (UINT_PTR)0x04);
}

static inline UINT_PTR unmark_ptr_cache(UINT_PTR p) {
	return(p & ~(UINT_PTR)0x04);
}
static inline int is_marked_ptr_cache(UINT_PTR p) {
	return (int)(p & (UINT_PTR)0x04);
}

static inline void write_data_wait(void* addr, size_t sz) {
    UINT_PTR p;

    for (p = (UINT_PTR)addr & ~(CACHE_LINE_SIZE - 1); p < (UINT_PTR)addr + sz; p += CACHE_LINE_SIZE) {
        durableTools::CLFLUSH((void*)p);
    }
}

inline void flush_and_try_unflag(int tid, PVOID* target) {        
    PVOID value = *target;
    if (is_marked_ptr_cache((UINT_PTR)value)) {
        write_data_wait(target, 1);
        CASB((volatile PVOID*)target, value, (PVOID)unmark_ptr_cache((UINT_PTR)value));
        GSTATS_ADD(tid, num_contains_flushes, 1);
    }
}

inline void flush_and_try_unflag_search(int tid, PVOID* target) {        
    PVOID value = *target;
    if (is_marked_ptr_cache((UINT_PTR)value)) {
        write_data_wait(target, 1);
        CASB((volatile PVOID*)target, value, (PVOID)unmark_ptr_cache((UINT_PTR)value));
        GSTATS_ADD(tid, num_search_flushes, 1);
    }
}

//links a node and persists it
//marks the link while it is doing the persist
inline PVOID link_and_persist(int tid, PVOID* target, PVOID oldvalue, PVOID value) {        
    PVOID res;
    res = CASV(target, (PVOID) oldvalue, (PVOID)mark_ptr_cache((UINT_PTR)value));
    
    //if cas successful, we updated the link, but it still needs flushing
    if (res != oldvalue) {
        return res; //nothing gets fluhed
    }
    write_data_wait(target, 1);
    CASB((volatile PVOID*)target, (PVOID)mark_ptr_cache((UINT_PTR)value), (PVOID)value);
    GSTATS_ADD(tid, num_update_flushes, 1);
    return res;
}

inline PVOID link_and_persist_search(int tid, PVOID* target, PVOID oldvalue, PVOID value) {        
    PVOID res;
    res = CASV(target, (PVOID) oldvalue, (PVOID)mark_ptr_cache((UINT_PTR)value));
    
    //if cas successful, we updated the link, but it still needs flushing
    if (res != oldvalue) {
        return res; //nothing gets fluhed
    }
    write_data_wait(target, 1);
    CASB((volatile PVOID*)target, (PVOID)mark_ptr_cache((UINT_PTR)value), (PVOID)value);
    GSTATS_ADD(tid, num_search_flushes, 1);	
    return res;
}

template<typename K, typename V>
struct Node {
    K key;
    V value;
    volatile node_t* next;
    
    char padding[BYTES_IN_CACHE_LINE - sizeof(K) - sizeof(V) - sizeof(void*)];
};


template<typename RecordManager, typename K, typename V>
class LinkedList {
private:

#ifdef RECORDMANAGER_PMEM_SUPPORT
	RecordManager * recmgr;
#elif defined(USE_MEMKIND)
	struct memkind *pmem_kind = NULL;
#elif defined(USE_LIBVMMALLOC)
	AllocatorLibvmmalloc_Simple* alloc;
#endif

    PAD;
    volatile node_t* min;
    volatile node_t* max;
    PAD;
    V noVal;


    volatile node_t* new_node_and_set_next(int tid, K key, V value, volatile Node<K,V>* next) {

#ifdef RECORDMANAGER_PMEM_SUPPORT
        node_t* the_node = recmgr->template allocate<node_t>(tid);		                    
#elif defined(USE_MEMKIND) 
		node_t* the_node = (node_t*)memkind_malloc(pmem_kind, sizeof(node_t));        
#elif defined(USE_LIBVMMALLOC)
		node_t* the_node = (node_t*)alloc->allocate(sizeof(node_t));
#endif
        GSTATS_ADD(tid, num_pmem_alloc, 1); 

        
        the_node->key = key;
        the_node->value = value;
        next = (node_t*)unmark_ptr_cache((uintptr_t)(next));
        the_node->next = next;
        
        write_data_wait((void*)the_node, CACHE_LINES_PER_NV_NODE);
        
        //note this SFENCE is explicitly present in the original C version of the algorithm
        //from the authors
        durableTools::SFENCE();
        GSTATS_ADD(tid, num_update_flushes, 1);	 

        return the_node;
    }

    inline int delete_right(int tid, volatile node_t* left, volatile node_t* right) {
        volatile node_t* nnext = UNMARKED_PTR(right->next);
        nnext = (node_t*)unmark_ptr_cache((uintptr_t)(nnext));        
    
        node_t* res = (node_t*)link_and_persist(tid, (PVOID*)&(left->next), (PVOID)right, (PVOID)nnext);
        
        int success = (res == right);

        if (success) {
#ifdef RECORDMANAGER_PMEM_SUPPORT          
            recmgr->retire(tid, (node_t*)right);
#else
#endif
        }

        return success;
    }

    inline volatile node_t* search(int tid, K key, volatile node_t** left_ptr) {
        volatile node_t* left = min;
        volatile node_t* leftp = min;
        volatile node_t* right = (node_t*)unmark_ptr_cache((uintptr_t)min->next);
        while (1) {
            if (!PTR_IS_MARKED(right->next)) {
                if (right->key >= key) {
                    break;
                }
                leftp=left;
                left = right;
            }
            else {
                delete_right(tid, left, right);
            }
            right = UNMARKED_PTR(right->next);
            right = (volatile node_t*) unmark_ptr_cache((UINT_PTR)right);
        }
        *left_ptr = left;

        flush_and_try_unflag_search(tid, (PVOID*)&(leftp->next));

        return right;
    }

public:
    LinkedList(int numThreads, K minKey, K maxKey, V reservedVal) {
#ifdef	RECORDMANAGER_PMEM_SUPPORT	
		recmgr = new RecordManager(numThreads);

        //TODO:: fix RecordManager then remove this later
		Node<K,V> * temp = recmgr->template allocate<Node<K,V>>(tid);
		recmgr->deallocate(0, temp);
#elif defined(USE_MEMKIND)
		int err = memkind_create_pmem(PMEM_POOL_PATH, PMEM_POOL_SIZE, &pmem_kind);
		if (err) {			
			printf("Error creating PMEM pool. Exiting\n");
			// print_err_message(err);
			exit(-1);
		}
#elif defined(USE_LIBVMMALLOC)
		alloc = new AllocatorLibvmmalloc_Simple();
		alloc->init();
#endif

        max = new_node_and_set_next(0, maxKey, 0, NULL);
        min = new_node_and_set_next(0, minKey, 0, max);     

        noVal = reservedVal;
    }

    ~LinkedList(){

    }

    node_t* getHead() {
        return (node_t*)min;
    }

	void initThread(int tid){
#ifdef RECORDMANAGER_PMEM_SUPPORT	
		recmgr->initThread(tid);
#endif

	}

	void deinitThread(int tid){
#ifdef RECORDMANAGER_PMEM_SUPPORT	
		recmgr->deinitThread(tid);
#endif		
	}

    V contains(int tid, K key) {   
#ifdef RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif

        volatile node_t* prev = min;
        volatile node_t* node = (node_t*)unmark_ptr_cache((uintptr_t)min->next);
        
        while (node->key < key) {
            prev = node;
            node = UNMARKED_PTR(node->next);
            node= (volatile node_t*)unmark_ptr_cache((UINT_PTR)node);
        }

        if ((node->key == key) && (!PTR_IS_MARKED(node->next))) {
    
            flush_and_try_unflag(tid, (PVOID*)&(prev->next));
            flush_and_try_unflag(tid, (PVOID*)&(node->next));
        
            return node->value;
        }
    
        flush_and_try_unflag(tid, (PVOID*)&(prev->next));
        flush_and_try_unflag(tid, (PVOID*)&(node->next));

        return noVal;
    }


    V insertIfAbsent(int tid, K key, V val) {    
#ifdef RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif
            
        do {
            volatile node_t* left;
            volatile node_t* right = search(tid, key, &left);

            flush_and_try_unflag_search(tid, (PVOID*)&(left->next));

            if (right->key == key) {                
                return right->value;
            }

            volatile node_t* to_add = new_node_and_set_next(tid, key, val, right);  //we persist the newly allocated data in new_node (done so in the call); 

            if ((node_t*)link_and_persist(tid, (PVOID*)&(left->next), (PVOID)right, (PVOID)to_add) == right) {            
                return noVal;
            }

#ifdef RECORDMANAGER_PMEM_SUPPORT 
            recmgr->retire(tid, (node_t*)to_add);
#elif defined(USE_MEMKIND)
            memkind_free(pmem_kind, (void*)to_add);
#elif defined(USE_LIBVMMALLOC)
			alloc->deallocate((void*)to_add);
#endif

        } while (1);
    }

    V remove(int tid, K key) {
#ifdef RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif

        node_t* res = NULL;
        node_t* unmarked;
        volatile node_t* left;
        volatile node_t* right;
        
        do {
            right = search(tid, key, &left);
                flush_and_try_unflag_search(tid, (PVOID*)&(left->next));

            if (right->key != key) {                
                return noVal;
            }

            unmarked = UNMARKED_PTR(right->next);
            node_t* marked = MARKED_PTR(unmarked);     
        
            res = (node_t*)link_and_persist(tid, (PVOID*)&(right->next), unmarked, marked);
        } while (res != unmarked);

        V val = right->value;
        
        delete_right(tid, left, right);
        
        return val;
    }
};