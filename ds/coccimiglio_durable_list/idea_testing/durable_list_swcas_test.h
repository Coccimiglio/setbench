#pragma once

/**
 * THIS IS OLD AND WILL NOT WORK ANYMORE
 * 
 * 
 * 
 * This version of the no descriptors no-help list still uses a style similar to the Ellen BST
 * where for deletes you mark the parent of the node to be deleted and you mark
 * the node to be deleted. The other version only marks the node to be deleted which is maybe more 
 * like a harris version
 * 
*/

#include "durable_tools.h"
#include <atomic>
#include <stdio.h>

#define MAX_FLUSHED_NODES 100

#define NODE_NEXT(nodePtr)			(PTR_TO_NODE(nodePtr->next.load()))

#define CAS(ptr, oldVal, newVal)	__sync_bool_compare_and_swap(ptr, oldVal, newVal)
#define VCAS(ptr, oldVal, newVal)	__sync_val_compare_and_swap(ptr, oldVal, newVal)


#define DURABLE_MASK                (0x1ll) //001
//#define MARK_NODE_DURABLE(x)     	((Node<K,V> *)(((uintptr_t)(x)) | DURABLE_MASK))
#define MARK_NODE_DURABLE(x)     	((Node<K,V>*)(x))
#define IS_DURABLE(x)               (((uintptr_t)(x)) & DURABLE_MASK)


//Ignores durability bit
#define PTR_MASK					(0x6ll) //110

//clean = 00X
#define CLEAN_MASK					(0x0ll) //00X
#define IS_CLEAN(x)					((((uintptr_t)(x)) & PTR_MASK) == CLEAN_MASK)

//Dflag = 01X
#define DFLAG_MASK 					(0x2ll) //01X
#define IS_DFLAGGED(x)				((((uintptr_t)(x)) & PTR_MASK) == DFLAG_MASK)
#define MARK_NODE_DFLAG(x)			((Node<K,V> *)(((uintptr_t)(x)) | DFLAG_MASK))
//#define UNMARK_NODE_DFLAG(x)		((Node<K,V> *)(((uintptr_t)(x)) & ~DFLAG_MASK))

//IFlag = 10X
#define IFLAG_MASK 					(0x4ll) //10X
#define IS_IFLAGGED(x)				((((uintptr_t)(x)) & PTR_MASK) == IFLAG_MASK)
#define MARK_NODE_IFLAG(x)			((Node<K,V> *)(((uintptr_t)(x)) | IFLAG_MASK))

#define UNFLAG_NODE(x)				((Node<K,V> *)(((uintptr_t)(x)) & ~PTR_MASK))


//DMark = 11X						
#define DEL_MASK 					(0x6ll) //11X - same as PTR_MASK
// #define DEL_MASK 					(0x4ll) //10X 
#define IS_MARKED_FOR_DEL(x)		((((uintptr_t)(x)) & PTR_MASK) == DEL_MASK)
#define MARK_NODE_DEL(x)			((Node<K,V> *)(((uintptr_t)(x)) | DEL_MASK))

#define TOTAL_MASK					(0x7ll) //111
#define PTR_TO_NODE(x)				((Node<K,V> *)((uintptr_t)(x) & ~TOTAL_MASK))


template<typename K, typename V>
struct Node{
	
	std::atomic<Node<K,V>*> next;	

	K key;
	V value;	
};

template <typename K, typename V, class RecordManager>
class [[deprecated]] List {
private:
	struct searchResult{	
		Node<K,V> * gp;	
		Node<K,V> * parent;
		Node<K,V> * curr;
	};

	PAD;
	Node<K,V> * head;
	Node<K,V> * tail;
	PAD;
	
	RecordManager * recmgr;
	V noVal;	
	PAD;

	Node<K,V> * allocNewNode(int tid, K key, V val, Node<K,V> *next){
		Node<K, V> * newNode = recmgr->template allocate<Node<K, V>>(tid);
		
		newNode->key = key;
		newNode->value = val;

		newNode->next.store(next);

					
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif

		GSTATS_ADD(tid, num_update_flushes, 1);
		

		return newNode;		
	}



	/**
	 * Since deletes need the parent when helping we need to return the grandparent in this seach along with
	 * the parent and curr. This is because we help on the parent
	*/
	searchResult search(K key){
		Node<K,V>* gp;
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;

		gp = NULL;
		parent = head;		
		parentNextPtr = parent->next.load();
		curr = PTR_TO_NODE(parentNextPtr);		
		
		

		while (true) {
			if (curr->key >= key) {
				break;
			}
			
			gp = parent;
			parent = curr;						
			parentNextPtr = parent->next.load();			
			curr = PTR_TO_NODE(parentNextPtr);								
		}
		
		return {gp, parent, curr};
	}



	void helpUpdate(int tid, Node<K,V>* parent, Node<K,V>* dirtyNode) {
		// assert(PTR_TO_NODE(parent) == parent);
		// assert(PTR_TO_NODE(dirtyNode) == dirtyNode);

		Node<K,V>* dirtyNodeNextPtr = dirtyNode->next.load();

		if (IS_DFLAGGED(dirtyNodeNextPtr)) {
			helpRemove(tid, dirtyNode, PTR_TO_NODE(dirtyNodeNextPtr));
		}
		else if (IS_MARKED_FOR_DEL(dirtyNodeNextPtr)) {
			helpMarked(tid, parent, dirtyNode);
		}
	}


	/**
	 * Performs the CAS that marks a node for deletion or if this CAS fails then the DFLAGGED
	 * 		parent is CAS'd back out of the DFLAGGED state
	*/
	void helpRemove(int tid, Node<K,V>* parent, Node<K,V>* nodeToDel) {
		//assert(PTR_TO_NODE(parent) == parent && IS_CLEAN(parent));
		//assert(PTR_TO_NODE(nodeToDel) == nodeToDel && IS_CLEAN(nodeToDel));

		while (PTR_TO_NODE(parent->next.load()) == nodeToDel) {
			Node<K,V>* nodeToDelSucc = PTR_TO_NODE(nodeToDel->next.load());	 

			Node<K,V>* expNext = nodeToDelSucc;		
			Node<K,V>* markedNode = MARK_NODE_DEL(nodeToDelSucc);		
					
			if (nodeToDel->next.compare_exchange_strong(expNext, markedNode)) {			
				this->helpMarked(tid, parent, nodeToDel);
				return;
			}
			else {
				if (IS_MARKED_FOR_DEL(expNext)){				
					this->helpMarked(tid, parent, nodeToDel);		
					return;
				}
				else if (IS_DFLAGGED(expNext)){
					helpRemove(tid, parent, nodeToDel);
				}
				else {
					//we failed bc nodeToDel->next was modified by an insert or a completed remove
				}	
			}
		}			
	}



	/**
	 * Performs the actual unlinking of a node marked for deletion
	 * 
	 * IMPORTANT_NOTE: the parent that we pass to this function is the gp returned from search
	 * 
	*/
	void helpMarked(int tid, Node<K,V>* parent, Node<K,V>* markedNode) {
		// assert(PTR_TO_NODE(parent) == parent && IS_CLEAN(parent));
		// assert(PTR_TO_NODE(markedNode) == markedNode && IS_CLEAN(markedNode));
					
		Node<K,V>* markedNodeSucc = PTR_TO_NODE(markedNode->next.load());

		Node<K,V>* expNext = MARK_NODE_DFLAG(markedNode);		

		if (parent->next.compare_exchange_strong(expNext, markedNodeSucc)){	
			recmgr->retire(tid, markedNode);
		}
	}


public:
	List(int numThreads, K minKey, K maxKey, V reservedVal){		
		recmgr = new RecordManager(numThreads);

		noVal = reservedVal;

		tail = new Node<K,V>();
		tail->key = maxKey;
		tail->value = 0;
		tail->next.store(NULL);

		head = new Node<K,V>();
		head->key = minKey;
		head->value = 0;
		head->next.store(MARK_NODE_DURABLE(tail));
	}

	~List(){
	}

	Node<K,V> * getHead(){
		return head;
	}

	void initThread(int tid){
		recmgr->initThread(tid);
	}

	void deinitThread(int tid){
		recmgr->deinitThread(tid);
	}

	bool containsPersistAll(int tid, K key) {				
		auto guard = recmgr->getGuard(tid);

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;

		parent = head;		
		curr = PTR_TO_NODE(parent->next.load());
		
		
		

		while (true) {
// 			if (!IS_DURABLE(parentNextPtr)) {			
// 				durableTools::FLUSH(&parent->nextPair);
// #if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
// 				durableTools::SFENCE();							
// #endif			
// 				parentNextPtr = MARK_NODE_DURABLE(parentNextPtr);
// 				parent->nextPair.compare_exchange_strong(parentNextPair, NodeNext<Node<K,V>>({parentNextPtr, NULL}));
				
// 				GSTATS_ADD(tid, num_contains_flushes, 1);								
// 			}

			if (curr->key >= key) {
				break;
			}
			
			
			parent = curr;						
			curr = PTR_TO_NODE(parent->next.load());								
		}
		
	
		return curr->key == key;
	}


	/**
	 *  
	*/
	V insertIfAbsent(int tid, K key, V value){
		auto guard = recmgr->getGuard(tid);

		//retry loop
		while (true){
			searchResult result = search(key);					
			Node<K,V>* gp = result.gp;
			Node<K,V>* parent = result.parent;					
			Node<K,V>* curr = result.curr;
			
			Node<K,V>* parentNextPtr = parent->next.load();			
			
			// assert(PTR_TO_NODE(curr) == curr);
			// assert(PTR_TO_NODE(parent) == parent);
			// assert(PTR_TO_NODE(gp) == gp);

			if (curr->key == key) {
				return curr->value;
			}			
			
			if (!IS_CLEAN(parentNextPtr)) { //parent is clean?
				helpUpdate(tid, gp, parent);
			}
			else {
				Node<K,V>* newNode = allocNewNode(tid, key, value, curr);
				
				Node<K,V>* expNext = curr;
				Node<K,V>* newNext = newNode;

				if (parent->next.compare_exchange_strong(expNext, newNext)){		
					return noVal;
				}
				else {
#if defined DEBUG_FAILED_CAS								
					GSTATS_ADD(tid, num_failed_insert_cas, 1);	
#endif					
					recmgr->deallocate(tid, newNode);					
				}
			}
		} //end retry loop
	}





	/**
	 *  
	*/
	V remove(int tid, K key){
		auto guard = recmgr->getGuard(tid);

		V retVal;

		//retry loop
		while (true){
			searchResult result = search(key);		
			Node<K,V>* gp = result.gp;
			Node<K,V>* parent = result.parent;						
			Node<K,V>* curr = result.curr;			

			Node<K,V>* parentNextPtr = parent->next.load();
			Node<K,V>* currNextPtr = curr->next.load();

			// assert(PTR_TO_NODE(curr) == curr);
			// assert(PTR_TO_NODE(parent) == parent);
			// assert(PTR_TO_NODE(gp) == gp);

			if (curr->key != key) {
				return noVal;
			}

			retVal = curr->value;
						

			if (!IS_CLEAN(currNextPtr)) {
				helpUpdate(tid, parent, curr); //curr clean?
			}
			else if (!IS_CLEAN(parentNextPtr)) { //parent is clean?
				helpUpdate(tid, gp, parent);
			}
			else {
				Node<K,V>* flaggedCurr = MARK_NODE_DFLAG(curr);
				Node<K,V>* expNext = curr;					

				if (parent->next.compare_exchange_strong(expNext, flaggedCurr)){
					helpRemove(tid, parent, curr);						
					return retVal;
				}
				else {					
#if defined DEBUG_FAILED_CAS								
					GSTATS_ADD(tid, num_failed_flag_cas, 1);	
#endif															
				}
			}
		} //end retry loop
	}

	void printSummary(){		
		printf("------- SUMMARY -------\n");

		Node<K,V>* curr = PTR_TO_NODE(head->next.load());
		printf("Head: %p, nextPtr: %p, key: %d\n", head, head->next.load(), head->key);
		while (curr != tail){
			printf("Node: %p, nextPtr: %p, key: %d\n", curr, curr->next.load(), curr->key);
			curr = PTR_TO_NODE(curr->next.load());
		}
		printf("Tail: %p, nextPtr: %p, key: %d\n", tail, tail->next.load(), tail->key);
		printf("------- END-SUMMARY -------\n\n");
	}
};
