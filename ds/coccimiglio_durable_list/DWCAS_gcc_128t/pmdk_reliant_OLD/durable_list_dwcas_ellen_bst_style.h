// #pragma once

// /**
//  * updates perform minimal flushing in this version
//  * 
//  * 
//  * upper bound fence complexity:
//  * - inserts = 3
//  * - removes = 3
//  * 
//  * lower bound fence complexity = 1
//  * 
//  * NOTE THIS REQUIRES THAT YOU USE ASYNCH FLUSH ie clflushopt or clwb
// */

// #include "durable_tools.h"
// #include <libpmemobj.h>

// typedef unsigned __int128 uint128_t;

// #define CAS(ptr, oldVal, newVal)	__sync_bool_compare_and_swap(ptr, oldVal, newVal)
// #define VCAS(ptr, oldVal, newVal)	__sync_val_compare_and_swap(ptr, oldVal, newVal)


// #define MAX_FLUSHED_NODES 1000

// #define NODE_NEXT(nodePtr)			(PTR_TO_NODE((Node<K,V>*)nodePtr->nextPair))
// #define NODE_NEXT_PTR(nodePtr)		((Node<K,V>*)nodePtr->nextPair)

// #define DURABLE_MASK                (0x1ll) //001
// #define MARK_NODE_DURABLE(x)     	((Node<K,V> *)(((uintptr_t)(x)) | DURABLE_MASK))
// #define IS_DURABLE(x)               (((uintptr_t)(x)) & DURABLE_MASK)


// //Ignores durability bit
// #define PTR_MASK					(0x6ll) //110

// //clean = 00X
// #define CLEAN_MASK					(0x0ll) //00X
// #define IS_CLEAN(x)					((((uintptr_t)(x)) & PTR_MASK) == CLEAN_MASK)

// //Dflag = 01X
// #define DFLAG_MASK 					(0x2ll) //01X
// #define IS_DFLAGGED(x)				((((uintptr_t)(x)) & PTR_MASK) == DFLAG_MASK)
// #define MARK_NODE_DFLAG(x)			((Node<K,V> *)(((uintptr_t)(x)) | DFLAG_MASK))

// //IFlag = 10X
// #define IFLAG_MASK 					(0x4ll) //10X
// #define IS_IFLAGGED(x)				((((uintptr_t)(x)) & PTR_MASK) == IFLAG_MASK)
// #define MARK_NODE_IFLAG(x)			((Node<K,V> *)(((uintptr_t)(x)) | IFLAG_MASK))

// #define UNFLAG_NODE(x)				((Node<K,V> *)(((uintptr_t)(x)) & ~PTR_MASK))

// //DMark = 11X						
// #define DEL_MASK 					(0x6ll) //11X - same as PTR_MASK
// #define IS_MARKED_FOR_DEL(x)		((((uintptr_t)(x)) & PTR_MASK) == DEL_MASK)
// #define MARK_NODE_DEL(x)			((Node<K,V> *)(((uintptr_t)(x)) | DEL_MASK))

// #define TOTAL_MASK					(0x7ll) //111
// #define PTR_TO_NODE(x)				((Node<K,V> *)((uintptr_t)(x) & ~TOTAL_MASK))



// #define MARK_OLD_IFLAG(x)			((((uintptr_t)(x)) | IFLAG_MASK))


// template<typename K, typename V>
// struct Node{
// 	uint128_t volatile nextPair;	

// 	K key;
// 	V value;	
// };

// template <typename K, typename V, class RecordManager>
// class List {
// private:
// 	struct searchResult{			
// 		Node<K,V> * gp;	
// 		Node<K,V> * parent;
// 		Node<K,V> * curr;
// 	};

// 	struct flushedNodeRecord {
// 		Node<K,V>* node;
// 		uint128_t nodeNext;		
// 	};


// 	PAD;	
// 	Node<K,V> * head;
// 	Node<K,V> * tail;
// 	PAD;
	
// 	PMEMobjpool * pop;
// 	//RecordManager * recmgr;
// 	V noVal;	
// 	PAD;
// 	// const int maxAsynchFlush;
// 	// PAD;

// 	Node<K,V> * allocNewNode(int tid, K key, V val, Node<K,V> *next){
// 		//Node<K, V> * newNode = recmgr->template allocate<Node<K, V>>(tid);
		
// 		newNode->key = key;
// 		newNode->value = val;

// 		newNode->nextPair = (uint128_t)next;

					
// 		durableTools::FLUSH(&newNode);	
// 		// durableTools::FLUSH(&newNode->key);			
// 		// durableTools::FLUSH(&newNode->value);
// 		// durableTools::FLUSH(&newNode->nextPair);				

// #if (defined(USE_CLFLUSHOPT) || defined(USE_CLWB)) && !defined(ONE_FLUSH_INSERT)			
// 		durableTools::SFENCE();
// 		GSTATS_ADD(tid, num_update_flushes, 1);									
// #endif
		

// 		return newNode;		
// 	}

// inline void persist(Node<K,V>* node, Node<K,V>* next){
// 		durableTools::FLUSH(&node->nextPair);
// #if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
// 		durableTools::SFENCE();							
// #endif	
// 		Node<K,V> *old = *(Node<K,V> **)((uintptr_t)node + 8);
// 		uint128_t exp = ((uint128_t)(old) << 64) + ((uint128_t)next);
// 		next = MARK_NODE_DURABLE(next);

// 		CAS(&node->nextPair, exp, (uint128_t)next);
// 	}

// 	inline void persist(Node<K,V>* node, Node<K,V>* next, Node<K,V>* old){
// 		durableTools::FLUSH(&node->nextPair);
// #if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
// 		durableTools::SFENCE();							
// #endif	
		
// 		uint128_t exp = ((uint128_t)(old) << 64) + ((uint128_t)next);

// 		next = MARK_NODE_DURABLE(next);
// 		CAS(&node->nextPair, exp, (uint128_t)next);
// 	}


// 	inline void persist(Node<K,V>* node, Node<K,V>* next, uint128_t exp){
// 		durableTools::FLUSH(&node->nextPair);
// #if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
// 		durableTools::SFENCE();							
// #endif	
// 		next = MARK_NODE_DURABLE(next);
// 		CAS(&node->nextPair, exp, (uint128_t)next);
// 	}




// 	/**
// 	 * Since deletes need the parent when helping we need to return the grandparent in this seach along with
// 	 * the parent and curr. This is because we help on the parent
// 	*/
// 	searchResult search(K key){		
// 		Node<K,V>* gp;
// 		Node<K,V>* parent;
// 		Node<K,V>* curr;	
// 		Node<K,V>* parentNextPtr;

// 		gp = NULL;
// 		parent = head;
// 		parentNextPtr = *(Node<K,V>**)parent;
// 		curr = PTR_TO_NODE(parentNextPtr);		
		
		

// 		while (true) {		
// 			if (curr->key >= key) {
// 				break;
// 			}
			
// 			gp = parent;
// 			parent = curr;						
// 			parentNextPtr = *(Node<K,V>**)parent;
// 			curr = PTR_TO_NODE(parentNextPtr);								
// 		}


// 		//confirm durability of parent via gp->next
// 		if (gp) {
// 			Node<K,V>* gpNextPtr = *(Node<K,V>**)gp;
// 			if (!IS_DURABLE(gpNextPtr)) {
// 				persist(gp, gpNextPtr);			
// 				GSTATS_ADD(tid, num_search_flushes, 1);				
// 			}
// 		}

// 		//confirm durability of curr via parent->next
// 		if (!IS_DURABLE(parentNextPtr)) {	
// 			persist(parent, parentNextPtr);	
// 			GSTATS_ADD(tid, num_search_flushes, 1);				
// 		}
		
// 		return {gp, parent, curr};
// 	}



// 	void helpUpdate(int tid, Node<K,V>* parent, Node<K,V>* dirtyNode) {
// 		// assert(PTR_TO_NODE(parent) == parent);
// 		// assert(PTR_TO_NODE(dirtyNode) == dirtyNode);

// 		Node<K,V>* dirtyNodeNextPtr = *(Node<K,V>**)dirtyNode;

// 		if (IS_DFLAGGED(dirtyNodeNextPtr)) {
// 			helpRemove_FromSearch(tid, dirtyNode, PTR_TO_NODE(dirtyNodeNextPtr));
// 		}
// 		else if (IS_MARKED_FOR_DEL(dirtyNodeNextPtr)) {
// 			helpMarked_FromSearch(tid, parent, dirtyNode);
// 		}
// 	}


// 	void helpRemove(int tid, Node<K,V>* parent, Node<K,V>* nodeToDel) {
// 		while (PTR_TO_NODE(*(Node<K,V>**)parent) == nodeToDel) {
// 			Node<K,V>* nodeToDelNextPtr = *(Node<K,V>**)nodeToDel;
// 			Node<K,V>* nodeToDelSucc = PTR_TO_NODE(nodeToDelNextPtr);	 
			
// 			if (!IS_DURABLE(nodeToDelNextPtr)) {
// 				persist(nodeToDel, nodeToDelNextPtr);
// 				GSTATS_ADD(tid, num_search_flushes, 1);			
// 			}



// 			uint128_t expNext = (uint128_t)(MARK_NODE_DURABLE(nodeToDelSucc));		
// 			uint128_t newNext = (uint128_t)(MARK_NODE_DURABLE(MARK_NODE_DEL(nodeToDelSucc)));		
					
// 			uint128_t result = VCAS(&nodeToDel->nextPair, expNext, newNext);
			
// 			//if (CAS(&nodeToDel->nextPair, expNext, newNext)) {			
// 			if (result == expNext || IS_MARKED_FOR_DEL(result)) {			
// 				this->helpMarked(tid, parent, nodeToDel);
// 				return;
// 			}
// 			else {
// 				if (IS_DFLAGGED(result)){
// 					helpRemove(tid, nodeToDel, PTR_TO_NODE(result));
// 				}				
// 				//else we failed bc nodeToDel->next was modified by an insert or a helper completed remove					
// 			}
// 		}			
// 	}

// 	void helpRemove_FromSearch(int tid, Node<K,V>* parent, Node<K,V>* nodeToDel) {
// 		while (PTR_TO_NODE(*(Node<K,V>**)parent) == nodeToDel) {
			
// 			Node<K,V>* nodeToDelNextPtr = *(Node<K,V>**)nodeToDel;
// 			Node<K,V>* nodeToDelSucc = PTR_TO_NODE(nodeToDelNextPtr);	 
			
// 			if (!IS_DURABLE(nodeToDelNextPtr)) {
// 				persist(nodeToDel, nodeToDelNextPtr);
// 				GSTATS_ADD(tid, num_search_flushes, 1);			
// 			}



// 			uint128_t expNext = (uint128_t)(MARK_NODE_DURABLE(nodeToDelSucc));		
// 			uint128_t newNext = (uint128_t)(MARK_NODE_DURABLE(MARK_NODE_DEL(nodeToDelSucc)));		
					
// 			uint128_t result = VCAS(&nodeToDel->nextPair, expNext, newNext);
			
// 			//if (CAS(&nodeToDel->nextPair, expNext, newNext)) {			
// 			if (result == expNext || IS_MARKED_FOR_DEL(result)) {			
// 				this->helpMarked(tid, parent, nodeToDel);
// 				return;
// 			}
// 			else {
// 				if (IS_DFLAGGED(result)){
// 					helpRemove_FromSearch(tid, nodeToDel, PTR_TO_NODE(result));
// 				}				
// 				//else we failed bc nodeToDel->next was modified by an insert or a helper completed remove					
// 			}
// 		}			
// 	}

// 	void helpMarked(int tid, Node<K,V>* parent, Node<K,V>* markedNode) {
// 		Node<K,V>* markedNodeNextPtr = *(Node<K,V>**)markedNode;
// 		Node<K,V>* markedNodeSucc = PTR_TO_NODE(markedNodeNextPtr);	

// 		uint128_t expNext = (uint128_t)(MARK_NODE_DURABLE(MARK_NODE_DFLAG(markedNode)));		
// 		uint128_t newNext = (expNext << 64) + ((uint128_t)markedNodeSucc);		


// 		if (CAS(&parent->nextPair, expNext, newNext)){
// 			persist(parent, markedNodeSucc, newNext);
// 			GSTATS_ADD(tid, num_update_flushes, 1);									
// 			//recmgr->retire(tid, markedNode);
// 		}
// 	}

// 	void helpMarked_FromSearch(int tid, Node<K,V>* parent, Node<K,V>* markedNode) {
// 		Node<K,V>* markedNodeNextPtr = *(Node<K,V>**)markedNode;
// 		Node<K,V>* markedNodeSucc = PTR_TO_NODE(markedNodeNextPtr);	

// 		uint128_t expNext = (uint128_t)(MARK_NODE_DURABLE(MARK_NODE_DFLAG(markedNode)));		
// 		uint128_t newNext = (expNext << 64) + ((uint128_t)markedNodeSucc);		


// 		if (CAS(&parent->nextPair, expNext, newNext)){
// 			persist(parent, markedNodeSucc, newNext);
// 			GSTATS_ADD(tid, num_search_flushes, 1);									
// 			//recmgr->retire(tid, markedNode);
// 		}
// 	}

// 	void makeNode() {

// 	}

// public:
// 	List(int numThreads, K minKey, K maxKey, V reservedVal, PMEMobjpool* pool){		
// 		init(numThreads, minKey, maxKey, reservedVal, pool);
// 	}

// 	List() {}

// 	~List(){
// 	}

// 	void init(int numThreads, K minKey, K maxKey, V reservedVal, PMEMobjpool* pool){
// 		pop = pool;
// 		//recmgr = new RecordManager(numThreads);

// 		pmemobj_alloc(pop, NULL, sizeof(Node<K,V>), makeNode, NULL);		

// 		noVal = reservedVal;

// 		tail = new Node<K,V>();
// 		tail->key = maxKey;
// 		tail->value = 0;
// 		tail->nextPair = 0;

// 		head = new Node<K,V>();
// 		head->key = minKey;
// 		head->value = 0;
// 		head->nextPair = (uint128_t)(MARK_NODE_DURABLE(tail));		
// 	}

// 	Node<K,V> * getHead(){
// 		return head;
// 	}

// 	void initThread(int tid){
// 		//recmgr->initThread(tid);
// 	}

// 	void deinitThread(int tid){
// 		//recmgr->deinitThread(tid);
// 	}

// 	bool containsPersistAll(int tid, K key) {				
// 		//auto guard = recmgr->getGuard(tid);

// 		Node<K,V>* parent;
// 		Node<K,V>* curr;	
// 		Node<K,V>* parentNextPtr;

// 		parent = head;
// 		parentNextPtr = *(Node<K,V>**)parent;
// 		curr = PTR_TO_NODE(parentNextPtr);		
		
		

// 		while (true) {
// 			if (!IS_DURABLE(parentNextPtr)) {	
// 				persist(parent, parentNextPtr);
// 				GSTATS_ADD(tid, num_contains_flushes, 1);				
// 			}

// 			if (curr->key >= key) {
// 				break;
// 			}
			
// 			parent = curr;									
// 			parentNextPtr = *(Node<K,V>**)parent;
// 			curr = PTR_TO_NODE(parentNextPtr);								
// 		}
		
	
// 		return curr->key == key;
// 	}

// 	bool containsAsynchPersistAll(int tid, K key) {
// 		//auto guard = recmgr->getGuard(tid);

// 		Node<K,V>* parent;
// 		Node<K,V>* curr;	
// 		Node<K,V>* parentNextPtr;

// 		parent = head;

// 		uint128_t parentNextPair = parent->nextPair;

// 		parentNextPtr = (Node<K,V>*)parentNextPair;
// 		curr = PTR_TO_NODE(parentNextPtr);		
		
// 		flushedNodeRecord flushedNodes[MAX_FLUSHED_NODES];
// 		int flushedNodeCount = 0;

// 		while (true) {
// 			if (!IS_DURABLE(parentNextPtr)) {			
// 				if (flushedNodeCount >= MAX_FLUSHED_NODES) {
// 					setbench_error("Set a higher max flushed nodes or implement a less lazy solution Guy.");
// 				}

// 				flushedNodes[flushedNodeCount] = {parent, parentNextPair};
// 				flushedNodeCount++;				
// 				durableTools::FLUSH(&parent->nextPair);							
// 			}

// 			if (curr->key >= key) {
// 				break;
// 			}
			
// 			parent = curr;						
// 			parentNextPair = parent->nextPair;
// 			parentNextPtr = (Node<K,V>*)parentNextPair;
// 			curr = PTR_TO_NODE(parentNextPtr);								
// 		}
		
		
// 		if (flushedNodeCount > 0) {
// #if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
// 			durableTools::SFENCE();							
// #endif
// 			GSTATS_ADD(tid, num_contains_flushes, 1);
		

// 			for (int i = 0; i < flushedNodeCount; i++){
// 				if (flushedNodes[i].node->nextPair == flushedNodes[i].nodeNext){
// 					Node<K,V>* durableNode = MARK_NODE_DURABLE((Node<K,V>*)flushedNodes[i].nodeNext);
					
// 					CAS(&flushedNodes[i].node->nextPair, flushedNodes[i].nodeNext, (uint128_t)durableNode);
// 				}
// 			}
// 		}

// 		return curr->key == key;
// 	}

// 	bool containsPersistLast(int tid, K key) {
// 		//auto guard = recmgr->getGuard(tid);
		
// 		Node<K,V>* parent;
// 		Node<K,V>* curr;	
// 		Node<K,V>* parentNextPtr;
		
// 		parent = head;
// 		parentNextPtr = *(Node<K,V>**)parent;
// 		curr = PTR_TO_NODE(parentNextPtr);		
		
// 		while (true) {
// 			if (curr->key >= key) {
// 				break;
// 			}
			
// 			parent = curr;									
// 			parentNextPtr = *(Node<K,V>**)parent;
// 			curr = PTR_TO_NODE(parentNextPtr);								
// 		}

// 		if (!IS_DURABLE(parentNextPtr) && *(Node<K,V>**)parent == parentNextPtr) {	
// 			persist(parent, parentNextPtr);
// 			GSTATS_ADD(tid, num_contains_flushes, 1);				
// 		}
		
	
// 		return curr->key == key;
// 	}

// 	bool containsNaivePersistAlways(int tid, K key) {		
// 		//auto guard = recmgr->getGuard(tid);
		
// 		Node<K,V>* parent;
// 		Node<K,V>* curr;	
// 		Node<K,V>* parentNextPtr;
		
// 		parent = head;
// 		parentNextPtr = *(Node<K,V> **)(parent);
// 		curr = PTR_TO_NODE(parentNextPtr);		
		
		

// 		while (true) {	
// 			durableTools::FLUSH(&parent->nextPair);
// #if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
// 			durableTools::SFENCE();							
// #endif			

// 			if (curr->key >= key) {
// 				break;
// 			}
						
// 			parent = curr;						
// 			parentNextPtr = *(Node<K,V> **)(parent);
// 			curr = PTR_TO_NODE(parentNextPtr);								
// 		}
		
	
// 		return curr->key == key;
// 	}

// 	bool containsNoFlush(int tid, K key) {
// 		//auto guard = recmgr->getGuard(tid);
		
// 		Node<K,V>* parent;
// 		Node<K,V>* curr;	
// 		Node<K,V>* parentNextPtr;
		
// 		parent = head;

// 		//(IMPORTANT): need extra read ordering to ensure that we dont have a change to one
// 		//field before the other field is read
// 		// NOTE: reading uint128_t (16 bytes) is compiled to 2 mov instructions ie. 2 reads
// 		parentNextPtr = *(Node<K,V> **)(parent);
// 		curr = PTR_TO_NODE(parentNextPtr);		
		
// 		while (true) {
// 			if (curr->key >= key) {
// 				break;
// 			}
			
// 			parent = curr;									
// 			parentNextPtr = *(Node<K,V> **)(parent);
// 			curr = PTR_TO_NODE(parentNextPtr);								
// 		}


// 		if (IS_DURABLE(parentNextPtr)) {//durable
// 			return curr->key == key;				
// 		}
// 		else { //not durable 
// 			Node<K,V>* oldNode1 = *(Node<K,V> **)((uintptr_t)parent + 8);			
// 			Node<K,V>* parentNextPtr2 =*(Node<K,V> **)(parent);
// 			Node<K,V>* oldNode2 = *(Node<K,V> **)((uintptr_t)parent + 8);
							
// 			if ((parentNextPtr != parentNextPtr2) || (oldNode1 != oldNode2) || (oldNode1 == oldNode2 && !oldNode1)){
// 				/*
// 				* next ptr changed - the original ptr we read was durable at some time OR
// 				* old ptr changed - the original ptr we read was durable at some time
// 				*/
// 				return curr->key == key;
// 			}			
// 			else {
// 				//old ptr and next ptr didnt change - this means oldNode1 is the old node we want
// 				if (IS_IFLAGGED(oldNode1)) {
// 					return false; //even if curr contains the key the insert is not durable
// 				}
// 				else {
// 					//not durable because of a remove
// 					if (curr->key == key) { 
// 						//we can return true in this case because either the remove will be in NVM or it wont
// 						//in the case where the remove is persisted curr is in NVM since the link we followed
// 						//will be persisted
// 						//in the case where the remove is NOT persisted, then deletedNode->next must be durable, 
// 						//and must point to curr since we cannot mark a next ptr that is not durable
// 						return true;
// 					}
// 					else {
// 						return PTR_TO_NODE(oldNode1)->key == key;
// 					}
// 				}
// 			}			
// 		}
// 	}

// 	V insertIfAbsent(int tid, K key, V value){
// 		//auto guard = recmgr->getGuard(tid);

// 		//retry loop
// 		while (true){
// 			searchResult result = search(key);					
// 			Node<K,V>* gp = result.gp;
// 			Node<K,V>* parent = result.parent;					
// 			Node<K,V>* curr = result.curr;
			
// 			Node<K,V>* parentNextPtr = *(Node<K,V>**)parent;			

// 			if (curr->key == key) {
// 				return curr->value;
// 			}			
			
// 			if (!IS_CLEAN(parentNextPtr)) { //parent is clean?
// 				helpUpdate(tid, gp, parent);
// 			}
// 			else {
// 				Node<K,V>* newNode = allocNewNode(tid, key, value, MARK_NODE_DURABLE(curr));
				
// 				uint128_t expNext = (uint128_t)(MARK_NODE_DURABLE(curr));
// #ifdef ONE_FLUSH_INSERT				
// 				uint128_t newNext = ((uint128_t)(MARK_OLD_IFLAG(curr)) << 64) + ((uint128_t)newNode);
// #else
// 				uint128_t newNext = (uint128_t)newNode;
// #endif


// 				if (CAS(&parent->nextPair, expNext, newNext)){
// 					persist(parent, newNode, newNext);								
// 					GSTATS_ADD(tid, num_update_flushes, 1);									
// 					return noVal;
// 				}
// 				else {
// 					//recmgr->deallocate(tid, newNode);					
// 				}
// 			}
// 		} //end retry loop
// 	}


// 	V remove(int tid, K key){
// 		//auto guard = recmgr->getGuard(tid);

// 		V retVal;

// 		//retry loop
// 		while (true){
// 			searchResult result = search(key);		
// 			Node<K,V>* gp = result.gp;
// 			Node<K,V>* parent = result.parent;						
// 			Node<K,V>* curr = result.curr;			

// 			Node<K,V>* parentNextPtr = *(Node<K,V>**)parent;
// 			Node<K,V>* currNextPtr = *(Node<K,V>**)curr;

// 			if (curr->key != key) {
// 				return noVal;
// 			}

// 			retVal = curr->value;
						

// 			if (!IS_CLEAN(currNextPtr)) {
// 				helpUpdate(tid, parent, curr); //curr clean?
// 			}
// 			else if (!IS_CLEAN(parentNextPtr)) { //parent is clean?
// 				helpUpdate(tid, gp, parent);
// 			}
// 			else {
// 				uint128_t expNext = (uint128_t)(MARK_NODE_DURABLE(curr));				
// 				uint128_t newNext = (uint128_t)(MARK_NODE_DURABLE(MARK_NODE_DFLAG(curr)));				

// 				if (CAS(&parent->nextPair, expNext, newNext)){
// 					helpRemove(tid, parent, curr);						
// 					return retVal;
// 				}
// 			}
// 		} //end retry loop
// 	}

// 	void printSummary(){		
// 		// printf("------- SUMMARY -------\n");

// 		// Node<K,V>* curr = PTR_TO_NODE(head->nextPair.load().next);
// 		// printf("Head: %p, nextPtr: %p, key: %d\n", head, head->nextPair.load().next, head->key);
// 		// while (curr != tail){
// 		// 	printf("Node: %p, nextPtr: %p, key: %d\n", curr, curr->nextPair.load().next, curr->key);
// 		// 	curr = PTR_TO_NODE(curr->nextPair.load().next);
// 		// }
// 		// printf("Tail: %p, nextPtr: %p, key: %d\n", tail, tail->nextPair.load().next, tail->key);
// 		// printf("------- END-SUMMARY -------\n\n");
// 	}
// };
