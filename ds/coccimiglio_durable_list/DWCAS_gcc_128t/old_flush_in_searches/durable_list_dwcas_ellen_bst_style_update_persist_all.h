#pragma once

/**
 * 
 * 
*/

#include "durable_tools.h"

typedef unsigned __int128 uint128_t;

#define CAS(ptr, oldVal, newVal)	__sync_bool_compare_and_swap(ptr, oldVal, newVal)
#define VCAS(ptr, oldVal, newVal)	__sync_val_compare_and_swap(ptr, oldVal, newVal)


#define MAX_FLUSHED_NODES 100

#define NODE_NEXT(nodePtr)			(PTR_TO_NODE((Node<K,V>*)nodePtr->nextPair))

#define DURABLE_MASK                (0x1ll) //001
#define MARK_NODE_DURABLE(x)     	((Node<K,V> *)(((uintptr_t)(x)) | DURABLE_MASK))
#define IS_DURABLE(x)               (((uintptr_t)(x)) & DURABLE_MASK)


//Ignores durability bit
#define PTR_MASK					(0x6ll) //110

//clean = 00X
#define CLEAN_MASK					(0x0ll) //00X
#define IS_CLEAN(x)					((((uintptr_t)(x)) & PTR_MASK) == CLEAN_MASK)

//Dflag = 01X
#define DFLAG_MASK 					(0x2ll) //01X
#define IS_DFLAGGED(x)				((((uintptr_t)(x)) & PTR_MASK) == DFLAG_MASK)
#define MARK_NODE_DFLAG(x)			((Node<K,V> *)(((uintptr_t)(x)) | DFLAG_MASK))

//IFlag = 10X
#define IFLAG_MASK 					(0x4ll) //10X
#define IS_IFLAGGED(x)				((((uintptr_t)(x)) & PTR_MASK) == IFLAG_MASK)
#define MARK_NODE_IFLAG(x)			((Node<K,V> *)(((uintptr_t)(x)) | IFLAG_MASK))

#define UNFLAG_NODE(x)				((Node<K,V> *)(((uintptr_t)(x)) & ~PTR_MASK))

//DMark = 11X						
#define DEL_MASK 					(0x6ll) //11X - same as PTR_MASK
#define IS_MARKED_FOR_DEL(x)		((((uintptr_t)(x)) & PTR_MASK) == DEL_MASK)
#define MARK_NODE_DEL(x)			((Node<K,V> *)(((uintptr_t)(x)) | DEL_MASK))

#define TOTAL_MASK					(0x7ll) //111
#define PTR_TO_NODE(x)				((Node<K,V> *)((uintptr_t)(x) & ~TOTAL_MASK))




template<typename K, typename V>
struct Node{
	uint128_t volatile nextPair;	

	K key;
	V value;	
};

template <typename K, typename V, class RecordManager>
class List {
private:
	struct searchResult{			
		Node<K,V> * gp;	
		Node<K,V> * parent;
		Node<K,V> * curr;
	};

	struct flushedNodeRecord {
		Node<K,V>* node;
		uint128_t nodeNext;		
	};


	PAD;	
	Node<K,V> * head;
	Node<K,V> * tail;
	PAD;
	
	RecordManager * recmgr;
	V noVal;	
	PAD;

	Node<K,V> * allocNewNode(int tid, K key, V val, Node<K,V> *next){
		Node<K, V> * newNode = recmgr->template allocate<Node<K, V>>(tid);
		
		newNode->key = key;
		newNode->value = val;

		newNode->nextPair = (uint128_t)next;

					
		durableTools::FLUSH(&newNode);	
		// durableTools::FLUSH(&newNode->key);			
		// durableTools::FLUSH(&newNode->value);
		// durableTools::FLUSH(&newNode->nextPair);				

#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif

		// GSTATS_ADD(tid, num_update_flushes, 1);
		

		return newNode;		
	}



	/**
	 * Since deletes need the parent when helping we need to return the grandparent in this seach along with
	 * the parent and curr. This is because we help on the parent
	*/
	searchResult search(K key){		
		Node<K,V>* gp;
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;

		gp = NULL;
		parent = head;

		uint128_t parentNextPair = parent->nextPair;

		parentNextPtr = (Node<K,V>*)parentNextPair;
		curr = PTR_TO_NODE(parentNextPtr);		
		
		

		while (true) {
			if (!IS_DURABLE(parentNextPtr)) {			
				durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
				durableTools::SFENCE();							
#endif			
				parentNextPtr = MARK_NODE_DURABLE(parentNextPtr);				
				CAS(&parent->nextPair, parentNextPair, (uint128_t)parentNextPtr);

				GSTATS_ADD(tid, num_search_flushes, 1);				
			}

			if (curr->key >= key) {
				break;
			}
			
			gp = parent;
			parent = curr;						
			parentNextPair = parent->nextPair;
			parentNextPtr = (Node<K,V>*)parentNextPair;
			curr = PTR_TO_NODE(parentNextPtr);								
		}
		
		return {gp, parent, curr};
	}



	void helpUpdate(int tid, Node<K,V>* parent, Node<K,V>* dirtyNode) {
		// assert(PTR_TO_NODE(parent) == parent);
		// assert(PTR_TO_NODE(dirtyNode) == dirtyNode);

		Node<K,V>* dirtyNodeNextPtr = (Node<K,V>*)dirtyNode->nextPair;

		if (IS_DFLAGGED(dirtyNodeNextPtr)) {
			helpRemove(tid, dirtyNode, PTR_TO_NODE(dirtyNodeNextPtr));
		}
		else if (IS_MARKED_FOR_DEL(dirtyNodeNextPtr)) {
			helpMarked(tid, parent, dirtyNode);
		}
	}


	void helpRemove(int tid, Node<K,V>* parent, Node<K,V>* nodeToDel) {
		while (PTR_TO_NODE((Node<K,V>*)parent->nextPair) == nodeToDel) {
			uint128_t nodeToDelNext = nodeToDel->nextPair;
			Node<K,V>* nodeToDelNextPtr = (Node<K,V>*)nodeToDelNext;
			Node<K,V>* nodeToDelSucc = PTR_TO_NODE(nodeToDelNextPtr);	 
			
			if (!IS_DURABLE(nodeToDelNextPtr)) {
				durableTools::FLUSH(&nodeToDel->nextPair);	
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
				durableTools::SFENCE();							
#endif					
				nodeToDelNextPtr = MARK_NODE_DURABLE(nodeToDelNextPtr);
				CAS(&nodeToDel->nextPair, nodeToDelNext, (uint128_t)nodeToDelNextPtr);

				GSTATS_ADD(tid, num_update_flushes, 1);			
			}



			uint128_t expNext = (uint128_t)(MARK_NODE_DURABLE(nodeToDelSucc));		
			uint128_t newNext = (uint128_t)(MARK_NODE_DURABLE(MARK_NODE_DEL(nodeToDelSucc)));		
					
			if (CAS(&nodeToDel->nextPair, expNext, newNext)) {			
				this->helpMarked(tid, parent, nodeToDel);
				return;
			}
			else {
				if (IS_MARKED_FOR_DEL(expNext)){				
					this->helpMarked(tid, parent, nodeToDel);		
					return;
				}
				else if (IS_DFLAGGED(expNext)){
					helpRemove(tid, parent, nodeToDel);
				}				
				//else we failed bc nodeToDel->next was modified by an insert or a helper completed remove					
			}
		}			
	}

	void helpMarked(int tid, Node<K,V>* parent, Node<K,V>* markedNode) {
		uint128_t markedNodeNext = markedNode->nextPair;
		Node<K,V>* markedNodeNextPtr = (Node<K,V>*)markedNodeNext;
		Node<K,V>* markedNodeSucc = PTR_TO_NODE(markedNodeNextPtr);	
		
		uint128_t expNext = (uint128_t)(MARK_NODE_DURABLE(MARK_NODE_DFLAG(markedNode)));		
		uint128_t newNext = (expNext << 64) + ((uint128_t)markedNodeSucc);		


		if (CAS(&parent->nextPair, expNext, newNext)){
			durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
			durableTools::SFENCE();							
#endif			
			CAS(&parent->nextPair, newNext, (uint128_t)(MARK_NODE_DURABLE(markedNodeSucc)));
			
			recmgr->retire(tid, markedNode);
		}
	}


public:
	List(int numThreads, K minKey, K maxKey, V reservedVal){		
		recmgr = new RecordManager(numThreads);

		noVal = reservedVal;

		tail = new Node<K,V>();
		tail->key = maxKey;
		tail->value = 0;
		tail->nextPair = 0;

		head = new Node<K,V>();
		head->key = minKey;
		head->value = 0;
		head->nextPair = (uint128_t)(MARK_NODE_DURABLE(tail));
	}

	~List(){
	}

	Node<K,V> * getHead(){
		return head;
	}

	void initThread(int tid){
		recmgr->initThread(tid);
	}

	void deinitThread(int tid){
		recmgr->deinitThread(tid);
	}

	bool containsPersistAll(int tid, K key) {				
		auto guard = recmgr->getGuard(tid);

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;

		parent = head;

		uint128_t parentNextPair = parent->nextPair;

		parentNextPtr = (Node<K,V>*)parentNextPair;
		curr = PTR_TO_NODE(parentNextPtr);		
		
		

		while (true) {
			if (!IS_DURABLE(parentNextPtr)) {			
				durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
				durableTools::SFENCE();							
#endif			
				parentNextPtr = MARK_NODE_DURABLE(parentNextPtr);				
				CAS(&parent->nextPair, parentNextPair, (uint128_t)parentNextPtr);

				GSTATS_ADD(tid, num_search_flushes, 1);				
			}

			if (curr->key >= key) {
				break;
			}
			
			parent = curr;						
			parentNextPair = parent->nextPair;
			parentNextPtr = (Node<K,V>*)parentNextPair;
			curr = PTR_TO_NODE(parentNextPtr);								
		}
		
	
		return curr->key == key;
	}

	bool containsAsynchPersistAll(int tid, K key) {
		auto guard = recmgr->getGuard(tid);

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;

		parent = head;

		uint128_t parentNextPair = parent->nextPair;

		parentNextPtr = (Node<K,V>*)parentNextPair;
		curr = PTR_TO_NODE(parentNextPtr);		
		
		flushedNodeRecord flushedNodes[MAX_FLUSHED_NODES];
		int flushedNodeCount = 0;

		while (true) {
			if (!IS_DURABLE(parentNextPtr)) {			
				if (flushedNodeCount >= MAX_FLUSHED_NODES) {
					setbench_error("Set a higher max flushed nodes or implement a less lazy solution Guy.");
				}

				flushedNodes[flushedNodeCount] = {parent, parentNextPair};
				flushedNodeCount++;				
				durableTools::FLUSH(&parent->nextPair);							
			}

			if (curr->key >= key) {
				break;
			}
			
			parent = curr;						
			parentNextPair = parent->nextPair;
			parentNextPtr = (Node<K,V>*)parentNextPair;
			curr = PTR_TO_NODE(parentNextPtr);								
		}
		
		
		if (flushedNodeCount > 0) {
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
			durableTools::SFENCE();							
#endif
			GSTATS_ADD(tid, num_contains_flushes, 1);
		

			for (int i = 0; i < flushedNodeCount; i++){
				if (flushedNodes[i].node->nextPair == flushedNodes[i].nodeNext){
					Node<K,V>* durableNode = MARK_NODE_DURABLE((Node<K,V>*)flushedNodes[i].nodeNext);
					
					CAS(&flushedNodes[i].node->nextPair, flushedNodes[i].nodeNext, (uint128_t)durableNode);
				}
			}
		}

		return curr->key == key;
	}

	bool containsPersistLast(int tid, K key) {
		auto guard = recmgr->getGuard(tid);
		
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;
		
		parent = head;

		uint128_t parentNextPair = parent->nextPair;

		parentNextPtr = (Node<K,V>*)parentNextPair;
		curr = PTR_TO_NODE(parentNextPtr);		
		
		while (true) {
			if (curr->key >= key) {
				break;
			}
			
			parent = curr;						
			parentNextPair = parent->nextPair;
			parentNextPtr = (Node<K,V>*)parentNextPair;
			curr = PTR_TO_NODE(parentNextPtr);								
		}

		if (!IS_DURABLE(parentNextPtr)) {			
			durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
			durableTools::SFENCE();							
#endif			
			parentNextPtr = MARK_NODE_DURABLE(parentNextPtr);				
			CAS(&parent->nextPair, parentNextPair, (uint128_t)parentNextPtr);

			GSTATS_ADD(tid, num_search_flushes, 1);				
		}
		
	
		return curr->key == key;
	}

	bool containsNaivePersistAlways(int tid, K key) {		
		auto guard = recmgr->getGuard(tid);
		
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;
		
		parent = head;

		uint128_t parentNextPair = parent->nextPair;

		parentNextPtr = (Node<K,V>*)parentNextPair;
		curr = PTR_TO_NODE(parentNextPtr);		
		
		

		while (true) {	
			durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
			durableTools::SFENCE();							
#endif			

			if (curr->key >= key) {
				break;
			}
						
			parent = curr;						
			parentNextPair = parent->nextPair;
			parentNextPtr = (Node<K,V>*)parentNextPair;
			curr = PTR_TO_NODE(parentNextPtr);								
		}
		
	
		return curr->key == key;
	}

	bool containsSpin(int tid, K key) {
		auto guard = recmgr->getGuard(tid);
		
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;

		parent = head;

		uint128_t parentNextPair = parent->nextPair;

		parentNextPtr = (Node<K,V>*)parentNextPair;
		curr = PTR_TO_NODE(parentNextPtr);		
		
		

		while (true) {
			while (!IS_DURABLE((Node<K,V>*)parent->nextPair)) {			
				//spin
			}

			if (curr->key >= key) {
				break;
			}
			
			parent = curr;						
			parentNextPair = parent->nextPair;
			parentNextPtr = (Node<K,V>*)parentNextPair;
			curr = PTR_TO_NODE(parentNextPtr);								
		}
		
	
		return curr->key == key;
	}

	bool containsNoFlush(int tid, K key) {
		auto guard = recmgr->getGuard(tid);
		
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;
		
		parent = head;

		//(IMPORTANT): need extra read ordering to ensure that we dont have a change to one
		//field before the other field is read
		// NOTE: reading uint128_t (16 bytes) is compiled to 2 mov instructions ie. 2 reads
		uint128_t parentNextPair = parent->nextPair;

		parentNextPtr = (Node<K,V>*)parentNextPair;
		curr = PTR_TO_NODE(parentNextPtr);		
		
		while (true) {
			if (curr->key >= key) {
				break;
			}
			
			parent = curr;						
			parentNextPair = parent->nextPair;
			parentNextPtr = (Node<K,V>*)parentNextPair;
			curr = PTR_TO_NODE(parentNextPtr);								
		}


		//we reread parent->next to see if the next ptr changed
		//if it didnt change we know that the old ptr didnt change
		//if it did change then we know that there was a time where the previous
		//value of the next ptr (markedCurr) was durable
		Node<K,V>* rereadCurr = (Node<K,V>*)(parent->nextPair);
		bool isCurrDurable;

		if (rereadCurr != parentNextPtr) {
			isCurrDurable = true;
		}
		else {
			isCurrDurable = IS_DURABLE(parentNextPtr);
		}



		if (curr->key == key) {
			if (!isCurrDurable) {
				//not durable but we found the key 
				return false;
			}
			else {
				return true;
			}
		}
		else {
			if (!isCurrDurable) {
				//not durable and didnt find the key - might be from pending delete
				Node<K,V>* oldNode = (Node<K,V>*)(parentNextPair >> 64);
				if (oldNode) {
					bool wasRemoved = oldNode->key == key ? true : false;
					return wasRemoved;
				}
				else {
					//Old node is null this must be an unfinished insert
					return false;
				}				
			}
			else {
				//durable and didnt find the key
				return false;
			}			
		}
	}

	V insertIfAbsent(int tid, K key, V value){
		auto guard = recmgr->getGuard(tid);

		//retry loop
		while (true){
			searchResult result = search(key);					
			Node<K,V>* gp = result.gp;
			Node<K,V>* parent = result.parent;					
			Node<K,V>* curr = result.curr;
			
			Node<K,V>* parentNextPtr = (Node<K,V>*)parent->nextPair;			

			if (curr->key == key) {
				return curr->value;
			}			
			
			if (!IS_CLEAN(parentNextPtr)) { //parent is clean?
				helpUpdate(tid, gp, parent);
			}
			else {
				Node<K,V>* newNode = allocNewNode(tid, key, value, MARK_NODE_DURABLE(curr));
				
				uint128_t expNext = (uint128_t)(MARK_NODE_DURABLE(curr));
				uint128_t newNext = (uint128_t)newNode;

				if (CAS(&parent->nextPair, expNext, newNext)){		
					durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
					durableTools::SFENCE();							
#endif			
					CAS(&parent->nextPair, newNext, (uint128_t)(MARK_NODE_DURABLE(newNode)));

					return noVal;
				}
				else {
#if defined DEBUG_FAILED_CAS								
					GSTATS_ADD(tid, num_failed_insert_cas, 1);	
#endif					
					recmgr->deallocate(tid, newNode);					
				}
			}
		} //end retry loop
	}


	V remove(int tid, K key){
		auto guard = recmgr->getGuard(tid);

		V retVal;

		//retry loop
		while (true){
			searchResult result = search(key);		
			Node<K,V>* gp = result.gp;
			Node<K,V>* parent = result.parent;						
			Node<K,V>* curr = result.curr;			

			Node<K,V>* parentNextPtr = (Node<K,V>*)parent->nextPair;
			Node<K,V>* currNextPtr = (Node<K,V>*)curr->nextPair;

			if (curr->key != key) {
				return noVal;
			}

			retVal = curr->value;
						

			if (!IS_CLEAN(currNextPtr)) {
				helpUpdate(tid, parent, curr); //curr clean?
			}
			else if (!IS_CLEAN(parentNextPtr)) { //parent is clean?
				helpUpdate(tid, gp, parent);
			}
			else {
				uint128_t expNext = (uint128_t)(MARK_NODE_DURABLE(curr));				
				uint128_t newNext = (uint128_t)(MARK_NODE_DURABLE(MARK_NODE_DFLAG(curr)));				

				if (CAS(&parent->nextPair, expNext, newNext)){
					helpRemove(tid, parent, curr);						
					return retVal;
				}
				else {					
#if defined DEBUG_FAILED_CAS								
					GSTATS_ADD(tid, num_failed_flag_cas, 1);	
#endif															
				}
			}
		} //end retry loop
	}

	void printSummary(){		
		// printf("------- SUMMARY -------\n");

		// Node<K,V>* curr = PTR_TO_NODE(head->nextPair.load().next);
		// printf("Head: %p, nextPtr: %p, key: %d\n", head, head->nextPair.load().next, head->key);
		// while (curr != tail){
		// 	printf("Node: %p, nextPtr: %p, key: %d\n", curr, curr->nextPair.load().next, curr->key);
		// 	curr = PTR_TO_NODE(curr->nextPair.load().next);
		// }
		// printf("Tail: %p, nextPtr: %p, key: %d\n", tail, tail->nextPair.load().next, tail->key);
		// printf("------- END-SUMMARY -------\n\n");
	}
};
