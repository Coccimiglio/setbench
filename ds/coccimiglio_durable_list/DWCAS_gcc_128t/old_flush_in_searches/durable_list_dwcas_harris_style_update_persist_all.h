#pragma once

/**
 * 
 * Harris style durable list with no-flush contains and recovery is O(n) for n keys
 * 
 * 
*/

#include "durable_tools.h"

typedef unsigned __int128 uint128_t;

#define CAS(ptr, oldVal, newVal)	__sync_bool_compare_and_swap(ptr, oldVal, newVal)
#define VCAS(ptr, oldVal, newVal)	__sync_val_compare_and_swap(ptr, oldVal, newVal)

#define MAX_FLUSHED_NODES 100

#define NODE_NEXT(nodePtr)			(PTR_TO_NODE((Node<K,V>*)nodePtr->nextPair))


#define DURABLE_MASK                (0x1ll) //001
#define MARK_NODE_DURABLE(x)     	((Node<K,V>*)(((uintptr_t)(x)) | DURABLE_MASK))
#define IS_DURABLE(x)               (((uintptr_t)(x)) & DURABLE_MASK)

//Ignores durability bit
#define PTR_MASK					(0x6ll) //110

#define DEL_MASK 					(0x4ll) 
#define IS_MARKED_FOR_DEL(x)		((((uintptr_t)(x)) & PTR_MASK) == DEL_MASK)
#define MARK_NODE_DEL(x)			((Node<K,V> *)(((uintptr_t)(x)) | DEL_MASK))

#define TOTAL_MASK					(0x7ll) //111
#define PTR_TO_NODE(x)				((Node<K,V> *)((uintptr_t)(x) & ~TOTAL_MASK))




template<typename K, typename V>
struct Node{	
	uint128_t volatile nextPair;	

	K key;
	V value;	
} __attribute__((aligned(32)));

template <typename K, typename V, class RecordManager>
class List {
private:
	struct searchResult{	
		Node<K,V> * parent;
		Node<K,V> * curr;
	};

	PAD;
	Node<K,V> * head;
	Node<K,V> * tail;
	PAD;	
	RecordManager * recmgr;
	V noVal;	
	PAD;

	Node<K,V> * allocNewNode(int tid, K key, V val, Node<K,V> *next){
		Node<K, V> * newNode = recmgr->template allocate<Node<K, V>>(tid);		
		
		newNode->key = key;
		newNode->value = val;

		newNode->nextPair = (uint128_t)(next);

			
		durableTools::FLUSH(&newNode);	
		// durableTools::FLUSH(&newNode->key);			
		// durableTools::FLUSH(&newNode->value);
		// durableTools::FLUSH(&newNode->nextPair);				

#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif

		GSTATS_ADD(tid, num_update_flushes, 1);


		return newNode;		
	}


	// it is expected that parent and curr have no marked bits set when passed here
	void trim(int tid, Node<K,V>* parent, Node<K,V>* curr){
		Node<K,V>* succ = PTR_TO_NODE((Node<K,V>*)curr->nextPair);
		
		uint128_t expNext = (uint128_t)(MARK_NODE_DURABLE(curr));
		uint128_t newNext = (expNext << 64) + (uint128_t(succ));

		//unlink		
		if(CAS(&parent->nextPair, expNext, newNext)){
			durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
			durableTools::SFENCE();							
#endif	
			expNext = newNext;
			newNext = (uint128_t)(MARK_NODE_DURABLE(succ));
			CAS(&parent->nextPair, expNext, newNext);

			recmgr->retire(tid, curr);			
		}
	}


	/**
	 * Since deletes need the parent when helping we need to return the grandparent in this seach along with
	 * the parent and curr. This is because we help on the parent
	*/
	searchResult search(K key){
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;
		
		parent = head;
		uint128_t parentNextPair = head->nextPair;
		parentNextPtr = (Node<K,V>*)parentNextPair;
		curr = PTR_TO_NODE(parentNextPtr);			 		

		while (true) {			
			if (IS_MARKED_FOR_DEL((Node<K,V>*)curr->nextPair)){
				trim(tid, parent, curr);				
			}
			else {
				if (!IS_DURABLE(parentNextPtr)) {			
					durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
					durableTools::SFENCE();							
#endif							
					parentNextPtr = MARK_NODE_DURABLE(parentNextPtr);
					CAS(&parent->nextPair, parentNextPair, (uint128_t)parentNextPtr);
					
					GSTATS_ADD(tid, num_search_flushes, 1);				
				}

				if (curr->key >= key) {
					break;
				}

				parent = curr;
			}

			parentNextPair = curr->nextPair;
			parentNextPtr = (Node<K,V>*)parentNextPair;
			curr = PTR_TO_NODE(parentNextPtr);			
		}
				
		return searchResult({parent, curr});
	}

public:
	List(int numThreads, K minKey, K maxKey, V reservedVal){		
		recmgr = new RecordManager(numThreads);

		noVal = reservedVal;

		tail = allocNewNode(0, maxKey, 0, NULL);
		head = allocNewNode(0, minKey, 0, MARK_NODE_DURABLE(tail));		
	}

	~List(){
	}

	Node<K,V> * getHead(){
		return head;
	}

	void initThread(int tid){
		recmgr->initThread(tid);
	}

	void deinitThread(int tid){
		recmgr->deinitThread(tid);
	}

	bool containsPersistAll(int tid, K key) {		
		auto guard = recmgr->getGuard(tid);

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;
		
		parent = head;
		uint128_t parentNextPair = head->nextPair;
		parentNextPtr = (Node<K,V>*)parentNextPair;
		curr = PTR_TO_NODE(parentNextPtr);			 		

		while (true) {					
			if (!IS_DURABLE(parentNextPtr)) {			
				durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
				durableTools::SFENCE();							
#endif							
				parentNextPtr = MARK_NODE_DURABLE(parentNextPtr);
				CAS(&parent->nextPair, parentNextPair, (uint128_t)parentNextPtr);
				
				GSTATS_ADD(tid, num_search_flushes, 1);				
			}

			if (curr->key >= key) {
				break;
			}

			parent = curr;
			parentNextPair = curr->nextPair;
			parentNextPtr = (Node<K,V>*)parentNextPair;
			curr = PTR_TO_NODE(parentNextPtr);			
		}
			
		return curr->key == key;
	}

	bool containsPersistLast(int tid, K key) {		
		auto guard = recmgr->getGuard(tid);

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;
		
		parent = head;
		uint128_t parentNextPair = head->nextPair;
		parentNextPtr = (Node<K,V>*)parentNextPair;
		curr = PTR_TO_NODE(parentNextPtr);			 		

		while (true) {					
			if (curr->key >= key) {
				break;
			}

			parent = curr;
			parentNextPair = curr->nextPair;
			parentNextPtr = (Node<K,V>*)parentNextPair;
			curr = PTR_TO_NODE(parentNextPtr);			
		}

		if (!IS_DURABLE(parentNextPtr)) {			
			durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
			durableTools::SFENCE();							
#endif							
			parentNextPtr = MARK_NODE_DURABLE(parentNextPtr);
			CAS(&parent->nextPair, parentNextPair, (uint128_t)parentNextPtr);
			
			GSTATS_ADD(tid, num_search_flushes, 1);				
		}
			
		return curr->key == key;
	}

	bool containsNoFlush(int tid, K key) {
		auto guard = recmgr->getGuard(tid);
		
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;
		
		parent = head;

		uint128_t parentNextPair = parent->nextPair;

		parentNextPtr = (Node<K,V>*)parentNextPair;
		curr = PTR_TO_NODE(parentNextPtr);		
		
		while (true) {
			if (curr->key >= key) {
				break;
			}
			
			parent = curr;						
			parentNextPair = parent->nextPair;
			parentNextPtr = (Node<K,V>*)parentNextPair;
			curr = PTR_TO_NODE(parentNextPtr);								
		}

		if (curr->key == key) {
			if (!IS_DURABLE(markedCurr)) {
				//not durable but we found the key
				return false;
			}
			else {
				return true;
			}
		}
		else {
			if (!IS_DURABLE(markedCurr)) {
				//not durable and didnt find the key - might be from pending delete
				Node<K,V>* oldNode = (Node<K,V>*)(parentNextPair >> 64);
				if (oldNode) {
					bool wasRemoved = oldNode->key == key ? true : false;
					return wasRemoved;
				}
				else {
					//Old node is null this must be an unfinished insert
					return false;
				}				
			}
			else {
				//durable and didnt find the key
				return false;
			}			
		}
	}

	/**
	 *  
	*/
	V insertIfAbsent(int tid, K key, V value){
		auto guard = recmgr->getGuard(tid);

		//retry loop
		while (true){
			searchResult result = search(key);					
			Node<K,V>* parent = result.parent;		
			Node<K,V>* curr = result.curr;									

			if (curr->key == key) {
				return curr->value;
			}

			
			Node<K,V>* newNode = allocNewNode(tid, key, value, MARK_NODE_DURABLE(curr));
			
			uint128_t expNext = (uint128_t)(MARK_NODE_DURABLE(curr));
			uint128_t newNext = (uint128_t)(newNode);

			if (CAS(&parent->nextPair, expNext, newNext)){
				durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
				durableTools::SFENCE();							
#endif

				CAS(&parent->nextPair, newNext, ((uint128_t)MARK_NODE_DURABLE(newNode)));
				GSTATS_ADD(tid, num_update_flushes, 1);				
				return noVal;
			}
			else {		
				recmgr->deallocate(tid, newNode);
			}
			
		} //end retry loop
	}





	/**
	 *  
	*/
	V remove(int tid, K key){
		auto guard = recmgr->getGuard(tid);

		V retVal;

		//retry loop
		while (true){
			searchResult result = search(key);		
			Node<K,V>* parent = result.parent;			
			Node<K,V>* curr = result.curr;
									
			if (curr->key != key) {
				return noVal;
			}


			uint128_t currNextPair = curr->nextPair;
			Node<K,V>* currNextPtr = (Node<K,V>*)currNextPair;

			if (!IS_DURABLE(currNextPtr)) {			
				durableTools::FLUSH(&curr->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
				durableTools::SFENCE();							
#endif							
				currNextPtr = MARK_NODE_DURABLE(currNextPtr);
				CAS(&curr->nextPair, currNextPair, (uint128_t)currNextPtr);
				
				GSTATS_ADD(tid, num_search_flushes, 1);				
			}

			retVal = curr->value;
			
			Node<K,V>* succ = MARK_NODE_DURABLE(PTR_TO_NODE(currNextPtr)); 			
			Node<K,V>* markedCurr = MARK_NODE_DEL(succ);

			uint128_t expNext = (uint128_t)succ;
			uint128_t newNext = (uint128_t)markedCurr;
			
			if (CAS(&curr->nextPair, expNext, newNext)){
				trim(tid, parent, curr);				
				return retVal;
			}
		} //end retry loop
	}

	void printSummary(){
		
	}
};
