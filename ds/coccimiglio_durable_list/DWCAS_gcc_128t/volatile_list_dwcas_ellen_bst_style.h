#pragma once

typedef unsigned __int128 uint128_t;

#define CAS(ptr, oldVal, newVal)	__sync_bool_compare_and_swap(ptr, oldVal, newVal)
#define VCAS(ptr, oldVal, newVal)	__sync_val_compare_and_swap(ptr, oldVal, newVal)


#define MAX_FLUSHED_NODES 1000

#define NODE_NEXT(nodePtr)			(PTR_TO_NODE((Node<K,V>*)nodePtr->next))
#define NODE_NEXT_PTR(nodePtr)		((Node<K,V>*)nodePtr->next)

#define DURABLE_MASK                (0x1ll) //001
#define MARK_NODE_DURABLE(x)     	((Node<K,V> *)(((uintptr_t)(x)) | DURABLE_MASK))
#define IS_DURABLE(x)               (((uintptr_t)(x)) & DURABLE_MASK)


//Ignores durability bit
#define PTR_MASK					(0x6ll) //110

//clean = 00X
#define CLEAN_MASK					(0x0ll) //00X
#define IS_CLEAN(x)					((((uintptr_t)(x)) & PTR_MASK) == CLEAN_MASK)

//Dflag = 01X
#define DFLAG_MASK 					(0x2ll) //01X
#define IS_DFLAGGED(x)				((((uintptr_t)(x)) & PTR_MASK) == DFLAG_MASK)
#define MARK_NODE_DFLAG(x)			((Node<K,V> *)(((uintptr_t)(x)) | DFLAG_MASK))

//IFlag = 10X
#define IFLAG_MASK 					(0x4ll) //10X
#define IS_IFLAGGED(x)				((((uintptr_t)(x)) & PTR_MASK) == IFLAG_MASK)
#define MARK_NODE_IFLAG(x)			((Node<K,V> *)(((uintptr_t)(x)) | IFLAG_MASK))

#define UNFLAG_NODE(x)				((Node<K,V> *)(((uintptr_t)(x)) & ~PTR_MASK))

//DMark = 11X						
#define DEL_MASK 					(0x6ll) //11X - same as PTR_MASK
#define IS_MARKED_FOR_DEL(x)		((((uintptr_t)(x)) & PTR_MASK) == DEL_MASK)
#define MARK_NODE_DEL(x)			((Node<K,V> *)(((uintptr_t)(x)) | DEL_MASK))

#define TOTAL_MASK					(0x7ll) //111
#define PTR_TO_NODE(x)				((Node<K,V> *)((uintptr_t)(x) & ~TOTAL_MASK))



#define MARK_OLD_IFLAG(x)			((((uintptr_t)(x)) | IFLAG_MASK))


template<typename K, typename V>
struct Node{
	Node<K,V>* volatile next;	

	K key;
	V value;	
} __attribute__((aligned((32))));

template <typename K, typename V, class RecordManager>
class List {
private:
	struct searchResult{			
		Node<K,V> * gp;	
		Node<K,V> * parent;
		Node<K,V> * curr;
	};

	PAD;	
	Node<K,V> * head;
	Node<K,V> * tail;
	PAD;
	
	RecordManager * recmgr;
	V noVal;	
	PAD;
	// const int maxAsynchFlush;
	// PAD;

	Node<K,V> * allocNewNode(int tid, K key, V val, Node<K,V> *next){
		Node<K, V> * newNode = recmgr->template allocate<Node<K, V>>(tid);
		
		newNode->key = key;
		newNode->value = val;

		newNode->next = next;

		return newNode;		
	}


	inline searchResult search(int tid, K key){		
		Node<K,V>* gp;
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;

		gp = NULL;
		parent = head;
		parentNextPtr = parent->next;
		curr = PTR_TO_NODE(parentNextPtr);		
		
		while (true) {		
			if (curr->key >= key) {
				break;
			}
			
			gp = parent;
			parent = curr;						
			parentNextPtr = parent->next;
			curr = PTR_TO_NODE(parentNextPtr);								
		}

		return {gp, parent, curr};
	}



	void helpUpdate(int tid, Node<K,V>* parent, Node<K,V>* dirtyNode) {
		// assert(PTR_TO_NODE(parent) == parent);
		// assert(PTR_TO_NODE(dirtyNode) == dirtyNode);

		Node<K,V>* dirtyNodeNextPtr = dirtyNode->next;

		if (IS_DFLAGGED(dirtyNodeNextPtr)) {
			helpRemove(tid, dirtyNode, PTR_TO_NODE(dirtyNodeNextPtr));
		}
		else if (IS_MARKED_FOR_DEL(dirtyNodeNextPtr)) {
			helpMarked(tid, parent, dirtyNode);
		}
	}


	void helpRemove(int tid, Node<K,V>* parent, Node<K,V>* nodeToDel) {
		while (PTR_TO_NODE(parent->next) == nodeToDel) {
			Node<K,V>* nodeToDelNextPtr = nodeToDel->next;
			Node<K,V>* nodeToDelSucc = PTR_TO_NODE(nodeToDelNextPtr);	 			
			
			Node<K,V>* markedSucc = MARK_NODE_DEL(nodeToDelSucc);

			Node<K,V>* result = VCAS(&nodeToDel->next, nodeToDelSucc, markedSucc);
			
			//if (CAS(&nodeToDel->nextPair, expNext, newNext)) {			
			if (result == nodeToDelSucc || IS_MARKED_FOR_DEL(result)) {			
				this->helpMarked(tid, parent, nodeToDel);
				return;
			}
			else {
				if (IS_DFLAGGED(result)){
					helpRemove(tid, nodeToDel, PTR_TO_NODE(result));
				}				
				//else we failed bc nodeToDel->next was modified by an insert or a helper completed remove					
			}
		}			
	}


	void helpMarked(int tid, Node<K,V>* parent, Node<K,V>* markedNode) {
		Node<K,V>* markedNodeNextPtr = markedNode->next;
		Node<K,V>* markedNodeSucc = PTR_TO_NODE(markedNodeNextPtr);	
	
		markedNode = MARK_NODE_DFLAG(markedNode);

		if (CAS(&parent->next, markedNode, markedNodeSucc)){									
			recmgr->retire(tid, markedNode);
		}
	}


public:
	List(int numThreads, K minKey, K maxKey, V reservedVal){		
		recmgr = new RecordManager(numThreads);

		noVal = reservedVal;

		tail = new Node<K,V>();
		tail->key = maxKey;
		tail->value = 0;
		tail->next = NULL;

		head = new Node<K,V>();
		head->key = minKey;
		head->value = 0;
		head->next = tail;		
	}

	~List(){
	}

	Node<K,V> * getHead(){
		return head;
	}

	void initThread(int tid){
		recmgr->initThread(tid);
	}

	void deinitThread(int tid){
		recmgr->deinitThread(tid);
	}

	bool contains(int tid, K key) {				
		auto guard = recmgr->getGuard(tid);

		searchResult result = search(tid, key);
		return result.curr->key == key;		
	}

	V insertIfAbsent(int tid, K key, V value){
		auto guard = recmgr->getGuard(tid);

		//retry loop
		while (true){
			searchResult result = search(tid, key);					
			Node<K,V>* gp = result.gp;
			Node<K,V>* parent = result.parent;					
			Node<K,V>* curr = result.curr;
			
			Node<K,V>* parentNextPtr = parent->next;			

			if (curr->key == key) {
				return curr->value;
			}			
			
			if (!IS_CLEAN(parentNextPtr)) { //parent is clean?
				helpUpdate(tid, gp, parent);
			}
			else {
				Node<K,V>* newNode = allocNewNode(tid, key, value, curr);

				if (CAS(&parent->next, curr, newNode)){												
					return noVal;
				}
				else {
					recmgr->deallocate(tid, newNode);					
				}
			}
		} //end retry loop
	}


	V remove(int tid, K key){
		auto guard = recmgr->getGuard(tid);

		V retVal;

		//retry loop
		while (true){
			searchResult result = search(tid, key);		
			Node<K,V>* gp = result.gp;
			Node<K,V>* parent = result.parent;						
			Node<K,V>* curr = result.curr;			

			Node<K,V>* parentNextPtr = parent->next;
			Node<K,V>* currNextPtr = curr->next;

			if (curr->key != key) {
				return noVal;
			}

			retVal = curr->value;
						

			if (!IS_CLEAN(currNextPtr)) {
				helpUpdate(tid, parent, curr); //curr clean?
			}
			else if (!IS_CLEAN(parentNextPtr)) { //parent is clean?
				helpUpdate(tid, gp, parent);
			}
			else {			
				Node<K,V>* dflaggedCurr = MARK_NODE_DFLAG(curr);

				if (CAS(&parent->next, curr, dflaggedCurr)){
					helpRemove(tid, parent, curr);						
					return retVal;
				}
			}
		} //end retry loop
	}

	void printSummary(){				
	}
};
