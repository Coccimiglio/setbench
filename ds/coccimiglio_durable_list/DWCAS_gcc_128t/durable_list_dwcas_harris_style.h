#pragma once

/**
 * 
 * Harris style durable list with no-flush contains and recovery is O(n) for n keys
 * 
 * 
*/

#include "durable_tools.h"

#if defined(USE_LIBVMMALLOC) && !defined(RECORDMANAGER_PMEM_SUPPORT)
	#include "persistent_alloc.h"
#endif

#if defined(USE_MEMKIND) && !defined(RECORDMANAGER_PMEM_SUPPORT)
	#include <memkind.h>
	
	#define PMEM_POOL_PATH "/mnt/pmem1_mount/"
	#define PMEM_POOL_SIZE 0 // 0 means the heap is not a fixed size and can grow as long as the temp file can grow
#endif

typedef unsigned __int128 uint128_t;

#define CAS(ptr, oldVal, newVal)	__sync_bool_compare_and_swap(ptr, oldVal, newVal)
#define VCAS(ptr, oldVal, newVal)	__sync_val_compare_and_swap(ptr, oldVal, newVal)

#define MAX_FLUSHED_NODES 100

#define NODE_NEXT(nodePtr)			(PTR_TO_NODE((Node<K,V>*)nodePtr->nextPair))
#define NODE_NEXT_PTR(nodePtr)		((Node<K,V>*)nodePtr->nextPair)

#define DURABLE_MASK                (0x1ll) //001
#define MARK_NODE_DURABLE(x)     	((Node<K,V>*)(((uintptr_t)(x)) | DURABLE_MASK))
#define IS_DURABLE(x)               (((uintptr_t)(x)) & DURABLE_MASK)

//Ignores durability bit
#define PTR_MASK					(0x6ll) //110

// #define DEL_MASK 					(0x4ll) 
// #define IS_MARKED_FOR_DEL(x)		((((uintptr_t)(x)) & PTR_MASK) == DEL_MASK)
// #define MARK_NODE_DEL(x)			((Node<K,V> *)(((uintptr_t)(x)) | DEL_MASK))

//----
//IFlag = 10X
// #define IFLAG_MASK 					(0x4ll) //10X
#define IFLAG_MASK 					(0x6ll) 
#define IS_IFLAGGED(x)				((((uintptr_t)(x)) & PTR_MASK) == IFLAG_MASK)
#define MARK_NODE_IFLAG(x)			((Node<K,V> *)(((uintptr_t)(x)) | IFLAG_MASK))

//DMark = 11X						
// #define DEL_MASK 					(0x6ll) //11X - same as PTR_MASK
#define DEL_MASK 					(0x4ll) 
#define IS_MARKED_FOR_DEL(x)		((((uintptr_t)(x)) & PTR_MASK) == DEL_MASK)
#define MARK_NODE_DEL(x)			((Node<K,V> *)(((uintptr_t)(x)) | DEL_MASK))

#define MARK_OLD_IFLAG(x)			((((uintptr_t)(x)) | IFLAG_MASK))

#define TOTAL_MASK					(0x7ll) //111
#define PTR_TO_NODE(x)				((Node<K,V> *)((uintptr_t)(x) & ~TOTAL_MASK))

template<typename K, typename V>
struct Node{	
	uint128_t volatile nextPair;	

	K key;
	V value;	
} __attribute__((aligned(32)));

template <typename K, typename V, class RecordManager>
class List {
private:
	struct searchResult{	
		Node<K,V> * parent;
		Node<K,V> * curr;
	};

	struct flushedNodeRecord {
		Node<K,V>* node;
		uint128_t nodeNext;		
	};

	PAD;
	Node<K,V> * head;
	Node<K,V> * tail;
	PAD;	
#ifdef RECORDMANAGER_PMEM_SUPPORT
	RecordManager * recmgr;
#elif defined(USE_MEMKIND)
	struct memkind *pmem_kind = NULL;
#elif defined(USE_LIBVMMALLOC)
	AllocatorLibvmmalloc_Simple* alloc;
#endif

	V noVal;	
	PAD;

	Node<K,V> * allocNewNode(int tid, K key, V val, Node<K,V> *next){
#ifdef RECORDMANAGER_PMEM_SUPPORT
		Node<K, V> * newNode = recmgr->template allocate<Node<K, V>>(tid);
#elif defined(USE_MEMKIND) 
		Node<K,V> * newNode = (Node<K,V>*)memkind_malloc(pmem_kind, sizeof(Node<K,V>));
#elif defined(USE_LIBVMMALLOC)
		Node<K, V> * newNode = (Node<K, V>*)alloc->allocate(sizeof(Node<K, V>));
#endif	
		GSTATS_ADD(tid, num_pmem_alloc, 1);  
		
		newNode->key = key;
		newNode->value = val;

		newNode->nextPair = (uint128_t)(next);

			
		durableTools::FLUSH(&newNode);	
		// durableTools::FLUSH(&newNode->key);			
		// durableTools::FLUSH(&newNode->value);
		// durableTools::FLUSH(&newNode->nextPair);				

#if (defined(USE_CLFLUSHOPT) || defined(USE_CLWB)) && !defined(ONE_FLUSH_INSERT)			
		durableTools::SFENCE();
		GSTATS_ADD(tid, num_update_flushes, 1);							
#endif

		return newNode;		
	}


	inline void persist(Node<K,V>* node, Node<K,V>* next){
		durableTools::FLUSH(&node->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif	

		
		Node<K,V> *old = *(Node<K,V> **)((uintptr_t)node + 8);
		
		uint128_t exp = ((uint128_t)(old) << 64) + ((uint128_t)next);

		next = MARK_NODE_DURABLE(next);
		CAS(&node->nextPair, exp, (uint128_t)next);
	}

	inline void persist(Node<K,V>* node, Node<K,V>* next, Node<K,V>* old){
		durableTools::FLUSH(&node->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif	
		
		uint128_t exp = ((uint128_t)(old) << 64) + ((uint128_t)next);

		next = MARK_NODE_DURABLE(next);
		CAS(&node->nextPair, exp, (uint128_t)next);
	}


	inline void persist(Node<K,V>* node, Node<K,V>* next, uint128_t exp){
		durableTools::FLUSH(&node->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif	
		next = MARK_NODE_DURABLE(next);
		CAS(&node->nextPair, exp, (uint128_t)next);
	}

	// it is expected that parent and curr have no marked bits set when passed here
	void trim(int tid, Node<K,V>* parent, Node<K,V>* curr){
		Node<K,V>* succ = PTR_TO_NODE(*(Node<K,V>**)curr);
		Node<K,V>* durableCurr = MARK_NODE_DURABLE(curr);

		uint128_t expNext = (uint128_t)(durableCurr);
		uint128_t newNext = (expNext << 64) + (uint128_t(succ));

		//unlink		
		if(CAS(&parent->nextPair, expNext, newNext)){
			persist(parent, succ, newNext);

			GSTATS_ADD(tid, num_update_flushes, 1);

#ifdef RECORDMANAGER_PMEM_SUPPORT
			recmgr->retire(tid, curr);
#else 		
#endif			
		}
	}


	void trim_FromSearch(int tid, Node<K,V>* parent, Node<K,V>* curr){
		Node<K,V>* succ = PTR_TO_NODE(*(Node<K,V>**)curr);
		
		uint128_t expNext = (uint128_t)(MARK_NODE_DURABLE(curr));
		uint128_t newNext = (expNext << 64) + (uint128_t(succ));

		//unlink		
		if(CAS(&parent->nextPair, expNext, newNext)){
			persist(parent, succ, newNext);

			GSTATS_ADD(tid, num_search_flushes, 1);

#ifdef RECORDMANAGER_PMEM_SUPPORT
			recmgr->retire(tid, curr);
#else 		
#endif				
		}
	}


	/**
	 * Since deletes need the parent when helping we need to return the grandparent in this seach along with
	 * the parent and curr. This is because we help on the parent
	*/
	inline searchResult search(int tid, K key){
		Node<K,V>* gp;
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;
		
		gp = NULL;
		parent = head;
		parentNextPtr = *(Node<K,V> **)(parent);
		curr = PTR_TO_NODE(parentNextPtr);			 		

		//uint128_t currNextPair;
		Node<K,V>* currNextPtr;

		while (true) {	
			currNextPtr = *(Node<K,V> **)(curr);

			if (IS_MARKED_FOR_DEL(currNextPtr)){
				if (!IS_DURABLE(currNextPtr)) {
					persist(curr, currNextPtr);
					GSTATS_ADD(tid, num_search_flushes, 1);
				}

				trim_FromSearch(tid, parent, curr);				
			}
			else {
				if (curr->key >= key) {
					break;
				}

				gp = parent;
				parent = curr;				
			}

			parentNextPtr = *(Node<K,V> **)(curr);
			curr = PTR_TO_NODE(parentNextPtr);			
		}


		//confirm parent durability
		if (gp) {
			Node<K,V>* gpNextPtr = *(Node<K,V> **)(gp);

			if (!IS_DURABLE(gpNextPtr)) {
				persist(gp, gpNextPtr);					
				GSTATS_ADD(tid, num_search_flushes, 1);				
			}
		}

		//confirm curr durability
		if (!IS_DURABLE(parentNextPtr)) {		
			persist(parent, parentNextPtr);	
			GSTATS_ADD(tid, num_search_flushes, 1);				
		}
		
				
		return searchResult({parent, curr});
	}

public:
	List(int numThreads, K minKey, K maxKey, V reservedVal){		
#ifdef	RECORDMANAGER_PMEM_SUPPORT	
		recmgr = new RecordManager(numThreads);

		//TODO:: fix RecordManager then remove this later (this is a dumb way to make sure memkind is initialized in allocator)
		Node<K,V> * temp = recmgr->template allocate<Node<K,V>>(tid);
		recmgr->deallocate(0, temp);
#elif defined(USE_MEMKIND)
		int err = memkind_create_pmem(PMEM_POOL_PATH, PMEM_POOL_SIZE, &pmem_kind);
		if (err) {			
			printf("Error creating PMEM pool. Exiting\n");
			// print_err_message(err);
			exit(-1);
		}
#elif defined(USE_LIBVMMALLOC)
		alloc = new AllocatorLibvmmalloc_Simple();
		alloc->init();
#endif
		noVal = reservedVal;

		tail = allocNewNode(0, maxKey, 0, 0);
		head = allocNewNode(0, minKey, 0, (MARK_NODE_DURABLE(tail)));		
	}

	~List(){
#if !defined(RECORDMANAGER_PMEM_SUPPORT) && defined(USE_MEMKIND)	
		int err = memkind_destroy_kind(pmem_kind);
    	if (err) {        
			printf("Error destroying pmem_kind\n");
		}
#endif
	}

	Node<K,V> * getHead(){
		return head;
	}

	void initThread(int tid){
#ifdef RECORDMANAGER_PMEM_SUPPORT	
		recmgr->initThread(tid);
#endif

	}

	void deinitThread(int tid){
#ifdef RECORDMANAGER_PMEM_SUPPORT	
		recmgr->deinitThread(tid);
#endif		
	}

	bool containsPersistAll(int tid, K key) {		
#ifdef	RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;
		
		parent = head;
		parentNextPtr = *(Node<K,V> **)(parent);
		curr = PTR_TO_NODE(parentNextPtr);			 		

		while (true) {					
			if (!IS_DURABLE(parentNextPtr)) {	
				persist(parent, parentNextPtr);						
				GSTATS_ADD(tid, num_contains_flushes, 1);				
			}

			if (curr->key >= key) {
				break;
			}

			parent = curr;
			parentNextPtr = *(Node<K,V> **)(parent);
			curr = PTR_TO_NODE(parentNextPtr);			
		}
			
		return curr->key == key;
	}

	bool containsAsynchPersistAll(int tid, K key) {
#ifdef RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;

		parent = head;

		parentNextPtr = *(Node<K,V> **)(parent);
		curr = PTR_TO_NODE(parentNextPtr);		
		
		flushedNodeRecord flushedNodes[MAX_FLUSHED_NODES];
		int flushedNodeCount = 0;

		while (true) {
			if (!IS_DURABLE(parentNextPtr)) {			
				if (flushedNodeCount >= MAX_FLUSHED_NODES) {
					setbench_error("Set a higher max flushed nodes or implement a less lazy solution Guy.");
				}

				Node<K,V>* old = *(Node<K,V> **)((uintptr_t)parent + 8);
				uint128_t exp = ((uint128_t)(old) << 64) + ((uint128_t)parentNextPtr);
				flushedNodes[flushedNodeCount] = {parent, exp};
				flushedNodeCount++;				
				durableTools::FLUSH(&parent->nextPair);							
			}

			if (curr->key >= key) {
				break;
			}
			
			parent = curr;						
			parentNextPtr = *(Node<K,V> **)(parent);
			curr = PTR_TO_NODE(parentNextPtr);								
		}
		
		bool currMarked = false;
		Node<K,V>* currNextPtr = *(Node<K,V>**)curr;
		if (curr->key == key && IS_MARKED_FOR_DEL(currNextPtr)) {
			if (!IS_DURABLE(currNextPtr)) {
				Node<K,V>* old = *(Node<K,V> **)((uintptr_t)curr + 8);
				uint128_t exp = ((uint128_t)(old) << 64) + ((uint128_t)currNextPtr);

				flushedNodes[flushedNodeCount] = {curr, exp};
				flushedNodeCount++;				
				durableTools::FLUSH(&curr->nextPair);		
			}
			
			currMarked = true;
		}
		
		if (flushedNodeCount > 0) {
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
			durableTools::SFENCE();							
#endif
			GSTATS_ADD(tid, num_contains_flushes, 1);
		

			for (int i = 0; i < flushedNodeCount; i++){
				if (flushedNodes[i].node->nextPair == flushedNodes[i].nodeNext){
					Node<K,V>* durableNode = MARK_NODE_DURABLE((Node<K,V>*)flushedNodes[i].nodeNext);
					
					CAS(&flushedNodes[i].node->nextPair, flushedNodes[i].nodeNext, (uint128_t)durableNode);
				}
			}
		}

		return !currMarked && curr->key == key;
	}

	bool containsPersistLast(int tid, K key) {		
#ifdef RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;
		
		parent = head;
		parentNextPtr = *(Node<K,V> **)(parent);
		curr = PTR_TO_NODE(parentNextPtr);			 		

		while (true) {					
			if (curr->key >= key) {
				break;
			}

			parent = curr;
			parentNextPtr = *(Node<K,V> **)(parent);
			curr = PTR_TO_NODE(parentNextPtr);			
		}
	
		if (!IS_DURABLE(parentNextPtr) && *(Node<K,V> **)(parent) == parentNextPtr) {	
			persist(parent, parentNextPtr);		
			GSTATS_ADD(tid, num_contains_flushes, 1);				
		}
			

		Node<K,V>* currNextPtr = *(Node<K,V>**)curr;
		if (curr->key == key && IS_MARKED_FOR_DEL(currNextPtr)) {
			if (!IS_DURABLE(currNextPtr)) {
				persist(curr, currNextPtr);
				GSTATS_ADD(tid, num_contains_flushes, 1);	
			}
			
			return false;
		}

		return curr->key == key;
	}

	bool containsNoFlush(int tid, K key) {
#ifdef RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif
		
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;
		
		parent = head;

		parentNextPtr = *(Node<K,V>**)(parent);
		curr = PTR_TO_NODE(parentNextPtr);		
		
		while (true) {
			if (curr->key >= key) {
				break;
			}
			
			parent = curr;						
			parentNextPtr = *(Node<K,V>**)(parent);
			curr = PTR_TO_NODE(parentNextPtr);								
		}

		//if a node is marked it can never become unmarked, but the durability bit of the mark
		//could change
		Node<K,V>* currNext = *(Node<K,V> **)(curr);	
		bool isCurrMarked = IS_MARKED_FOR_DEL(currNext);
		bool isMarkDurable = IS_DURABLE(currNext);	

		if (IS_DURABLE(parentNextPtr)) {//durable
			return curr->key == key && (!isCurrMarked || (isCurrMarked && !isMarkDurable));;				
		}
		else { //not durable 		
			Node<K,V>* oldNode1 = *(Node<K,V> **)((uintptr_t)parent + 8);	
			Node<K,V>* parentNextPtr2 = *(Node<K,V> **)(parent);
			Node<K,V>* oldNode2 = *(Node<K,V> **)((uintptr_t)parent + 8);

			/* 
				if either the old ptr or next ptr changed
				then curr was pointed to by durable ptr at some time during this
				contains operation

				if old didnt change but it is null then that also means curr was pointed
				to by durable ptr during this contains operation bc DWCAS implementation
				never has non-durable next ptr with a null old ptr
			*/		
			if ((parentNextPtr != parentNextPtr2) || (oldNode1 != oldNode2) || (!oldNode1 && !oldNode2)){
				/*
				* next ptr changed - the original ptr we read was durable at some time OR
				* old ptr changed - the original ptr we read was durable at some time
				*/	
				if (curr->key == key) {
					//we must recheck if curr is marked only if 
					//we found it was NOT marked or the mark was NOT durable
					/*
						* if (!isCurrMarked || (isCurrMarked && !isMarkDurable))
						* is the same as:
						* if (!(isCurrMarked && isMarkDurable))
						* however, the first can short circut early if curr is not marked
					*/
					if (!isCurrMarked || (isCurrMarked && !isMarkDurable)) {
						currNext = *(Node<K,V> **)(curr);
						isCurrMarked = IS_MARKED_FOR_DEL(currNext);					
						isMarkDurable = IS_DURABLE(currNext);	
					}
					return (!isCurrMarked || (isCurrMarked && !isMarkDurable));
				}
				else {
					return false;
				}
			}
			else {
				//the next and old ptr didnt change so there is no time where next ptr was durable
				if (IS_IFLAGGED(oldNode1)) { //not durable bc of insert
					//regardless of if curr contains the key we return false
					return false;
				}
				else {//not durable bc of delete
					if (curr->key == key) {						
						//we must recheck if curr is marked only if 
						//we found it was NOT marked or the mark was NOT durable
						/*
							* IMPORTANT - we dont care if the ptr into curr changes even if it becomes
							* non-durable. Marking for logical deletion cannot be reversed thus:
							* If this check reveals that curr is unmarked then it was never marked
							* otherwise if it is marked then the ptr into curr is irrevant since it cannot be
							* marked unless the insert of curr was durable 
						*/
						if (!isCurrMarked || (isCurrMarked && !isMarkDurable)) {
							currNext = *(Node<K,V> **)(curr);
							isCurrMarked = IS_MARKED_FOR_DEL(currNext);					
							isMarkDurable = IS_DURABLE(currNext);	
						}
						return (!isCurrMarked || (isCurrMarked && !isMarkDurable));					
					}
					else {											
						return PTR_TO_NODE(oldNode1)->key == key;						
					}
				}
			}

			

			


			// //if we found curr was marked it cant become unmarked
			// //if it wasnt marked
			// if (isCurrMarked && !isMarkDurable) {
			// 	currNext = *(Node<K,V> **)(curr);					
			// 	isMarkDurable = IS_DURABLE(currNext);	
			// }			
			// else if (!isCurrMarked) {
			// 	isCurrMarked = IS_MARKED_FOR_DEL(currNext);
			// 	isMarkDurable = IS_DURABLE(currNext);	
			// }



			// if ((parentNextPtr != parentNextPtr2) || (oldNode1 != oldNode2) || (!oldNode1 && !oldNode2)){
			// 	/*
			// 	* next ptr changed - the original ptr we read was durable at some time OR
			// 	* old ptr changed - the original ptr we read was durable at some time
			// 	*/				
			// 	return (curr->key == key) && (!isCurrMarked || (isCurrMarked && !isMarkDurable));
			// }			
			// else {
			// 	if (IS_IFLAGGED(oldNode1)) {
			// 		return false; //even if curr contains the key the insert is not durable
			// 	}
			// 	else {
			// 		//old ptr and next ptr didnt change - this means oldNode1 is the old node we want
			// 		if (curr->key == key) {						
			// 			if (isCurrMarked && isMarkDurable) {							
			// 				return false;							
			// 			}

			// 			//we can return true in this case because either the remove will be in NVM or it wont
			// 			//in the case where the remove is persisted curr is in NVM since the link we followed
			// 			//will be persisted
			// 			//in the case where the remove is NOT persisted, then deletedNode->next must be durable, 
			// 			//and must point to curr since we cannot mark a next ptr that is not durable
			// 			return true;
			// 		}
			// 		else {
			// 			return oldNode1->key == key;
			// 		}
			// 	}
			// }			
		}
	}

	/**
	 *  
	*/
	V insertIfAbsent(int tid, K key, V value){
#ifdef RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif

		//retry loop
		while (true){
			searchResult result = search(tid, key);					
			Node<K,V>* parent = result.parent;		
			Node<K,V>* curr = result.curr;									

			if (curr->key == key) {
				return curr->value;
			}

			
			Node<K,V>* newNode = allocNewNode(tid, key, value, MARK_NODE_DURABLE(curr));
			
			uint128_t expNext = (uint128_t)(MARK_NODE_DURABLE(curr));
#ifdef ONE_FLUSH_INSERT				
			uint128_t newNext = ((uint128_t)(MARK_OLD_IFLAG(curr)) << 64) + ((uint128_t)newNode);
#else
			uint128_t newNext = (uint128_t)newNode;
#endif

			if (CAS(&parent->nextPair, expNext, newNext)){
				persist(parent, newNode, newNext);
				GSTATS_ADD(tid, num_update_flushes, 1);				
				return noVal;
			}
			else {		
#ifdef RECORDMANAGER_PMEM_SUPPORT
				recmgr->deallocate(tid, newNode);
#elif defined(USE_MEMKIND) 		
				memkind_free(pmem_kind, (void*)newNode);
#elif defined(USE_LIBVMMALLOC)
				alloc->deallocate(newNode);
#endif	
			}
			
		} //end retry loop
	}





	/**
	 *  
	*/
	V remove(int tid, K key){
#ifdef RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif

		V retVal;

		//retry loop
		while (true){
			searchResult result = search(tid, key);		
			Node<K,V>* parent = result.parent;			
			Node<K,V>* curr = result.curr;
									
			if (curr->key != key) {
				return noVal;
			}


			Node<K,V>* currNextPtr = *(Node<K,V>**)curr;

			if (!IS_DURABLE(currNextPtr)) {	
				persist(curr, currNextPtr);
				GSTATS_ADD(tid, num_search_flushes, 1);				
			}

			retVal = curr->value;
			
			Node<K,V>* succ = PTR_TO_NODE(currNextPtr); 			
			Node<K,V>* markedCurr = MARK_NODE_DEL(succ);
			Node<K,V>* durableSucc = MARK_NODE_DURABLE(succ);

			uint128_t expNext = (uint128_t)(durableSucc);
			uint128_t newNext = (uint128_t)markedCurr;
			

			if (CAS(&curr->nextPair, expNext, newNext)){
				persist(curr, markedCurr, newNext);
				GSTATS_ADD(tid, num_update_flushes, 1);

				trim(tid, parent, curr);				
				return retVal;
			}
		} //end retry loop
	}

	void printSummary(){
		// Node<K,V>* curr = head;
		// Node<K,V>* markedCurr = head;
		// while (curr != NULL){
		// 	if (curr != head && curr != tail && markedCurr != MARK_NODE_DURABLE(curr)){
		// 		printf("Next node is problem:\n");
		// 	}

		// 	//printf("Node: %p, key: %l\n", markedCurr, curr->key);
		// 	markedCurr = (Node<K,V>*)curr->nextPair;
		// 	curr = PTR_TO_NODE(markedCurr);			
		// }
	}
};
