#pragma once

/**
 * 
 * Volatile Harris style list
 * 
 * 
*/
#define CAS(ptr, oldVal, newVal)	__sync_bool_compare_and_swap(ptr, oldVal, newVal)
#define VCAS(ptr, oldVal, newVal)	__sync_val_compare_and_swap(ptr, oldVal, newVal)

#define NODE_NEXT(nodePtr)			(PTR_TO_NODE(nodePtr->next))
#define NODE_NEXT_PTR(nodePtr)		(nodePtr->next)

//DMark = 001						
#define DEL_MASK 					(0x1ll)
#define IS_MARKED_FOR_DEL(x)		(((uintptr_t)(x)) & DEL_MASK)
#define MARK_NODE_DEL(x)			((Node<K,V> *)(((uintptr_t)(x)) | DEL_MASK))

#define PTR_TO_NODE(x)				((Node<K,V> *)((uintptr_t)(x) & ~DEL_MASK))

template<typename K, typename V>
struct Node{	
	Node<K,V>* volatile next;	

	K key;
	V value;	
} __attribute__((aligned(32)));

template <typename K, typename V, class RecordManager>
class List {
private:
	struct searchResult{	
		Node<K,V> * parent;
		Node<K,V> * curr;
	};
	
	PAD;
	Node<K,V> * head;
	Node<K,V> * tail;
	PAD;	
	RecordManager * recmgr;
	V noVal;	
	PAD;

	Node<K,V> * allocNewNode(int tid, K key, V val, Node<K,V> *next){
		Node<K, V> * newNode = recmgr->template allocate<Node<K, V>>(tid);		
		
		newNode->key = key;
		newNode->value = val;

		newNode->next = next;
		return newNode;		
	}

	// it is expected that parent and curr have no marked bits set when passed here
	void trim(int tid, Node<K,V>* parent, Node<K,V>* curr, Node<K,V>* currNextPtr){
		Node<K,V>* succ = PTR_TO_NODE(currNextPtr);

		//unlink		
		if(CAS(&parent->next, curr, succ)){			
			recmgr->retire(tid, curr);			
		}
	}


	inline searchResult search(int tid, K key){		
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;
				
		parent = head;
		parentNextPtr = parent->next;
		curr = PTR_TO_NODE(parentNextPtr);			 		
		
		Node<K,V>* currNextPtr;

		while (true) {	
			currNextPtr = curr->next;

			if (IS_MARKED_FOR_DEL(currNextPtr)){				
				trim(tid, parent, curr, currNextPtr);				
			}
			else {
				if (curr->key >= key) {
					break;
				}
				
				parent = curr;				
			}

			parentNextPtr = parent->next;
			curr = PTR_TO_NODE(parentNextPtr);			
		}
				
		return searchResult({parent, curr});
	}

public:
	List(int numThreads, K minKey, K maxKey, V reservedVal){		
		recmgr = new RecordManager(numThreads);

		noVal = reservedVal;

		tail = new Node<K,V>();
		tail->key = maxKey;
		tail->value = 0;
		tail->next = NULL;
		
		head = new Node<K,V>();
		head->key = minKey; 
		head->value = 0;
		head->next = tail;		
	}

	~List(){
	}

	Node<K,V> * getHead(){
		return head;
	}

	void initThread(int tid){
		recmgr->initThread(tid);
	}

	void deinitThread(int tid){
		recmgr->deinitThread(tid);
	}

	bool contains(int tid, K key) {		
		auto guard = recmgr->getGuard(tid);

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;
				
		parent = head;
		parentNextPtr = parent->next;
		curr = PTR_TO_NODE(parentNextPtr);			 		
		
		Node<K,V>* currNextPtr;

		while (true) {							
			if (curr->key >= key) {
				break;
			}
			
			parent = curr;							
			parentNextPtr = parent->next;
			curr = PTR_TO_NODE(parentNextPtr);			
		}

		if (curr->key == key) {
			currNextPtr = curr->next;
			return !IS_MARKED_FOR_DEL(currNextPtr);
		}

		return false;
	}

	/**
	 *  
	*/
	V insertIfAbsent(int tid, K key, V value){
		auto guard = recmgr->getGuard(tid);

		//retry loop
		while (true){
			searchResult result = search(tid, key);					
			Node<K,V>* parent = result.parent;		
			Node<K,V>* curr = result.curr;									

			if (curr->key == key) {
				return curr->value;
			}

			Node<K,V>* newNode = allocNewNode(tid, key, value, curr);
	
			if (CAS(&parent->next, curr, newNode)){
				return noVal;
			}
			else {		
				recmgr->deallocate(tid, newNode);
			}
			
		} //end retry loop
	}





	/**
	 *  
	*/
	V remove(int tid, K key){
		auto guard = recmgr->getGuard(tid);

		V retVal;

		//retry loop
		while (true){
			searchResult result = search(tid, key);		
			Node<K,V>* parent = result.parent;			
			Node<K,V>* curr = result.curr;
									
			if (curr->key != key) {
				return noVal;
			}

			retVal = curr->value;

			Node<K,V>* currNextPtr = PTR_TO_NODE(curr->next);						
			Node<K,V>* markedCurr = MARK_NODE_DEL(currNextPtr);						

			if (CAS(&curr->next, currNextPtr, markedCurr)){				
				trim(tid, parent, curr, markedCurr);				
				return retVal;
			}
		} //end retry loop
	}

	void printSummary(){
	}
};
