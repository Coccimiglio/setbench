#pragma once

/**
 * 
 * 
 * 
 * Harris style durable list with no-flush contains and recovery is O(n) for n keys
 * 
 * 
*/

#include "durable_tools.h"
#include <stdio.h>

#if defined(USE_LIBVMMALLOC) && !defined(RECORDMANAGER_PMEM_SUPPORT)
	#include "persistent_alloc.h"
#endif

#if defined(USE_MEMKIND) && !defined(RECORDMANAGER_PMEM_SUPPORT)
	#include <memkind.h>

	#define PMEM_POOL_PATH "/mnt/pmem1_mount/"
	#define PMEM_POOL_SIZE (1024 * 1024 * 128)
#endif

#define CAS(ptr, oldVal, newVal)	__sync_bool_compare_and_swap(ptr, oldVal, newVal)
#define VCAS(ptr, oldVal, newVal)	__sync_val_compare_and_swap(ptr, oldVal, newVal)

#define MAX_FLUSHED_NODES 100

#define NODE_NEXT(nodePtr)			(PTR_TO_NODE(nodePtr->next))
#define NODE_NEXT_PTR(nodePtr)		((Node<K,V>*)nodePtr->next)

#define DURABLE_MASK                (0x1ll) //001
#define MARK_NODE_DURABLE(x)     	((Node<K,V>*)(((uintptr_t)(x)) | DURABLE_MASK))
#define IS_DURABLE(x)               (((uintptr_t)(x)) & DURABLE_MASK)

// #define MARK_NODE_DURABLE(x)     	((Node<K,V>*)(x))
// #define IS_DURABLE(x)               (true)


//Ignores durability bit
#define PTR_MASK					(0x6ll) //110

//clean = 00X
#define CLEAN_MASK					(0x0ll) //00X
#define IS_CLEAN(x)					((((uintptr_t)(x)) & PTR_MASK) == CLEAN_MASK)

//Dflag = 01X
#define DFLAG_MASK 					(0x2ll) //01X
#define IS_DFLAGGED(x)				((((uintptr_t)(x)) & PTR_MASK) == DFLAG_MASK)
#define MARK_NODE_DFLAG(x)			((Node<K,V> *)(((uintptr_t)(x)) | DFLAG_MASK))

//IFlag = 10X (unused)
#define IFLAG_MASK 					(0x4ll) //10X
#define IS_IFLAGGED(x)				((((uintptr_t)(x)) & PTR_MASK) == IFLAG_MASK)
#define MARK_NODE_IFLAG(x)			((Node<K,V> *)(((uintptr_t)(x)) | IFLAG_MASK))

#define UNFLAG_NODE(x)				((Node<K,V> *)(((uintptr_t)(x)) & ~PTR_MASK))

//DMark = 11X						
#define DEL_MASK 					(0x6ll) //11X - same as PTR_MASK
#define IS_MARKED_FOR_DEL(x)		((((uintptr_t)(x)) & PTR_MASK) == DEL_MASK)
#define MARK_NODE_DEL(x)			((Node<K,V> *)(((uintptr_t)(x)) | DEL_MASK))

#define TOTAL_MASK					(0x7ll) //111
#define PTR_TO_NODE(x)				((Node<K,V> *)((uintptr_t)(x) & ~TOTAL_MASK))


template<typename K, typename V>
struct Node{	
	Node<K,V>* volatile next;	
	Node<K,V>* volatile old;	

	K key;
	V value;	
} __attribute__((aligned(32)));

template <typename K, typename V, class RecordManager>
class List {
private:
	struct searchResult{	
		Node<K,V> * gp;
		Node<K,V> * parent;
		Node<K,V> * curr;
	};

	struct flushedNodeRecord {
		Node<K,V>* node;
		Node<K,V>* nodeNext;		
	};

	PAD;
	Node<K,V> * head;
	Node<K,V> * tail;
	PAD;	
#ifdef RECORDMANAGER_PMEM_SUPPORT
	RecordManager * recmgr;
#elif defined(USE_MEMKIND)
	struct memkind *pmem_kind = NULL;
#elif defined(USE_LIBVMMALLOC)
	AllocatorLibvmmalloc_Simple* alloc;
#endif
	V noVal;	
	PAD;

	Node<K,V> * allocNewNode(int tid, K key, V val, Node<K,V> *next){

#ifdef RECORDMANAGER_PMEM_SUPPORT
		Node<K, V> * newNode = recmgr->template allocate<Node<K, V>>(tid);
#elif defined(USE_MEMKIND) 
		Node<K,V> * newNode = (Node<K,V>*)memkind_malloc(pmem_kind, sizeof(Node<K,V>));

		if (!newNode) {
			printf("Allocation Error \n");		
			newNode = (Node<K,V>*)memkind_malloc(pmem_kind, sizeof(Node<K,V>));
			printf("%p\n", newNode);
		}
#elif defined(USE_LIBVMMALLOC)
		Node<K, V> * newNode = (Node<K, V>*)alloc->allocate(sizeof(Node<K, V>));
#endif		
				
		newNode->key = key;
		newNode->value = val;

		newNode->next = next;
		newNode->old = NULL;

			
		durableTools::FLUSH(&newNode);		

#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif

		// GSTATS_ADD(tid, num_update_flushes, 1);


		return newNode;		
	}



	/**
	 * Since deletes need the parent when helping we need to return the grandparent in this seach along with
	 * the parent and curr. This is because we help on the parent
	*/
	inline searchResult search(int tid, K key){
		Node<K,V>* gp;
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;

	
		gp = NULL;
		parent = head;		
		parentNextPtr = head->next;
		curr = PTR_TO_NODE(parentNextPtr);			 		

		while (true) {
			if (curr->key >= key) {
				break;
			}

			gp = parent;
			parent = curr;				
			
			parentNextPtr = curr->next;
			curr = PTR_TO_NODE(parentNextPtr);			
		}

		Node<K,V>* old = NULL;
		if (gp) {
			//confirm parent durability
			Node<K,V>* gpNextPtr = gp->next;

			if (!IS_DURABLE(gpNextPtr)) {							
				persist(gp, gpNextPtr);
				old = gp->old;
				gpNextPtr = gp->next;
				
				if (PTR_TO_NODE(old) != PTR_TO_NODE(gpNextPtr) && IS_DURABLE(gpNextPtr)) {
					CAS(&gp->old, old, NULL);
				}
				GSTATS_ADD(tid, num_search_flushes, 1);		
			}
		}

		//confirm curr durability
		if (!IS_DURABLE(parentNextPtr)) {						
			persist(parent, parentNextPtr);
			old = parent->old;
			parentNextPtr = parent->next;
			
			if (PTR_TO_NODE(old) != PTR_TO_NODE(parentNextPtr) && IS_DURABLE(parentNextPtr)) {
				CAS(&parent->old, old, NULL);
			}
			GSTATS_ADD(tid, num_search_flushes, 1);
		}
		
				
		return searchResult({gp, parent, curr});
	}


	void helpUpdate(int tid, Node<K,V>* parent, Node<K,V>* dirtyNode) {
		Node<K,V>* dirtyNodeNextPtr = dirtyNode->next;

		if (IS_DFLAGGED(dirtyNodeNextPtr)) {
			helpRemove_FromSearch(tid, dirtyNode, PTR_TO_NODE(dirtyNodeNextPtr));
		}
		else if (IS_MARKED_FOR_DEL(dirtyNodeNextPtr)) {
			helpMarked_FromSearch(tid, parent, dirtyNode);
		}
	}


	void helpRemove(int tid, Node<K,V>* parent, Node<K,V>* nodeToDel) {
		while (PTR_TO_NODE(parent->next) == nodeToDel) {
			Node<K,V>* nodeToDelNext = nodeToDel->next;			
			Node<K,V>* nodeToDelSucc = PTR_TO_NODE(nodeToDelNext);	 
			
			if (!IS_DURABLE(nodeToDelNext)) {
				persist(nodeToDel, nodeToDelSucc);
				Node<K,V>* old = nodeToDel->old;
				nodeToDelNext = nodeToDel->next;
				
				if (PTR_TO_NODE(old) != PTR_TO_NODE(nodeToDelNext) && IS_DURABLE(nodeToDelNext)) {
					CAS(&nodeToDel->old, old, NULL);
				}

				GSTATS_ADD(tid, num_search_flushes, 1);			
			}

			Node<K,V>* expNext = MARK_NODE_DURABLE(nodeToDelSucc);		
			Node<K,V>* newNext = MARK_NODE_DURABLE(MARK_NODE_DEL(nodeToDelSucc));		

			Node<K,V>* result = VCAS(&nodeToDel->next, expNext, newNext);
			if (result == expNext || IS_MARKED_FOR_DEL(result)) {			
				this->helpMarked(tid, parent, nodeToDel);
				return;
			}
			else {
				if (IS_DFLAGGED(result)){
					helpRemove(tid, nodeToDel, PTR_TO_NODE(result));
				}				
				//else we failed bc nodeToDel->next was modified by an insert or a helper completed remove					
			}
		}			
	}

	void helpRemove_FromSearch(int tid, Node<K,V>* parent, Node<K,V>* nodeToDel) {
		while (PTR_TO_NODE(parent->next) == nodeToDel) {
			Node<K,V>* nodeToDelNext = nodeToDel->next;			
			Node<K,V>* nodeToDelSucc = PTR_TO_NODE(nodeToDelNext);	 
			
			if (!IS_DURABLE(nodeToDelNext)) {
				persist(nodeToDel, nodeToDelSucc);
				Node<K,V>* old = nodeToDel->old;
				nodeToDelNext = nodeToDel->next;
				
				if (PTR_TO_NODE(old) != PTR_TO_NODE(nodeToDelNext) && IS_DURABLE(nodeToDelNext)) {
					CAS(&nodeToDel->old, old, NULL);
				}

				GSTATS_ADD(tid, num_search_flushes, 1);			
			}

			Node<K,V>* expNext = MARK_NODE_DURABLE(nodeToDelSucc);		
			Node<K,V>* newNext = MARK_NODE_DURABLE(MARK_NODE_DEL(nodeToDelSucc));		

			Node<K,V>* result = VCAS(&nodeToDel->next, expNext, newNext);
			if (result == expNext || IS_MARKED_FOR_DEL(result)) {			
				this->helpMarked_FromSearch(tid, parent, nodeToDel);
				return;
			}
			else {
				if (IS_DFLAGGED(result)){
					helpRemove_FromSearch(tid, nodeToDel, PTR_TO_NODE(result));
				}				
				//else we failed bc nodeToDel->next was modified by an insert or a helper completed remove					
			}
		}			
	}

	void helpMarked(int tid, Node<K,V>* parent, Node<K,V>* markedNode) {
		//set old of parent then helpOld
		Node<K,V>* expOld = NULL;

		Node<K,V>* old = VCAS(&parent->old, expOld, markedNode);

		if (old == expOld || old == markedNode){
			helpOld(tid, parent, markedNode);
		}
	}

	void helpMarked_FromSearch(int tid, Node<K,V>* parent, Node<K,V>* markedNode) {
		//set old of parent then helpOld
		Node<K,V>* expOld = NULL;

		Node<K,V>* old = VCAS(&parent->old, expOld, markedNode);

		if (old == expOld || old == markedNode){
			helpOld_FromSearch(tid, parent, markedNode);
		}
	}


	bool helpOld(int tid, Node<K,V>* node, Node<K,V>* old){		
		bool successCAS = false;

		//get the successor from the old node 
		Node<K,V>* succ = PTR_TO_NODE(old->next);

		if (CAS(&node->next, MARK_NODE_DFLAG(MARK_NODE_DURABLE(old)), succ)) {				
			successCAS = true;
		}			

		Node<K,V>* updatedNext = node->next;
		Node<K,V>* updatedNextNode = PTR_TO_NODE(updatedNext);		

		// if ((successCAS) || (updatedNextNode == succ && !IS_DURABLE(updatedNext))) {
		// 	persist(node, succ);
		if (!IS_DURABLE(updatedNext)) {
			persist(node, updatedNextNode);
			GSTATS_ADD(tid, num_update_flushes, 1);
		}		

		// if ((successCAS) || (updatedNextNode == succ) || (!IS_CLEAN(updatedNext)) || (old != updatedNextNode)) {
		// 	CAS(&node->old, old, NULL);
		// }		
		CAS(&node->old, old, NULL);

		if (successCAS) {
#ifdef RECORDMANAGER_PMEM_SUPPORT
			recmgr->retire(tid, old);
#else 		
#endif	
		}

		return successCAS;
	}

	bool helpOld_FromSearch(int tid, Node<K,V>* node, Node<K,V>* old){		
		bool successCAS = false;

		//get the successor from the old node 
		Node<K,V>* succ = PTR_TO_NODE(old->next);

		if (CAS(&node->next, MARK_NODE_DFLAG(MARK_NODE_DURABLE(old)), succ)) {				
			successCAS = true;
		}			

		Node<K,V>* updatedNext = node->next;
		Node<K,V>* updatedNextNode = PTR_TO_NODE(updatedNext);		

		// if ((successCAS) || (updatedNextNode == succ && !IS_DURABLE(updatedNext))) {
		if (!IS_DURABLE(updatedNext)) {
			// persist(node, succ);
			persist(node, updatedNext);
			GSTATS_ADD(tid, num_search_flushes, 1);
		}		

		// if ((successCAS) || (updatedNextNode == succ) || (!IS_CLEAN(updatedNext)) || (old != updatedNextNode)) {
		// 	CAS(&node->old, old, NULL);
		// }		
		CAS(&node->old, old, NULL);

		if (successCAS) {
#ifdef RECORDMANAGER_PMEM_SUPPORT
			recmgr->retire(tid, old);
#else 		
#endif	
		}

		return successCAS;
	}

	void persist(Node<K,V>* node, Node<K,V>* next){		
		durableTools::FLUSH(&node->next);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif		
		if (IS_IFLAGGED(next)) {
			CAS(&node->next, next, MARK_NODE_DURABLE(PTR_TO_NODE(next)));				
		}
		else {
			CAS(&node->next, next, MARK_NODE_DURABLE(next));			
		}
	}

	void persistAndClean(Node<K,V>* node, Node<K,V>* next){		
		durableTools::FLUSH(&node->next);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif		
		CAS(&node->next, next, MARK_NODE_DURABLE(PTR_TO_NODE(next)));			
	}



public:
	List(int numThreads, K minKey, K maxKey, V reservedVal){		
#ifdef	RECORDMANAGER_PMEM_SUPPORT	
		recmgr = new RecordManager(numThreads);

		//TODO:: fix RecordManager then remove this later
		Node<K,V> * temp = recmgr->template allocate<Node<K,V>>(tid);
		recmgr->deallocate(0, temp);
#elif defined(USE_MEMKIND)
		int err = memkind_create_pmem(PMEM_POOL_PATH, PMEM_POOL_SIZE, &pmem_kind);
		if (err) {			
			printf("Error creating PMEM pool. Exiting\n");
			// print_err_message(err);
			exit(-1);
		}
#elif defined(USE_LIBVMMALLOC)
		alloc = new AllocatorLibvmmalloc_Simple();
		alloc->init();
#endif
		

		noVal = reservedVal;

		tail = allocNewNode(0, maxKey, 0, NULL);
		tail->old = NULL;
		head = allocNewNode(0, minKey, 0, (MARK_NODE_DURABLE(tail)));		
		head->old = NULL;
	}

	~List(){
	}

	Node<K,V> * getHead(){
		return head;
	}

	void initThread(int tid){
#ifdef RECORDMANAGER_PMEM_SUPPORT	
		recmgr->initThread(tid);
#endif

	}

	void deinitThread(int tid){
#ifdef RECORDMANAGER_PMEM_SUPPORT	
		recmgr->deinitThread(tid);
#endif		
	}

	bool containsPersistAll(int tid, K key) {		
#ifdef RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;
		Node<K,V>* old;

		parent = head;		
		parentNextPtr = head->next;
		curr = PTR_TO_NODE(parentNextPtr);			 		
		
		while (true) {					
			if (!IS_DURABLE(parentNextPtr)) {				
				persist(parent, parentNextPtr);
				old = parent->old;
				parentNextPtr = parent->next;
				
				if (PTR_TO_NODE(old) != PTR_TO_NODE(parentNextPtr) && IS_DURABLE(parentNextPtr)) {
					CAS(&parent->old, old, NULL);
				}
				GSTATS_ADD(tid, num_contains_flushes, 1);
			}

			if (curr->key >= key) {
				break;
			}

			parent = curr;			
			parentNextPtr = parent->next;
			curr = PTR_TO_NODE(parentNextPtr);			
		}
			
		return curr->key == key;
	}

	bool containsPersistLast(int tid, K key) {		
#ifdef RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;
		
		parent = head;		
		parentNextPtr = head->next;
		curr = PTR_TO_NODE(parentNextPtr);			 		

		while (true) {							
			if (curr->key >= key) {
				break;
			}

			parent = curr;			
			parentNextPtr = parent->next;
			curr = PTR_TO_NODE(parentNextPtr);			
		}
					

		if (!IS_DURABLE(parentNextPtr)) {			
			persist(parent, parentNextPtr);
			Node<K,V>* old = parent->old;
			parentNextPtr = parent->next;
			
			if (PTR_TO_NODE(old) != PTR_TO_NODE(parentNextPtr) && IS_DURABLE(parentNextPtr)) {
				CAS(&parent->old, old, NULL);
			}			
			GSTATS_ADD(tid, num_contains_flushes, 1);
		}
			
		return curr->key == key;
	}

	bool containsAsynchPersistAll(int tid, K key) {		
#ifdef RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;
		Node<K,V>* old;

		parent = head;		
		parentNextPtr = head->next;
		curr = PTR_TO_NODE(parentNextPtr);		

		flushedNodeRecord flushedNodes[MAX_FLUSHED_NODES];
		int flushedNodeCount = 0;	 		
		
		while (true) {					
			if (!IS_DURABLE(parentNextPtr)) {				
				if (flushedNodeCount >= MAX_FLUSHED_NODES) {
					setbench_error("Set a higher max flushed nodes or implement a less lazy solution Guy.");
				}

				flushedNodes[flushedNodeCount] = {parent, parentNextPtr};
				flushedNodeCount++;				
				durableTools::FLUSH(&parent->next);	
			}

			if (curr->key >= key) {
				break;
			}

			parent = curr;			
			parentNextPtr = parent->next;
			curr = PTR_TO_NODE(parentNextPtr);			
		}

		if (flushedNodeCount > 0) {
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
			durableTools::SFENCE();							
#endif
			GSTATS_ADD(tid, num_contains_flushes, 1);
		

			for (int i = 0; i < flushedNodeCount; i++){
				if (flushedNodes[i].node->next == flushedNodes[i].nodeNext){
					//persist(flushedNodes[i].node, flushedNodes[i].nodeNext);
					if (IS_IFLAGGED(flushedNodes[i].nodeNext)) {
						CAS(&flushedNodes[i].node->next , flushedNodes[i].nodeNext, MARK_NODE_DURABLE(PTR_TO_NODE(flushedNodes[i].nodeNext)));				
					}
					else {
						CAS(&flushedNodes[i].node->next , flushedNodes[i].nodeNext, MARK_NODE_DURABLE(flushedNodes[i].nodeNext));			
					}


					// old = flushedNodes[i].node->old;
					// flushedNodes[i].next = flushedNodes[i].node->next;
					// if (PTR_TO_NODE(old) != PTR_TO_NODE(parentNextPtr) && IS_DURABLE(parentNextPtr)) {
					// 	CAS(&parent->old, old, NULL);
					// }	
				}
			}
		}

		return curr->key == key;
	}

	bool containsNoFlush(int tid, K key) {
#ifdef RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif
		
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;
		
		parent = head;	
		parentNextPtr = head->next;
		curr = PTR_TO_NODE(parentNextPtr);		
		
		while (true) {
			if (curr->key >= key) {
				break;
			}
			
			parent = curr;									
			parentNextPtr = parent->next;
			curr = PTR_TO_NODE(parentNextPtr);								
		}


		if (IS_DURABLE(parentNextPtr)) {//durable
			return curr->key == key;				
		}
		else if (!IS_DURABLE(parentNextPtr) && IS_IFLAGGED(parentNextPtr)) { //not durable bc of insert
			//if curr has the key we are looking for or not we return false either way since
			//curr is not durable and iflagged
			return false;
		}
		else { //not durable bc of remove
			if (curr->key == key) { 
				//we can return true in this case because either the remove will be in NVM or it wont
				//in the case where the remove is persisted curr is in NVM since the link we followed
				//will be persisted
				//in the case where the remove is NOT persisted, then deletedNode->next must be durable, 
				//and must point to curr since we cannot mark a next ptr that is not durable
				return true;
			}
			else {
				Node<K,V>* oldNode1 = parent->old;
				Node<K,V>* parentNextPtr2 = parent->next;
				Node<K,V>* oldNode2 = parent->old; 
				
				if (parentNextPtr != parentNextPtr2 || oldNode1 != oldNode2 || (oldNode1 == oldNode2 && !oldNode1)) {
					//old ptr changed - the original ptr we read was durable at some time
					return false;
				}
				else {
					//old ptr and next ptr didnt change - this means oldNode1 is the old node we want
					return PTR_TO_NODE(oldNode1)->key == key;
				}
			}
		}
	}

	bool testContains(K key) {
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;
		
		parent = head;	
		parentNextPtr = head->next;
		curr = PTR_TO_NODE(parentNextPtr);		
		
		while (true) {
			if (curr->key >= key) {
				break;
			}
			
			parent = curr;									
			parentNextPtr = parent->next;
			curr = PTR_TO_NODE(parentNextPtr);								
		}

		Node<K,V>* currOld = curr->old;
		if (!IS_MARKED_FOR_DEL(currOld) && curr->key == key) {
			return true;
		}
		return false;
	}

	/**
	 *  
	*/
	V insertIfAbsent(int tid, K key, V value){
#ifdef RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif

		//retry loop
		while (true){
			searchResult result = search(tid, key);					
			Node<K,V>* parent = result.parent;		
			Node<K,V>* curr = result.curr;	
			Node<K,V>* gp = result.gp;

			if (curr->key == key) {
				return curr->value;
			}

			Node<K,V>* parentOld = parent->old;
			Node<K,V>* parentNext = parent->next;
			
			if (!IS_CLEAN(parentNext)) {
				helpUpdate(tid, gp, parent);
			}
			if (parentOld != NULL) {				
				helpOld_FromSearch(tid, parent, parentOld);
			}
			else {	
				Node<K,V>* newNode = allocNewNode(tid, key, value, MARK_NODE_DURABLE(curr));				
				Node<K,V>* iflagNewNode = MARK_NODE_IFLAG(newNode); 
				
				
				//assert(!testContains(key));
				if (CAS(&parent->next, MARK_NODE_DURABLE(curr), iflagNewNode)){					
					persistAndClean(parent, iflagNewNode);
					GSTATS_ADD(tid, num_update_flushes, 1);
					return noVal;
				}
				else {		
#ifdef RECORDMANAGER_PMEM_SUPPORT
					recmgr->deallocate(tid, newNode);
#elif defined(USE_MEMKIND) 		
					memkind_free(pmem_kind, (void*)newNode);
#elif defined(USE_LIBVMMALLOC)
					alloc->deallocate(newNode);
#endif	
				}
			}
		} //end retry loop
	}





	/**
	 *  
	*/
	V remove(int tid, K key){
#ifdef RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif

		V retVal;

		//retry loop
		while (true){
			searchResult result = search(tid, key);		
			Node<K,V>* parent = result.parent;			
			Node<K,V>* curr = result.curr;
			Node<K,V>* gp = result.gp;
									
			if (curr->key != key) {
				return noVal;
			}

			retVal = curr->value;

			Node<K,V>* parentOld = parent->old;
			Node<K,V>* currOld = curr->old;
			
			Node<K,V>* parentNext = parent->next;
			Node<K,V>* currNext = curr->next;

			if (!IS_CLEAN(parentNext)) {
				helpUpdate(tid, gp, parent);
			}
			else if (parentOld != NULL) {
				helpOld_FromSearch(tid, parent, parentOld);
			}
			else if (!IS_CLEAN(currNext)) {
				helpUpdate(tid, parent, curr);
			}
			else if (currOld != NULL) {				
				helpOld_FromSearch(tid, curr, currOld);
			}			
			else {				
				Node<K,V>* flaggedCurr = MARK_NODE_DURABLE(MARK_NODE_DFLAG(curr));

				if (CAS(&parent->next, MARK_NODE_DURABLE(curr), flaggedCurr)) {					
					helpRemove(tid, parent, curr);
					return retVal;
				}
			}
		} //end retry loop
	}

	void printSummary(){
		// Node<K,V>* curr = head;
		// Node<K,V>* old = curr->old;

		// int count = 0;
		// while (curr != tail){
		// 	printf("Node: %p, next: %p, old: %p, key %llu\n", curr, curr->next, old, curr->key);			
		// 	curr = PTR_TO_NODE(curr->next);
		// 	old = curr->old;
		// 	count++;			
		// }
		// printf("COUNT=%d\n", count);
	}
};
