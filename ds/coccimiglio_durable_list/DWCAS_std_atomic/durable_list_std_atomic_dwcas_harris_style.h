#pragma once

/**
 * 
 * Harris style durable list with no-flush contains and recovery is O(n) for n keys
 * 
 * 
*/

#include "durable_tools.h"
#include <atomic>
#include <stdio.h>

#define MAX_FLUSHED_NODES 100

#define NODE_NEXT(nodePtr)			(PTR_TO_NODE(nodePtr->nextPair.load().next))


#define DURABLE_MASK                (0x1ll) //001
#define MARK_NODE_DURABLE(x)     	((Node<K,V>*)(((uintptr_t)(x)) | DURABLE_MASK))
#define IS_DURABLE(x)               (((uintptr_t)(x)) & DURABLE_MASK)

//Ignores durability bit
#define PTR_MASK					(0x6ll) //110

#define DEL_MASK 					(0x4ll) 
#define IS_MARKED_FOR_DEL(x)		((((uintptr_t)(x)) & PTR_MASK) == DEL_MASK)
#define MARK_NODE_DEL(x)			((Node<K,V> *)(((uintptr_t)(x)) | DEL_MASK))

#define TOTAL_MASK					(0x7ll) //111
#define PTR_TO_NODE(x)				((Node<K,V> *)((uintptr_t)(x) & ~TOTAL_MASK))

template <typename NodeType>
struct NodeNext {
	NodeType* next;
	NodeType* oldNode;
};


template<typename K, typename V>
struct Node{
	
	std::atomic<NodeNext<Node>> nextPair;	

	K key;
	V value;	
} __attribute__((aligned(32)));

template <typename K, typename V, class RecordManager>
class List {
private:
	struct searchResult{	
		Node<K,V> * parent;
		Node<K,V> * curr;
	};

	PAD;
	Node<K,V> * head;
	Node<K,V> * tail;
	PAD;	
	RecordManager * recmgr;
	V noVal;	
	PAD;

	Node<K,V> * allocNewNode(int tid, K key, V val, Node<K,V> *next){
		Node<K, V> * newNode = recmgr->template allocate<Node<K, V>>(tid);		
		
		newNode->key = key;
		newNode->value = val;

		newNode->nextPair.store(NodeNext<Node<K,V>>({next, NULL}));

			
		durableTools::FLUSH(&newNode);	
		// durableTools::FLUSH(&newNode->key);			
		// durableTools::FLUSH(&newNode->value);
		// durableTools::FLUSH(&newNode->nextPair);				

#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif

		GSTATS_ADD(tid, num_update_flushes, 1);


		return newNode;		
	}


	// it is expected that parent and curr have no marked bits set when passed here
	void trim(int tid, Node<K,V>* parent, Node<K,V>* curr){
		//assert(PTR_TO_NODE(curr) == curr);
		//assert(PTR_TO_NODE(parent) == parent);

		Node<K,V>* succ = PTR_TO_NODE(curr->nextPair.load().next);
		
		NodeNext<Node<K,V>> expNext = NodeNext<Node<K,V>>({MARK_NODE_DURABLE(curr), NULL});
		NodeNext<Node<K,V>> newNext = NodeNext<Node<K,V>>({succ, MARK_NODE_DURABLE(curr)});
		//NodeNext<Node<K,V>> newNext = NodeNext<Node<K,V>>({succ, NULL});

		//unlink
		if(parent->nextPair.compare_exchange_strong(expNext, newNext)){
			durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
			durableTools::SFENCE();							
#endif	
			expNext = newNext;
			newNext = NodeNext<Node<K,V>>({MARK_NODE_DURABLE(succ), NULL});
			parent->nextPair.compare_exchange_strong(expNext, newNext);

			recmgr->retire(tid, curr);		
			// return true;
		}
		// else {
		// 	return false;			
		// }				
	}


	/**
	 * Since deletes need the parent when helping we need to return the grandparent in this seach along with
	 * the parent and curr. This is because we help on the parent
	*/
	inline searchResult search(int tid, K key){
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;
		
		parent = head;
		NodeNext<Node<K,V>> parentNextPair = head->nextPair.load();
		parentNextPtr = parentNextPair.next;
		curr = PTR_TO_NODE(parentNextPtr);			 		

		while (true) {			
			if (IS_MARKED_FOR_DEL(curr->nextPair.load().next)){
				trim(tid, parent, curr);				
			}
			else {
				if (!IS_DURABLE(parentNextPtr)) {			
					durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
					durableTools::SFENCE();							
#endif							
					parentNextPtr = MARK_NODE_DURABLE(parentNextPtr);
					parent->nextPair.compare_exchange_strong(parentNextPair, NodeNext<Node<K,V>>({parentNextPtr, NULL}));
					
					GSTATS_ADD(tid, num_search_flushes, 1);				
				}

				if (curr->key >= key) {
					break;
				}

				parent = curr;
			}

			parentNextPair = curr->nextPair.load();
			parentNextPtr = parentNextPair.next;
			curr = PTR_TO_NODE(parentNextPtr);
			//curr = PTR_TO_NODE(curr->nextPair.load().next);			
		}
		
		//return searchResult({parent, markedCurr});
		return searchResult({parent, curr});
	}

public:
	List(int numThreads, K minKey, K maxKey, V reservedVal){		
		recmgr = new RecordManager(numThreads);

		noVal = reservedVal;

		tail = allocNewNode(0, maxKey, 0, NULL);
		head = allocNewNode(0, minKey, 0, MARK_NODE_DURABLE(tail));

		//assert(head->info.is_lock_free());
	}

	~List(){
	}

	Node<K,V> * getHead(){
		return head;
	}

	void initThread(int tid){
		recmgr->initThread(tid);
	}

	void deinitThread(int tid){
		recmgr->deinitThread(tid);
	}

	bool containsPersistAll(int tid, K key) {		
		auto guard = recmgr->getGuard(tid);

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;

		parent = head;
		NodeNext<Node<K,V>> parentNextPair = parent->nextPair.load();

		parentNextPtr = parentNextPair.next;
		curr = PTR_TO_NODE(parentNextPtr);
					
		while (true) {
			if (!IS_DURABLE(parentNextPtr)) {			
				durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
				durableTools::SFENCE();							
#endif			
				parentNextPtr = MARK_NODE_DURABLE(parentNextPtr);
				parent->nextPair.compare_exchange_strong(parentNextPair, NodeNext<Node<K,V>>({parentNextPtr, NULL}));
				
				GSTATS_ADD(tid, num_contains_flushes, 1);							
			}

			if (curr->key >= key) {
				break;
			}			
			
			parent = curr;						
			parentNextPair = parent->nextPair.load();
			
			parentNextPtr = parentNextPair.next;
			curr = PTR_TO_NODE(parentNextPtr);								
		}
			
		return curr->key == key;
	}

	bool containsPersistLast(int tid, K key) {		
		auto guard = recmgr->getGuard(tid);

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;

		parent = head;
		NodeNext<Node<K,V>> parentNextPair = parent->nextPair.load();

		parentNextPtr = parentNextPair.next;
		curr = PTR_TO_NODE(parentNextPtr);
					
		while (true) {
			if (curr->key >= key) {
				break;
			}			
			
			parent = curr;						
			parentNextPair = parent->nextPair.load();
			
			parentNextPtr = parentNextPair.next;
			curr = PTR_TO_NODE(parentNextPtr);								
		}

		if (!IS_DURABLE(parentNextPtr)) {			
			durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
			durableTools::SFENCE();							
#endif			
			//There are 2 ways we can leave a non-durable pointer in place during an update
			//		1 - an insert that completed the CAS but didnt set the durability bit
			//		2 - a delete that has completed the unlinking CAS (in helpMakred) but hasnt set the durability bit
			//		In both cases we should have a NULL oldNode after setting the durability bit
			parentNextPtr = MARK_NODE_DURABLE(parentNextPtr);
			parent->nextPair.compare_exchange_strong(parentNextPair, NodeNext<Node<K,V>>({parentNextPtr, NULL}));
			
			GSTATS_ADD(tid, num_contains_flushes, 1);							
		}
			
		return curr->key == key;
	}


	/**
	 *  
	*/
	V insertIfAbsent(int tid, K key, V value){
		auto guard = recmgr->getGuard(tid);

		//retry loop
		while (true){
			searchResult result = search(tid, key);					
			Node<K,V>* parent = result.parent;		
			Node<K,V>* curr = result.curr;									

			if (curr->key == key) {
				return curr->value;
			}

			
			Node<K,V>* newNode = allocNewNode(tid, key, value, MARK_NODE_DURABLE(curr));
			
			NodeNext<Node<K,V>> expNext = {MARK_NODE_DURABLE(curr), NULL};
			NodeNext<Node<K,V>> newNext = {newNode, NULL};

			if (parent->nextPair.compare_exchange_strong(expNext, newNext)){
				durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
				durableTools::SFENCE();							
#endif
				parent->nextPair.compare_exchange_strong(newNext, NodeNext<Node<K,V>>({MARK_NODE_DURABLE(newNode), NULL}));
				GSTATS_ADD(tid, num_update_flushes, 1);				
				return noVal;
			}
			else {
#if defined DEBUG_FAILED_CAS								
				GSTATS_ADD(tid, num_failed_insert_cas, 1);	
#endif					
				recmgr->deallocate(tid, newNode);
			}
			
		} //end retry loop
	}





	/**
	 *  
	*/
	V remove(int tid, K key){
		auto guard = recmgr->getGuard(tid);

		V retVal;

		//retry loop
		while (true){
			searchResult result = search(tid, key);		
			Node<K,V>* parent = result.parent;			
			Node<K,V>* curr = result.curr;

			//assert(PTR_TO_NODE(parent) == parent);
			//assert(PTR_TO_NODE(curr) == curr);										
									
			if (curr->key != key) {
				return noVal;
			}

			retVal = curr->value;
			
			Node<K,V>* succ = MARK_NODE_DURABLE(PTR_TO_NODE(curr->nextPair.load().next)); 			
			Node<K,V>* markedCurr = MARK_NODE_DEL(succ);

			NodeNext<Node<K,V>> expNext = {succ, NULL};	
			NodeNext<Node<K,V>> newNext = {markedCurr, NULL};

			if (curr->nextPair.compare_exchange_strong(expNext, newNext)){
				trim(tid, parent, curr);				
				return retVal;
			}
			else {					
#if defined DEBUG_FAILED_CAS								
				GSTATS_ADD(tid, num_failed_flag_cas, 1);	
#endif									
			}
			
		} //end retry loop
	}

	void printSummary(){
		
	}
};
