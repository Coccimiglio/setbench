#pragma once

/**
 * 
 * 
*/

#include "durable_tools.h"
#include <atomic>
#include <stdio.h>

#define MAX_FLUSHED_NODES 100

#define NODE_NEXT(nodePtr)			(PTR_TO_NODE(nodePtr->nextPair.load().next))

#define CAS(ptr, oldVal, newVal)	__sync_bool_compare_and_swap(ptr, oldVal, newVal)
#define VCAS(ptr, oldVal, newVal)	__sync_val_compare_and_swap(ptr, oldVal, newVal)


#define DURABLE_MASK                (0x1ll) //001
#define MARK_NODE_DURABLE(x)     	((Node<K,V> *)(((uintptr_t)(x)) | DURABLE_MASK))
#define IS_DURABLE(x)               (((uintptr_t)(x)) & DURABLE_MASK)


//Ignores durability bit
#define PTR_MASK					(0x6ll) //110

//clean = 00X
#define CLEAN_MASK					(0x0ll) //00X
#define IS_CLEAN(x)					((((uintptr_t)(x)) & PTR_MASK) == CLEAN_MASK)

//Dflag = 01X
#define DFLAG_MASK 					(0x2ll) //01X
#define IS_DFLAGGED(x)				((((uintptr_t)(x)) & PTR_MASK) == DFLAG_MASK)
#define MARK_NODE_DFLAG(x)			((Node<K,V> *)(((uintptr_t)(x)) | DFLAG_MASK))

// //IFlag = 10X
// #define IFLAG_MASK 					(0x4ll) //10X
// #define IS_IFLAGGED(x)				((((uintptr_t)(x)) & PTR_MASK) == IFLAG_MASK)
// #define MARK_NODE_IFLAG(x)			((Node<K,V> *)(((uintptr_t)(x)) | IFLAG_MASK))

#define UNFLAG_NODE(x)				((Node<K,V> *)(((uintptr_t)(x)) & ~PTR_MASK))

//DMark = 11X						
#define DEL_MASK 					(0x6ll) //11X - same as PTR_MASK
#define IS_MARKED_FOR_DEL(x)		((((uintptr_t)(x)) & PTR_MASK) == DEL_MASK)
#define MARK_NODE_DEL(x)			((Node<K,V> *)(((uintptr_t)(x)) | DEL_MASK))

#define TOTAL_MASK					(0x7ll) //111
#define PTR_TO_NODE(x)				((Node<K,V> *)((uintptr_t)(x) & ~TOTAL_MASK))


template <typename NodeType>
struct NodeNext {
	NodeType* next;
	NodeType* other;
};


template<typename K, typename V>
struct Node{
	
	std::atomic<NodeNext<Node<K,V>>> nextPair;	

	K key;
	V value;	
};

template <typename K, typename V, class RecordManager>
class List {
private:
	struct searchResult{			
		Node<K,V> * gp;	
		Node<K,V> * parent;
		Node<K,V> * curr;
	};

	struct flushedNodeRecord {
		Node<K,V>* node;
		NodeNext<Node<K,V>> nodeNext;		
	};


	PAD;	
	Node<K,V> * head;
	Node<K,V> * tail;
	PAD;
	
	RecordManager * recmgr;
	V noVal;	
	PAD;

	Node<K,V> * allocNewNode(int tid, K key, V val, Node<K,V> *next){
		Node<K, V> * newNode = recmgr->template allocate<Node<K, V>>(tid);
		
		newNode->key = key;
		newNode->value = val;

		newNode->nextPair.store(NodeNext<Node<K,V>>{next, NULL});

					
		// durableTools::FLUSH(&newNode);	
		// durableTools::FLUSH(&newNode->key);			
		// durableTools::FLUSH(&newNode->value);
		// durableTools::FLUSH(&newNode->nextPair);				

// #if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
// 		durableTools::SFENCE();							
// #endif

		// GSTATS_ADD(tid, num_update_flushes, 1);
		

		return newNode;		
	}



	/**
	 * Since deletes need the parent when helping we need to return the grandparent in this seach along with
	 * the parent and curr. This is because we help on the parent
	*/
	inline searchResult search(int tid, K key){		
		Node<K,V>* gp;
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;
		
		gp = NULL;
		parent = head;
		NodeNext<Node<K,V>> parentNextPair = parent->nextPair.load();

		parentNextPtr = parentNextPair.next;
		curr = PTR_TO_NODE(parentNextPtr);		
		
		

		while (true) {
			if (!IS_DURABLE(parentNextPtr)) {			
				durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
				durableTools::SFENCE();							
#endif			
				parentNextPtr = MARK_NODE_DURABLE(parentNextPtr);
				parent->nextPair.compare_exchange_strong(parentNextPair, NodeNext<Node<K,V>>({parentNextPtr, NULL}));
				
				GSTATS_ADD(tid, num_search_flushes, 1);				
			}

			if (curr->key >= key) {
				break;
			}
			
			gp = parent;
			parent = curr;						
			parentNextPair = parent->nextPair.load();
			
			parentNextPtr = parentNextPair.next;
			curr = PTR_TO_NODE(parentNextPtr);								
		}
		
		return {gp, parent, curr};
	}



	void helpUpdate(int tid, Node<K,V>* parent, Node<K,V>* dirtyNode) {
		// assert(PTR_TO_NODE(parent) == parent);
		// assert(PTR_TO_NODE(dirtyNode) == dirtyNode);

		Node<K,V>* dirtyNodeNextPtr = dirtyNode->nextPair.load().next;

		if (IS_DFLAGGED(dirtyNodeNextPtr)) {
			helpRemove(tid, dirtyNode, PTR_TO_NODE(dirtyNodeNextPtr));
		}
		else if (IS_MARKED_FOR_DEL(dirtyNodeNextPtr)) {
			helpMarked(tid, parent, dirtyNode);
		}
	}


	void helpRemove(int tid, Node<K,V>* parent, Node<K,V>* nodeToDel) {
		//assert(PTR_TO_NODE(parent) == parent && IS_CLEAN(parent));
		//assert(PTR_TO_NODE(nodeToDel) == nodeToDel && IS_CLEAN(nodeToDel));

		while (PTR_TO_NODE(parent->nextPair.load().next) == nodeToDel) {
			NodeNext<Node<K,V>> nodeToDelNext = nodeToDel->nextPair.load();
			Node<K,V>* nodeToDelNextPtr = nodeToDelNext.next;
			Node<K,V>* nodeToDelSucc = PTR_TO_NODE(nodeToDelNextPtr); 

			if (!IS_DURABLE(nodeToDelNextPtr)) {
				durableTools::FLUSH(&nodeToDel->nextPair);	
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
				durableTools::SFENCE();							
#endif					
				nodeToDelNextPtr = MARK_NODE_DURABLE(nodeToDelNextPtr);
				nodeToDel->nextPair.compare_exchange_strong(nodeToDelNext, NodeNext<Node<K,V>>({nodeToDelNextPtr, NULL}));

				GSTATS_ADD(tid, num_update_flushes, 1);			
			}


			NodeNext<Node<K,V>> expNext = NodeNext<Node<K,V>>({MARK_NODE_DURABLE(nodeToDelSucc), NULL});		
			NodeNext<Node<K,V>> newNext = NodeNext<Node<K,V>>({MARK_NODE_DURABLE(MARK_NODE_DEL(nodeToDelSucc)), NULL});		
					
			if (nodeToDel->nextPair.compare_exchange_strong(expNext, newNext)) {			
				this->helpMarked(tid, parent, nodeToDel);
				return;
			}
			else {
				if (IS_MARKED_FOR_DEL(expNext.next)){				
					this->helpMarked(tid, parent, nodeToDel);		
					return;
				}
				else if (IS_DFLAGGED(expNext.next)){
					helpRemove(tid, parent, nodeToDel);
				}				
				//else we failed bc nodeToDel->next was modified by an insert or a helper completed remove					
			}
		}			
	}

	void helpMarked(int tid, Node<K,V>* parent, Node<K,V>* markedNode) {
		// assert(PTR_TO_NODE(parent) == parent && IS_CLEAN(parent));
		// assert(PTR_TO_NODE(markedNode) == markedNode && IS_CLEAN(markedNode));


		Node<K,V>* markedNodeSucc = PTR_TO_NODE(markedNode->nextPair.load().next);
		NodeNext<Node<K,V>> expNext = NodeNext<Node<K,V>>({MARK_NODE_DURABLE(MARK_NODE_DFLAG(markedNode)), NULL});		
		NodeNext<Node<K,V>> newNext = NodeNext<Node<K,V>>({markedNodeSucc, MARK_NODE_DURABLE(MARK_NODE_DFLAG(markedNode))});				

		if (parent->nextPair.compare_exchange_strong(expNext, newNext)){
			durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
			durableTools::SFENCE();							
#endif			
			parent->nextPair.compare_exchange_strong(newNext, NodeNext<Node<K,V>>({MARK_NODE_DURABLE(markedNodeSucc), NULL}));
			
			recmgr->retire(tid, markedNode);
		}
	}


public:
	List(int numThreads, K minKey, K maxKey, V reservedVal){		
		recmgr = new RecordManager(numThreads);

		noVal = reservedVal;

		tail = new Node<K,V>();
		tail->key = maxKey;
		tail->value = 0;
		tail->nextPair.store(NodeNext<Node<K,V>>({NULL, NULL}));

		head = new Node<K,V>();
		head->key = minKey;
		head->value = 0;
		head->nextPair.store(NodeNext<Node<K,V>>({MARK_NODE_DURABLE(tail), NULL}));
	}

	~List(){
	}

	Node<K,V> * getHead(){
		return head;
	}

	void initThread(int tid){
		recmgr->initThread(tid);
	}

	void deinitThread(int tid){
		recmgr->deinitThread(tid);
	}

	bool containsPersistAll(int tid, K key) {				
		auto guard = recmgr->getGuard(tid);

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;

		parent = head;
		NodeNext<Node<K,V>> parentNextPair = head->nextPair.load();

		parentNextPtr = parentNextPair.next;
		curr = PTR_TO_NODE(parentNextPtr);
		
		
		while (true) {
			if (!IS_DURABLE(parentNextPtr)) {			
				durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
				durableTools::SFENCE();							
#endif			
				parentNextPtr = MARK_NODE_DURABLE(parentNextPtr);
				parent->nextPair.compare_exchange_strong(parentNextPair, NodeNext<Node<K,V>>({parentNextPtr, NULL}));
				
				GSTATS_ADD(tid, num_contains_flushes, 1);								
			}

			if (curr->key >= key) {
				break;
			}
			
			
			parent = curr;						
			parentNextPair = parent->nextPair.load();
			
			parentNextPtr = parentNextPair.next;
			curr = PTR_TO_NODE(parentNextPtr);								
		}
		
	
		return curr->key == key;
	}

	bool containsAsynchPersistAll(int tid, K key) {
		auto guard = recmgr->getGuard(tid);

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;

		parent = head;
		NodeNext<Node<K,V>> parentNextPair = head->nextPair.load();

		parentNextPtr = parentNextPair.next;
		curr = PTR_TO_NODE(parentNextPtr);
		
		flushedNodeRecord flushedNodes[MAX_FLUSHED_NODES];
		int flushedNodeCount = 0;

		while (true) {
			if (!IS_DURABLE(parentNextPtr)) {			
				if (flushedNodeCount >= MAX_FLUSHED_NODES) {
					setbench_error("Set a higher max flushed nodes or implement a less lazy solution Guy.");
				}

				flushedNodes[flushedNodeCount] = {parent, parentNextPair};
				flushedNodeCount++;				
				durableTools::FLUSH(&parent->nextPair);							
			}

			if (curr->key >= key) {
				break;
			}
			
			
			parent = curr;						
			parentNextPair = parent->nextPair.load();
			
			parentNextPtr = parentNextPair.next;
			curr = PTR_TO_NODE(parentNextPtr);								
		}
		
		if (flushedNodeCount > 0) {
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
			durableTools::SFENCE();							
#endif
			GSTATS_ADD(tid, num_contains_flushes, 1);
		

			for (int i = 0; i < flushedNodeCount; i++){
				if (flushedNodes[i].node->nextPair.load() == flushedNodes[i].nodeNext){
					Node<K,V>* durableNode = MARK_NODE_DURABLE(flushedNodes[i].nodeNext.next);

					flushedNodes[i].node->nextPair.compare_exchange_strong(flushedNodes[i].nodeNext, NodeNext<Node<K,V>>({durableNode, NULL}));
				}
			}
		}

		return curr->key == key;
	}

	bool containsPersistLast(int tid, K key) {
		auto guard = recmgr->getGuard(tid);

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;

		parent = head;
		NodeNext<Node<K,V>> parentNextPair = head->nextPair.load();

		parentNextPtr = parentNextPair.next;
		curr = PTR_TO_NODE(parentNextPtr);
		
		
		while (true) {			
			if (curr->key >= key) {
				break;
			}			
			
			parent = curr;						
			parentNextPair = parent->nextPair.load();
			
			parentNextPtr = parentNextPair.next;
			curr = PTR_TO_NODE(parentNextPtr);								
		}
		
		if (!IS_DURABLE(parentNextPtr)) {			
			durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
			durableTools::SFENCE();							
#endif			
			parentNextPtr = MARK_NODE_DURABLE(parentNextPtr);
			parent->nextPair.compare_exchange_strong(parentNextPair, NodeNext<Node<K,V>>({parentNextPtr, NULL}));
			
			GSTATS_ADD(tid, num_contains_flushes, 1);								
		}

		return curr->key == key;
	}

	bool containsNaivePersistAlways(int tid, K key) {		
		auto guard = recmgr->getGuard(tid);

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;

		parent = head;
		NodeNext<Node<K,V>> parentNextPair = head->nextPair.load();

		parentNextPtr = parentNextPair.next;
		curr = PTR_TO_NODE(parentNextPtr);
		
		
		while (true) {			
			durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
			durableTools::SFENCE();							
#endif										
			GSTATS_ADD(tid, num_contains_flushes, 1);								
			

			if (curr->key >= key) {
				break;
			}
			
			
			parent = curr;						
			parentNextPair = parent->nextPair.load();
			
			parentNextPtr = parentNextPair.next;
			curr = PTR_TO_NODE(parentNextPtr);								
		}
		
	
		return curr->key == key;
	}

	bool containsSpin(int tid, K key) {
		auto guard = recmgr->getGuard(tid);

		Node<K,V> * parent;			
		Node<K,V> * curr;			
		Node<K,V> * markedCurr;

		parent = head;			
		markedCurr = parent->next;
		curr = PTR_TO_NODE(markedCurr);
		
		 
		
		while (true) {									
			while (!IS_DURABLE(parent->nextPair.load().next)) {			
				//spin
			}

			if (curr->key >= key) {
				break;	
			}
			
			markedCurr = curr->next;
			curr = PTR_TO_NODE(markedCurr);				
		}

		return curr->key == key;
	}


	bool containsNoFlush(int tid, K key) {
		auto guard = recmgr->getGuard(tid);

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* parentNextPtr;

		parent = head;
		NodeNext<Node<K,V>> parentNextPair = head->nextPair.load();

		parentNextPtr = parentNextPair.next;
		curr = PTR_TO_NODE(parentNextPtr);
		
		
		while (true) {			
			if (curr->key >= key) {
				break;
			}			
			
			parent = curr;						
			parentNextPair = parent->nextPair.load();
			
			parentNextPtr = parentNextPair.next;
			curr = PTR_TO_NODE(parentNextPtr);								
		}
		
		if (curr->key == key) {
			if (!IS_DURABLE(markedCurr)) {
				//not durable but we found the key - must be from a pending insert
				return false;
			}
			else {
				return true;
			}
		}
		else {
			if (!IS_DURABLE(markedCurr)) {
				//not durable and didnt find the key - might be from pending delete
				Node<K,V>* oldNode = parentNextPair.other;
				if (oldNode) {
					bool wasRemoved = oldNode->key == key ? true : false;
					return wasRemoved;
				}
				else {
					//Old node is null this must be an unfinished insert
					return false;
				}				
			}
			else {
				//durable and didnt find the key
				return false;
			}			
		}
	}

	/**
	 *  
	*/
	V insertIfAbsent(int tid, K key, V value){
		auto guard = recmgr->getGuard(tid);

		//retry loop
		while (true){
			searchResult result = search(tid, key);					
			Node<K,V>* gp = result.gp;
			Node<K,V>* parent = result.parent;					
			Node<K,V>* curr = result.curr;
			
			Node<K,V>* parentNextPtr = parent->nextPair.load().next;			
			
			if (curr->key == key) {
				return curr->value;
			}			
			
			if (!IS_CLEAN(parentNextPtr)) { //parent is clean?
				helpUpdate(tid, gp, parent);
			}
			else {
				Node<K,V>* newNode = allocNewNode(tid, key, value, MARK_NODE_DURABLE(curr));
				
				NodeNext<Node<K,V>> expNext = NodeNext<Node<K,V>>{MARK_NODE_DURABLE(curr), NULL};
				NodeNext<Node<K,V>> newNext = NodeNext<Node<K,V>>{newNode, NULL};

				if (parent->nextPair.compare_exchange_strong(expNext, newNext)){
					durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
					durableTools::SFENCE();							
#endif			
					parent->nextPair.compare_exchange_strong(newNext, NodeNext<Node<K,V>>({MARK_NODE_DURABLE(newNode), NULL}));
					return noVal;
				}
				else {							
					recmgr->deallocate(tid, newNode);
					//helpUpdate(tid, gp, parent);
				}
			}
		} //end retry loop
	}





	/**
	 *  
	*/
	V remove(int tid, K key){
		auto guard = recmgr->getGuard(tid);

		V retVal;

		//retry loop
		while (true){
			searchResult result = search(tid, key);		
			Node<K,V>* gp = result.gp;
			Node<K,V>* parent = result.parent;						
			Node<K,V>* curr = result.curr;			

			Node<K,V>* parentNextPtr = parent->nextPair.load().next;
			Node<K,V>* currNextPtr = curr->nextPair.load().next;

			if (curr->key != key) {
				return noVal;
			}

			retVal = curr->value;
						

			if (!IS_CLEAN(currNextPtr)) {
				helpUpdate(tid, parent, curr); //curr clean?
			}
			else if (!IS_CLEAN(parentNextPtr)) { //parent is clean?
				helpUpdate(tid, gp, parent);
			}			
			else {
				Node<K,V>* flaggedParentPtr = MARK_NODE_DFLAG(MARK_NODE_DURABLE(curr));

				NodeNext<Node<K,V>> expNext = NodeNext<Node<K,V>>{MARK_NODE_DURABLE(curr), NULL};	
				NodeNext<Node<K,V>> newNext = NodeNext<Node<K,V>>{flaggedParentPtr, NULL};

				if (parent->nextPair.compare_exchange_strong(expNext, newNext)){
					helpRemove(tid, parent, curr);
					return retVal;					
				}
			}
		} //end retry loop
	}

	void printSummary(){		
		// printf("------- SUMMARY -------\n");

		// Node<K,V>* curr = PTR_TO_NODE(head->nextPair.load().next);
		// printf("Head: %p, nextPtr: %p, key: %d\n", head, head->nextPair.load().next, head->key);
		// while (curr != tail){
		// 	printf("Node: %p, nextPtr: %p, key: %d\n", curr, curr->nextPair.load().next, curr->key);
		// 	curr = PTR_TO_NODE(curr->nextPair.load().next);
		// }
		// printf("Tail: %p, nextPtr: %p, key: %d\n", tail, tail->nextPair.load().next, tail->key);
		// printf("------- END-SUMMARY -------\n\n");
	}
};
