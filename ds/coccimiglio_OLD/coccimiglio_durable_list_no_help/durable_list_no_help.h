#pragma once

/**
 *  
 * This implementation is nearly identical to the durable_lists_persist_all implementation
 * with minor adjustments to allow contains() operations to ignore persistence checking
 * 
 * TODO: we dont need to pass the NodeNext to the helper functions, we can just pass the op. This
 * 		will be a minor optimization if any at all (we will still require casting the op at least
 * 		once)
 * 
 * 
 * TODO: this needs to be updated to match the no-op version
 * 
*/

#include "durable_tools.h"
#include <atomic>


#define CAS(ptr, oldVal, newVal)	__sync_bool_compare_and_swap(ptr, oldVal, newVal)
#define VCAS(ptr, oldVal, newVal)	__sync_val_compare_and_swap(ptr, oldVal, newVal)


#define DURABLE_MASK                (0x1ll) //001
#define MARK_NODE_DURABLE(x)     	((Node<K,V>*)(((uintptr_t)(x)) | DURABLE_MASK))
#define IS_DURABLE(x)               (((uintptr_t)(x)) & DURABLE_MASK)


//Ignores durability bit
#define PTR_MASK					(0x6ll) //110

//clean = 00X
#define CLEAN_MASK					(0x0ll) //00X
#define IS_CLEAN(x)					((((uintptr_t)(x)) & PTR_MASK) == CLEAN_MASK)

//Dflag = 01X
#define DFLAG_MASK 					(0x2ll) //01X
#define IS_DFLAGGED(x)				((((uintptr_t)(x)) & PTR_MASK) == DFLAG_MASK)
#define MARK_NODE_DFLAG(x)			((Node<K,V> *)(((uintptr_t)(x)) | DFLAG_MASK))

//DMark = 11X
//#define DEL_MASK 					(PTR_MASK) 
//10X
#define DEL_MASK 					(0x4ll) 
#define IS_MARKED_FOR_DEL(x)		((((uintptr_t)(x)) & PTR_MASK) == DEL_MASK)
#define MARK_NODE_DEL(x)			((Node<K,V> *)(((uintptr_t)(x)) | DEL_MASK))

#define TOTAL_MASK					(0x7ll) //111
#define PTR_TO_NODE(x)				((Node<K,V> *)((uintptr_t)(x) & ~TOTAL_MASK))


struct NodeNext {
	void* next;
	void* op;
};


template<typename K, typename V>
struct Node{
	
	std::atomic<NodeNext> nextPair;	

	K key;
	V value;	
};

template <typename K, typename V, class RecordManager>
class [[deprecated]] List {
private:
	struct UpdateOp {
		void* parent;
		void* oldNode;
		void* succ;
	};

	struct searchResult{		
		Node<K,V> * parent;
		Node<K,V> * curr;
	};

	PAD;
	Node<K,V> * head;
	Node<K,V> * tail;
	PAD;
	
	RecordManager * recmgr;
	V noVal;	

	Node<K,V> * allocNewNode(int tid, K key, V val, Node<K,V> *next, bool persistNow){
		Node<K, V> * newNode = recmgr->template allocate<Node<K, V>>(tid);
		
		newNode->key = key;
		newNode->value = val;

		newNode->nextPair = {next, NULL};

		if (persistNow) {			
			durableTools::FLUSH(&newNode->key);
			durableTools::FLUSH(&newNode->value);
			durableTools::FLUSH(&newNode->nextPair);				

#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
			durableTools::SFENCE();							
#endif

			GSTATS_ADD(tid, num_update_flushes, 1);
		}

		return newNode;		
	}



	/**
	 * * This search is used by updates only - contains has its own search embedded in the function
	*/
	searchResult search(K key){
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* markedCurr;

		parent = head;
		NodeNext parentNextPair = parent->nextPair.load();

		curr = PTR_TO_NODE(parentNextPair.next);
		markedCurr = (Node<K,V>*)parentNextPair.next;
		
		

		while (true) {
			if (!IS_DURABLE(markedCurr)) {			
				durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
				durableTools::SFENCE();							
#endif				
				//There are 2 ways we can leave a non-durable pointer in place during an update
				//		1 - an insert that completed the CAS but didnt set the durability bit
				//		2 - a delete that has completed the unlinking CAS (in helpMakred) but hasnt set the durability bit
				//		In both cases we should have a NULL op after the durability bit is set
				Node<K,V>* durableCurr = MARK_NODE_DURABLE(markedCurr);
				parent->nextPair.compare_exchange_strong(parentNextPair, NodeNext({durableCurr, NULL}));
				GSTATS_ADD(tid, num_search_flushes, 1);

				markedCurr = durableCurr;
			}

			if (curr->key >= key) {
				break;
			}
			
			parent = curr;						
			parentNextPair = parent->nextPair.load();
			
			markedCurr = (Node<K,V>*)parentNextPair.next;
			curr = PTR_TO_NODE(markedCurr);								
		}

		return {parent, markedCurr};
	}

	void helpUpdate(NodeNext nextPair) {
		Node<K,V>* next = (Node<K,V>*)nextPair.next;
		if (IS_DFLAGGED(next)) {
			helpRemove(nextPair);
		}
		else if (IS_MARKED_FOR_DEL(next)) {
			helpMarked(nextPair);
		}
	}


	/**
	 * Performs the CAS that marks a node for deletion or if this CAS fails then the DFLAGGED
	 * 		parent is CAS'd back out of the DFLAGGED state
	 * 
	 * I dont think we have any ABA problems here since when we rollback the DFLAG CAS we do so
	 * with a double wide CAS where the latter word is an op pointer which is only ever used once.
	 * The op is deallocated if the parent is CAS'd out of DFLAGGED state
	 * 
	*/
	bool helpRemove(NodeNext nn) {
		
		UpdateOp* op = (UpdateOp*)nn.op;
		NodeNext oldNodeNextPair = ((Node<K,V>*)op->oldNode)->nextPair.load();
		NodeNext expectedNext = {oldNodeNextPair.next, NULL};

		//Marking a node deleted does not change the durability bit
		NodeNext newNext = {MARK_NODE_DEL(op->succ), op};

		if (((Node<K,V>*)op->oldNode)->nextPair.compare_exchange_strong(expectedNext, newNext)) {
			this->helpMarked(newNext);
			return true;
		}
		else {			
			if (((Node<K,V>*)op->parent)->nextPair.compare_exchange_strong(nn, NodeNext({op->oldNode, NULL}))) {
				recmgr->retire(tid, op);
			}
			
			return false;
		}
		
	}



	/**
	 * Performs the actual unlinking of a node marked for deletion
	 * 
	*/
	void helpMarked(NodeNext nn) {
		UpdateOp* op = (UpdateOp*)nn.op;
		NodeNext expectedNext = {op->oldNode, op};

		if (!IS_DURABLE(op->succ)) {
			durableTools::FLUSH(&((Node<K,V>*)op->oldNode)->nextPair);
			NodeNext oldNodeNext = ((Node<K,V>*)op->oldNode)->nextPair;
			((Node<K,V>*)op->oldNode)->nextPair.compare_exchange_strong(oldNodeNext, NodeNext{MARK_NODE_DURABLE(op->succ), NULL});

			GSTATS_ADD(tid, num_update_flushes, 1);			
		}


		//TODO - does saving succ cause problems here? I am worried that a delete can start on succ
		//Update to the above - I think were safe since to delete succ we would need to mark its part (oldNode)
		//which isnt possible until this delete is finished
		NodeNext newNext = {PTR_TO_NODE(op->succ), op};

		if (((Node<K,V>*)op->parent)->nextPair.compare_exchange_strong(expectedNext, newNext)){
			durableTools::FLUSH(&((Node<K,V>*)op->parent)->nextPair);
			
			((Node<K,V>*)op->parent)->nextPair.compare_exchange_strong(newNext, NodeNext{MARK_NODE_DURABLE(op->succ), NULL});

			GSTATS_ADD(tid, num_update_flushes, 1);
			
			recmgr->retire(tid, PTR_TO_NODE(op->oldNode));
			recmgr->retire(tid, op);
		}
	}


public:
	List(int numThreads, K minKey, K maxKey, V reservedVal){		
		recmgr = new RecordManager(numThreads);

		noVal = reservedVal;

		tail = allocNewNode(0, maxKey, 0, NULL, true);
		head = allocNewNode(0, minKey, 0, MARK_NODE_DURABLE(tail), true);

		//assert(head->info.is_lock_free());
	}

	~List(){
	}

	Node<K,V> * getHead(){
		return head;
	}

	void initThread(int tid){
		recmgr->initThread(tid);
	}

	void deinitThread(int tid){
		recmgr->deinitThread(tid);
	}

	bool contains(int tid, K key) {		
		auto guard = recmgr->getGuard(tid);

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* markedCurr;

		parent = head;
		NodeNext parentNextPair = parent->nextPair.load();

		curr = PTR_TO_NODE(parentNextPair.next);
		markedCurr = (Node<K,V>*)parentNextPair.next;
		
		while (curr->key <= key) {
			parent = curr;						
			parentNextPair = parent->nextPair.load();
			
			markedCurr = (Node<K,V>*)parentNextPair.next;
			curr = PTR_TO_NODE(markedCurr);								
		}

		if (curr->key == key) {
			if (!IS_DURABLE(markedCurr)) {
				//not durable but we found the key - must be from a pending insert
				return false;
			}
			else {
				return true;
			}
		}
		else {
			if (!IS_DURABLE(markedCurr)) {
				//not durable and didnt find the key - might be from pending delete
				if (parentNextPair.op) {
					//An unfinished remove because we have an non-null op
					UpdateOp * op = (UpdateOp *)parentNextPair.op;
					bool wasRemoved = ((Node<K,V>*)(op->oldNode))->key == key ? true : false;
					
					return wasRemoved;
				}
				else {
					//An unfinished insert
					return false;
				}				
			}
			else {
				//durable and didnt find the key
				return false;
			}			
		}
	}


	/**
	 *  
	*/
	V insertIfAbsent(int tid, K key, V value){
		auto guard = recmgr->getGuard(tid);

		//retry loop
		while (true){
			searchResult result = search(key);		
			Node<K,V> * parent = PTR_TO_NODE(result.parent);			
			Node<K,V> * markedCurr = result.curr;
			Node<K,V> * curr = PTR_TO_NODE(markedCurr);

			if (curr->key == key) {
				return curr->value;
			}

			if (!IS_CLEAN(markedCurr)) {				
				helpUpdate(parent->nextPair.load());
			}
			else {
				Node<K,V>* newNode = allocNewNode(tid, key, value, MARK_NODE_DURABLE(curr), true);

				NodeNext expectedNext = {markedCurr, NULL};	
				NodeNext newNext = {newNode, NULL};

				if (parent->nextPair.compare_exchange_strong(expectedNext, newNext)){	
					durableTools::FLUSH(&parent->nextPair);
					parent->nextPair.compare_exchange_strong(newNext, NodeNext({MARK_NODE_DURABLE(newNode), NULL}));
					GSTATS_ADD(tid, num_update_flushes, 1);	
								
					return noVal;
				}
				else {
					recmgr->deallocate(tid, newNode);
					helpUpdate(parent->nextPair.load());
				}
			}
		} //end retry loop
	}





	/**
	 *  
	*/
	V remove(int tid, K key){
		auto guard = recmgr->getGuard(tid);

		V retVal;

		//retry loop
		while (true){
			searchResult result = search(key);		
			Node<K,V>* parent = PTR_TO_NODE(result.parent);			
			Node<K,V>* markedCurr = result.curr;
			Node<K,V>* curr = PTR_TO_NODE(markedCurr);
			Node<K,V>* succ = PTR_TO_NODE(curr->nextPair.load().next);

			
						
			if (curr->key != key) {
				return noVal;
			}

			retVal = curr->value;

			if (!IS_CLEAN(markedCurr)) {
				helpUpdate(parent->nextPair.load());
			}
			else {
				UpdateOp* op = recmgr->template allocate<UpdateOp>(tid);
				op->parent = parent;
				op->oldNode = markedCurr;
				op->succ = succ;

				NodeNext expectedNext = {markedCurr, NULL};	

				//DFLAGGING a node does not change the durability bit
				NodeNext newNext = {MARK_NODE_DFLAG(markedCurr), op};

				if (parent->nextPair.compare_exchange_strong(expectedNext, newNext)){
					if (helpRemove(newNext)) {
						return retVal;
					}
				}
				else {
					recmgr->deallocate(tid, op);
					helpUpdate(parent->nextPair.load());
				}
			}
		} //end retry loop
	}
};