#pragma once

/**
 * 
 * This version of the no descriptors no-help list still uses a style similar to the Ellen BST
 * where for deletes you mark the parent of the node to be deleted and you mark
 * the node to be deleted. The other version only marks the node to be deleted which is maybe more 
 * like a harris version
 * 
*/

#include "durable_tools.h"
#include <atomic>


#define CAS(ptr, oldVal, newVal)	__sync_bool_compare_and_swap(ptr, oldVal, newVal)
#define VCAS(ptr, oldVal, newVal)	__sync_val_compare_and_swap(ptr, oldVal, newVal)


#define DURABLE_MASK                (0x1ll) //001
#define MARK_NODE_DURABLE(x)     	((Node<K,V>*)(((uintptr_t)(x)) | DURABLE_MASK))
#define IS_DURABLE(x)               (((uintptr_t)(x)) & DURABLE_MASK)


//Ignores durability bit
#define PTR_MASK					(0x6ll) //110

//clean = 00X
#define CLEAN_MASK					(0x0ll) //00X
#define IS_CLEAN(x)					((((uintptr_t)(x)) & PTR_MASK) == CLEAN_MASK)

//Dflag = 01X
#define DFLAG_MASK 					(0x2ll) //01X
#define IS_DFLAGGED(x)				((((uintptr_t)(x)) & PTR_MASK) == DFLAG_MASK)
#define MARK_NODE_DFLAG(x)			((Node<K,V> *)(((uintptr_t)(x)) | DFLAG_MASK))
#define UNMARK_NODE_DFLAG(x)		((Node<K,V> *)(((uintptr_t)(x)) | ~DFLAG_MASK))

//DMark = 11X
//#define DEL_MASK 					(PTR_MASK) 
//10X
#define DEL_MASK 					(0x4ll) 
#define IS_MARKED_FOR_DEL(x)		((((uintptr_t)(x)) & PTR_MASK) == DEL_MASK)
#define MARK_NODE_DEL(x)			((Node<K,V> *)(((uintptr_t)(x)) | DEL_MASK))

#define TOTAL_MASK					(0x7ll) //111
#define PTR_TO_NODE(x)				((Node<K,V> *)((uintptr_t)(x) & ~TOTAL_MASK))

struct NodeNext {
	void* next;
	void* oldNode;
};


template<typename K, typename V>
struct Node{
	
	std::atomic<NodeNext> nextPair;	

	K key;
	V value;	
} __attribute__((aligned(32)));

template <typename K, typename V, class RecordManager>
class [[deprecated]] List {
private:
	struct searchResult{	
		Node<K,V> * gp;	
		Node<K,V> * parent;
		Node<K,V> * curr;
	};

	PAD;
	Node<K,V> * head;
	Node<K,V> * tail;
	PAD;
	
	RecordManager * recmgr;
	V noVal;	

	Node<K,V> * allocNewNode(int tid, K key, V val, Node<K,V> *next, bool persistNow){
		Node<K, V> * newNode = recmgr->template allocate<Node<K, V>>(tid);
		
		newNode->key = key;
		newNode->value = val;

		newNode->nextPair = {next, NULL};

		if (persistNow) {		
			//if the fields of newNode are not guaranteed to be on the same cache line you need multiple
			//asynchronous flushes + 1 SFENCE
			durableTools::FLUSH(&newNode);	
			// durableTools::FLUSH(&newNode->key);			
			// durableTools::FLUSH(&newNode->value);
			// durableTools::FLUSH(&newNode->nextPair);				

#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
			durableTools::SFENCE();							
#endif

			GSTATS_ADD(tid, num_update_flushes, 1);
		}

		return newNode;		
	}



	/**
	 * Since deletes need the parent when helping we need to return the grandparent in this seach along with
	 * the parent and curr. This is because we help on the parent
	*/
	searchResult search(K key){
		Node<K,V>* gp;
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* markedCurr;

		gp = NULL;
		parent = head;
		NodeNext parentNextPair = parent->nextPair.load();

		curr = PTR_TO_NODE(parentNextPair.next);
		markedCurr = (Node<K,V>*)parentNextPair.next;
		
		

		while (true) {
			if (!IS_DURABLE(markedCurr)) {			
				durableTools::FLUSH(&parent->nextPair);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
				durableTools::SFENCE();							
#endif			
				//There are 2 ways we can leave a non-durable pointer in place during an update
				//		1 - an insert that completed the CAS but didnt set the durability bit
				//		2 - a delete that has completed the unlinking CAS (in helpMakred) but hasnt set the durability bit
				//		In both cases we should have a NULL oldNode after setting the durability bit
				Node<K,V>* durableCurr = MARK_NODE_DURABLE(markedCurr);
				parent->nextPair.compare_exchange_strong(parentNextPair, NodeNext({durableCurr, NULL}));
				
				GSTATS_ADD(tid, num_search_flushes, 1);				

				markedCurr = durableCurr;
			}

			if (curr->key >= key) {
				break;
			}
			
			gp = parent;
			parent = curr;						
			parentNextPair = parent->nextPair.load();
			
			markedCurr = (Node<K,V>*)parentNextPair.next;
			curr = PTR_TO_NODE(markedCurr);								
		}
		
		return {gp, parent, markedCurr};
	}


	/**
	 * When an update calls this function it passes the gp and parent based on the node that the updates
	 * search ended at. GP and parent might not be the best names for these nodes in this function since 
	 * the tyoe pending update we are helping is what determines how we know what the GP and parent are
	 * 
	 * For a remove we pass down the parent since the subsequent mark will need it
	 * For a marked we pass down the grand parent because helpUpdate was called off of the "parent"
	 * 		and if its marked we need to perform the CAS on its gp
	 * 
	 * The above is a digusting mess that just makes me want to rename stuff but Im leaving it for now
	 * 
	 * NOTE: we pass the NodeNext here because it is required for the CAS steps - this is because of the use
	 * 		of std::atomic
	 * 
	*/
	void helpUpdate(int tid, Node<K,V>* gp, Node<K,V>* parent) {

		//the parent's next ptr contains the actual bit we need to check (either DFLAG or marked)
		//if we remove the marks then this is also a pointer to parent successor which has its durability
		//confirmed by the search
		Node<K,V>* markedCurr = (Node<K,V>*)parent->nextPair.load().next;

		if (IS_DFLAGGED(markedCurr)) {
			helpRemove(tid, parent, markedCurr);
		}
		else if (IS_MARKED_FOR_DEL(markedCurr)) {
			helpMarked(tid, gp, parent);
		}
	}


	/**
	 * Performs the CAS that marks a node for deletion or if this CAS fails then the DFLAGGED
	 * 		parent is CAS'd back out of the DFLAGGED state
	*/
	bool helpRemove(int tid, Node<K,V>* parent, Node<K,V>* nodeToDel) {

		NodeNext nodeToDelNext = PTR_TO_NODE(nodeToDel)->nextPair.load();
		Node<K,V>* nodeToDelSucc = (Node<K,V>*)nodeToDelNext.next;

		//we dont need to check the durability of nodeToDelSucc because we will confirm it in the helpMarked function
		//so it is redudant here - we also don't care about the marked node trickling to NVM
		//since we can just remove the mark later

		Node<K,V>* markedNode = MARK_NODE_DEL(nodeToDelSucc);
		NodeNext expectedNext = {nodeToDelSucc, NULL};
		NodeNext newNext = {markedNode, NULL};

		if (nodeToDel->nextPair.compare_exchange_strong(expectedNext, newNext)) {
			this->helpMarked(tid, parent, nodeToDel);
			return true;
		}
		else {			
			//we failed to mark the node we want to remove, we must unflag the parent to guarantee lock freedom
			expectedNext = {nodeToDel, NULL};
			newNext = {UNMARK_NODE_DFLAG(nodeToDel), NULL};
			parent->nextPair.compare_exchange_strong(expectedNext, newNext);			
			
#if defined DEBUG_FAILED_CAS						
			GSTATS_ADD(tid, num_failed_mark_cas, 1);	
#endif			

			return false;
		}
		
	}



	/**
	 * Performs the actual unlinking of a node marked for deletion
	 * 
	 * IMPORTANT_NOTE: the parent that we pass to this function is the gp returned from search
	 * 
	*/
	void helpMarked(int tid, Node<K,V>* parent, Node<K,V>* markedNode) {
		
		NodeNext markedNodeNext = PTR_TO_NODE(markedNode)->nextPair.load();
		Node<K,V>* markedNodeSucc = (Node<K,V>*)markedNodeNext.next;

		if (!IS_DURABLE(markedNodeSucc)) {
			durableTools::FLUSH(&markedNode->nextPair);			
			markedNode->nextPair.compare_exchange_strong(markedNodeNext, NodeNext{MARK_NODE_DURABLE(markedNodeSucc), NULL});

			GSTATS_ADD(tid, num_update_flushes, 1);			
		}

		NodeNext expectedNext = {markedNode, NULL};
		NodeNext newNext = {markedNodeSucc, markedNode};

		if (parent->nextPair.compare_exchange_strong(expectedNext, newNext)){
			durableTools::FLUSH(&parent->nextPair);
			parent->nextPair.compare_exchange_strong(newNext, NodeNext{MARK_NODE_DURABLE(markedNodeSucc), NULL});

			//TODO do we need the PTR_TO_NODE here
			recmgr->retire(tid, PTR_TO_NODE(markedNode));
		}
		else {
#if defined DEBUG_FAILED_CAS						
			GSTATS_ADD(tid, num_failed_delete_cas, 1);	
#endif			
		}
	}


public:
	List(int numThreads, K minKey, K maxKey, V reservedVal){		
		recmgr = new RecordManager(numThreads);

		noVal = reservedVal;

		tail = allocNewNode(0, maxKey, 0, NULL, true);
		head = allocNewNode(0, minKey, 0, MARK_NODE_DURABLE(tail), true);

		//assert(head->info.is_lock_free());
	}

	~List(){
	}

	Node<K,V> * getHead(){
		return head;
	}

	void initThread(int tid){
		recmgr->initThread(tid);
	}

	void deinitThread(int tid){
		recmgr->deinitThread(tid);
	}

	bool contains(int tid, K key) {		
		auto guard = recmgr->getGuard(tid);

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* markedCurr;

		parent = head;
		NodeNext parentNextPair = parent->nextPair.load();

		curr = PTR_TO_NODE(parentNextPair.next);
		markedCurr = (Node<K,V>*)parentNextPair.next;
		
		while (curr->key <= key) {
			parent = curr;						
			parentNextPair = parent->nextPair.load();
			
			markedCurr = (Node<K,V>*)parentNextPair.next;
			curr = PTR_TO_NODE(markedCurr);								
		}

		if (curr->key == key) {
			if (!IS_DURABLE(markedCurr)) {
				//not durable but we found the key - must be from a pending insert
				return false;
			}
			else {
				return true;
			}
		}
		else {
			if (!IS_DURABLE(markedCurr)) {
				//not durable and didnt find the key - might be from pending delete
				if (parentNextPair.oldNode) {
					bool wasRemoved = ((Node<K,V>*)parentNextPair.oldNode)->key == key ? true : false;
					return wasRemoved;
				}
				else {
					//An unfinished insert
					return false;
				}				
			}
			else {
				//durable and didnt find the key
				return false;
			}			
		}
	}


	/**
	 *  
	*/
	V insertIfAbsent(int tid, K key, V value){
		auto guard = recmgr->getGuard(tid);

		//retry loop
		while (true){
			searchResult result = search(key);					
			Node<K,V>* parent = PTR_TO_NODE(result.parent);		
			Node<K,V>* markedCurr = result.curr;
			Node<K,V>* curr = PTR_TO_NODE(markedCurr);
			Node<K,V>* gp = result.gp;
			
			if (curr->key == key) {
				return curr->value;
			}

			if (!IS_CLEAN(markedCurr)) {
				helpUpdate(tid, gp, parent);
			}
			else {
				Node<K,V>* newNode = allocNewNode(tid, key, value, MARK_NODE_DURABLE(curr), true);
				
				NodeNext expectedNext = {markedCurr, NULL};
				NodeNext newNext = {newNode, NULL};

				if (parent->nextPair.compare_exchange_strong(expectedNext, newNext)){
					durableTools::FLUSH(&parent->nextPair);
					parent->nextPair.compare_exchange_strong(newNext, NodeNext({MARK_NODE_DURABLE(newNode), NULL}));
					GSTATS_ADD(tid, num_update_flushes, 1);
					return noVal;
				}
				else {
#if defined DEBUG_FAILED_CAS								
					GSTATS_ADD(tid, num_failed_insert_cas, 1);	
#endif					

					recmgr->deallocate(tid, newNode);
					//helpUpdate(tid, gp, parent);
				}
			}
		} //end retry loop
	}





	/**
	 *  
	*/
	V remove(int tid, K key){
		auto guard = recmgr->getGuard(tid);

		V retVal;

		//retry loop
		while (true){
			searchResult result = search(key);		
			Node<K,V>* parent = PTR_TO_NODE(result.parent);			
			Node<K,V>* markedCurr = result.curr;
			Node<K,V>* curr = PTR_TO_NODE(markedCurr);
			Node<K,V>* gp = result.gp;
									
			if (curr->key != key) {
				return noVal;
			}

			retVal = curr->value;
			
			if (!IS_CLEAN(markedCurr)) {
				helpUpdate(tid, gp, parent);
			}
			else {
				NodeNext expectedNext = {markedCurr, NULL};	

				Node<K,V>* flaggedCurr = MARK_NODE_DFLAG(markedCurr);
				NodeNext newNext = {flaggedCurr, NULL};

				if (parent->nextPair.compare_exchange_strong(expectedNext, newNext)){
					if (helpRemove(tid, parent, flaggedCurr)) {
						return retVal;
					}
				}
				else {					
#if defined DEBUG_FAILED_CAS								
					GSTATS_ADD(tid, num_failed_flag_cas, 1);	
#endif					

					//helpUpdate(tid, gp, parent);
				}
			}
		} //end retry loop
	}
};
