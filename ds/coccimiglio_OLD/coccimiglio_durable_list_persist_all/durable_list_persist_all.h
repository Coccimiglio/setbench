#pragma once

/**
 * 
 * This version of the no descriptors no-help list still uses a style similar to the Ellen BST
 * where for deletes you mark the parent of the node to be deleted and you mark
 * the node to be deleted. The other version only marks the node to be deleted which is maybe more 
 * like a harris version
 * 
*/

#include "durable_tools.h"
#include <atomic>


#define CAS(ptr, oldVal, newVal)	__sync_bool_compare_and_swap(ptr, oldVal, newVal)
#define VCAS(ptr, oldVal, newVal)	__sync_val_compare_and_swap(ptr, oldVal, newVal)


#define DURABLE_MASK                (0x1ll) //001
#define MARK_NODE_DURABLE(x)     	((Node<K,V>*)(((uintptr_t)(x)) | DURABLE_MASK))
#define IS_DURABLE(x)               (((uintptr_t)(x)) & DURABLE_MASK)


//Ignores durability bit
#define PTR_MASK					(0x6ll) //110

//clean = 00X
#define CLEAN_MASK					(0x0ll) //00X
#define IS_CLEAN(x)					((((uintptr_t)(x)) & PTR_MASK) == CLEAN_MASK)

//Dflag = 01X
#define DFLAG_MASK 					(0x2ll) //01X
#define IS_DFLAGGED(x)				((((uintptr_t)(x)) & PTR_MASK) == DFLAG_MASK)
#define MARK_NODE_DFLAG(x)			((Node<K,V> *)(((uintptr_t)(x)) | DFLAG_MASK))
#define UNMARK_NODE_DFLAG(x)		((Node<K,V> *)(((uintptr_t)(x)) | ~DFLAG_MASK))

//DMark = 11X
//#define DEL_MASK 					(PTR_MASK) 
//10X
#define DEL_MASK 					(0x4ll) 
#define IS_MARKED_FOR_DEL(x)		((((uintptr_t)(x)) & PTR_MASK) == DEL_MASK)
#define MARK_NODE_DEL(x)			((Node<K,V> *)(((uintptr_t)(x)) | DEL_MASK))

#define TOTAL_MASK					(0x7ll) //111
#define PTR_TO_NODE(x)				((Node<K,V> *)((uintptr_t)(x) & ~TOTAL_MASK))


template<typename K, typename V>
struct Node{
	
	Node<K,V>* next;	

	K key;
	V value;	
};

template <typename K, typename V, class RecordManager>
class [[deprecated("Moved to durable_list_with_flush_helping")]] List {
private:
	struct searchResult{	
		Node<K,V> * gp;	
		Node<K,V> * parent;
		Node<K,V> * curr;
	};

	PAD;
	Node<K,V> * head;
	Node<K,V> * tail;
	PAD;
	
	RecordManager * recmgr;
	V noVal;	

	Node<K,V> * allocNewNode(int tid, K key, V val, Node<K,V> *next, bool persistNow){
		Node<K, V> * newNode = recmgr->template allocate<Node<K, V>>(tid);
		
		newNode->key = key;
		newNode->value = val;

		newNode->next = next;

		if (persistNow) {			
			durableTools::FLUSH(&newNode->key);
			durableTools::FLUSH(&newNode->value);
			durableTools::FLUSH(&newNode->next);				

#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
			durableTools::SFENCE();							
#endif

			GSTATS_ADD(tid, num_flushes, 1);
		}

		return newNode;		
	}



	/**
	 * Since deletes need the parent when helping we need to return the grandparent in this seach along with
	 * the parent and curr. This is because we help on the parent
	*/
	searchResult search(K key){
		Node<K,V>* gp;
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* markedCurr;

		gp = NULL;
		parent = head;
		markedCurr = parent->next;
		curr = PTR_TO_NODE(markedCurr);
		
		
		

		while (true) {
			if (!IS_DURABLE(markedCurr)) {			
				durableTools::FLUSH(&parent->next);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
				durableTools::SFENCE();							
#endif							
				CAS(&parent->next, markedCurr, MARK_NODE_DURABLE(markedCurr));

				GSTATS_ADD(tid, num_flushes, 1);
			}

			if (curr->key > key || curr == tail) {
				break;
			}
			
			gp = parent;
			parent = curr;											
			markedCurr = parent->next;
			curr = PTR_TO_NODE(markedCurr);
		}
		
		return {gp, parent, markedCurr};
	}


	/**
	 * When an update calls this function it passes the gp and parent based on the node that the updates
	 * search ended at. GP and parent might not be the best names for these nodes in this function since 
	 * the tyoe pending update we are helping is what determines how we know what the GP and parent are
	 * 
	 * For a remove we pass down the parent since the subsequent mark will need it
	 * For a marked we pass down the grand parent because helpUpdate was called off of the "parent"
	 * 		and if its marked we need to perform the CAS on its gp
	 * 
	 * The above is a digusting mess that just makes me want to rename stuff but Im leaving it for now
	 * 
	 * NOTE: we pass the NodeNext here because it is required for the CAS steps - this is because of the use
	 * 		of std::atomic
	 * 
	*/
	void helpUpdate(Node<K,V>* gp, Node<K,V>* parent) {

		//the parent's next ptr contains the actual bit we need to check (either DFLAG or marked)
		//if we remove the marks then this is also a pointer to parent successor which has its durability
		//confirmed by the search
		Node<K,V>* parentNextPtr = parent->next;

		if (IS_DFLAGGED(parentNextPtr)) {
			helpRemove(parent, parentNextPtr);
		}
		else if (IS_MARKED_FOR_DEL(parentNextPtr)) {
			helpMarked(gp, parent);
		}
	}


	/**
	 * Performs the CAS that marks a node for deletion or if this CAS fails then the DFLAGGED
	 * 		parent is CAS'd back out of the DFLAGGED state
	*/
	bool helpRemove(Node<K,V>* parent, Node<K,V>* nodeToDel) {		
		Node<K,V>* nodeToDelSucc = (Node<K,V>*)nodeToDel->next;

		//we dont need to check the durability of nodeToDelSucc because we will confirm it in the helpMarked function
		//so it is redudant here 

		if (CAS(&nodeToDel->next, nodeToDelSucc, markedNode)) {
			this->helpMarked(parent, nodeToDel);
			return true;
		}
		else {			
			//we failed to mark the node we want to remove, we must unflag the parent to guarantee lock freedom						
			CAS(&parent->next, nodeToDel, UNMARK_NODE_DFLAG(nodeToDel))

			return false;
		}
		
	}



	/**
	 * Performs the actual unlinking of a node marked for deletion
	 * 
	 * IMPORTANT_NOTE: the parent that we pass to this function is the gp returned from search
	 * 
	*/
	void helpMarked(Node<K,V>* parent, Node<K,V>* markedNode) {
	
		Node<K,V>* markedNodeSucc = markedNode->next;

		if (!IS_DURABLE(markedNodeSucc)) {
			durableTools::FLUSH(&markedNode->nextPair);			
			CAS(&markedNode->next, markedNodeSucc, MARK_NODE_DURABLE(markedNodeSucc));

			GSTATS_ADD(tid, num_flushes, 1);			
		}

		if (CAS(&parent->next, markedNodeSucc, markedNode)){
			durableTools::FLUSH(&parent->nextPair);
			CAS(&parent->next, markedNodeSucc, MARK_NODE_DURABLE(markedNodeSucc));

			recmgr->retire(tid, markedNode);
		}
	}


public:
	List(int numThreads, K minKey, K maxKey, V reservedVal){		
		recmgr = new RecordManager(numThreads);

		noVal = reservedVal;

		tail = allocNewNode(0, maxKey, 0, NULL, true);
		head = allocNewNode(0, minKey, 0, MARK_NODE_DURABLE(tail), true);

		//assert(head->info.is_lock_free());
	}

	~List(){
	}

	Node<K,V> * getHead(){
		return head;
	}

	void initThread(int tid){
		recmgr->initThread(tid);
	}

	void deinitThread(int tid){
		recmgr->deinitThread(tid);
	}

	bool contains(int tid, K key) {		
		searchResult result = search(key);
		Node<K,V>* curr = PTR_TO_NODE(result.curr);		

		return curr->key == key;
	}

	/**
	 *  
	*/
	V insertIfAbsent(int tid, K key, V value){
		auto guard = recmgr->getGuard(tid);

		//retry loop
		while (true){
			searchResult result = search(key);					
			Node<K,V>* parent = PTR_TO_NODE(result.parent);		
			Node<K,V>* markedCurr = result.curr;
			Node<K,V>* curr = PTR_TO_NODE(markedCurr);
			Node<K,V>* gp = result.gp;
			
			if (curr->key == key) {
				return curr->value;
			}

			if (!IS_CLEAN(markedCurr)) {
				helpUpdate(gp, parent);
			}
			else {
				Node<K,V>* newNode = allocNewNode(tid, key, value, MARK_NODE_DURABLE(curr), true);
				
				CAS(&parent->next, markedCurr, newNode)

				if (CAS(&parent->next, markedCurr, newNode)){
					durableTools::FLUSH(&parent->next);
					CAS(&parent->next, newNode, MARK_NODE_DURABLE(newNode));
					GSTATS_ADD(tid, num_flushes, 1);
					return noVal;
				}
				else {
					recmgr->deallocate(tid, newNode);
					helpUpdate(gp, parent);
				}
			}
		} //end retry loop
	}





	/**
	 *  
	*/
	V remove(int tid, K key){
		auto guard = recmgr->getGuard(tid);

		V retVal;

		//retry loop
		while (true){
			searchResult result = search(key);		
			Node<K,V>* parent = PTR_TO_NODE(result.parent);			
			Node<K,V>* markedCurr = result.curr;
			Node<K,V>* curr = PTR_TO_NODE(markedCurr);
			Node<K,V>* gp = result.gp;
									
			if (curr->key != key) {
				return noVal;
			}

			retVal = curr->value;
			
			if (!IS_CLEAN(markedCurr)) {
				helpUpdate(gp, parent);
			}
			else {
				Node<K,V>* flaggedCurr = MARK_NODE_DFLAG(markedCurr);				

				if (CAS(&parent->next, markedCurr, flaggedCurr)){
					if (helpRemove(parent, flaggedCurr)) {
						return retVal;
					}
				}
				else {					
					helpUpdate(gp, parent);
				}
			}
		} //end retry loop
	}
};
