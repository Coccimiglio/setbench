#pragma once

/**
 * 
 * In this algorithm searches conditionally help persist all pointers they follow
 * 
 * This is a Harris style list where searches need to help with updates since updates can leave deleted nodes in the DS
 * 
 * TODO: deletes need to use marking since were using CAS now
 * 
 * TODO:: minor optimization in searches - we return a marked parent and successor (we dont actually use the marked parent or successor)
 * 		so we can just remove the code for this - I left it for now incase I want it later and for simplicity (no special cases right now)
 * 
*/

#include "durable_tools.h"

#define MAX_FLUSHED_NODES 100

#define DURABLE_MASK                (0x1ll)
#define MARK_NODE_DURABLE(x)     	((Node<K,V> *)(((uintptr_t)(x)) | DURABLE_MASK))
#define IS_DURABLE(x)               (((uintptr_t)(x)) & DURABLE_MASK)

#define DELETED_MASK				(0x2ll)
#define MARK_NODE_DELETED(x)		((Node<K,V> *)(((uintptr_t)(x)) | DELETED_MASK))
#define IS_DELETED(x)				(((uintptr_t)(x)) & DELETED_MASK)

#define TOTAL_MASK					(0x3ll)
#define PTR_TO_NODE(x)				((Node<K,V> *)((uintptr_t)(x) & ~TOTAL_MASK))

#define CAS(ptr, oldVal, newVal)	__sync_bool_compare_and_swap(ptr, oldVal, newVal)

template<typename K, typename V>
struct Node{		
	K key;
	V value;
	Node<K,V> * volatile next;	
};

template <typename K, typename V, class RecordManager>
class [[deprecated ("Moved to durable_list_with_flush_helping")]] List {
private:
	struct searchResult{		
		Node<K,V> * parent;
		Node<K,V> * curr;
		Node<K,V> * succ;
	};

	struct flushedNodeRecord {
		Node<K,V>* node;
		Node<K,V>* nodeNext;		
	};

	Node<K,V> * head;
	Node<K,V> * tail;
	RecordManager * recmgr;
	V noVal;	

	Node<K,V> * allocNewNode(int tid, K key, V val, Node<K,V> *next, bool persistNow){
		Node<K, V> * newNode = recmgr->template allocate<Node<K, V>>(tid);
		
		newNode->key = key;
		newNode->value = val;
		newNode->next = next;

		if (persistNow) {			
#ifdef USE_CLFLUSHOPT	
			durableTools::CLFLUSHOPT(&newNode->key);
			durableTools::CLFLUSHOPT(&newNode->value);
			durableTools::CLFLUSHOPT(&newNode->next);
			
			durableTools::SFENCE();			
#else
	#ifdef USE_CLWB
			durableTools::CLWB(&newNode->key);
			durableTools::CLWB(&newNode->value);
			durableTools::CLWB(&newNode->next);
			
			durableTools::SFENCE();			
	#else 
			durableTools::FLUSH(&newNode->key);
			durableTools::FLUSH(&newNode->value);
			durableTools::FLUSH(&newNode->next);					
	#endif
#endif
			GSTATS_ADD(tid, num_flushes, 1);
		}

		return newNode;		
	}

	
	searchResult search(K key, bool includeSuccessor = false){
		Node<K,V> * parent;
		Node<K,V> * curr;	
		Node<K,V> * succ;
		
		parent = head;	
		curr = PTR_TO_NODE(head->next);					
		succ = head; //initializing to head just so we dont get weird errors where we dont want succ

		Node<K,V> * markedCurr = parent->next;				
		
		 
		flushedNodeRecord flushedNodes[MAX_FLUSHED_NODES];
		int flushedNodeCount = 0;

		while (curr->key <= key && curr != tail) {									
			if (parent != NULL && !IS_DURABLE(markedCurr)) {			
				if (flushedNodeCount >= MAX_FLUSHED_NODES) {
					setbench_error("Set a high max flushed nodes or implement a less lazy solution Guy.");
				}

				flushedNodes[flushedNodeCount] = {parent, markedCurr};
				flushedNodeCount++;				
				durableTools::CLFLUSHOPT(&parent->next);
			}

			parent = curr;			
			markedCurr = parent->next;
			curr = PTR_TO_NODE(markedCurr);				
		} 
		

		if (includeSuccessor) {		
			//just resuing the old poorly named stack variable for succ
			markedCurr = curr->next;
			succ = PTR_TO_NODE(markedCurr);
			if (!IS_DURABLE(markedCurr)){
				flushedNodes[flushedNodeCount] = {curr, markedCurr};
				flushedNodeCount++;				
				durableTools::CLFLUSHOPT(&curr->next);
			}
		}

		durableTools::SFENCE();

		for (int i = 0; i < flushedNodeCount; i++){
			if (flushedNodes[i].node->next == flushedNodes[i].nodeNext){
				Node<K,V>* markedNode = MARK_NODE_DURABLE(flushedNodes[i].nodeNext);

				//we dont care about failing these CAS's			
				bool sucess = CAS(&flushedNodes[i].node->next, flushedNodes[i].nodeNext, markedNode);
				
				if (flushedNodes[i].node->next == curr) {
					markedCurr = markedNode;
					continue; 
				}
			}
		}

		return {parent, markedCurr, succ};
	}


public:
	List(int numThreads, K minKey, K maxKey, V reservedVal){
		recmgr = new RecordManager(numThreads);

		noVal = reservedVal;

		tail = allocNewNode(0, maxKey, 0, NULL, true);
		head = allocNewNode(0, minKey, 0, MARK_NODE_DURABLE(tail), true);
	}

	~List(){
	}

	Node<K,V> * getHead(){
		return head;
	}

	void initThread(int tid){
		recmgr->initThread(tid);
	}

	void deinitThread(int tid){
		recmgr->deinitThread(tid);
	}

	bool contains(int tid, K key) {	
		auto guard = recmgr->getGuard(tid);	
		Node<K,V> * curr = PTR_TO_NODE(search(key).curr);	
		
		if (curr->key == key){
			return true;
		}
		else {		
			return false;
		}
	}


	/**
	 *  
	*/
	V insertIfAbsent(int tid, K key, V value){
		auto guard = recmgr->getGuard(tid);

		//retry loop
		while (true){
			searchResult result = search(key);		
			Node<K,V> * parent = PTR_TO_NODE(result.parent);
			Node<K,V> * curr = PTR_TO_NODE(result.curr);

			Node<K,V>* markedCurr = result.curr;			

			if (curr->key == key) {
				return curr->value;
			}

			Node<K,V>* newNode = allocNewNode(tid, key, value, MARK_NODE_DURABLE(curr), true);

			if (CAS(&parent->next, markedCurr, newNode)) {
				durableTools::FLUSH(&parent->next);
				CAS(&parent->next, newNode, MARK_NODE_DURABLE(newNode));

				return noVal;
			}
		} //end retry loop
	}





	/**
	 *  
	*/
	V remove(int tid, K key){
		auto guard = recmgr->getGuard(tid);

		V retVal;

		//retry loop
		while (true){
			searchResult result = search(key, true);		
			Node<K,V> * parent = PTR_TO_NODE(result.parent);
			Node<K,V> * curr = PTR_TO_NODE(result.curr);
			Node<K,V> * succ = PTR_TO_NODE(result.succ);

			Node<K,V>* markedCurr = result.curr;

			if (curr->key != key) {
				return noVal;
			}

			retVal = curr->value;

			Node<K,V> * deletedCurr = MARK_NODE_DELETED(curr);

			if (CAS(&parent->next, markedCurr, deletedCurr)){				
				durableTools::FLUSH(&parent->next);
				CAS(&parent->next, deletedCurr, MARK_NODE_DURABLE(deletedCurr));
				return retVal;
			}
		} //end retry loop
	}
};