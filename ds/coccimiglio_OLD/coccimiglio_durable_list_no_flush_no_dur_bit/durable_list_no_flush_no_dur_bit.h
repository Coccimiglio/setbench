#pragma once

/**
 * 
 * This is just for some experiment testing - this list is not durable
 * 
*/

#include <atomic>

#define MAX_FLUSHED_NODES 100

#define NODE_NEXT(nodePtr)			(PTR_TO_NODE(nodePtr->nextPair.load().next))

#define CAS(ptr, oldVal, newVal)	__sync_bool_compare_and_swap(ptr, oldVal, newVal)
#define VCAS(ptr, oldVal, newVal)	__sync_val_compare_and_swap(ptr, oldVal, newVal)


//Ignores durability bit
#define PTR_MASK					(0x6ll) //110

//clean = 00X
#define CLEAN_MASK					(0x0ll) //00X
#define IS_CLEAN(x)					((((uintptr_t)(x)) & PTR_MASK) == CLEAN_MASK)

//Dflag = 01X
#define DFLAG_MASK 					(0x2ll) //01X
#define IS_DFLAGGED(x)				((((uintptr_t)(x)) & PTR_MASK) == DFLAG_MASK)
#define MARK_NODE_DFLAG(x)			((Node<K,V> *)(((uintptr_t)(x)) | DFLAG_MASK))
#define UNMARK_NODE_DFLAG(x)		((Node<K,V> *)(((uintptr_t)(x)) | ~DFLAG_MASK))

//DMark = 11X
//#define DEL_MASK 					(PTR_MASK) 
//10X
#define DEL_MASK 					(0x4ll) 
#define IS_MARKED_FOR_DEL(x)		((((uintptr_t)(x)) & PTR_MASK) == DEL_MASK)
#define MARK_NODE_DEL(x)			((Node<K,V> *)(((uintptr_t)(x)) | DEL_MASK))

#define TOTAL_MASK					(0x7ll) //111
#define PTR_TO_NODE(x)				((Node<K,V> *)((uintptr_t)(x) & ~TOTAL_MASK))

struct NodeNext {
	void* next;
	void* oldNode;
};


template<typename K, typename V>
struct Node{
	
	std::atomic<NodeNext> nextPair;	

	K key;
	V value;	
};

template <typename K, typename V, class RecordManager>
class List {
private:
	struct searchResult{	
		Node<K,V> * gp;	
		Node<K,V> * parent;
		Node<K,V> * curr;
	};

	struct flushedNodeRecord {
		Node<K,V>* node;
		NodeNext nodeNext;		
	};

	PAD;
	Node<K,V> * head;
	Node<K,V> * tail;
	PAD;
	
	RecordManager * recmgr;
	V noVal;	

	Node<K,V> * allocNewNode(int tid, K key, V val, Node<K,V> *next, bool persistNow){
		Node<K, V> * newNode = recmgr->template allocate<Node<K, V>>(tid);
		
		newNode->key = key;
		newNode->value = val;

		newNode->nextPair = {next, NULL};

		return newNode;		
	}



	/**
	 * Since deletes need the parent when helping we need to return the grandparent in this seach along with
	 * the parent and curr. This is because we help on the parent
	*/
	searchResult search(K key){
		Node<K,V>* gp;
		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* markedCurr;

		gp = NULL;
		parent = head;
		NodeNext parentNextPair = parent->nextPair.load();

		curr = PTR_TO_NODE(parentNextPair.next);
		markedCurr = (Node<K,V>*)parentNextPair.next;
		
		while (true) {
			if (curr->key >= key) {
				break;
			}
			
			gp = parent;
			parent = curr;						
			parentNextPair = parent->nextPair.load();
			
			markedCurr = (Node<K,V>*)parentNextPair.next;
			curr = PTR_TO_NODE(markedCurr);								
		}
		
		return {gp, parent, markedCurr};
	}


	
	void helpUpdate(Node<K,V>* gp, Node<K,V>* parent) {
		Node<K,V>* markedCurr = (Node<K,V>*)parent->nextPair.load().next;

		if (IS_DFLAGGED(markedCurr)) {
			helpRemove(parent, markedCurr);
		}
		else if (IS_MARKED_FOR_DEL(markedCurr)) {
			helpMarked(gp, parent);
		}
	}


	/**
	 * Performs the CAS that marks a node for deletion or if this CAS fails then the DFLAGGED
	 * 		parent is CAS'd back out of the DFLAGGED state
	*/
	bool helpRemove(Node<K,V>* parent, Node<K,V>* nodeToDel) {

		NodeNext nodeToDelNext = PTR_TO_NODE(nodeToDel)->nextPair.load();
		Node<K,V>* nodeToDelSucc = (Node<K,V>*)nodeToDelNext.next;

		//we dont need to check the durability of nodeToDelSucc because we will confirm it in the helpMarked function
		//so it is redudant here - we also don't care about the marked node trickling to NVM
		//since we can just remove the mark later

		Node<K,V>* markedNode = MARK_NODE_DEL(nodeToDelSucc);
		NodeNext expectedNext = {nodeToDelSucc, NULL};
		NodeNext newNext = {markedNode, NULL};

		if (nodeToDel->nextPair.compare_exchange_strong(expectedNext, newNext)) {
			this->helpMarked(parent, nodeToDel);
			return true;
		}
		else {			
			//we failed to mark the node we want to remove, we must unflag the parent to guarantee lock freedom
			expectedNext = {nodeToDel, NULL};
			newNext = {UNMARK_NODE_DFLAG(nodeToDel), NULL};
			parent->nextPair.compare_exchange_strong(expectedNext, newNext);			
			
#if defined DEBUG_FAILED_CAS						
			GSTATS_ADD(tid, num_failed_mark_cas, 1);	
#endif			

			return false;
		}
		
	}



	/**
	 * Performs the actual unlinking of a node marked for deletion
	 * 
	 * IMPORTANT_NOTE: the parent that we pass to this function is the gp returned from search
	 * 
	*/
	void helpMarked(Node<K,V>* parent, Node<K,V>* markedNode) {
		
		NodeNext markedNodeNext = PTR_TO_NODE(markedNode)->nextPair.load();
		Node<K,V>* markedNodeSucc = (Node<K,V>*)markedNodeNext.next;

		NodeNext expectedNext = {markedNode, NULL};
		NodeNext newNext = {markedNodeSucc, markedNode};

		if (parent->nextPair.compare_exchange_strong(expectedNext, newNext)){
			recmgr->retire(tid, PTR_TO_NODE(markedNode));
		}
		else {
#if defined DEBUG_FAILED_CAS						
			GSTATS_ADD(tid, num_failed_delete_cas, 1);	
#endif			
		}
	}


public:
	List(int numThreads, K minKey, K maxKey, V reservedVal){		
		recmgr = new RecordManager(numThreads);

		noVal = reservedVal;

		tail = allocNewNode(0, maxKey, 0, NULL, true);
		head = allocNewNode(0, minKey, 0, tail, true);

		//assert(head->info.is_lock_free());
	}

	~List(){
	}

	Node<K,V> * getHead(){
		return head;
	}

	void initThread(int tid){
		recmgr->initThread(tid);
	}

	void deinitThread(int tid){
		recmgr->deinitThread(tid);
	}

	bool contains(int tid, K key) {				
		auto guard = recmgr->getGuard(tid);

		Node<K,V>* parent;
		Node<K,V>* curr;	
		Node<K,V>* markedCurr;

		parent = head;
		NodeNext parentNextPair = parent->nextPair.load();

		curr = PTR_TO_NODE(parentNextPair.next);
		markedCurr = (Node<K,V>*)parentNextPair.next;
		
		while (true) {
			if (curr->key >= key) {
				break;
			}
			
			parent = curr;						
			parentNextPair = parent->nextPair.load();
			
			markedCurr = (Node<K,V>*)parentNextPair.next;
			curr = PTR_TO_NODE(markedCurr);								
		}
		
	
		return curr->key == key;
	}


	/**
	 *  
	*/
	V insertIfAbsent(int tid, K key, V value){
		auto guard = recmgr->getGuard(tid);

		//retry loop
		while (true){
			searchResult result = search(key);					
			Node<K,V>* parent = PTR_TO_NODE(result.parent);		
			Node<K,V>* markedCurr = result.curr;
			Node<K,V>* curr = PTR_TO_NODE(markedCurr);
			Node<K,V>* gp = result.gp;
			
			if (curr->key == key) {
				return curr->value;
			}

			if (!IS_CLEAN(markedCurr)) {
				helpUpdate(gp, parent);
			}
			else {
				Node<K,V>* newNode = allocNewNode(tid, key, value, curr, true);
				
				NodeNext expectedNext = {markedCurr, NULL};
				NodeNext newNext = {newNode, NULL};

				if (parent->nextPair.compare_exchange_strong(expectedNext, newNext)){					
					return noVal;
				}
				else {
#if defined DEBUG_FAILED_CAS								
					GSTATS_ADD(tid, num_failed_insert_cas, 1);	
#endif					

					recmgr->deallocate(tid, newNode);
					helpUpdate(gp, parent);
				}
			}
		} //end retry loop
	}





	/**
	 *  
	*/
	V remove(int tid, K key){
		auto guard = recmgr->getGuard(tid);

		V retVal;

		//retry loop
		while (true){
			searchResult result = search(key);		
			Node<K,V>* parent = PTR_TO_NODE(result.parent);			
			Node<K,V>* markedCurr = result.curr;
			Node<K,V>* curr = PTR_TO_NODE(markedCurr);
			Node<K,V>* gp = result.gp;
									
			if (curr->key != key) {
				return noVal;
			}

			retVal = curr->value;
			
			if (!IS_CLEAN(markedCurr)) {
				helpUpdate(gp, parent);
			}
			else {
				NodeNext expectedNext = {markedCurr, NULL};	

				Node<K,V>* flaggedCurr = MARK_NODE_DFLAG(markedCurr);
				NodeNext newNext = {flaggedCurr, NULL};

				if (parent->nextPair.compare_exchange_strong(expectedNext, newNext)){
					if (helpRemove(parent, flaggedCurr)) {
						return retVal;
					}
				}
				else {					
#if defined DEBUG_FAILED_CAS								
					GSTATS_ADD(tid, num_failed_flag_cas, 1);	
#endif					

					helpUpdate(gp, parent);
				}
			}
		} //end retry loop
	}
};
