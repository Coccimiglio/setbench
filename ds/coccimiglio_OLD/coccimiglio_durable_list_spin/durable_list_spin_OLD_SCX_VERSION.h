#pragma once

/**
 * 
 * TODO: I think we should just use CAS for the durability marking. Using LLX/SCX adds
 * a large amount of complication that even in this simple data structure causes various problems
 * 
 * 
 * Note this is basically the same list as algorithm coccimiglio_durable_list
 * 
 * This algorithm uses searches that spin when they find a non-durable pointers during their traversal
 *  
*/

#include "scx_provider.h"
#include "durable_tools.h"

#define MAX_SCX_DEP 3

#define DURABLE_MASK                (0x1ll)
#define MARK_NODE_DURABLE(x)     	((Node<K,V> *)(((uintptr_t)(x)) | DURABLE_MASK))
#define IS_DURABLE(x)               (((uintptr_t)(x)) & DURABLE_MASK)
#define MARK_PTR_DURABLE(x)     	((Node<K,V> * volatile)(((uintptr_t)(x)) | DURABLE_MASK))

#define PTR_TO_NODE(x)				((Node<K,V> *)((uintptr_t)(x) & ~DURABLE_MASK))

#define CAS(ptr, oldVal, newVal)	__sync_bool_compare_and_swap(ptr, oldVal, newVal)

template<typename K, typename V>
struct Node{		
	K key;
	V value;
	Node<K,V> * volatile next;	
	scx_handle_t scxPtr;
	int marked;
};

template <typename K, typename V, class RecordManager>
class [[deprecated]] List {
private:
	struct searchResult{
		Node<K,V> * gp;
		Node<K,V> * p;
		Node<K,V> * curr;
	};

	Node<K,V> * head;
	Node<K,V> * tail;
	RecordManager * recmgr;
	V noVal;
	SCXProvider<Node<K,V>, MAX_SCX_DEP> * prov; 
	int threadFlushCounters[MAX_THREADS_POW2];

	Node<K,V> * allocNewNode(int tid, K key, V val, Node<K,V> *next, bool persistNow){
		Node<K, V> * newNode = recmgr->template allocate<Node<K, V>>(tid);
		prov->initNode(newNode);		
		newNode->key = key;
		newNode->value = val;
		newNode->next = next;

		if (persistNow) {			
			durableTools::CLFLUSHOPT(&newNode->key);
			durableTools::CLFLUSHOPT(&newNode->value);
			durableTools::CLFLUSHOPT(&newNode->next);
			
			//probably dont need to persist the scxPtr but might be useful for recovery?
			durableTools::CLFLUSHOPT(&newNode->scxPtr);

			durableTools::CLFLUSHOPT(&newNode->marked);
			durableTools::SFENCE();			
		}

		return newNode;		
	}


public:
	List(int numThreads, K minKey, K maxKey, V reservedVal){
		for (int i = 0; i < MAX_THREADS_POW2; i++){
			threadFlushCounters[i] = 0;
		}

		prov = new SCXProvider<Node<K,V>, MAX_SCX_DEP>(numThreads);
		recmgr = new RecordManager(numThreads);

		noVal = reservedVal;

		tail = allocNewNode(0, maxKey, 0, NULL, true);
		head = allocNewNode(0, minKey, 0, MARK_NODE_DURABLE(tail), true);
	}

	~List(){
		//TODO:: free memory u lazy scum - Guy

		int totalFlushes = 0;
		for (int i = 0; i < MAX_THREADS_POW2; i++){
			totalFlushes += threadFlushCounters[i];
		}

		printf("$$$$$$$$$$$$$$$ \n");
		printf("flush-count: %d", totalFlushes);
		printf("$$$$$$$$$$$$$$$ \n");
	}

	Node<K,V> * getHead(){
		return head;
	}

	void initThread(int tid){
		recmgr->initThread(tid);
	}

	void deinitThread(int tid){
		recmgr->deinitThread(tid);
	}

	bool contains(int tid, K key) {
		auto guard = recmgr->getGuard(tid);
		Node<K,V> * parent;
		Node<K,V> * curr;	

		parent = NULL;	
		curr = head;					

		Node<K,V> * next = NULL;
		
		do {			
			parent = curr;
			
			while (!IS_DURABLE(curr->next)) {
				//spin while curr->next is not durable
			}

			curr = PTR_TO_NODE(curr->next);

		} while (curr->key <= key && curr != tail);

		if (curr->key == key){
			return true;
		}
		else {		
			return false;
		}
	}


	/**
	 *  
	*/
	V insertIfAbsent(int tid, K key, V value){
		auto guard = recmgr->getGuard(tid);

		//retry loop
		while (true){
			//search		
			Node<K,V> * parent;
			Node<K,V> * curr;

			
			searchRetry:			
			parent = NULL;
			curr = head;
			uintptr_t parentNextPtr = (uintptr_t) (parent == NULL ? NULL : parent->next);				
			
			do {									
				while (parent != NULL && !IS_DURABLE(parentNextPtr)) {
					prov->scxInit(tid);
					scx_handle_t llxResult;

					llxResult = prov->llx(tid, parent);
					if (!prov->isSuccessfulLLXResult(llxResult)) {
						if (llxResult == prov->FINALIZED){
							//parent was deleted  
							goto searchRetry;
						}

						//parent->next was changed
						parentNextPtr = (uintptr_t) parent->next;						 	
						curr = PTR_TO_NODE(parentNextPtr);						
						continue;												
            		}
					
					//if we made it through the LL of curr but parent->next has changed then the change was before
					//we did the LL so we need to update our stale stack variable
					parentNextPtr = (uintptr_t) parent->next;
					if (PTR_TO_NODE(parentNextPtr) != curr){						 
						curr = PTR_TO_NODE(parentNextPtr);												
					}

					//mark curr->next as durable
					Node<K,V> * markedCurr = MARK_PTR_DURABLE(parentNextPtr);

					prov->scxAddNode(tid, parent, false, llxResult);


					retryCurrLL:
					llxResult = prov->llx(tid, curr);
					if (!prov->isSuccessfulLLXResult(llxResult)) {
						if (llxResult == prov->FINALIZED){
							//TODO curr was deleted confirm its safe to just get the next pointer of parent again
							parentNextPtr = (uintptr_t) parent->next;						 	
							curr = PTR_TO_NODE(parentNextPtr);	
							continue;
						}

						//curr->next was changed
						goto retryCurrLL;												
            		}					
					prov->scxAddNode(tid, curr, false, llxResult);					

					durableTools::FLUSH(&parent->next);
					threadFlushCounters[tid]++;
					if (prov->scxExecute(tid, (void * volatile *) &parent->next, curr, markedCurr)) {
						break;
					}
					
					//TODO: would need to change SCX to return the node that caused failure
					//if we want to do something other more clever than retrying the entire SCX 											
				}

				parent = curr;
				parentNextPtr = (uintptr_t) parent->next;
				curr = PTR_TO_NODE(parentNextPtr);				
			} while (curr->key <= key && curr != tail);	//dont really need to check if tail since we have maxKey		
			//end of search



			if (curr->key == key) {
				return curr->value;
			}


			//---------------------------------------------
			//The actual insert
			//---------------------------------------------

			scx_handle_t llxResult;			
			prov->scxInit(tid);

			//-------------CURR LLX---------------
			retryInsertCurrLL:
			llxResult = prov->llx(tid, curr);
			if (!prov->isSuccessfulLLXResult(llxResult)) {
				if (llxResult == prov->FINALIZED) {
					//if curr was finalized then it has been deleted
					//for now we will full retry but I think there is a better way to do this
					continue;
				}
				
				//if we get here then we hit the case where curr->next was changed				
				goto retryInsertCurrLL;				
			}			
			prov->scxAddNode(tid, curr, false, llxResult);


			//-------------PARENT LLX---------------
			llxResult = prov->llx(tid, parent);
			if (!prov->isSuccessfulLLXResult(llxResult)) {
				//there is no change to parent that we can tolerate so we must retry the operation

				//for insert we might be able to save on retries by searching from parent 
				//I am not sure if this means we need to re-confirm parents durability 
				//if we do then I think we will end up just full retrying anyways
				continue; 
			}			
			prov->scxAddNode(tid, parent, false, llxResult);


			//reread parent->next to confirm that it was not changed before we LL'd
			parentNextPtr = (uintptr_t) parent->next;
			if (PTR_TO_NODE(parentNextPtr) != curr){
				//need to retry for same reason as the above
				//TODO:: maybe we could not full retry the search but retry search FROM parent	
				//not sure if this will break durability arguments
				continue; 
			}	
	

			Node<K,V>* newNode = allocNewNode(tid, key, value, MARK_NODE_DURABLE(curr), true);

			if (prov->scxExecute(tid, (void * volatile *) &parent->next, (Node<K,V>*)parentNextPtr, newNode)){				
				
				//------------------------------
				//mark and flush the update
				//------------------------------
				prov->scxInit(tid);
				llxResult = prov->llx(tid, parent);
				if (!prov->isSuccessfulLLXResult(llxResult) || parent->next != newNode) {										
					// There is no way we can fail the LL of parent without having some other thread
					// make parent->next durable before performing the update to parent that causes the LL
					// to fail so we will just return success for this insert
					return noVal;
				}				

				prov->scxAddNode(tid, parent, false, llxResult);

				durableTools::FLUSH(&parent->next);
				threadFlushCounters[tid]++;

				//if we fail this SCX for any reason it is no longer our responsibility to make the pointer durable
				//we can fail if the pointer changed (meaning some other thread will have made it durable first)				
				//or we can fail if the pointer was made durable but is still succ
				prov->scxExecute(tid, (void * volatile *) &parent->next, newNode, MARK_NODE_DURABLE(newNode));					

				return noVal;				
			} // if we fail the SCX we retry the operation -- TOOD:: maybe retry without re-searching
		} //end retry loop
	}











	/**
	 *  
	*/
	V remove(int tid, K key){
		auto guard = recmgr->getGuard(tid);

		V retVal;

		//retry loop
		while (true){
			//search		
			Node<K,V> * parent;
			Node<K,V> * curr;		
			Node<K,V> * succ;
			
			searchRetry:			
			parent = NULL;
			curr = head;
			uintptr_t parentNextPtr = (uintptr_t) (parent == NULL ? NULL : parent->next);	
						
			do {									
				while (parent != NULL && !IS_DURABLE(parentNextPtr)) {
					prov->scxInit(tid);
					scx_handle_t llxResult;

					llxResult = prov->llx(tid, parent);
					if (!prov->isSuccessfulLLXResult(llxResult)) {
						if (llxResult == prov->FINALIZED){
							//parent was deleted 
							goto searchRetry;
						}

						//parent->next was changed
						parentNextPtr = (uintptr_t) parent->next;
						curr = PTR_TO_NODE(parentNextPtr);						
						continue;												
            		}
					
					//if we made it through the LL of curr but parent->next has changed then the change was before
					//we did the LL so we need to update our stale stack variable
					parentNextPtr = (uintptr_t) parent->next;
					if (PTR_TO_NODE(parentNextPtr) != curr){
						curr = PTR_TO_NODE(parentNextPtr);												
					}

					//mark curr->next as durable
					Node<K,V> * markedCurr = MARK_PTR_DURABLE(parentNextPtr);

					prov->scxAddNode(tid, parent, false, llxResult);


					retryCurrLL:
					llxResult = prov->llx(tid, curr);
					if (!prov->isSuccessfulLLXResult(llxResult)) {
						if (llxResult == prov->FINALIZED){
							//TODO curr was deleted confirm its safe to just get the next pointer of parent again
							parentNextPtr = (uintptr_t) parent->next;						 	
							curr = PTR_TO_NODE(parentNextPtr);	
							continue;
						}

						//curr->next was changed
						goto retryCurrLL;												
            		}					
					prov->scxAddNode(tid, curr, false, llxResult);					

					durableTools::FLUSH(&parent->next);
					threadFlushCounters[tid]++;
					if (prov->scxExecute(tid, (void * volatile *) &parent->next, curr, markedCurr)) {
						break;
					}
					
					//TODO: would need to change SCX to return the node that caused failure
					//if we want to do something other more clever than retrying the entire SCX 											
				}

				parent = curr;
				parentNextPtr = (uintptr_t) parent->next;
				curr = PTR_TO_NODE(parentNextPtr);								
			} while (curr->key <= key && curr != tail);	//dont really need to check if tail since we have maxKey		
			//end of search



			if (curr->key != key) {
				return noVal;
			}




			//---------------------------------------------
			// get successor and make sure its durable
			//---------------------------------------------
			fullSuccDurRetry:
			uintptr_t currNextPtr = (uintptr_t)(curr->next == NULL ? NULL : curr->next);

			succ = PTR_TO_NODE(currNextPtr);			
			while (curr != NULL && !IS_DURABLE(currNextPtr)) {											
				prov->scxInit(tid);
				scx_handle_t llxResult;

				retryCurrDurLL:
				llxResult = prov->llx(tid, curr);
				if (!prov->isSuccessfulLLXResult(llxResult)) {
					if (llxResult == prov->FINALIZED) {
						//if curr was deleted then some other thread did the operation we are in progress on
						return noVal;
					}

					//only other failure case is curr->next changed 
					currNextPtr = (uintptr_t) curr->next;					
					succ = PTR_TO_NODE(currNextPtr);
					goto retryCurrDurLL; 
				}

				//if we made it through the LL of curr but curr->next has changed then the change was before
				//we did the LL so we need to update our stale stack variable and confirm durability
				currNextPtr = (uintptr_t) curr->next;	
				if (PTR_TO_NODE(currNextPtr) != succ){
					succ = PTR_TO_NODE(currNextPtr);					
					continue;	
				}
				prov->scxAddNode(tid, curr, false, llxResult);


				retrySuccDurLL:
				llxResult = prov->llx(tid, succ);
				if (!prov->isSuccessfulLLXResult(llxResult)) {
					if (llxResult == prov->FINALIZED) {
						//succ being deleted implies the following SCX will fail since curr->next cannot possibly be
						//succ anymore so we need to retry this durability loop
						currNextPtr = (uintptr_t) curr->next;	
						succ = PTR_TO_NODE(currNextPtr);
						continue;
					}

					//only other failure case is succ->next changed which we dont care about		
					goto retrySuccDurLL; 
				}				
				
				//Successfully LL'd succ make sure we didnt LL off a stale stack variable
				currNextPtr = (uintptr_t) curr->next;
				if (PTR_TO_NODE(currNextPtr) != succ){	
					succ = PTR_TO_NODE(currNextPtr);				
					goto retrySuccDurLL;		
				}
				prov->scxAddNode(tid, succ, false, llxResult);


				//mark curr->next as durable
				Node<K,V> * markedSucc = MARK_PTR_DURABLE(curr->next);
				
				durableTools::FLUSH(&curr->next);
				threadFlushCounters[tid]++;
				if (prov->scxExecute(tid, (void * volatile *) &curr->next, succ, markedSucc)) {
					break;
				}

				//same as previous durability related SCXs if we want to not retry the whole SCX process
				//we need a way to determine which node caused the SCX to fail
			}




			//---------------------------------------------
			//The actual remove
			//---------------------------------------------

			scx_handle_t llxResult;			
			prov->scxInit(tid);


			//----------------CURR LLX-----------------------
			retryRemoveCurrLL:
			llxResult = prov->llx(tid, curr);
			if (!prov->isSuccessfulLLXResult(llxResult)) {
				if (llxResult == prov->FINALIZED) {
					//if curr was finalized then someone else deleted it
					return noVal;
				}
				
				//if we get here then we hit the case where curr->next was changed
				currNextPtr = (uintptr_t) curr->next;
				succ = PTR_TO_NODE(currNextPtr);
				if (curr != NULL && !IS_DURABLE(currNextPtr)){
					goto fullSuccDurRetry;			
				}
				else {
					goto retryRemoveCurrLL;
				}
			}			
			//we finalize nodes for deletion
			prov->scxAddNode(tid, curr, true, llxResult);





			//----------------PARENT LLX-----------------------
			llxResult = prov->llx(tid, parent);
			if (!prov->isSuccessfulLLXResult(llxResult)) {
				//there is no change to parent that we can tolerate so we must retry the operation

				//Technically we could write a work around for when we failed because parent->next changed
				//but this is basically going to be the same loops as retrying the operation, we would save
				//if someone we failed the LL because someone else deleted curr but I dont think we have
				//a good way to detect that
				continue; 
			}			
			prov->scxAddNode(tid, parent, false, llxResult);

			//reread parent->next to confirm that it was not changed before we LL'd
			parentNextPtr = (uintptr_t) parent->next;
			if (PTR_TO_NODE(parentNextPtr) != curr){
				//need to retry for same reason as the above
				//TODO:: maybe we could not full retry the search but retry search FROM parent	
				continue; 
			}	


			//----------------SUCC LLX-----------------------
			retryRemoveSuccLL:
			llxResult = prov->llx(tid, succ);
			if (!prov->isSuccessfulLLXResult(llxResult)) {
				if (llxResult == prov->FINALIZED) {
					//if succ was finalized then it was deleted this implies curr->next cannot be succ anymore
					//which means our SCX will fail
					//TODO:: can we avoid a full retry here?
					continue;
				}

				//if we get here then we hit the case where succ->next was changed				
				goto retryRemoveSuccLL;			
			}			
			prov->scxAddNode(tid, succ, false, llxResult);

			currNextPtr = (uintptr_t) curr->next;
			if (PTR_TO_NODE(currNextPtr) != succ){				
				succ = PTR_TO_NODE(currNextPtr);
				goto retryRemoveSuccLL;
			}

			retVal = curr->value;

			if (prov->scxExecute(tid, (void * volatile *) &parent->next, (Node<K,V>*)parentNextPtr, succ)){				
				recmgr->retire(tid, curr);

				//------------------------------
				//mark and flush the update
				//------------------------------
				prov->scxInit(tid);
				llxResult = prov->llx(tid, parent);
				if (!prov->isSuccessfulLLXResult(llxResult) || PTR_TO_NODE(parent->next) != succ) {											
					// There is no way we can fail the LL of parent without having some other thread
					// make parent->next durable before performing the update to parent that causes the LL
					// to fail so we will just return success for this insert
					return retVal;
				}				

				prov->scxAddNode(tid, parent, false, llxResult);

				durableTools::FLUSH(&parent->next);
				threadFlushCounters[tid]++;

				//if we fail this SCX for any reason it is no longer our responsibility to make the pointer durable
				//we can fail if the pointer changed (meaning some other thread will have made it durable first)				
				//or we can fail if the pointer was made durable but is still succ
				prov->scxExecute(tid, (void * volatile *) &parent->next, succ, MARK_NODE_DURABLE(succ));					

				return retVal;				
			} // if we fail the SCX we retry the operation -- TOOD:: maybe retry without re-searching
		} //end retry loop
	}
};