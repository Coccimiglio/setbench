#pragma once

/**
 * 
 * TODO: deletes need to use marking since were using CAS now
 * 
 * Here we ignore durability during searches except for the last pointer we follow. We conditionally
 * 		help persist the last pointer we followed
 * 
*/

#include "durable_tools.h"

#define DURABLE_MASK                (0x1ll)
#define MARK_NODE_DURABLE(x)     	((Node<K,V> *)(((uintptr_t)(x)) | DURABLE_MASK))
#define IS_DURABLE(x)               (((uintptr_t)(x)) & DURABLE_MASK)
#define MARK_PTR_DURABLE(x)     	((Node<K,V> * volatile)(((uintptr_t)(x)) | DURABLE_MASK))

#define PTR_TO_NODE(x)				((Node<K,V> *)((uintptr_t)(x) & ~DURABLE_MASK))

#define CAS(ptr, oldVal, newVal)	__sync_bool_compare_and_swap(ptr, oldVal, newVal)

template<typename K, typename V>
struct Node{		
	K key;
	V value;
	Node<K,V> * volatile next;		
};

template <typename K, typename V, class RecordManager>
class [[deprecated("Moved to durable_list_with_flush_helping")]] List {
private:
	struct searchResult{
		Node<K,V> * parent;
		Node<K,V> * curr;		
	};

	Node<K,V> * head;
	Node<K,V> * tail;
	RecordManager * recmgr;
	V noVal;

	Node<K,V> * allocNewNode(int tid, K key, V val, Node<K,V> *next, bool persistNow){
		Node<K, V> * newNode = recmgr->template allocate<Node<K, V>>(tid);
				
		newNode->key = key;
		newNode->value = val;
		newNode->next = next;

		if (persistNow) {			
#if defined(USE_CLFLUSHOPT)
			durableTools::CLFLUSHOPT(&newNode->key);
			durableTools::CLFLUSHOPT(&newNode->value);
			durableTools::CLFLUSHOPT(&newNode->next);
			
			durableTools::SFENCE();
#elif defined(USE_CLWB) 
			durableTools::CLWB(&newNode->key);
			durableTools::CLWB(&newNode->value);
			durableTools::CLWB(&newNode->next);
			
			durableTools::SFENCE();	
#else
			durableTools::FLUSH(&newNode->key);
			durableTools::FLUSH(&newNode->value);
			durableTools::FLUSH(&newNode->next);				
#endif
			GSTATS_ADD(tid, num_flushes, 1);
		}

		return newNode;		
	}


	searchResult searchPersistLast(K key){
		Node<K,V> * parent;
		Node<K,V> * curr;	

		parent = head;
		curr = PTR_TO_NODE(head->next);					
		Node<K,V> * markedCurr = parent->next;
		
		while (curr->key <= key && curr != tail) {			
			parent = curr;
			markedCurr = curr->next;
			curr = PTR_TO_NODE(markedCurr);
		} 


		//Note I dont think we will ever get a NULL parent unless we search for the min key
		//meaning curr == head, I guess we can still check against it just incase
		Node<K,V>* parentNextPtr = parent == NULL ? NULL : parent->next;

		if (!IS_DURABLE(parentNextPtr) && PTR_TO_NODE(parentNextPtr) == curr && parent != NULL){													
			durableTools::FLUSH(&parent->next);
			GSTATS_ADD(tid, num_flushes, 1);
			CAS(&parent->next, curr, markedCurr);
		}

		//Note parent is an unmarked reference
		return {parent, markedCurr};
	}


	searchResult searchPersistAll(K key) {
		Node<K,V> * parent;
		Node<K,V> * curr;	
		
		parent = NULL;	
		curr = head;					

		Node<K,V> * markedParent = parent;		
		Node<K,V> * markedCurr = curr;
		
		do {									
			if (parent != NULL && !IS_DURABLE(markedCurr)) {						
				durableTools::FLUSH(&parent->next);
				GSTATS_ADD(tid, num_flushes, 1);
				CAS(&parent->next, markedCurr, MARK_NODE_DURABLE(curr));
			}

			parent = curr;
			markedParent = markedCurr;
			markedCurr = parent->next;
			curr = PTR_TO_NODE(markedCurr);				
		} while (curr->key <= key && curr != tail);	//dont really need to check if tail since we have maxKey		
		//end of search

		return {markedParent, markedCurr};
	}

public:
	List(int numThreads, K minKey, K maxKey, V reservedVal){		
		recmgr = new RecordManager(numThreads);

		noVal = reservedVal;

		tail = allocNewNode(0, maxKey, 0, NULL, true);
		head = allocNewNode(0, minKey, 0, MARK_NODE_DURABLE(tail), true);
	}

	~List(){
	}

	Node<K,V> * getHead(){
		return head;
	}

	void initThread(int tid){
		recmgr->initThread(tid);
	}

	void deinitThread(int tid){
		recmgr->deinitThread(tid);
	}

	bool contains(int tid, K key) {
		auto guard = recmgr->getGuard(tid);
		Node<K,V> * curr = PTR_TO_NODE(searchPersistLast(key).curr);	

		if (curr->key == key){
			return true;
		}
		else {		
			return false;
		}
	}


	/**
	 *  
	*/
	V insertIfAbsent(int tid, K key, V value){
		auto guard = recmgr->getGuard(tid);

		//retry loop
		while (true){
			searchResult result = searchPersistAll(key);
			
			Node<K,V> * parent = PTR_TO_NODE(result.parent);
			Node<K,V> * curr = PTR_TO_NODE(result.curr);

			Node<K,V>* markedCurr = result.curr;

			if (curr->key == key) {
				return curr->value;
			}

			Node<K,V>* newNode = allocNewNode(tid, key, value, MARK_NODE_DURABLE(curr), true);

			if (CAS(&parent->next, markedCurr, newNode)) {
				durableTools::FLUSH(&parent->next);
				GSTATS_ADD(tid, num_flushes, 1);
				CAS(&parent->next, newNode, MARK_NODE_DURABLE(newNode));

				return noVal;
			}			
		} //end retry loop
	}






	/**
	 *  
	*/
	V remove(int tid, K key){
		auto guard = recmgr->getGuard(tid);

		V retVal;

		//retry loop
		while (true){
			searchResult result = searchPersistAll(key);
			
			Node<K,V> * parent = PTR_TO_NODE(result.parent);
			Node<K,V> * curr = PTR_TO_NODE(result.curr);

			Node<K,V>* markedCurr = result.curr;

			if (curr->key != key) {
				return noVal;
			}

			retVal = curr->value;

			Node<K,V> * markedSucc = (curr->next == NULL ? NULL : curr->next);
			Node<K,V> * succ = PTR_TO_NODE(markedSucc); 

			if (!IS_DURABLE(markedSucc)) {
				durableTools::FLUSH(&curr->next);
				CAS(&curr->next, succ, MARK_PTR_DURABLE(markedSucc));
			}

			retVal = curr->value;

			if (CAS(&parent->next, markedCurr, succ)) {
				durableTools::FLUSH(&parent->next);
				CAS(&parent->next, succ, MARK_PTR_DURABLE(succ));
				
				return retVal;
			}
		} //end retry loop
	}
};