#pragma once

/**
 * 
 * TODO: deletes need to use marking since were using CAS now
 * 
 * Here we ignore durability during searches except for the last pointer we follow. We conditionally
 * 		help persist the last pointer we followed
 * 
*/

#include "durable_tools.h"

#define DURABLE_MASK                (0x1ll)
#define MARK_NODE_DURABLE(x)     	((Node<K,V> *)(((uintptr_t)(x)) | DURABLE_MASK))
#define IS_DURABLE(x)               (((uintptr_t)(x)) & DURABLE_MASK)
#define MARK_PTR_DURABLE(x)     	((Node<K,V> * volatile)(((uintptr_t)(x)) | DURABLE_MASK))

#define DELETED_MASK				(0x2ll)
#define MARK_NODE_DELETED(x)		((Node<K,V> *)(((uintptr_t)(x)) | DELETED_MASK))
#define IS_DELETED(x)				(((uintptr_t)(x)) & DELETED_MASK)

#define TOTAL_MASK					(0x3ll)
#define PTR_TO_NODE(x)				((Node<K,V> *)((uintptr_t)(x) & ~TOTAL_MASK))

#define CAS(ptr, oldVal, newVal)	__sync_bool_compare_and_swap(ptr, oldVal, newVal)

template<typename K, typename V>
struct Node{		
	K key;
	V value;
	Node<K,V> * volatile next;		
};

template <typename K, typename V, class RecordManager>
class [[deprecated]] List {
private:
	struct searchResult{
		Node<K,V> * parent;
		Node<K,V> * curr;		
	};

	PAD;
	Node<K,V> * head;
	Node<K,V> * tail;
	PAD;
	
	RecordManager * recmgr;
	V noVal;

	Node<K,V> * allocNewNode(int tid, K key, V val, Node<K,V> *next, bool persistNow){
		Node<K, V> * newNode = recmgr->template allocate<Node<K, V>>(tid);
				
		newNode->key = key;
		newNode->value = val;
		newNode->next = next;

		if (persistNow) {			
#ifdef USE_CLFLUSHOPT	
			durableTools::CLFLUSHOPT(&newNode->key);
			durableTools::CLFLUSHOPT(&newNode->value);
			durableTools::CLFLUSHOPT(&newNode->next);
			
			durableTools::SFENCE();			
#else
	#ifdef USE_CLWB
			durableTools::CLWB(&newNode->key);
			durableTools::CLWB(&newNode->value);
			durableTools::CLWB(&newNode->next);
			
			durableTools::SFENCE();			
	#else 
			durableTools::FLUSH(&newNode->key);
			durableTools::FLUSH(&newNode->value);
			durableTools::FLUSH(&newNode->next);					
	#endif
#endif
			GSTATS_ADD(tid, num_flushes, 1);
		}

		return newNode;		
	}


	/**
	 * Harris method kind of breaks this algorithms flush complexity unless we want to just remove
	 * deleted nodes but we dont flush it. 
	 * 
	 * If we didnt flush it we could crash and have a deleted node still in the list but this means no update
	 * progressed past this deleted node so its next pointer can still be used so we should still be able to recover.
	 * 
	 * 
	*/
	searchResult searchPersistLast(K key){
		Node<K,V> * parent;
		Node<K,V> * curr;	
		
		searchRetry:
		parent = head;
		curr = PTR_TO_NODE(head->next);

		Node<K,V> * markedCurr = parent->next;
		Node<K,V> * markedCurrNext;		
		Node<K,V> * succ;				
		Node<K,V> * parentNext = curr;

		bool foundDeleted = false;
		bool ranOverSearchKey = false;
		
		while (curr->key <= key && curr != tail) {			
			parent = curr;
			markedCurr = curr->next;
			curr = PTR_TO_NODE(markedCurr);
		} 

		while (curr->key <= key && curr != tail) {
			markedCurrNext = curr->next;
			while (IS_DELETED(markedCurrNext)) {
				foundDeleted = true;

				markedCurr = curr->next;
				curr = PTR_TO_NODE(markedCurr);
				markedCurrNext = curr->next;				

				if (curr->key > key || curr == tail) {
					ranOverSearchKey = true;
				}
			}

			if (foundDeleted) {
				succ = curr->next == NULL ? tail : PTR_TO_NODE(curr->next);
				if (CAS (&parent->next, parentNext, succ)) {					
					//This is the flush + CAS to persist the removal of a deleted node					
					/*
					durableTools::FLUSH(&parent->next);
					CAS (&parent->next, succ, MARK_NODE_DURABLE(succ));					
					*/
					curr = succ;
				}
				else if (IS_DELETED(parent->next)) {
					goto searchRetry;					
				}

				if (ranOverSearchKey) {
					//we searched past our search key because of hitting one or more deleted nodes
					break;
				}
			}
			
			parent = curr;						
			markedCurr = parent->next;
			parentNext = markedCurr;
			curr = PTR_TO_NODE(markedCurr);					
		}

		//Note I dont think we will ever get a NULL parent unless we search for the min key
		//meaning curr == head, I guess we can still check against it just incase
		Node<K,V>* parentNextPtr = parent == NULL ? NULL : parent->next;

		if (!IS_DURABLE(parentNextPtr) && PTR_TO_NODE(parentNextPtr) == curr && parent != NULL){													
			durableTools::FLUSH(&parent->next);
			CAS(&parent->next, curr, markedCurr);
			GSTATS_ADD(tid, num_flushes, 1);
		}
		
		//Note parent is an unmarked reference
		return {parent, markedCurr};
	}


	/**
	 * This search assumes you will never search for the key in head ie minKey
	*/
	searchResult searchPersistAll(K key){
		Node<K,V> * parent;
		Node<K,V> * curr;	
		
		searchRetry:
		parent = head;
		curr = PTR_TO_NODE(head->next);

		Node<K,V> * markedCurr = parent->next;
		Node<K,V> * markedCurrNext;		
		Node<K,V> * succ;				
		Node<K,V> * parentNext = curr;

		bool foundDeleted = false;
		bool ranOverSearchKey = false;

		while (curr->key <= key && curr != tail) {
			markedCurrNext = curr->next;
			while (IS_DELETED(markedCurrNext)) {
				foundDeleted = true;

				markedCurr = curr->next;
				curr = PTR_TO_NODE(markedCurr);
				markedCurrNext = curr->next;				

				if (curr->key > key || curr == tail) {
					ranOverSearchKey = true;
				}
			}

			if (foundDeleted) {
				succ = curr->next == NULL ? tail : PTR_TO_NODE(curr->next);
				if (CAS (&parent->next, parentNext, succ)) {
#if defined(USE_CLFLUSHOPT)
					durableTools::CLFLUSHOPT(&parent->next);			
					durableTools::SFENCE();
#elif defined(USE_CLWB) 
					durableTools::CLWB(&parent->next);			
					durableTools::SFENCE();	
#else
					durableTools::FLUSH(&parent->next);			
#endif														
					GSTATS_ADD(tid, num_flushes, 1);
					CAS (&parent->next, succ, MARK_NODE_DURABLE(succ));
					curr = succ;
				}
				else if (IS_DELETED(parent->next)) {
					goto searchRetry;					
				}

				if (ranOverSearchKey) {
					//we searched past our search key because of hitting one or more deleted nodes
					break;
				}
			}
			
			if (!foundDeleted && !IS_DURABLE(markedCurr)) {			
				durableTools::FLUSH(&parent->next);
				GSTATS_ADD(tid, num_flushes, 1);
				CAS (&parent->next, markedCurr, MARK_NODE_DURABLE(markedCurr));
			}
			

			parent = curr;						
			markedCurr = parent->next;
			parentNext = markedCurr;
			curr = PTR_TO_NODE(markedCurr);					
		}

		//TODO: need to confirm the durability of markedCurr here

		return {parent, markedCurr};
	}

public:
	List(int numThreads, K minKey, K maxKey, V reservedVal){		
		recmgr = new RecordManager(numThreads);

		noVal = reservedVal;

		tail = allocNewNode(0, maxKey, 0, NULL, true);
		head = allocNewNode(0, minKey, 0, MARK_NODE_DURABLE(tail), true);
	}

	~List(){
	}

	Node<K,V> * getHead(){
		return head;
	}

	void initThread(int tid){
		recmgr->initThread(tid);
	}

	void deinitThread(int tid){
		recmgr->deinitThread(tid);
	}

	bool contains(int tid, K key) {
		auto guard = recmgr->getGuard(tid);
		Node<K,V> * curr = PTR_TO_NODE(searchPersistLast(key).curr);	

		if (curr->key == key){
			return true;
		}
		else {		
			return false;
		}
	}


	/**
	 *  
	*/
	V insertIfAbsent(int tid, K key, V value){
		auto guard = recmgr->getGuard(tid);

		//retry loop
		while (true){
			searchResult result = searchPersistAll(key);
			
			Node<K,V> * parent = PTR_TO_NODE(result.parent);
			Node<K,V> * curr = PTR_TO_NODE(result.curr);

			Node<K,V>* markedCurr = result.curr;

			if (curr->key == key) {
				return curr->value;
			}

			Node<K,V>* newNode = allocNewNode(tid, key, value, MARK_NODE_DURABLE(curr), true);

			if (CAS(&parent->next, markedCurr, newNode)) {
#if defined(USE_CLFLUSHOPT)
				durableTools::CLFLUSHOPT(&parent->next);			
				durableTools::SFENCE();
#elif defined(USE_CLWB) 
				durableTools::CLWB(&parent->next);			
				durableTools::SFENCE();	
#else
				durableTools::FLUSH(&parent->next);			
#endif														
				GSTATS_ADD(tid, num_flushes, 1);
				CAS(&parent->next, newNode, MARK_NODE_DURABLE(newNode));

				return noVal;
			}			
		} //end retry loop
	}






	/**
	 *  
	*/
	V remove(int tid, K key){
		auto guard = recmgr->getGuard(tid);

		V retVal;

		//retry loop
		while (true){
			searchResult result = searchPersistAll(key);		
			Node<K,V> * parent = PTR_TO_NODE(result.parent);
			Node<K,V> * curr = PTR_TO_NODE(result.curr);

			Node<K,V>* markedCurr = result.curr;

			if (curr->key != key) {
				return noVal;
			}

			retVal = curr->value;

			Node<K,V> * deletedCurr = MARK_NODE_DELETED(curr);

			if (CAS(&parent->next, markedCurr, deletedCurr)){				
#if defined(USE_CLFLUSHOPT)
				durableTools::CLFLUSHOPT(&parent->next);			
				durableTools::SFENCE();
#elif defined(USE_CLWB) 
				durableTools::CLWB(&parent->next);			
				durableTools::SFENCE();	
#else
				durableTools::FLUSH(&parent->next);			
#endif														
				GSTATS_ADD(tid, num_flushes, 1);
				CAS(&parent->next, deletedCurr, MARK_NODE_DURABLE(deletedCurr));

				//search here to clear out deleted nodes
				searchPersistAll(PTR_TO_NODE(curr->next)->key);

				return retVal;
			}
		} //end retry loop
	}
};