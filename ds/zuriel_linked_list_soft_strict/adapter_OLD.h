#ifndef ZURIEL_SOFT_LIST_H
#define ZURIEL_SOFT_LIST_H

#include <iostream>
#include <csignal>
#include "errors.h"

#include "record_manager.h"
#ifdef USE_TREE_STATS
#   include "tree_stats.h"
#endif

#include "SOFTLinkedList.h"
#include "SOFTUtilities.h"

#define RECORD_MANAGER_T record_manager<Reclaim, Alloc, Pool, Node<K, V>, PNode<K,V> >
#define DATA_STRUCTURE_T SOFTList<RECORD_MANAGER_T, K, V>


template <typename K, typename V, class Reclaim = reclaimer_debra<K>, class Alloc = allocator_new<K>, class Pool = pool_none<K>>
class ds_adapter {
private:    
    DATA_STRUCTURE_T * const ds;
    V noVal;

public:
    ds_adapter(const int NUM_THREADS,
               const K& KEY_MIN,
               const K& KEY_MAX,
               const V& VALUE_RESERVED,
               Random64 * const unused2)
    : ds(new DATA_STRUCTURE_T(NUM_THREADS, KEY_MIN, KEY_MAX, VALUE_RESERVED)), noVal(VALUE_RESERVED)
    { }
    
    ~ds_adapter() {
        delete ds;
    }
    
    V getNoValue() {
        return noVal;
    }
    
    void initThread(const int tid) {
        ds->initThread(tid);
    }
    void deinitThread(const int tid) {
        ds->deinitThread(tid);
    }

    V insert(const int tid, const K& key, const V& val) {
        setbench_error("Insert-replace functionality not implemented for this data structure");
    }
    
    V insertIfAbsent(const int tid, const K& key, const V& val) {        
        return ds->insertIfAbsent(tid, key, val);
    }
    
    V erase(const int tid, const K& key) {
        return ds->remove(tid, key);
    }
    
    V find(const int tid, const K& key) {
		setbench_error("Find not implemented for this data structure");
    }
    
    bool contains(const int tid, const K& key) {
        return ds->contains(tid, key);
    }
    int rangeQuery(const int tid, const K& lo, const K& hi, K * const resultKeys, V * const resultValues) {
        setbench_error("not implemented");
    }
    void printSummary() {
		//ds->printSummary();
        return;
    }
    bool validateStructure() {
        return ds->validate();        
    }
    
    void printObjectSizes() {
        std::cout << "Size of volatile node=" << (sizeof(Node<K, V>)) << std::endl;
        std::cout << "Size of persistent node=" << (sizeof(PNode<K, V>)) << std::endl;
    }
    
#ifdef USE_TREE_STATS
class NodeHandler {
    public:
        typedef Node<K, V> * NodePtrType;
        K minKey;
        K maxKey;        
        
        NodeHandler(const K& _minKey, const K& _maxKey) {
            minKey = _minKey;
            maxKey = _maxKey;            
        }
        
        class ChildIterator {            
        private:
            bool calledNext;
            NodePtrType node; // node being iterated over
        public:
            ChildIterator(NodePtrType _node) {
                node = _node;
                calledNext = false;
            }
            
            bool hasNext() {
                return softUtils::getRef(node)->next.load() != NULL && !calledNext;
            }
            
            NodePtrType next() {
                // bool foundBad = false;
                // while (softUtils::getState(node) != 0) {                                        
                //     //GUY::IMPORTANT:: I am not sure how you would convert to uintptr_t without loading since 
                //     //NodePtrType is atomic which means this should break if we used some other atomic library or volatiles
                //     std::cout << "Found Bad Node State " << std::endl;
                //     foundBad = true;
                //     auto nextPtrLong = (uintptr_t)(node);
                //     nextPtrLong &= ~STATE_MASK;
                //     node = (NodePtrType)(nextPtrLong);
                //     std::cout << "Bad node converted: "<< node << std::endl;
                //     std::cout << "key of converted node" << node->key << std::endl;

                //     node = node->next;
                //     std::cout << "Bad node skipped. New node: "<< node << std::endl;
                //     std::cout << "State of New node: " << softUtils::getState(node) << std::endl;                                    

                //     if (softUtils::getState(node) == 0){
                //         return node;
                //     }
                // }

                calledNext = true;
                return softUtils::getRef(node)->next.load();
            }
        };
        
        bool isLeaf(NodePtrType node) {
            return softUtils::getRef(node)->next.load() == NULL ? true : false;
        }
        size_t getNumChildren(NodePtrType node) {            
            //seg faulted here with node = 0x36100007ffe35 which is clearly in non 0 state
            return softUtils::getRef(node)->next.load() == NULL ? 0 : 1;
        }
        size_t getNumKeys(NodePtrType node) {   
            if (node != NULL){
                //state == 3 is deleted 2 is intend to insert
                if (softUtils::getState(softUtils::getRef(node)->next.load()) == 3 || softUtils::getState(softUtils::getRef(node)->next.load()) == 2) { 
                    return 0;
                }        
                else {
                    size_t key = softUtils::getRef(node)->key;
                    return key == minKey ? 0 : (key == maxKey ? 0 : 1);       
                }
            }
            else {
                return 0;
            }            
        }
        
        size_t getSumOfKeys(NodePtrType node) {      
            if (node != NULL){
                // std::cout << "## Node: " << node << " has state: " << softUtils::getState(node);
                // std::cout << "key: " << node->key << std::endl;
                //state == 3 is deleted 2 is intend to insert
                if (softUtils::getState(softUtils::getRef(node)->next.load()) == 3 || softUtils::getState(softUtils::getRef(node)->next.load()) == 2) { 
                    //std::cout << "Bad state on node " << node << " key " <<  softUtils::getRef(node)->key << std::endl;
                    return  (size_t) 0;
                }        
                else {
                    size_t key = softUtils::getRef(node)->key;
                    return (size_t) (key == minKey ? 0 : (key == maxKey ? 0 : key));
                }
            }
            else {
                return  (size_t) 0;
            }               
        }
        ChildIterator getChildIterator(NodePtrType node) {
            return ChildIterator(node);
        }
};
    TreeStats<NodeHandler> * createTreeStats(const K& _minKey, const K& _maxKey) {        
        return new TreeStats<NodeHandler>(new NodeHandler(_minKey, _maxKey), ds->getHead(), false);
    }
#endif
};

#endif
