#ifndef _SOFT_LIST_H_
#define _SOFT_LIST_H_

#include "PNode.h"
#include "SOFTUtilities.h"
#include "VolatileNode.h"
#include <atomic>
#include <unordered_set>


#if defined(USE_LIBVMMALLOC) && !defined(RECORDMANAGER_PMEM_SUPPORT)
	#include "persistent_alloc.h"
#endif

#if defined(USE_MEMKIND) && !defined(RECORDMANAGER_PMEM_SUPPORT)
    #include <memkind.h>
#endif

#ifndef RECORDMANAGER_PMEM_SUPPORT
	#define PMEM_POOL_PATH "/mnt/pmem1_mount/"
	#define PMEM_POOL_SIZE 0 // 0 means the heap is not a fixed size and can grow as long as the temp file can grow
#endif

//previously part of zurielCommon
#define UNLIKELY(x) __builtin_expect((x), 0)
#define LIKELY(x) __builtin_expect((x), 1)

typedef softUtils::state state;

template <class PRecordManager, class VRecordManager, typename K, typename V>
class SOFTList
{
  private:
    PAD;
    Node<K, V> *head;
    Node<K, V> *tail;
    PAD;
    
    VRecordManager * v_recmgr;  

#ifdef RECORDMANAGER_PMEM_SUPPORT
    PRecordManager * p_recmgr;  
#elif defined(USE_MEMKIND)
    struct memkind *pmem_kind = NULL;
#elif defined(USE_LIBVMMALLOC)
	AllocatorLibvmmalloc_Simple* alloc;
#endif

    V noVal;    
    PAD;
    //std::atomic<int> failedCAS;

    PNode<K, V> * allocNewPNode(int tid)
    {             
#ifdef RECORDMANAGER_PMEM_SUPPORT
		PNode<K, V>* newPnode = p_recmgr->template allocate<PNode<K, V>>(tid);             
#elif defined(USE_MEMKIND)
        PNode<K, V>* newPnode = (PNode<K,V>*)memkind_malloc(pmem_kind, sizeof(PNode<K,V>));
        GSTATS_ADD(tid, num_pmem_alloc, 1);    
#elif defined(USE_LIBVMMALLOC)
		PNode<K, V>* newPnode = (PNode<K, V>*)alloc->allocate(sizeof(PNode<K, V>));
#endif	                             

        return newPnode;
        //return static_cast<PNode<K, V> *>(ssmem_alloc(alloc, sizeof(PNode<K, V>)));
    }

	Node<K, V>* allocNewVolatileNode(int tid, K key, V value, PNode<K, V>* pptr, bool pValidity){
        Node<K, V>* n = v_recmgr->template allocate<Node<K, V>>(tid);

		n->key = key;
		n->value = value;
		n->pptr = pptr;
		n->pValidity = pValidity;
		return n;
	}
  
    bool trim(int tid, Node<K, V> *prev, Node<K, V> *curr)
    {
        //dont need to guard here since this function is never called independantly
        //it is called from insert or remove

        //the state of prev is encoded in its
        //node->next pointer 
        //(I wonder if this assumption that curr is still prev->next can be untrue)
        state prevState = softUtils::getState(curr);

        Node<K, V>* currRef = softUtils::getRef<Node<K, V>>(curr);
        Node<K, V>* succ = softUtils::getRef<Node<K, V>>(currRef->next.load());
        
        //this will return a pointer that is succ marked with the prevState
        //this is just copying the state marker to the soon to be next pointer of prev
        succ = softUtils::createRef<Node<K, V>>(succ, prevState);

        bool result = prev->next.compare_exchange_strong(curr, succ);
        
        if (result) {    
#ifdef RECORDMANAGER_PMEM_SUPPORT            
            p_recmgr->retire(tid, currRef->pptr);                        
#else
#endif
            //GUY::IMPORANT:: I added this reclaim of the volatile node 
            //original SOFT list doesnt reclaim volatile nodes, it also did not appear to reclaim via SSMEM
            v_recmgr->retire(tid, currRef);
        }
        return result;
    }

    // returns clean reference in pred, ref+state of pred in return and the state of curr in the last arg
    Node<K, V> *find(int tid, K key, Node<K, V> **predPtr, state *currStatePtr)
    {
        //dont need to guard here since this function is never called independantly
        //it is called from insert or remove

        Node<K, V>* prev = head;
        Node<K, V>* curr = prev->next.load();
        Node<K, V>* succ;
        Node<K, V>* succRef;

        Node<K, V>* currRef = softUtils::getRef<Node<K, V>>(curr);

        state prevState = softUtils::getState(curr); 
        state cState;

        while (true)
        {
            /*
                NOTE currRef->next is both the pointer to succ but it is also the state of curr
                so checking if cState is DELETED is only checking if curr is DELETED. 

                When find is called from insert the curr returned is the successor. Which should mean
                a concurrent insert should not cause a trim CAS to fail so a failed CAS in trim
                would mean some other thread completed the exact CAS that failed.
            */
            succ = currRef->next.load();
            succRef = softUtils::getRef<Node<K, V>>(succ);
            cState = softUtils::getState(succ);
            if (LIKELY(cState != state::DELETED))
            {
                if (UNLIKELY(currRef->key >= key))
                    break;
                prev = currRef;
                prevState = cState;
            }
            else
            {
                trim(tid, prev, curr);
            }
            curr = softUtils::createRef<Node<K, V>>(succRef, prevState);
            currRef = succRef;
        }
        *predPtr = prev;
        *currStatePtr = cState;
        return curr;
    }

  public:
    SOFTList(int numThreads, K minKey, K maxKey, V noValue)
    {
        noVal = noValue;   
        v_recmgr = new VRecordManager(numThreads); 
#ifdef RECORDMANAGER_PMEM_SUPPORT            
        p_recmgr = new PRecordManager(numThreads); 
        //This is just to force initialization, somewhat hacky but doesnt matter since allocation of the list
        // is not measured
		PNode<K,V>* temp = p_recmgr->template allocate<PNode<K,V>>(tid);
		p_recmgr->deallocate(0, temp);
#elif defined(USE_MEMKIND)
		int err = memkind_create_pmem(PMEM_POOL_PATH, PMEM_POOL_SIZE, &pmem_kind);
		if (err) {			
			printf("Error creating PMEM pool. Exiting\n");
			exit(-1);
		}
#elif defined(USE_LIBVMMALLOC)
		alloc = new AllocatorLibvmmalloc_Simple();
		alloc->init();
#endif	              

        head = allocNewVolatileNode(0, minKey, 0, nullptr, false);
        tail = allocNewVolatileNode(0, maxKey, 0, nullptr, false);
        head->next = tail;
    }

	void initThread(const int tid) {
        v_recmgr->initThread(tid);
#ifdef RECORDMANAGER_PMEM_SUPPORT          
		p_recmgr->initThread(tid);
#endif
	}
    
    void deinitThread(const int tid) {				
        v_recmgr->deinitThread(tid);		
#ifdef RECORDMANAGER_PMEM_SUPPORT  
		p_recmgr->deinitThread(tid);		
#endif
	}

    Node<K,V> * getHead() {
        return head;
    }

    bool validate(){
		return true;
	}

    V insertIfAbsent(int tid, K key, V value)
    {	
        auto guard1 = v_recmgr->getGuard(tid);       
#ifdef RECORDMANAGER_PMEM_SUPPORT  		
		auto guard2 = p_recmgr->getGuard(tid);       
#endif

        Node<K, V> *pred, *currRef;
        state currState, predState;        

    retry:
        while (true)
        {
            Node<K, V>* curr = find(tid, key, &pred, &currState);
            currRef = softUtils::getRef<Node<K, V>>(curr);
            predState = softUtils::getState(curr);

            Node<K, V> *resultNode;
            bool result = false;    

            if (currRef->key == key)
            {
                resultNode = currRef;
                // if (currState != state::INTEND_TO_INSERT)
                //     return resultNode->value;
                if (currState == state::INSERTED) {
                    return resultNode->value;
                }

                if (currState == state::INTEND_TO_DELETE) {
                    currRef->pptr->destroy(currRef->pValidity);

                    while (softUtils::getState(currRef->next.load()) == state::INTEND_TO_DELETE)
                        softUtils::stateCAS<Node<K, V>>(currRef->next, state::INTEND_TO_DELETE, state::DELETED);
                    
                    return resultNode->value;
                }
            }
            else
            {
                PNode<K, V> *newPNode = allocNewPNode(tid);
                bool pValid = newPNode->alloc();

                Node<K, V> *newNode = allocNewVolatileNode(tid, key, value, newPNode, pValid);
                newNode->next.store(static_cast<Node<K, V> *>(softUtils::createRef(currRef, state::INTEND_TO_INSERT)), std::memory_order_relaxed);
                if (!pred->next.compare_exchange_strong(curr, static_cast<Node<K, V> *>(softUtils::createRef(newNode, predState)))){                    
#ifdef RECORDMANAGER_PMEM_SUPPORT                                     
                    p_recmgr->deallocate(tid, newPNode);                    
#elif defined(USE_MEMKIND)
                    memkind_free(pmem_kind, newPNode);                    
#elif defined(USE_LIBVMMALLOC)
					alloc->deallocate(newPNode);
#endif                          
                    v_recmgr->deallocate(tid, newNode);                    
                    goto retry;                    
                }
                resultNode = newNode;
                result = true;
            }

            resultNode->pptr->create(resultNode->key, resultNode->value, resultNode->pValidity);
            if (result) {
                GSTATS_ADD(tid, num_update_flushes, 1);    
            }
            else {
                GSTATS_ADD(tid, num_search_flushes, 1);    
            }


            while (softUtils::getState(resultNode->next.load()) == state::INTEND_TO_INSERT)                
                softUtils::stateCAS<Node<K, V>>(resultNode->next, state::INTEND_TO_INSERT, state::INSERTED);

            if (result) {                
                return noVal; //return true
            }
            else {
                /*
                    the only way to get here where result == false is if we found the key 
                    but its state was INTENT_TO_INSERT we will help that insert in the 
                    while loop and transition its state to INSERTED but we were not the thread
                    to insert it so we will return its value not noVal
                */
                return resultNode->value;    
            }
        }
    }

    V remove(int tid, K key)
    {					
        auto guard1 = v_recmgr->getGuard(tid);  	
#ifdef RECORDMANAGER_PMEM_SUPPORT  
		auto guard2 = p_recmgr->getGuard(tid);  
#endif
        bool casResult = false;        
        Node<K, V> *pred, *curr, *currRef;
        state predState, currState;

        curr = find(tid, key, &pred, &currState); 
               
        currRef = softUtils::getRef<Node<K, V>>(curr);
        predState = softUtils::getState(curr);

        V val = curr->value;

        if (currRef->key != key)
        {
            return noVal;
        }

        // if (currState == state::INTEND_TO_INSERT || currState == state::DELETED)
        if (currState == state::DELETED)
        {
            return noVal;
        }

        if (currState == state::INTEND_TO_INSERT) {
            currRef->pptr->create(currRef->key, currRef->value, currRef->pValidity);
                                                    
            while (softUtils::getState(currRef->next.load()) == state::INTEND_TO_INSERT)                
                softUtils::stateCAS<Node<K, V>>(currRef->next, state::INTEND_TO_INSERT, state::INSERTED);   
        }

        while (!casResult && softUtils::getState(currRef->next.load()) == state::INSERTED)            
            casResult = softUtils::stateCAS<Node<K, V>>(currRef->next, state::INSERTED, state::INTEND_TO_DELETE);

        currRef->pptr->destroy(currRef->pValidity);
        if (casResult) {
            GSTATS_ADD(tid, num_update_flushes, 1);    
        }
        else {
            GSTATS_ADD(tid, num_search_flushes, 1);    
        }


        while (softUtils::getState(currRef->next.load()) == state::INTEND_TO_DELETE)
            softUtils::stateCAS<Node<K, V>>(currRef->next, state::INTEND_TO_DELETE, state::DELETED);

        if(casResult) {
            trim(tid, pred, curr);
            return val;
        }
        else {
            return noVal;
        }
    }

    bool contains(int tid, K key)
    {					
        auto guard1 = v_recmgr->getGuard(tid);
#ifdef RECORDMANAGER_PMEM_SUPPORT  
		auto guard2 = p_recmgr->getGuard(tid);
#endif
        Node<K, V> *curr = head->next.load();
        while(curr->key < key)
        {
            curr = softUtils::getRef<Node<K, V>>(curr->next.load());
        }
        state currState = softUtils::getState(curr->next.load());

        if (curr->key != key) {
            return false;
        }

        if (curr->key == key && softUtils::getState(curr->next.load()) == state::INTEND_TO_INSERT) {
            curr->pptr->create(curr->key, curr->value, curr->pValidity);
                        
            GSTATS_ADD(tid, num_contains_flushes, 1);                

            while (softUtils::getState(curr->next.load()) == state::INTEND_TO_INSERT)                
                softUtils::stateCAS<Node<K, V>>(curr->next, state::INTEND_TO_INSERT, state::INSERTED);   

            return true;
        }

        if (curr->key == key && softUtils::getState(curr->next.load()) == state::INTEND_TO_DELETE) {
            curr->pptr->destroy(curr->pValidity);

            GSTATS_ADD(tid, num_contains_flushes, 1);                

            while (softUtils::getState(curr->next.load()) == state::INTEND_TO_DELETE)
                softUtils::stateCAS<Node<K, V>>(curr->next, state::INTEND_TO_DELETE, state::DELETED);

            return false;
        }
        
        return (currState == state::INSERTED);
    }

    void printSummary() {
        // std::cout << "-----------------------------Start Summary------------------" << std::endl;        

        // Node<K, V> *curr = head;
        // Node<K, V> *currRef = softUtils::getRef<Node<K, V>>(head);
        // Node<K, V> *succ;

        // state currState;
        // state succState;

        // size_t keySum = 0;

        // while (currRef != tail)
        // {       
        //     succ = currRef->next.load();
        //     succState = softUtils::getState(succ);

        //     std::cout << "Node: " << curr << " has state: " << succState;
        //     std::cout << " key: " << currRef->key << std:: endl;
            
        //     keySum += currRef->key;

        //     curr = softUtils::createRef<Node<K, V>>(succ, succState);
        //     currRef = softUtils::getRef<Node<K, V>>(succ);
        // }        	            
        // std::cout << "-----------------------------------------------------------" << std::endl;        
        
        // std::cout << "Keysum: " << keySum << std::endl;

        // std::cout << "Head: " << head << " has state: " << softUtils::getState(head);
        // std::cout << " key: " << head->key << std:: endl;

        // std::cout << "Tail: " << tail << " has state: " << softUtils::getState(tail);
        // std::cout << " key: " << tail->key << std:: endl;

        // std::cout << "-----------------------------End Summary-------------------" << std::endl;        
        // return;
    }
};

#endif