#pragma once

#include <atomic>
#include "zurielCommon.h"

template <typename K, typename V>
class PNode
{
  public:
	std::atomic<bool> validStart;
	std::atomic<bool> validEnd;
	std::atomic<bool> deleted;
	std::atomic<K> key;
	std::atomic<V> value;

	//PNode() : key(0), validStart(false), validEnd(false), deleted(false) {}
	PNode() : validStart(false), validEnd(false), deleted(false) {}

	PNode(bool vStart, bool vEnd, bool del, K key, V val) : validStart(vStart), validEnd(vEnd), deleted(del), key(key), value(val) {}

	bool alloc()
	{
		return !this->validStart.load();
	}

	void create(K key, V value, bool validity)
	{
		this->validStart.store(validity, std::memory_order_relaxed);
		std::atomic_thread_fence(std::memory_order_release);
		this->key.store(key, std::memory_order_relaxed);
		this->value.store(value, std::memory_order_relaxed);
		this->validEnd.store(validity, std::memory_order_release);
		BARRIER(this);		
	}

	void destroy(bool validity)
	{
		this->deleted.store(validity, std::memory_order_release);
		BARRIER(this);		
	}

	bool isValid()
	{
		return validStart.load() == validEnd.load() && validEnd.load() != deleted.load();
	}

	bool isDeleted()
	{
		return validStart.load() == validEnd.load() && validEnd.load() == deleted.load();
	}

	bool recoveryValidity()
	{
		return validStart.load();
	}

} __attribute__((aligned((32))));
