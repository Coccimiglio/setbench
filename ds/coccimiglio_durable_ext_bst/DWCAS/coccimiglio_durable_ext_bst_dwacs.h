/* 
 * Converted the ellen_ext_bst_lf to support durable linearizability
 */

#ifndef ELLEN_H
#define ELLEN_H

#include "record_manager.h"
#include "durable_tools.h"

typedef unsigned __int128 uint128_t;

#define STATE_CLEAN 0
#define STATE_DFLAG 1
#define STATE_IFLAG 2
#define STATE_MARK 3

#define GETFLAG(ptr) (((uint64_t) (ptr)) & 3)
#define FLAG(ptr, flag) (info_t<K, V> *) ((((uint64_t) (ptr)) & 0xfffffffffffffffc) | (flag))
#define UNFLAG(ptr) (info_t<K, V> *) (((uint64_t) (ptr)) & 0xfffffffffffffffc)

#define DURABLE_MASK                (0x4ll) //100
#define MARK_NODE_DURABLE(x)     	((node_t<K,V>*)(((uintptr_t)(x)) | DURABLE_MASK))
#define IS_DURABLE(x)               (((uintptr_t)(x)) & DURABLE_MASK)

template <typename K, typename V>
union info_t; 

template <typename K, typename V>
struct node_t;

template <typename K, typename V>
struct insertInfo_t {
    node_t<K, V> * p;
    node_t<K, V> * new_internal;
    node_t<K, V> * l;
};

template <typename K, typename V>
struct deleteInfo_t {
    node_t<K, V> * gp;
    node_t<K, V> * p;
    node_t<K, V> * l;
    info_t<K, V> * pupdate;
};

template <typename K, typename V>
union info_t {
    insertInfo_t<K, V> iinfo;
    deleteInfo_t<K, V> dinfo;
#if defined(LARGE_DES)
    uint8_t padding[128 - 4*sizeof(void *)];
#else
    uint8_t padding[64];
#endif
};

template <typename K, typename V>
struct node_t {
    K key;
    V value;
    info_t<K, V> * volatile update;
    uint128_t volatile left; 
    uint128_t volatile right;
    // node_t<K, V> * volatile left;
    // node_t<K, V> * volatile right;
#ifdef USE_PADDING
    char pad[128 - sizeof(key) - sizeof(value) - sizeof(update) - sizeof(left) - sizeof(right)];
#endif
};

template <typename K, typename V, class RecMgr>
class ellen {
private:
PAD;
    const unsigned int idx_id;
PAD;
    node_t<K, V> * root;
PAD;
    const int NUM_THREADS;
    const K KEY_MIN;
    const K KEY_MAX;
    const V NO_VALUE;
PAD;
    RecMgr * const recmgr;
PAD;
    int init[MAX_THREADS_POW2] = {0,}; // this suffers from false sharing, but is only touched once per thread! so no worries.
PAD;

    bool bst_cas_child(const int tid, node_t<K, V> * parent, node_t<K, V> * old, node_t<K, V> * nnode);
    void bst_help(const int tid, info_t<K, V>* u);
    void bst_help_marked(const int tid, info_t<K, V>* op);
    bool bst_help_delete(const int tid, info_t<K, V>* op);
    void bst_help_insert(const int tid, info_t<K, V> * op);

    node_t<K, V> * create_node(const int tid, K key, V value, node_t<K, V> * left, node_t<K, V> * right) {
        auto result = recmgr->template allocate<node_t<K, V>>(tid);
        if (result == NULL) setbench_error("out of memory");
        result->key = key;
        result->value = value;
        result->update = NULL;
        result->left = left;
        result->right = right;

#ifndef ONE_FLUSH_INSERT				
        durableTools::FLUSH(&result);
    #if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
    #endif	
#endif

        return result;
    }
    
    info_t<K, V> * create_insertInfo_t(const int tid, node_t<K, V> * p, node_t<K, V> * ni, node_t<K, V> * l) {
        auto result = recmgr->template allocate<info_t<K, V>>(tid);
        if (result == NULL) setbench_error("out of memory");
        result->iinfo.p = p;
        result->iinfo.new_internal = ni;
        result->iinfo.l = l;
        return result;
    }

    info_t<K, V> * create_deleteInfo_t(const int tid, node_t<K, V> * gp, node_t<K, V> * p, node_t<K, V> * l, info_t<K, V> * u) {
        auto result = recmgr->template allocate<info_t<K, V>>(tid);
        if (result == NULL) setbench_error("out of memory");
        result->dinfo.gp = gp;
        result->dinfo.p = p;
        result->dinfo.l = l;
        result->dinfo.pupdate = u;
        return result;
    }
public:

    ellen(const int _NUM_THREADS, const K& _KEY_MIN, const K& _KEY_MAX, const V& _VALUE_RESERVED, unsigned int id)
    : idx_id(id), NUM_THREADS(_NUM_THREADS), KEY_MIN(_KEY_MIN), KEY_MAX(_KEY_MAX), NO_VALUE(_VALUE_RESERVED), recmgr(new RecMgr(NUM_THREADS)) {
        const int tid = 0;
        initThread(tid);

        recmgr->endOp(tid); // enter an initial quiescent state.

        auto i1 = create_node(tid, KEY_MAX, NO_VALUE, NULL, NULL);
        auto i2 = create_node(tid, KEY_MAX, NO_VALUE, NULL, NULL);
        root = create_node(tid, KEY_MAX, NO_VALUE, i1, i2);
    }

    ~ellen() {
        recmgr->printStatus();
        delete recmgr;
    }
    
    void printTree(node_t<K, V> * node, int depth) {
        //if (depth > 5) return;
        std::cout<<"depth="<<depth<<" key="<<node->key<<std::endl;
        if (node->left) printTree(node->left, depth+1);
        if (node->right) printTree(node->right, depth+1);
    }
    void printTree() {
        printTree(root, 0);
    }

    void initThread(const int tid) {
        if (init[tid]) return;
        else init[tid] = !init[tid];
        recmgr->initThread(tid);
    }

    void deinitThread(const int tid) {
        if (!init[tid]) return;
        else init[tid] = !init[tid];
        recmgr->deinitThread(tid);
    }

    V contains(const int tid, K key);
    V insertIfAbsent(const int tid, K key, V value);
    V remove(const int tid, K key);
    
    node_t<K, V> * get_root(){
        return root; 
    }
    
    RecMgr * debugGetRecMgr() {
        return recmgr;
    }    
};

template <typename K, typename V, class RecMgr>
V ellen<K, V, RecMgr>::contains(const int tid, K key) {
    auto guard = recmgr->getGuard(tid, true);
    
    auto l = root->left;
    while (l->left) l = (key < l->key) ? l->left : l->right;
    return (l->key == key) ? l->value : NO_VALUE;
}

template <typename K, typename V, class RecMgr>
V ellen<K, V, RecMgr>::insertIfAbsent(const int tid, const K key, const V value) {
    while (1) {
        auto guard = recmgr->getGuard(tid);
        
        auto p = root;
        auto pupdate = p->update;
        SOFTWARE_BARRIER;
        auto l = p->left;
        while (l->left) {
            p = l;
            pupdate = p->update;
            SOFTWARE_BARRIER;
            l = (key < l->key) ? l->left : l->right;
        }
        if (l->key == key) {
            return l->value;
        }
        if (GETFLAG(pupdate) != STATE_CLEAN) {
            bst_help(tid, pupdate);
        } else {
            auto new_node = create_node(tid, key, value, NULL, NULL);
            auto new_sibling = create_node(tid, l->key, l->value, NULL, NULL);
            auto new_internal = (key < l->key)
                    ? create_node(tid, l->key, NO_VALUE, new_node, new_sibling)
                    : create_node(tid, key, NO_VALUE, new_sibling, new_node);
            auto op = create_insertInfo_t(tid, p, new_internal, l);
            auto result = CASV(&p->update, pupdate, FLAG(op, STATE_IFLAG));
            if (result == pupdate) {
                bst_help_insert(tid, op);
                return NO_VALUE;
            } else {
                recmgr->deallocate(tid, new_node);
                recmgr->deallocate(tid, new_sibling);
                recmgr->deallocate(tid, new_internal);
                recmgr->deallocate(tid, op);
                bst_help(tid, result);
            }
        }
    }
}

template <typename K, typename V, class RecMgr>
V ellen<K, V, RecMgr>::remove(const int tid, K key) {
    while (1) {
        auto guard = recmgr->getGuard(tid);
        
        node_t<K, V> * gp = NULL;
        info_t<K, V> * gpupdate = NULL;
        auto p = root;
        auto pupdate = p->update;
        SOFTWARE_BARRIER;
        auto l = p->left;
        while (l->left) {
            gp = p;
            p = l;
            gpupdate = pupdate;
            pupdate = p->update;
            SOFTWARE_BARRIER;
            l = (key < l->key) ? l->left : l->right;
        }
        if (l->key != key) {
            return NO_VALUE;
        }
        auto found_value = l->value;
        if (GETFLAG(gpupdate) != STATE_CLEAN) {
            bst_help(tid, gpupdate);
        } else if (GETFLAG(pupdate) != STATE_CLEAN) {
            bst_help(tid, pupdate);
        } else {
            auto op = create_deleteInfo_t(tid, gp, p, l, pupdate);
            auto result = CASV(&gp->update, gpupdate, FLAG(op, STATE_DFLAG));
            if (result == gpupdate) {
                if (bst_help_delete(tid, op)) return found_value;
            } else {
                recmgr->deallocate(tid, op);
                bst_help(tid, result);
            }
        }
    }
}

template <typename K, typename V, class RecMgr>
bool ellen<K, V, RecMgr>::bst_cas_child(const int tid, node_t<K, V> * parent, node_t<K, V> * old, node_t<K, V> * nnode) {
    if (old == parent->left) {        
        if (CASB(&parent->left, old, nnode)) {
            node_t<K,V>* durableNnode = MARK_NODE_DURABLE(nnode);
            CASB(&parent->left, nnode, durableNnode);    
            return true;
        } 
        else {
            return false;
        }        
    } else if (old == parent->right) {        
        if (CASB(&parent->right, old, nnode)) {
            node_t<K,V>* durableNnode = MARK_NODE_DURABLE(nnode);
            CASB(&parent->right, nnode, durableNnode);    
            return true;
        } 
        else {
            return false;
        } 
    } else {
        return false;
    }
}

template <typename K, typename V, class RecMgr>
void ellen<K, V, RecMgr>::bst_help_insert(const int tid, info_t<K, V> * op) {
    if (bst_cas_child(tid, op->iinfo.p, op->iinfo.l, op->iinfo.new_internal)) {
        recmgr->retire(tid, op->iinfo.l);
    }
    if (CASB(&op->iinfo.p->update, FLAG(op, STATE_IFLAG), FLAG(op, STATE_CLEAN))) {
        recmgr->retire(tid, op);
    }
}
 
template <typename K, typename V, class RecMgr>
bool ellen<K, V, RecMgr>::bst_help_delete(const int tid, info_t<K, V> * op) {
    auto result = CASV(&op->dinfo.p->update, op->dinfo.pupdate, FLAG(op, STATE_MARK));
    if ((result == op->dinfo.pupdate) || (result == FLAG(op, STATE_MARK))) {
        bst_help_marked(tid, op);
        return true;
    } else {
        bst_help(tid, result);
        if (CASB(&op->dinfo.gp->update, FLAG(op, STATE_DFLAG), FLAG(op, STATE_CLEAN))) {
            recmgr->retire(tid, op);
        }
        return false;
    }
}

template <typename K, typename V, class RecMgr>
void ellen<K, V, RecMgr>::bst_help_marked(const int tid, info_t<K, V> * op) {
    node_t<K, V> * other;
    if (op->dinfo.p->right == op->dinfo.l) {
        other = op->dinfo.p->left;
    } else {
        other = op->dinfo.p->right;
    }
    if (bst_cas_child(tid, op->dinfo.gp, op->dinfo.p, other)) {
        recmgr->retire(tid, op->dinfo.l);
        recmgr->retire(tid, op->dinfo.p);
    }
    if (CASB(&op->dinfo.gp->update, FLAG(op, STATE_DFLAG), FLAG(op, STATE_CLEAN))) {
        recmgr->retire(tid, op);
    }
}

template <typename K, typename V, class RecMgr>
void ellen<K, V, RecMgr>::bst_help(const int tid, info_t<K, V> * u) {
    if (GETFLAG(u) == STATE_DFLAG) {
        bst_help_delete(tid, UNFLAG(u));
    } else if (GETFLAG(u) == STATE_IFLAG) {
        bst_help_insert(tid, UNFLAG(u));
    } else if (GETFLAG(u) == STATE_MARK) {
        bst_help_marked(tid, UNFLAG(u));
    }
}

#endif /* ELLEN_H */