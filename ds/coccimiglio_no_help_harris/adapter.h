#pragma once

#include <iostream>
#include <csignal>
#include "errors.h"

#include "record_manager.h"
#ifdef USE_TREE_STATS
#   include "tree_stats.h"
#endif


#include "../coccimiglio_durable_list/DWCAS_gcc_128t/durable_list_dwcas_harris_style.h"    


#define RECORD_MANAGER_T record_manager<Reclaim, Alloc, Pool, Node<K, V> >
#define DATA_STRUCTURE_T List<K, V, RECORD_MANAGER_T>


template <typename K, typename V, class Reclaim = reclaimer_debra<K>, class Alloc = allocator_new<K>, class Pool = pool_none<K>>
class ds_adapter {
private:    
    DATA_STRUCTURE_T * const ds;
    V reservedVal;
public:
    ds_adapter(const int NUM_THREADS,
               const K& KEY_MIN,
               const K& KEY_MAX,
               const V& VALUE_RESERVED,
               Random64 * const unused2)
    : ds(new DATA_STRUCTURE_T(NUM_THREADS, KEY_MIN, KEY_MAX, VALUE_RESERVED)), reservedVal(VALUE_RESERVED)
    { }
    
    ~ds_adapter() {
        delete ds;
    }
    
    V getNoValue() {
        return reservedVal;
    }
    
    void initThread(const int tid) {
        ds->initThread(tid);
    }
    void deinitThread(const int tid) {
        ds->deinitThread(tid);
    }

    V insert(const int tid, const K& key, const V& val) {
        setbench_error("Insert-replace functionality not implemented for this data structure");
    }
    
    V insertIfAbsent(const int tid, const K& key, const V& val) {
        return ds->insertIfAbsent(tid, key, val);
    }
    
    V erase(const int tid, const K& key) {
        return ds->remove(tid, key);
    }
    
    V find(const int tid, const K& key) {
		setbench_error("Find not implemented for this data structure");
    }
    
    bool contains(const int tid, const K& key) {
        return ds->containsNoFlush(tid, key);
    }
    int rangeQuery(const int tid, const K& lo, const K& hi, K * const resultKeys, V * const resultValues) {
        setbench_error("not implemented");
    }
    void printSummary() {
		std::cout << "No summary" << std::endl;
        //call ds print sum and rcmgr sum etc
    }
    bool validateStructure() {
        return true;
    }
    
    void printObjectSizes() {
        std::cout << "Size of node=" << (sizeof(Node<K,V>)) << std::endl;
    }
    
#ifdef USE_TREE_STATS
class NodeHandler {
    public:
        typedef Node<K, V> * NodePtrType;
        K minKey;
        K maxKey;         

        NodeHandler(const K& _minKey, const K& _maxKey) {
            minKey = _minKey;
            maxKey = _maxKey;            
        }
        
        class ChildIterator {
        private:
            bool calledNext;
            NodePtrType node; // node being iterated over
        public:
            ChildIterator(NodePtrType _node) {
                node = _node;
                calledNext = false;
            }
            
            bool hasNext() {                            
                return NODE_NEXT(node) != NULL && !calledNext;
            }
            
            NodePtrType next() {
                calledNext = true;
                
                return NODE_NEXT(node);
            }
        };
        
        bool isLeaf(NodePtrType node) {
            return NODE_NEXT(node) == NULL;
        }
        size_t getNumChildren(NodePtrType node) {
            return NODE_NEXT(node) == NULL ? 0 : 1;
        }
        size_t getNumKeys(NodePtrType node) {
            if (IS_MARKED_FOR_DEL(NODE_NEXT_PTR(node))) {
                return 0;
            }   

            return node == NULL ? 0 : (node->key == minKey ? 0 : (node->key == maxKey ? 0 : 1));
        }
        
        size_t getSumOfKeys(NodePtrType node) {
            if (IS_MARKED_FOR_DEL(NODE_NEXT_PTR(node))) {
                return 0;
            }
            
            return (size_t) (node->key == minKey ? 0 : (node->key == maxKey ? 0 : node->key));
        }
        ChildIterator getChildIterator(NodePtrType node) {
            return ChildIterator(node);
        }
    };
    TreeStats<NodeHandler> * createTreeStats(const K& _minKey, const K& _maxKey) {        
        return new TreeStats<NodeHandler>(new NodeHandler(_minKey, _maxKey), ds->getHead(), false);
    }
#endif
};
