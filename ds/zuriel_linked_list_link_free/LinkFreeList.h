#pragma once

#include <vector>
#include <climits>
#include "LinkFreeUtilities.h"
#include <atomic>
#include <cassert>
#include <stdint.h>
#include <stdlib.h>
#include <unordered_set>

#if defined(USE_LIBVMMALLOC) && !defined(RECORDMANAGER_PMEM_SUPPORT)
	#include "persistent_alloc.h"
#endif

#if defined(USE_MEMKIND) && !defined(RECORDMANAGER_PMEM_SUPPORT)
	#include <memkind.h>
#endif 

#if defined(USE_MEMKIND) && !defined(RECORDMANAGER_PMEM_SUPPORT)
	#define PMEM_POOL_PATH "/mnt/pmem1_mount/"
	#define PMEM_POOL_SIZE 0 // 0 means the heap is not a fixed size and can grow as long as the temp file can grow
#endif

//#define LOAD_MO std::memory_order_relaxed
#define LOAD_MO

template <typename K, typename V>
class Node
{
public:
	std::atomic<uchar> metaData;
	std::atomic<bool> insertFlag;
	std::atomic<bool> deleteFlag;
	K key;
	V value;
	std::atomic<Node *> next;

	Node() : metaData(0), next(nullptr), insertFlag(false), deleteFlag(false) {}

	Node(K key, V value, Node *next) : key(key), value(value), next(next), insertFlag(false), deleteFlag(false) {}

	bool isMarked()
	{
		return linkFreeUtils::isMarked(next.load());
	}
} __attribute__((aligned((32))));

template <class RecordManager, typename K, typename V>
class LinkFreeList
{
private:
	PAD;
	Node<K, V>  * head;
	PAD;

#ifdef RECORDMANAGER_PMEM_SUPPORT
	RecordManager * recmgr;
#elif defined(USE_MEMKIND)
	struct memkind *pmem_kind = NULL;
#elif defined(USE_LIBVMMALLOC)
	AllocatorLibvmmalloc_Simple* alloc;
#endif

	V noVal;
	PAD;

	Node<K, V> * allocNode(int tid, K key, V value, Node<K, V>  *next)
	{
		//Node *newNode = static_cast<Node *>(ssmem_alloc(alloc, sizeof(Node)));	
#ifdef RECORDMANAGER_PMEM_SUPPORT
		Node<K, V> * newNode = recmgr->template allocate<Node<K, V>>(tid);
		// Node<K, V> * newNode = (Node<K, V>*)malloc(sizeof(Node<K, V>));
#elif defined(USE_MEMKIND)
		Node<K,V> * newNode = (Node<K,V>*)memkind_malloc(pmem_kind, sizeof(Node<K,V>));
#elif defined(USE_LIBVMMALLOC)
		Node<K,V> * newNode = (Node<K,V>*)alloc->allocate(sizeof(Node<K, V>));
#endif		
		GSTATS_ADD(tid, num_pmem_alloc, 1);  

		linkFreeUtils::flipV1(&newNode->metaData);
		std::atomic_thread_fence(std::memory_order_release);
		newNode->insertFlag.store(false, std::memory_order_relaxed);
		newNode->deleteFlag.store(false, std::memory_order_relaxed);
		newNode->key = key;
		newNode->value = value;
		newNode->next.store(next, std::memory_order_relaxed);
		return newNode;
	}

	void FLUSH_DELETE(int tid, Node<K, V>  *n)
	{
		if (LIKELY(n->deleteFlag.load()))
			return;
		durableTools::FLUSH(n);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif			
		n->deleteFlag.store(true, std::memory_order_release);
		GSTATS_ADD(tid, num_update_flushes, 1);	
	}

	void CONTAINS_FLUSH_DELETE(int tid, Node<K, V>  *n)
	{
		if (LIKELY(n->deleteFlag.load()))
			return;
		durableTools::FLUSH(n);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif			
		n->deleteFlag.store(true, std::memory_order_release);
		GSTATS_ADD(tid, num_contains_flushes, 1);	
	}

	void SEARCH_FLUSH_DELETE(int tid, Node<K, V>  *n)
	{
		if (LIKELY(n->deleteFlag.load()))
			return;
		durableTools::FLUSH(n);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif			
		n->deleteFlag.store(true, std::memory_order_release);
		GSTATS_ADD(tid, num_search_flushes, 1);
	}




	void CONTAINS_FLUSH_INSERT(int tid, Node<K, V>  *n)
	{
		if (LIKELY(n->insertFlag.load()))
			return;
		durableTools::FLUSH(n);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif			
		n->insertFlag.store(true, std::memory_order_release);
		GSTATS_ADD(tid, num_contains_flushes, 1);	
	}

	void SEARCH_FLUSH_INSERT(int tid, Node<K, V>  *n)
	{
		if (LIKELY(n->insertFlag.load()))
			return;
		durableTools::FLUSH(n);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif			
		n->insertFlag.store(true, std::memory_order_release);
		GSTATS_ADD(tid, num_search_flushes, 1);
	}

	void FLUSH_INSERT(int tid, Node<K, V>  *n)
	{
		if (LIKELY(n->insertFlag.load()))
			return;
		durableTools::FLUSH(n);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif			
		n->insertFlag.store(true, std::memory_order_release);
		GSTATS_ADD(tid, num_update_flushes, 1);	
	}

	//trim curr
	bool trim(int tid, Node<K, V>  *pred, Node<K, V>  *curr)
	{
		FLUSH_DELETE(tid, curr);			
		Node<K, V>  *succ = linkFreeUtils::getRef<Node<K, V> >(curr->next.load());
		bool result = pred->next.compare_exchange_strong(curr, succ);
		if (LIKELY(result))
			//ssmem_free(alloc, curr);
#ifdef RECORDMANAGER_PMEM_SUPPORT
			recmgr->retire(tid, curr);
#else 		
#endif	
		return result;
	}

	bool trim_search(int tid, Node<K, V>  *pred, Node<K, V>  *curr)
	{
		SEARCH_FLUSH_DELETE(tid, curr);			
		Node<K, V>  *succ = linkFreeUtils::getRef<Node<K, V> >(curr->next.load());
		bool result = pred->next.compare_exchange_strong(curr, succ);
		if (LIKELY(result))
			//ssmem_free(alloc, curr);
#ifdef RECORDMANAGER_PMEM_SUPPORT
			recmgr->retire(tid, curr);
#else 		
#endif	
		return result;
	}

	Node<K, V> * find(int tid, K key, Node<K, V>  **predPtr)
	{
		Node<K, V>  *prev = head, *curr = head->next.load();

		while (true)
		{
			// curr is not marked
			if (LIKELY(!linkFreeUtils::isMarked(curr->next)))
			{
				if (UNLIKELY(curr->key >= key))
					break;
				prev = curr;
			}
			else
			{
				trim_search(tid, prev, curr);
				//putting the search-flush counter add here because trim is also called from Remove				
			}
			curr = linkFreeUtils::getRef<Node<K, V> >(curr->next);
		}
		*predPtr = prev;
		return curr;
	}

public:
	LinkFreeList(int numThreads, K minKey, K maxKey, V noValue)
	{		
#ifdef	RECORDMANAGER_PMEM_SUPPORT	
		recmgr = new RecordManager(numThreads);

		// TODO:: fix RecordManager then remove this later
		Node<K,V> * temp = recmgr->template allocate<Node<K,V>>(tid);
		recmgr->deallocate(0, temp);	
#elif defined(USE_MEMKIND)
		int err = memkind_create_pmem(PMEM_POOL_PATH, PMEM_POOL_SIZE, &pmem_kind);
		if (err) {			
			printf("Error creating PMEM pool. Exiting\n");
			// print_err_message(err);
			exit(-1);
		}
#elif defined(USE_LIBVMMALLOC)
		alloc = new AllocatorLibvmmalloc_Simple();
		alloc->init();
#endif
		
		Node<K, V>  *max = allocNode(0, maxKey, 0, nullptr);
		Node<K, V>  *min = allocNode(0, minKey, 0, max);
		
		head = min;
		noVal = noValue;
	}

	Node<K, V> * getHead() {
		return head;
	}

	void initThread(const int tid) {
#ifdef RECORDMANAGER_PMEM_SUPPORT		
		recmgr->initThread(tid);
#endif
	}
    
    void deinitThread(const int tid) {
#ifdef RECORDMANAGER_PMEM_SUPPORT				
		recmgr->deinitThread(tid);
#endif
	}

	V insertIfAbsent(int tid, K key, V value)
	{
#ifdef RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif

		do
		{
			Node<K, V>  *pred = nullptr;
			Node<K, V>  *curr = find(tid, key, &pred);

			if (curr->key == key)
			{
				linkFreeUtils::makeValid(&curr->metaData);
				SEARCH_FLUSH_INSERT(tid, curr);
				return curr->value;
			}

			Node<K, V>  *newNode = allocNode(tid, key, value, curr);

			if (pred->next.compare_exchange_strong(curr, newNode))
			{
				linkFreeUtils::makeValid(&newNode->metaData);
				FLUSH_INSERT(tid, newNode);						
				return noVal;
			}
			else {
				// freeing newNode in a deleted and valid state
				newNode->next.store(linkFreeUtils::mark<Node<K, V> >(nullptr));
				linkFreeUtils::makeValid(&newNode->metaData);
				//ssmem_free(alloc, newNode);
#ifdef RECORDMANAGER_PMEM_SUPPORT
				recmgr->deallocate(tid, newNode);
#elif defined(USE_MEMKIND)
			 	memkind_free(pmem_kind, newNode);
#elif defined(USE_LIBVMMALLOC)
				alloc->deallocate(newNode);
#endif				
			}
		} while (true);
	}

	V remove(int tid, K key)
	{
#ifdef RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif

		bool result = false;
		Node<K, V>  *pred, *curr, *succ, *markedSucc;
		V val = noVal;
		do
		{
			curr = find(tid, key, &pred);
			val = curr->value;
			if (curr->key != key)
				return noVal;

			succ = linkFreeUtils::getRef<Node<K, V> >(curr->next);
			markedSucc = linkFreeUtils::mark<Node<K, V> >(succ);
			linkFreeUtils::makeValid(&curr->metaData);
			result = curr->next.compare_exchange_strong(succ, markedSucc);
		} while (!result);
		trim(tid, pred, curr);
		return val;
	}

	bool contains(int tid, K key)
	{
#ifdef RECORDMANAGER_PMEM_SUPPORT						
		auto guard = recmgr->getGuard(tid);
#endif

		Node<K, V>  *curr = head->next.load();
		bool marked = false;
		//wait free find
		while (curr->key < key)
		{
			curr = linkFreeUtils::getRef<Node<K, V> >(curr->next.load());
		}
		if (curr->key != key)
			return false;

		marked = linkFreeUtils::isMarked(curr->next.load());
		if (marked)
		{
			//if the node is marked, it must be valid
			CONTAINS_FLUSH_DELETE(tid, curr);
			return false;
		}
		linkFreeUtils::makeValid(&curr->metaData);
		CONTAINS_FLUSH_INSERT(tid, curr);		
		return true;
	}

	bool validate(){
		// std::unordered_set<K> keySet = {};
		// Node<K, V> * curr = head->next;

		// int64_t total = 0; 
		// int64_t count = 0;
    	// while(curr != NULL){
		// 	K key = curr->key;
			
		// 	assert(keySet.count(key) == 0);
		// 	keySet.insert(key);

		// 	++count;

		// 	total += curr->key;
		// 	curr = curr->next;
    	// }

		// if (count != keySet.size()){
		// 	return false;
		// }
		
		return true;
	}

	/*
	//this is only used during recovery
	void quickInsert(Node *newNode)
	{
		K key = newNode->key;
		Node *pred = nullptr, *curr = nullptr, *succ = nullptr;
	retry:
		pred = const_cast<Node *>(head);
		curr = linkFreeUtils::getRef<Node>(pred->next.load());
		//inline the find function
		while (true)
		{
			succ = curr->next.load();
			//trimming
			while (linkFreeUtils::isMarked(succ))
			{
				assert(false);
			}
			//continue searching
			if (curr->key < key)
			{
				pred = curr;
				curr = succ;
			}
			//found the same
			else if (curr->key == key)
			{
				assert(false);
			}
			else
			{
				newNode->next.store(curr, std::memory_order_relaxed);
				if (!pred->next.compare_exchange_strong(curr, newNode))
					goto retry;
				return;
			}
		}
	}
	*/

	/*	
	//this recovery function relies on knowing things about the allocator and I need to check if
	//setbench's exposes the required information
	void recover()
	{
		
		auto curr = alloc->mem_chunks;
		for (; curr != nullptr; curr = curr->next)
		{
			Node *currChunk = static_cast<Node *>(curr->obj);
			uint64_t numOfNodes = SSMEM_DEFAULT_MEM_SIZE / sizeof(Node);
			for (uint64_t i = 0; i < numOfNodes; i++)
			{
				Node *currNode = currChunk[i];
				// the node was never initialized, no need to free it or add it
				if (currNode->next.load() == nullptr && linkFreeUtils::isValid(currNode->metaData.load()))
					continue;
				if (!linkFreeUtils::isValid(currNode->metaData.load()) || currNode->isMarked())
				{
					currNode->next.store(linkFreeUtils::mark<Node>(nullptr));
					linkFreeUtils::makeValid(&currNode->metaData);
					ssmem_free(alloc, currNode);
				}
				else
					quickInsert(currNode);
			}
		}
		
	}
	*/
};