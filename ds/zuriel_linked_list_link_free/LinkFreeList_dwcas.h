#pragma once

#include <vector>
#include <climits>
#include "LinkFreeUtilities.h"
#include <atomic>
#include <cassert>
#include <stdint.h>
#include <stdlib.h>
#include <unordered_set>

//#define LOAD_MO std::memory_order_relaxed
#define LOAD_MO

template<typename NodeType>
struct NodeNext
{
	NodeType * next;
	void * garbage;
};

template <typename K, typename V>
class Node
{
public:
	std::atomic<uchar> metaData;
	std::atomic<bool> insertFlag;
	std::atomic<bool> deleteFlag;
	K key;
	V value;
	std::atomic<NodeNext<Node>> next;

	Node() : metaData(0), insertFlag(false), deleteFlag(false) {
		next.store(NodeNext<Node<K,V>>({nullptr, nullptr}));
	}

	Node(K key, V value, Node *next) : key(key), value(value), insertFlag(false), deleteFlag(false) {
		this->next.store(NodeNext<Node<K,V>>({next, next}));
	}

	bool isMarked()
	{
		return linkFreeUtils::isMarked(next.load().next);
	}
} __attribute__((aligned((32))));

template <class RecordManager, typename K, typename V>
class LinkFreeList
{
private:
	Node<K, V>  * head;
	RecordManager * recmgr;
	V noVal;

	Node<K, V> * allocNode(int tid, K key, V value, Node<K, V>  *next)
	{
		//Node *newNode = static_cast<Node *>(ssmem_alloc(alloc, sizeof(Node)));
		Node<K, V> *newNode = recmgr->template allocate<Node<K, V>>(tid);

		linkFreeUtils::flipV1(&newNode->metaData);
		std::atomic_thread_fence(std::memory_order_release);
		newNode->insertFlag.store(false, std::memory_order_relaxed);
		newNode->deleteFlag.store(false, std::memory_order_relaxed);
		newNode->key = key;
		newNode->value = value;
		newNode->next.store(NodeNext<Node<K,V>>({next, next}), std::memory_order_relaxed);
		return newNode;
	}

	void FLUSH_DELETE(Node<K, V>  *n)
	{
		if (LIKELY(n->deleteFlag.load()))
			return;
		durableTools::FLUSH(n);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif			
		n->deleteFlag.store(true, std::memory_order_release);
	}

	void FLUSH_INSERT(Node<K, V>  *n)
	{
		if (LIKELY(n->insertFlag.load()))
			return;
		durableTools::FLUSH(n);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
		durableTools::SFENCE();							
#endif			
		n->insertFlag.store(true, std::memory_order_release);
	}

	//trim curr
	bool trim(int tid, Node<K, V>  *pred, Node<K, V>  *curr)
	{
		FLUSH_DELETE(curr);			
		Node<K, V>  *succ = linkFreeUtils::getRef<Node<K, V> >(curr->next.load().next);
		
		NodeNext<Node<K,V>> expNext = NodeNext<Node<K,V>>({curr, curr});
		NodeNext<Node<K,V>> newNext = NodeNext<Node<K,V>>({succ, succ});

		bool result = pred->next.compare_exchange_strong(expNext, newNext);
		if (LIKELY(result))
			//ssmem_free(alloc, curr);
			recmgr->retire(tid, curr);
		return result;
	}

	Node<K, V> * find(int tid, K key, Node<K, V>  **predPtr)
	{
		Node<K, V>  *prev = head, *curr = head->next.load().next;

		while (true)
		{
			// curr is not marked
			if (LIKELY(!linkFreeUtils::isMarked(curr->next.load().next)))
			{
				if (UNLIKELY(curr->key >= key))
					break;
				prev = curr;
			}
			else
			{
				trim(tid, prev, curr);
				//putting the search-flush counter add here because trim is also called from Remove
				GSTATS_ADD(tid, num_search_flushes, 1);
			}
			curr = linkFreeUtils::getRef<Node<K, V> >(curr->next.load().next);
		}
		*predPtr = prev;
		return curr;
	}

public:
	LinkFreeList(int numThreads, K minKey, K maxKey, V noValue)
	{		
		Node<K, V>  *max = new Node<K, V> (maxKey, 0, nullptr);
		Node<K, V>  *min = new Node<K, V> (minKey, 0, max);
		recmgr = new RecordManager(numThreads);
		head = min;
		noVal = noValue;
	}

	Node<K, V> * getHead() {
		return head;
	}

	void initThread(const int tid) {
		recmgr->initThread(tid);
	}
    
    void deinitThread(const int tid) {
		recmgr->deinitThread(tid);
	}

	V insertIfAbsent(int tid, K key, V value)
	{
		auto guard = recmgr->getGuard(tid);

		do
		{
			Node<K, V>  *pred = nullptr;
			Node<K, V>  *curr = find(tid, key, &pred);

			if (curr->key == key)
			{
				linkFreeUtils::makeValid(&curr->metaData);
				FLUSH_INSERT(curr);
				GSTATS_ADD(tid, num_search_flushes, 1);	
				return curr->value;
			}

			Node<K, V>  *newNode = allocNode(tid, key, value, curr);

			NodeNext<Node<K,V>> expNext = NodeNext<Node<K,V>>({curr, curr});
			NodeNext<Node<K,V>> newNext = NodeNext<Node<K,V>>({newNode, newNode});

			if (pred->next.compare_exchange_strong(expNext, newNext))
			{
				linkFreeUtils::makeValid(&newNode->metaData);
				FLUSH_INSERT(newNode);
				GSTATS_ADD(tid, num_update_flushes, 1);				
				return noVal;
			}
			else {
				// freeing newNode in a deleted and valid state
				Node<K,V>* n = linkFreeUtils::mark<Node<K, V> >(nullptr);
				newNode->next.store(NodeNext<Node<K,V>>({n,n}));
				linkFreeUtils::makeValid(&newNode->metaData);
				//ssmem_free(alloc, newNode);
				recmgr->retire(tid, newNode);
			}
		} while (true);
	}

	V remove(int tid, K key)
	{
		auto guard = recmgr->getGuard(tid);

		bool result = false;
		Node<K, V>  *pred, *curr, *succ, *markedSucc;
		V val = noVal;
		do
		{
			curr = find(tid, key, &pred);
			val = curr->value;
			if (curr->key != key)
				return noVal;

			succ = linkFreeUtils::getRef<Node<K, V> >(curr->next.load().next);
			markedSucc = linkFreeUtils::mark<Node<K, V> >(succ);
			linkFreeUtils::makeValid(&curr->metaData);
			
			NodeNext<Node<K,V>> expNext = NodeNext<Node<K,V>>({succ, succ});
			NodeNext<Node<K,V>> newNext = NodeNext<Node<K,V>>({markedSucc, markedSucc});

			result = curr->next.compare_exchange_strong(expNext, newNext);
		} while (!result);
		trim(tid, pred, curr);
		//the flush for the remove is inside trim()
		GSTATS_ADD(tid, num_update_flushes, 1);
		return val;
	}

	bool contains(int tid, K key)
	{
		auto guard = recmgr->getGuard(tid);

		Node<K, V>  *curr = head->next.load().next;
		bool marked = false;
		//wait free find
		while (curr->key < key)
		{
			curr = linkFreeUtils::getRef<Node<K, V> >(curr->next.load().next);
		}
		if (curr->key != key)
			return false;

		marked = linkFreeUtils::isMarked(curr->next.load().next);
		if (marked)
		{
			//if the node is marked, it must be valid
			FLUSH_DELETE(curr);
			GSTATS_ADD(tid, num_contains_flushes, 1);	
			return false;
		}
		linkFreeUtils::makeValid(&curr->metaData);
		FLUSH_INSERT(curr);
		GSTATS_ADD(tid, num_contains_flushes, 1);	
		return true;
	}

	bool validate(){
		// std::unordered_set<K> keySet = {};
		// Node<K, V> * curr = head->next;

		// int64_t total = 0; 
		// int64_t count = 0;
    	// while(curr != NULL){
		// 	K key = curr->key;
			
		// 	assert(keySet.count(key) == 0);
		// 	keySet.insert(key);

		// 	++count;

		// 	total += curr->key;
		// 	curr = curr->next;
    	// }

		// if (count != keySet.size()){
		// 	return false;
		// }
		
		return true;
	}

	/*
	//this is only used during recovery
	void quickInsert(Node *newNode)
	{
		K key = newNode->key;
		Node *pred = nullptr, *curr = nullptr, *succ = nullptr;
	retry:
		pred = const_cast<Node *>(head);
		curr = linkFreeUtils::getRef<Node>(pred->next.load());
		//inline the find function
		while (true)
		{
			succ = curr->next.load();
			//trimming
			while (linkFreeUtils::isMarked(succ))
			{
				assert(false);
			}
			//continue searching
			if (curr->key < key)
			{
				pred = curr;
				curr = succ;
			}
			//found the same
			else if (curr->key == key)
			{
				assert(false);
			}
			else
			{
				newNode->next.store(curr, std::memory_order_relaxed);
				if (!pred->next.compare_exchange_strong(curr, newNode))
					goto retry;
				return;
			}
		}
	}
	*/

	/*	
	//this recovery function relies on knowing things about the allocator and I need to check if
	//setbench's exposes the required information
	void recover()
	{
		
		auto curr = alloc->mem_chunks;
		for (; curr != nullptr; curr = curr->next)
		{
			Node *currChunk = static_cast<Node *>(curr->obj);
			uint64_t numOfNodes = SSMEM_DEFAULT_MEM_SIZE / sizeof(Node);
			for (uint64_t i = 0; i < numOfNodes; i++)
			{
				Node *currNode = currChunk[i];
				// the node was never initialized, no need to free it or add it
				if (currNode->next.load() == nullptr && linkFreeUtils::isValid(currNode->metaData.load()))
					continue;
				if (!linkFreeUtils::isValid(currNode->metaData.load()) || currNode->isMarked())
				{
					currNode->next.store(linkFreeUtils::mark<Node>(nullptr));
					linkFreeUtils::makeValid(&currNode->metaData);
					ssmem_free(alloc, currNode);
				}
				else
					quickInsert(currNode);
			}
		}
		
	}
	*/
};