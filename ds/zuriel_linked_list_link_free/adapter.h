#ifndef ZURIEL_LINK_FREE_LIST_H
#define ZURIEL_LINK_FREE_LIST_H

#include <iostream>
#include <csignal>
#include "errors.h"

#include "record_manager.h"
#ifdef USE_TREE_STATS
#   include "tree_stats.h"
#endif

#include "LinkFreeList.h"
//#include "LinkFreeList_dwcas.h"

#define RECORD_MANAGER_T record_manager<Reclaim, Alloc, Pool, Node<K, V> >
#define DATA_STRUCTURE_T LinkFreeList<RECORD_MANAGER_T, K, V>


template <typename K, typename V, class Reclaim = reclaimer_debra<K>, class Alloc = allocator_new<K>, class Pool = pool_none<K>>
class ds_adapter {
private:    
    DATA_STRUCTURE_T * const ds;
    V reservedVal;
public:
    ds_adapter(const int NUM_THREADS,
               const K& KEY_MIN,
               const K& KEY_MAX,
               const V& VALUE_RESERVED,
               Random64 * const unused2)
    : ds(new DATA_STRUCTURE_T(NUM_THREADS, KEY_MIN, KEY_MAX, VALUE_RESERVED)), reservedVal(VALUE_RESERVED)
    { }
    
    ~ds_adapter() {
        delete ds;
    }
    
    V getNoValue() {
        return reservedVal;
    }
    
    void initThread(const int tid) {
        ds->initThread(tid);
    }
    void deinitThread(const int tid) {
        ds->deinitThread(tid);
    }

    V insert(const int tid, const K& key, const V& val) {
        setbench_error("Insert-replace functionality not implemented for this data structure");
    }
    
    V insertIfAbsent(const int tid, const K& key, const V& val) {
        return ds->insertIfAbsent(tid, key, val);
    }
    
    V erase(const int tid, const K& key) {
        return ds->remove(tid, key);
    }
    
    V find(const int tid, const K& key) {
		setbench_error("Find not implemented for this data structure");
    }
    
    bool contains(const int tid, const K& key) {
        return ds->contains(tid, key);
    }
    int rangeQuery(const int tid, const K& lo, const K& hi, K * const resultKeys, V * const resultValues) {
        setbench_error("not implemented");
    }
    void printSummary() {
		std::cout << "No summary" << std::endl;
        //call ds print sum and rcmgr sum etc
    }
    bool validateStructure() {
        return ds->validate();
        //not required can just return true
    }
    
    void printObjectSizes() {
        std::cout << "Size of node=" << (sizeof(Node<K, V>)) << std::endl;
    }
    
#ifdef USE_TREE_STATS
class NodeHandler {
    public:
        typedef Node<K, V> * NodePtrType;
        K minKey;
        K maxKey;        

        NodeHandler(const K& _minKey, const K& _maxKey) {
            minKey = _minKey;
            maxKey = _maxKey;
        }
        
        class ChildIterator {
        private:
            bool calledNext;
            NodePtrType node; // node being iterated over
        public:
            ChildIterator(NodePtrType _node) {
                node = _node;
                calledNext = false;
            }
            
            bool hasNext() {                            
                return linkFreeUtils::getRef(node)->next.load() != NULL && !calledNext;
            }
            
            NodePtrType next() {
                calledNext = true;
                return linkFreeUtils::getRef(node)->next.load();
            }
        };
        
        bool isLeaf(NodePtrType node) {
            return linkFreeUtils::getRef(node)->next.load() == NULL;
        }
        size_t getNumChildren(NodePtrType node) {           
            return linkFreeUtils::getRef(node)->next.load() == NULL ? 0 : 1;
        }
        size_t getNumKeys(NodePtrType node) {            
            if (linkFreeUtils::isMarked(linkFreeUtils::getRef(node)->next.load())) {
                return 0;
            }   

            size_t key = linkFreeUtils::getRef(node)->key;
            return node == NULL ? 0 : (key == minKey ? 0 : (key == maxKey ? 0 : 1));
        }
        
        size_t getSumOfKeys(NodePtrType node) {
            if (linkFreeUtils::isMarked(linkFreeUtils::getRef(node)->next.load())) {
                return 0;
            }  

            size_t key = linkFreeUtils::getRef(node)->key;
            return (size_t) (key == minKey ? 0 : (key == maxKey ? 0 : key));
        }
        ChildIterator getChildIterator(NodePtrType node) {
            return ChildIterator(node);
        }
    };
    TreeStats<NodeHandler> * createTreeStats(const K& _minKey, const K& _maxKey) {        
        return new TreeStats<NodeHandler>(new NodeHandler(_minKey, _maxKey), ds->getHead(), false);
    }
#endif
};

#endif
