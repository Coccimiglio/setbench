#pragma once

#include "durable_tools.h"

// ----------------------------------------------
//previously this code was located in common.h of the original repo
// ----------------------------------------------
#define compiler_fence std::atomic_thread_fence(std::memory_order_release)
#define MFENCE __sync_synchronize
#define MAX_LEVEL (21) //one cache-line node; use 13 for two cache-line nodes

#define UNLIKELY(x) __builtin_expect((x), 0)
#define LIKELY(x) __builtin_expect((x), 1)

typedef unsigned char uchar;

static inline void BARRIER(void *p)
{
    durableTools::FLUSH(p);
#if defined(USE_CLFLUSHOPT) || defined(USE_CLWB)			
	durableTools::SFENCE();							
#endif    
}
// ----------------------------------------------
// ----------------------------------------------