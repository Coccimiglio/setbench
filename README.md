# The Fence Complexity of Persistent Sets - Experiments

This repo contains code to produce the experimental results from The Fence Complexity of Persistent Sets SSS'2: https://link.springer.com/chapter/10.1007/978-3-031-44274-2_3

# Data structures:

All data structures can be found in the ds directory. All data structure from the paper is prefixed with "coccimiglio_"

# Requirements:

For the requirements of the benchmark refer to the Readme in https://gitlab.com/trbot86/setbench

We utilize the libvmmalloc library for allocating persistent memory. You will need to install this library on your machine to reproduce the experiments. You will also need some amount of NVRAM. Experiments in the paper use Intel Optane DCPMMs configured in app-direct mode.

# Compiling and Running:
Scripts for compilining, running and plotting experiment results can be found in the /microbench_experiments/pyke_experiments/ subdirectory.

The parameters of the experiments can be configured in the experiment.py file. You should also execute the setupLibvmalloc.sh script (this configures the NVRAM limits utilized by the library)

Once you are happy with the experiment parameters you can compile and run by executing the compile_experiments.sh